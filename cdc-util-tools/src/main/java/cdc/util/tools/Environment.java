package cdc.util.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class Environment {
    private Environment() {
    }

    public static void main(String[] args) {
        final Map<String, String> env = System.getenv();
        final List<String> keys = new ArrayList<>();
        keys.addAll(env.keySet());
        Collections.sort(keys);
        for (final String key : keys) {
            System.out.format("%s=%s%n", key, env.get(key));
        }
    }
}