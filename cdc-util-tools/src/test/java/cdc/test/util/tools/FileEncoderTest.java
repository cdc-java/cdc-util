package cdc.test.util.tools;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.tools.FileEncoder;

public class FileEncoderTest {
    protected static final Logger LOGGER = LogManager.getLogger(FileEncoderTest.class);

    private final List<String> lines = new ArrayList<>();
    private final File filePlatform = new File("target", getClass().getSimpleName() + "-platform.txt");
    private final File fileUtf8 = new File("target", getClass().getSimpleName() + "-utf8.txt");
    private final File fileUtf16 = new File("target", getClass().getSimpleName() + "-utf16.txt");

    @Test
    public void test() throws IOException {
        lines.add("Line 1: aaa");
        lines.add("Line 2: ààà");
        lines.add("Line 3");

        // Generate a Platform file
        try (PrintStream out = new PrintStream(filePlatform)) {
            for (final String line : lines) {
                out.println(line);
            }
            out.close();
        }

        final FileEncoder.MainArgs margs = new FileEncoder.MainArgs();
        margs.setEnabled(FileEncoder.MainArgs.Feature.VERBOSE, true);
        margs.input = filePlatform;
        margs.inputCharset = null;

        // Converts it to UTF-8
        margs.output = fileUtf8;
        margs.outputCharset = "UTF-8";
        FileEncoder.execute(margs);

        // Converts it to UTF-16
        margs.output = fileUtf16;
        margs.outputCharset = "UTF-16";
        FileEncoder.execute(margs);
    }
}