# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
- Created `Building` and `Builder` intrerfaces.
- Created new methods in `ExceptionWrapper`.

### Changed
- Updated dependencies:
    - org.junit-5.12.0


## [0.54.0] - 2025-01-03
### Changed
- Updated dependencies:
    - org.apache.log4j-2.24.3
    - org.junit-5.11.4

### Removed
- Removed code that was deprecated in 2023.


## [0.53.0] - 2024-09-28
### Added
- Created `FunctionCache` and `BiFunctionCache`.

### Changed
- Updated dependencies:
    - commons-cli-1.9.0
    - org.junit-5.11.1


## [0.52.1] - 2024-05-18
### Changed
- Updated dependencies:
    - commons-cli-1.7.0
    - org.apache.log4j-2.23.1
    - org.junit-5.10.2
- Updated maven plugins.

### Fixed
- Fixed `StringComparison.compareDecimalDigits()` for large numbers.


## [0.52.0] - 2024-01-01
### Added
- Added `StringUtils.splitLines()`.

### Changed
- Added `FileEncoder.exec()` and call `System.exit()` in `FileEncoder.main()`.
- Updated maven plugins.


## [0.51.0] - 2023-12-09
### Changed
- Moved `AbstractMainSupport.Result` to `MainResult`.  
  This change has a very low impact probability as `getResult()` was protected.
- `AbstractMainSupport.getResult()` is now public. 


## [0.50.0] - 2023-11-25
### Changed
- Moved to Java 17

### Deprecated
- Deprecated `CollectionUtils.toUnmodifiableList`
- Deprecated `CollectionUtils.toUnmodifiableSet`


## [0.33.2] - 2023-11-18
### Added
- Added `--help-width` standard option to `AbstractMainSupport` to set help width.  
  Default width is unchanged: 74.

### Changed
- Updated dependencies:
    - commons-cli-1.6.0
    - org.junit-5.10.1


## [0.33.1] - 2023-10-21
### Added
- Added declaration of missing plugins.
 
### Changed
- Updated dependencies:
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.33.0] - 2023-07-15
### Added
- Added `AbstractMainSupport.createGroup(...)`.


## [0.32.0] - 2023-05-01
### Added
- Added `WeightComparator`.

### Changed
- Updated dependencies:
    - org.junit-5.9.3

### Removed
- Removed old deprecated code.


## [0.31.0] - 2023-02-25
### Added
- Added `StringUtils.format()` function.

### Changed
- Updated dependencies:
    - org.apache.log4j-2.20.0


## [0.30.0] - 2023-02-14
### Fixed
- Fixed bug in `StringComparison`. #63


## [0.29.0] - 2023-01-28
### Added
- Added `--args-file-charset` option to `AbstractMainSupport`.  
  This may be used when the args-file encoding is not the OS default encoding.

### Changed
- Updated dependencies:
    - org.junit-5.9.2


## [0.28.2] - 2023-01-02
### Changed
- Added OPTIONS to `AbstractMainSupport`.
- Changed display of usage in `AbstractMainSupport`.

### Fixed
- Fixed `StringUtils.numberOfLines()`. It only counted `\n`. Now it counts `\n`, `\r` and `\r\n`.


## [0.28.1] - 2022-11-08
### Changed
- Updated dependencies:
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1
- Added a way to escape '#' with `--args-file` option. #62


## [0.28.0] - 2022-08-23
### Added
- Created `CharPredicate` and some implementations. This code comes from `cdc-applic`.
- Created `CharFunction`.
- Added `StringUtils.wrap`.

### Changed
- Improved `MetaDataEncoder`. It now recognizes more characters as spaces or line terminators.
  It supports new lines in keys and values. #61
- Updated dependencies:
    - org.junit-5.9.0

### Fixed
- Fixed `StringUtils.numberOfLines()`.
- Fixed `StringUtils.maxLineLength()`.


## [0.27.0] - 2022-07-07
### Added
- Added a method to `Introspection`.
- Added methods `StringUtils`.
- Created `getOrCreateValueAsDirectory` to replace `getValueAsDirectory` that is now deprecated. #60

### Changed
- Updated dependencies:
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1

### Deprecated
- Deprecated  `getValueAsDirectory`. Use `getOrCreateValueAsDirectory` instead. #60


## [0.26.0] - 2022-06-18
### Added
- Added `hasArgsFile()`, `getReferencePath()` and `resolveToReferencePath` to `AbstractMainSupport`.

### Deprecated
- Some methods related to files in `AbstractMainArgs` have been deprecated.  
  They are replaced by simpler ones.


## [0.25.0] - 2022-05-19
### Added
- Created `StringComparison` utility. #59

### Changed
- Updated maven plugins


## [0.24.0] - 2022-04-19
### Added
- Created `ObjectUtils.identityHashString()`.


## [0.23.0] - 2022-03-11
### Added
- Created `Path.isValidName()`.

### Changed
- Changed signature of `IterableUtils.filter(Iterable, Predicate)`.
- Updated dependencies:
    - org.apache.log4j-2.17.2


## [0.22.0] - 2022-02-21
### Changed
- `Path` separator can now be any of `/`, `\` or `|`. #56 and #57


## [0.21.0] - 2022-02-17
### Added
- Created `ManifestUtils`. #55


## [0.20.0] - 2022-02-13
### Added
- Created primitive type mutable holders (`BooleanHolder`, `CharHolder`, ...). 

### Changed
- Updated maven plugins
- Upgraded to Java 11


## [0.14.2] - 2022-01-03
### Added
- Added char conversion to `StringConversion`. #54

### Security
- Updated dependencies:
    - org.apache.log4j-2.17.1 #52


## [0.14.1] - 2021-12-28
### Added
- Added `Path.subpath()`.

### Fixed
- Fixed a bug with empty Paths.

### Security
- Updated dependencies:
    - org.apache.log4j-2.17.0 #52


## [0.14.0] - 2021-12-14
### Added
- Created `Verbosity` and `ControlledPrintable` to allow control of verbosity. #51
- Added methods to Path. #53

### Changed
- Updated dependencies:
    - junit-1.8.2

### Security
- Updated dependencies:
    - org.apache.log4j-2.16.0. #52


## [0.13.2] - 2021-11-01
### Added
- Added new method to `Coverage`.

### Changed
- Updated dependencies.


## [0.13.1] - 2021-10-17
### Fixed
- Fixed handling of exceptions `AbstractMainSupport`.


## [0.13.0] - 2021-10-17
### Added
- Added new methods to `AbstractMainSupport`.  
  It is now possible to configure exception propagation in main().

### Changed
- Created `demos` module and moved code to it. #50
- Changed implementation of `KeyValueSplitter` for a better one. #49


## [0.12.0] - 2021-10-02
### Added
- Added `CaseConverter.Builder`
- Added `KeyValue` and `KeyValueSplitter`
- Added new methods to `EnumUtils`, `Encoders` and `ExtensionEncoder`.

### Changed
- Deprecated `CaseConverter` public constructor.


## [0.11.0] - 2021-07-22
### Added
- Added `Charset` option getter to `AbstractMainSupport`.
- Added `ProgressEvent` importance
- Added close() to `ProgressSupplier`.
- Added `StringUtils.nullify`

### Fixed
- ProgressEventFilter behaves correctly with an important ProgressEvent.


## [0.10.2] - 2021-06-05
### Added
- Added time utility to Chronometer.
- Created Consumers.
- Added new methods to Evaluation.
- Added new methods to AbstractMainSupport.


## [0.10.1] - 2021-05-10
### Fixed
- **IterableUtils.toSet** performance has been improved. cdc-java.kernel#1


## [0.10.0] - 2021-05-03
### Added
- CHANGELOG.md.
- Print more details in GraphPrinter.print. #35
- `migrate-to-0.10.0.sh` script to support migration to 0.10.0.

### Changed
- Moved Config to `cdc-util-tools`.

### Removed
- **tuples** module. It has been moved to **cdc-tuples** project. #36  
  One needs to update `poms` using new modules and replace `cdc.util.tuples` by `cdc.tuples` in Java code.
- **gv** module. It has been moved to **cdc-gv** project. #38  
  One needs to update `poms` using new modules and replace `cdc.util.gv` by `cdc.gv.api` or `cdc.gv.tools` in Java code.
- **graphs** modules. They have been moved to **cdc-graphs** project. #37  
  One needs to update `poms` using new modules, and replace `cdc.util.graphs.?` by `cdc.graphs.?` in Java code.
- **prefs** module. It has been moved to **cdc-kernel** project. #42  
  One needs to update `poms` using new modules, and replace `cdc.util.prefs` by `cdc.prefs` in Java code.
- **rdb** module. It has been moved to **cdc-rdb** project. #39  
  One needs to update `poms` using new modules, and replace `cdc.util.rdb` by `cdc.rdb` in Java code.
- **enums** module. It has been moved to **cdc-kernel** project. #40  
  One needs to update `poms` using new modules, and replace `cdc.util.enums` by `cdc.enums` in Java code.
- **office** modules. They have been moved to **cdc-office** project. #41  
  One needs to update `poms` using new modules, and replace `cdc.util.office` by `cdc.office` in Java code.
- **csv** module. It has been moved to **cdc-office** project. #41  
  One needs to update `poms` using new modules, and replace `cdc.util.csv` by `cdc.office.csv` in Java code.
- **tables** module. It has been moved to **cdc-office** project. #41  
  One needs to update `poms` using new modules, and replace `cdc.util.tables` by `cdc.office.tables` in Java code.
- **args** module. It has been moved to **cdc-kernel** project. #45  
  One needs to update `poms` using new modules, and replace `cdc.util.args` by `cdc.args` in Java code.  
  IO code has been moved to `cdc.args.io`.
- **converters** module. It has been moved to **cdc-kernel** project. #44  
  One needs to update `poms` using new modules, and replace `cdc.util.converters` by `cdc.converters` in Java code.  
  IO code has been moved to `cdc.converters.io`.
- **informers** module. It has been moved to **cdc-kernel** project. #47  
  One needs to update `poms` using new modules, and replace `cdc.util.informers` by `cdc.informers` in Java code.  
  IO code has been moved to `cdc.informers.io`.
- **rids** module. It has been moved to **cdc-kernel** project. #48  
  One needs to update `poms` using new modules, and replace `cdc.util.rids` by `cdc.rids` in Java code.
- **validation** module. It has been moved to **cdc-kernel** project. #46  
  One needs to update `poms` using new modules, and replace `cdc.util.validation` by `cdc.validation` in Java code.  
  IO code has been moved to `cdc.validation.io`.
- `cdc.util.io` package has been move to `cdc-io-utils`


## [0.9.0] - 2021-03-29
### Added
- Description and Builder to FormalArgs.
- VerboseProgressController, VerboseLinesHandler, TableRowsCounter
- Progress control during loading of workbooks. #32
- Text can now be loaded or saved as CDATA in data. #31
- PRETTY_CONFIG to JsonpUtils.

### Changed
- New parameter (systemId) in StAXLoader, StAXParser
- New parameter (numberOfRows) in TableHandler.processBegin. #32


## [0.8.0] - 2021-03-07
### Added
- New functions to GraphAdapter.
- New features to WorkbookWriter to saturate/truncate cells. #30
- clearEdges to BasicLightGraoph.

### Changed
- Renamed getNodes(N, EdgeDirection) to getConnectedNodes in GraphAdapter.


## [0.7.0] - 2021-03-01
### Changed
- Handling of features in WorkbookWriter.

### Removed
- pstrings module. It is now a standalone project.

## [0.6.0] - 2021-02-22
### Added
- Indentation of traces in Json parsing.
- Compare(N, N) to GraphPartialOrder.
- Parsing of all sheets in a workbook is now possible. #22
- AUTO_FILTER_COLUMNS in WorkbookWriter. #25
- Timeout to GvToAny. #21


## [0.5.0] - 2021-02-07
### Added
- New formating functions to Checks. #12
- Lexicographic comparison of arrays.
- JSON module.
- XmlWriter: new option to force transformation of some chars to entities, when it is  not mandatory. #13
- ObjectUtils
- New GV functions.
- New data functions. #19

### Changed
- Xml writing and StAX reading.

### Fixed
- XmlNormalizer
- Explain in Checker. #16
- XmlToHtml, using a solution that should have the same behavior on more OSes. #15
- Bug in SPath. #18

### Removed
- Code hat was improperly placed in src/main/resources/src/...  #14
- getPart0 and getPart1 in AbstractMainSupport. Replaced with getPart

## [0.4.0] - 2021-01-17
### Added
### Changed
### Fixed


## [0.3.0] - 2020-12-13
### Added
### Changed
### Fixed


## [0.2.0] - 2020-10-06
### Added
### Changed
### Fixed


## [0.1.1] - 2020-05-06
### Added
### Changed
### Fixed


## [0.0.22] - 2020-02-03
### Added
### Changed
### Fixed


## [0.0.21.1] - 2020-01-14
### Added
### Changed
### Fixed


## [0.0.21] - 2020-01-12
### Added
### Changed
### Fixed


## [0.0.20] - 2010-11-24
### Added
### Changed
### Fixed


## [0.0.19] - 2010-10-30
### Added
### Changed
### Fixed


## [0.0.18] - 2019-10-20
### Added
### Changed
### Fixed


## [0.0.17] - 2019-09-29
### Added
### Changed
### Fixed


## [0.0.16] - 2019-09-08
### Added
### Changed
### Fixed


## [0.0.15] - 2019-07-06
### Added
### Changed
### Fixed


## [0.0.14] - 2019-05-28
### Added
### Changed
### Fixed


## [0.0.13] - 2019-04-26
### Added
### Changed
### Fixed


## [0.0.12] - 2019-03-25
### Added
### Changed
### Fixed


## [0.0.11.4] - 2019-03-12
### Added
### Changed
### Fixed


## [0.0.11.3] - 2019-03-09
### Added
### Changed
### Fixed


## [0.0.11.2] - 2019-03-01
### Added
### Changed
### Fixed


## [0.0.11] - 2019-01-20
### Added
### Changed
### Fixed


## [0.0.10] - 2018-12-09
### Added
### Changed
### Fixed


## [0.0.9.2] - 2018-11-10
### Added
### Changed
### Fixed


## [0.0.9.1] - 2018-10-26
### Added
### Changed
### Fixed


## [0.0.9] - 2018-10-10
### Added
### Changed
### Fixed


## [0.0.8] - 2018-09-17
### Added
### Changed
### Fixed


## [0.0.7] - 2018-05-21
### Added
### Changed
### Fixed


## [0.0.6] - 2018-04-30
### Added
### Changed
### Fixed


## [0.0.5] - 2018-03-27
### Added
### Changed
### Fixed


## [0.0.4] - 2018-02-13
### Added
### Changed
### Fixed


## [0.0.3] - 2017-12-10
### Added
### Changed
### Fixed


## [0.0.2] - 2017-10-30
### Added
### Changed
### Fixed


## [0.0.1] - 2017-10-15
### Added

