#!/bin/bash

# Script that can help migration to cdc-util-0.10.0
# It changes imporst in *.java and pom.xml files
# some additional actions are necessary in pom.xml.

# Use tabulations as IFS
# Can be commented if directories don't contain spaces 
#IFS="	"

# List of root directories.
# Separate by tabulation or comment above line
dirs="workspace-ibacs/git/ibacs/ibacs"

function fixjava {
   file=$1
   echo $file
   sed -i\
       -e "s/cdc\.util\.args/cdc.args/g" \
       -e "s/cdc\.util\.compress/cdc.io.compress/g" \
       -e "s/cdc\.util\.converters/cdc.converters/g" \
       -e "s/cdc\.util\.csv/cdc.office.csv/g" \
       -e "s/cdc\.util\.data/cdc.io.data/g" \
       -e "s/cdc\.util\.enums/cdc.enums/g" \
       -e "s/cdc\.util\.graphs/cdc.graphs/g" \
       -e "s/cdc\.util\.gv/cdc.gv/g" \
       -e "s/cdc\.util\.informers/cdc.informers/g" \
       -e "s/cdc\.util\.json/cdc.io.json/g" \
       -e "s/cdc\.util\.office/cdc.office/g" \
       -e "s/cdc\.util\.prefs/cdc.prefs/g" \
       -e "s/cdc\.util\.rdb/cdc.rdb/g" \
       -e "s/cdc\.util\.rids/cdc.rids/g" \
       -e "s/cdc\.util\.tables/cdc.office.tables/g" \
       -e "s/cdc\.util\.tuples/cdc.tuples/g" \
       -e "s/cdc\.util\.txt/cdc.io.txt/g" \
       -e "s/cdc\.util\.validation/cdc.validation/g" \
       -e "s/cdc\.util\.xml/cdc.io.xml/g" \
       -e "s/args\.ArgsIo/args.io.ArgsIo/g" \
       -e "s/converters\.ConvertersIo/converters.io.ConvertersIo/g" \
       -e "s/validation\.ValidationIo/validation.io.ValidationIo/g" \
       $file

    unix2dos $file
}

function setuppom() {
   pat="; { /cdc-util-XXX/ { s/cdc-util-XXX/cdc-kernel-XXX/; s/cdc-java\.util/cdc-java.kernel/ }}"
   for name in args converters enums informers prefs rids validation
   do
      repl+=`echo "$pat" | sed -e "s/XXX/$name/g"`
   done 

   pat="; { /cdc-util-XXX/ { s/cdc-util-XXX/cdc-io-XXX/; s/cdc-java\.util/cdc-java.io/ }}"
   for name in compress data json txt xml
   do
      repl+=`echo "$pat" | sed -e "s/XXX/$name/g"`
   done 

   pat="; { /cdc-util-XXX/ { s/cdc-util-XXX/cdc-office-XXX/; s/cdc-java\.util/cdc-java.office/ }}"
   for name in csv tables
   do
      repl+=`echo "$pat" | sed -e "s/XXX/$name/g"`
   done 
   
   pat="; { /cdc-util-XXX/ { s/cdc-util-XXX/cdc-XXX/; s/cdc-java\.util/cdc-java.office/ }}"
   for name in office-doc office-ss
   do
      repl+=`echo "$pat" | sed -e "s/XXX/$name/g"`
   done 

   pat="; { /cdc-util-XXX/ { s/cdc-util-XXX/cdc-XXX/; s/cdc-java\.util/cdc-java.gv/ }}"
   for name in gv
   do
      repl+=`echo "$pat" | sed -e "s/XXX/$name/g"`
   done 

   pat="; { /cdc-util-XXX/ { s/cdc-util-XXX/cdc-XXX/; s/cdc-java\.util/cdc-java.graphs/ }}"
   for name in graphs
   do
      repl+=`echo "$pat" | sed -e "s/XXX/$name/g"`
   done 
}

function fixpom {
   file=$1
   echo $file

   sed -i\
       -e "/<groupId>com.gitlab.cdc-java.util<\/groupId>/ {N; { $repl }; P; D}" \
       $file
   
   sed -i\
       -e "s/<artifactId>cdc-gv<\/artifactId>/<artifactId>cdc-gv-api<\/artifactId>/" \
       -e "s/<artifactId>cdc-graphs<\/artifactId>/<artifactId>cdc-graphs-api<\/artifactId>/" \
       $file
   unix2dos $file
}

setuppom

for dir in $dirs
do
   for file in `find $dir -name pom.xml`
   do
      fixpom $file
   done

   for file in `find $dir -name "*.java"`
   do
      fixjava $file
   done
done
