package cdc.util.cli;

/**
 * Interface implemented by enums that can be used as options.
 * <p>
 *
 * @author Damien Carbonne
 */
public interface OptionEnum {

    /**
     * @return The option long name.
     */
    public String getName();

    /**
     * @return The optional option short name.<br>
     *         The default implementation returns {@code null}.
     */
    public default String getShortName() {
        return null;
    }

    /**
     * @return The option description.
     */
    public String getDescription();
}