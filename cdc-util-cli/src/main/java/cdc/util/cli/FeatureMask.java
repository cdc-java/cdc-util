package cdc.util.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;

/**
 * Utility to handle features.
 *
 * @author Damien Carbonne
 *
 * @param <E> The feature enum type.
 */
public final class FeatureMask<E extends Enum<E>> {
    private final Set<E> features = new HashSet<>();

    public FeatureMask() {
        super();
    }

    public final void setEnabled(E feature,
                                 boolean enabled) {
        if (enabled) {
            features.add(feature);
        } else {
            features.remove(feature);
        }
    }

    public final void add(E feature) {
        features.add(feature);
    }

    @SafeVarargs
    public final void addAll(E... features) {
        for (final E feature : features) {
            add(feature);
        }
    }

    public final void addAll(Collection<E> features) {
        this.features.addAll(features);
    }

    public final void addAll(FeatureMask<E> mask) {
        addAll(mask.features);
    }

    public final void remove(E feature) {
        this.features.remove(feature);
    }

    public final boolean isEnabled(E feature) {
        return features.contains(feature);
    }

    public boolean isEmpty() {
        return features.isEmpty();
    }

    public final boolean contains(E feature) {
        return features.contains(feature);
    }

    public final Set<E> toSet() {
        final Set<E> set = new HashSet<>();
        set.addAll(features);
        return set;
    }

    public final List<E> toList() {
        final List<E> list = new ArrayList<>();
        list.addAll(features);
        return list;
    }

    /**
     * Check that at most one feature is enabled in a group of features.
     *
     * @param logger The logger to use.
     * @param reaction The reaction to adopt in case of failure.
     * @param features The features.
     * @return {@code true} if at most one feature among {@code features}
     *         is enabled in this FeatureMask.
     */
    @SafeVarargs
    public final boolean checkAtMostOne(Logger logger,
                                        FailureReaction reaction,
                                        E... features) {
        final FeatureMask<E> other = new FeatureMask<>();
        other.addAll(features);
        other.features.retainAll(this.features);
        if (other.features.size() <= 1) {
            return true;
        } else {
            FailureReaction.onError("Expected at most one enabled feature among " + Arrays.toString(features),
                                    logger,
                                    reaction,
                                    IllegalArgumentException::new);
            return false;
        }
    }

    /**
     * Check that exactly one feature is enabled in a group of features.
     *
     * @param logger The logger to use.
     * @param reaction The reaction to adopt in case of failure.
     * @param features The features.
     * @return {@code true} if exactly one feature among {@code features}
     *         is enabled in this FeatureMask.
     */
    @SafeVarargs
    public final boolean checkExactlyOne(Logger logger,
                                         FailureReaction reaction,
                                         E... features) {
        final FeatureMask<E> other = new FeatureMask<>();
        other.addAll(features);
        other.features.retainAll(this.features);
        if (other.features.size() == 1) {
            return true;
        } else {
            FailureReaction.onError("Expected exactly one enabled feature among " + Arrays.toString(features),
                                    logger,
                                    reaction,
                                    IllegalArgumentException::new);
            return false;
        }
    }
}