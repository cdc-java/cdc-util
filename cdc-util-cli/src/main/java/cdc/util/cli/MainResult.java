package cdc.util.cli;

public enum MainResult {
    SUCCESS(0),
    COMMAND_LINE_ERROR(1),
    EXECUTION_ERROR(2);

    private final int code;

    private MainResult(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}