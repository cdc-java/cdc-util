package cdc.util.cli;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import cdc.util.files.Files;
import cdc.util.lang.Checks;
import cdc.util.lang.ExceptionWrapper;

/**
 * Utility that can be used to create main programs.
 * <p>
 * The purpose is to force the adoption of a common pattern for such classes.
 * <p>
 * Some options are automatically defined and handled:
 *
 * <ul>
 * <li><b>help:</b> if passed, help is printed and program is exited.
 *
 * <li><b>version:</b> if passed, version is printed and program is exited.<br>
 * Override {@link AbstractMainSupport#getVersion()} to define version.
 *
 * <li><b>args-list &lt;FILE&gt;:</b> if passed, command line arguments
 * are read from the designated file and added to other command line arguments.<br>
 * This is useful when command line is too long.<br>
 * Override {@link AbstractMainSupport#addArgsFileOption(Options)} to force its presence.
 * </ul>
 *
 * <b>Args file syntax</b><br>
 * The file must contains one argument (option of value) per line.<br>
 * A line that is empty or starts with any number of white spaces followed by '#' and any other characters is a ignored.<br>
 * Exemple:<br>
 * <pre>
 * # (ignored)
 * --option1
 * --option2
 * --option3
 * value1
 * value2
 *    # (ignored)
 * value3
 * </pre>
 *
 * @author Damien Carbonne
 *
 * @param <A> MainArgs type.
 * @param <R> Returned type.
 */
public abstract class AbstractMainSupport<A, R> {
    private final Class<?> mainClass;
    private final Logger logger;
    private MainResult result = MainResult.SUCCESS;
    /** Set to true if args file option has been passed. */
    private boolean hasArgsFile = false;
    private int helpWidth = HelpFormatter.DEFAULT_WIDTH;
    private Path referencePath;

    public static final String DEFAULT_PARTS_SEPARATOR = "::";

    public static final String ARGS_FILE = "args-file";
    public static final String ARGS_FILE_CHARSET = "args-file-charset";
    public static final String CHARSET = "charset";
    public static final String DRIVER = "driver";
    public static final String HELP = "help";
    public static final String HELP_WIDTH = "help-width";
    public static final String INPUT = "input";
    public static final String INPUT_DIR = "input-dir";
    public static final String OUTPUT = "output";
    public static final String OUTPUT_DIR = "output-dir";
    public static final String PASSWORD = "password";
    public static final String PATH = "path";
    public static final String PREFIX = "prefix";
    public static final String TMP_DIR = "tmp-dir";
    public static final String URL = "url";
    public static final String USER = "user";
    public static final String VERSION = "version";

    public enum ExceptionThrowing {
        NEVER,
        EXECUTION,
        COMMAND_LINE_AND_EXECUTION;

        public boolean matchesExecution() {
            return this == EXECUTION || this == COMMAND_LINE_AND_EXECUTION;
        }

        public boolean matchesCommandLine() {
            return this == COMMAND_LINE_AND_EXECUTION;
        }
    }

    protected AbstractMainSupport(Class<?> mainClass,
                                  Logger logger) {
        this.mainClass = mainClass;
        this.logger = logger;
    }

    /**
     * Returns {@code true} if {@link #ARGS_FILE} option must be added.
     * <p>
     * The default implementation returns {@code true} if one option
     * contains multiple values.
     *
     * @param options The options.
     * @return {@code true} if {@link #ARGS_FILE} option must be added.
     */
    protected boolean addArgsFileOption(Options options) {
        return hasMultipleValuesOption(options);
    }

    protected void addStandardOptions(Options options) {
        final Option help = Option.builder("h")
                                  .longOpt(HELP)
                                  .desc("Prints this help and exits.")
                                  .build();
        options.addOption(help);

        options.addOption(Option.builder()
                                .longOpt(HELP_WIDTH)
                                .desc("Optional help width (default: " + HelpFormatter.DEFAULT_WIDTH + ").")
                                .hasArg()
                                .build());

        if (getVersion() != null) {
            final Option version = Option.builder("v")
                                         .longOpt(VERSION)
                                         .desc("Prints version and exits.")
                                         .build();
            options.addOption(version);

            final OptionGroup group = new OptionGroup();
            group.addOption(help);
            group.addOption(version);
            options.addOptionGroup(group);
        }
        if (addArgsFileOption(options)) {
            options.addOption(Option.builder()
                                    .longOpt(ARGS_FILE)
                                    .desc("Optional name of the file from which options can be read.\n"
                                            + "A line is either ignored or interpreted as a single argument (option or value).\n"
                                            + "A line is ignored when it is empty or starts by any number of white spaces followed by '#'.\n"
                                            + "A line that only contains white spaces is an argument.\n"
                                            + "A comment starts by a '#' not following a '\\'. The \"\\#\" sequence is read as '#'.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ARGS_FILE_CHARSET)
                                    .desc("Optional name of the args file charset."
                                            + "\nIt may be used if args file encoding is not the OS default file encoding.")
                                    .hasArg()
                                    .build());
        }
    }

    public final Class<?> getMainClass() {
        return mainClass;
    }

    public final Logger getLogger() {
        return logger;
    }

    /**
     * Returns the version.
     * <p>
     * If {@code null} is returned (default implementation), no version option is available.
     *
     * @return The version.
     */
    protected String getVersion() {
        return null;
    }

    /**
     * Returns the help header.
     * <p>
     * If {@code null} is returned (default implementation), no help header is printed.
     *
     * @return The help header.
     */
    protected String getHelpHeader() {
        return null;
    }

    /**
     * Returns the help footer.
     * <p>
     * If {@code null} is returned (default implementation), no help footer is printed.
     *
     * @return The help footer.
     */
    protected String getHelpFooter() {
        return null;
    }

    /**
     * Creates specific options and add them to an options collection.
     * <p>
     * The standard options must not be added.
     *
     * @param options The options.
     */
    protected abstract void addSpecificOptions(Options options);

    /**
     * Analyzes the command line.
     * <p>
     * These options are already handled:
     * <ul>
     * <li>help
     * <li>version
     * <li>args-file
     * </ul>
     *
     * @param cl The command line.
     * @return A MainARgs instance.
     * @throws ParseException When command line parsing has a problem.
     */
    protected abstract A analyze(CommandLine cl) throws ParseException;

    /**
     * Executes the main program.
     *
     * @param margs The main arguments.
     * @return The optional result.
     * @throws Exception When program has a problem.
     */
    protected abstract R execute(A margs) throws Exception;

    public static <E extends Enum<E> & OptionEnum> void addNoArgOptions(Options options,
                                                                        Class<E> enumClass) {
        for (final E e : enumClass.getEnumConstants()) {
            options.addOption(Option.builder(e.getShortName())
                                    .longOpt(e.getName())
                                    .desc(e.getDescription())
                                    .build());
        }
    }

    public static <E extends Enum<E> & OptionEnum> void addGroupedNoArgOptions(Options options,
                                                                               Class<E> enumClass,
                                                                               boolean required) {
        addNoArgOptions(options, enumClass);
        createGroup(options, required, enumClass.getEnumConstants());
    }

    public static OptionGroup createGroup(Options options,
                                          boolean required,
                                          String... opts) {
        final OptionGroup group = new OptionGroup();
        for (final String opt : opts) {
            group.addOption(options.getOption(opt));
        }
        options.addOptionGroup(group);
        group.setRequired(required);
        return group;
    }

    public static OptionGroup createGroup(Options options,
                                          String... opts) {
        return createGroup(options, false, opts);
    }

    public static OptionGroup createGroup(Options options,
                                          boolean required,
                                          Option... opts) {
        final OptionGroup group = new OptionGroup();
        for (final Option opt : opts) {
            group.addOption(opt);
        }
        options.addOptionGroup(group);
        group.setRequired(required);
        return group;
    }

    public static OptionGroup createGroup(Options options,
                                          Option... opts) {
        return createGroup(options, false, opts);
    }

    @SafeVarargs
    public static <E extends Enum<E> & OptionEnum> OptionGroup createGroup(Options options,
                                                                           boolean required,
                                                                           E... values) {
        final OptionGroup group = new OptionGroup();
        for (final E value : values) {
            group.addOption(options.getOption(value.getName()));
        }
        options.addOptionGroup(group);
        group.setRequired(required);
        return group;
    }

    @SafeVarargs
    public static <E extends Enum<E> & OptionEnum> OptionGroup createGroup(Options options,
                                                                           E... values) {
        return createGroup(options, false, values);
    }

    @FunctionalInterface
    public static interface Maskable<E extends Enum<E>> {
        public void setEnabled(E e,
                               boolean enabled);
    }

    public static <E extends Enum<E> & OptionEnum> void setMask(CommandLine cl,
                                                                Class<E> enumClass,
                                                                Maskable<E> maskable) {
        for (final E e : enumClass.getEnumConstants()) {
            maskable.setEnabled(e, cl.hasOption(e.getName()));
        }
    }

    /**
     * Removes the comment part of a string.
     * <p>
     * A comment starts with any number of white spaces followed by '#'
     *
     * @param s The string
     * @return {@code s} with comments removed.
     */
    static String removeComments(String s) {
        final int length = s.length();
        int index = 0;
        final StringBuilder builder = new StringBuilder();
        boolean hasComments = false;
        while (index < length) {
            final char c = s.charAt(index);
            if (c == '\\') {
                if (index < length - 1 && s.charAt(index + 1) == '#') {
                    builder.append('#');
                    index++;
                } else {
                    builder.append('\\');
                }
            } else if (c == '#') {
                // Start of comments
                index = length;
                hasComments = true;
            } else {
                builder.append(c);
            }
            index++;
        }
        final String k = builder.toString();
        if (hasComments) {
            if (k.trim().isBlank()) {
                return "";
            } else {
                return k.stripTrailing();
            }
        } else {
            return k;
        }
    }

    private static String[] loadArgsFile(File file,
                                         String charsetName) throws IOException {
        try (final InputStream in = new BufferedInputStream(new FileInputStream(file));
                final BufferedReader reader =
                        new BufferedReader(charsetName == null
                                ? new InputStreamReader(in)
                                : new InputStreamReader(in, charsetName))) {
            final List<String> list = new ArrayList<>();
            String line = null;
            while ((line = reader.readLine()) != null) {
                line = removeComments(line);
                if (!line.isEmpty()) {
                    list.add(line);
                }
            }
            return list.toArray(new String[list.size()]);
        }
    }

    /**
     * Default main program.
     * <p>
     * This does the following things:
     * <ol>
     * <li>Build options.
     * <li>Parse the command line strings and creates a CommandLine instance.
     * <li>Analyze the CommandLine instance and create a MainArgs instance.
     * <li>Calls execute with the MainArgs instance.
     * </ol>
     *
     * @param args The command line arguments.
     * @return The optional result.
     */
    public R main(String[] args) {
        // Build all options as declared
        final Options allOptions = buildOptions();
        // Build all options as optional
        final Options optionalOptions = getOptionsAsOptional();
        final DefaultParser parser = new DefaultParser();
        Exception x = null;
        try {
            final String[] effectiveArgs;
            // Analysis of optional args file
            final CommandLine cl0 = parser.parse(optionalOptions, args);
            final CommandLine cl1;
            final File argsFile = getValueAsFile(cl0, ARGS_FILE, IS_NULL_OR_FILE);
            final String argsFileCharset = getValueAsString(cl0, ARGS_FILE_CHARSET, null);
            final File currentDir = Files.currentDir();
            if (argsFile != null) {
                final String[] fileArgs = loadArgsFile(argsFile, argsFileCharset);
                // Concatenate file args followed by normal args
                effectiveArgs = Stream.concat(Arrays.stream(fileArgs), Arrays.stream(args))
                                      .toArray(String[]::new);
                cl1 = parser.parse(optionalOptions, effectiveArgs);
                this.hasArgsFile = true;
                this.referencePath = currentDir.toPath().resolve(argsFile.toPath()).getParent();
            } else {
                effectiveArgs = args;
                cl1 = cl0;
                this.hasArgsFile = false;
                this.referencePath = currentDir.toPath();
            }

            this.helpWidth = getValueAsInt(cl1, HELP_WIDTH, HelpFormatter.DEFAULT_WIDTH);

            // First parsing and processing
            if (cl1.hasOption(HELP)) {
                printHelp(allOptions, null);
                return null;
            }
            if (cl1.hasOption(VERSION)) {
                printVersion();
                return null;
            }

            // Normal parsing and processing
            final CommandLine cl2 = parser.parse(allOptions, effectiveArgs);
            final A margs = analyze(cl2);
            try {
                return execute(margs);
            } catch (final Exception e) {
                result = MainResult.EXECUTION_ERROR;
                getLogger().catching(e);
                x = e;
            }
        } catch (final Exception e) {
            result = MainResult.COMMAND_LINE_ERROR;
            printHelp(allOptions, e);
            x = e;
        }

        // Here x != null
        if (result == MainResult.EXECUTION_ERROR) {
            if (getExceptionThrowing().matchesExecution()) {
                throw ExceptionWrapper.wrap(x);
            } else {
                return null;
            }
        } else if (result == MainResult.COMMAND_LINE_ERROR) {
            if (getExceptionThrowing().matchesCommandLine()) {
                throw ExceptionWrapper.wrap(x);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public final MainResult getResult() {
        return result;
    }

    /**
     * @return {@code true} if an args file was passed and analyzed.<br>
     *         This shall not be called before {@link #main(String[])} is called.
     */
    public final boolean hasArgsFile() {
        return hasArgsFile;
    }

    /**
     * Returns the reference directory as a {@link Path}.
     * <p>
     * It is the directory that contains args-file, if this option has been used,
     * or the current directory otherwise.
     *
     * @return The reference directory.
     */
    public final Path getReferencePath() {
        return referencePath;
    }

    public File resolveToReferencePath(File file) {
        return referencePath.resolve(file.toPath()).toFile();
    }

    /**
     * @return When exceptions shall be thrown.
     *         Default to {@link ExceptionThrowing#EXECUTION}.
     */
    protected ExceptionThrowing getExceptionThrowing() {
        return ExceptionThrowing.EXECUTION;
    }

    /**
     * @param options The options.
     * @return {@code true} if {@code options} contains an options having multiple values.
     */
    private static boolean hasMultipleValuesOption(Options options) {
        for (final Option option : options.getOptions()) {
            if (option.hasArgs()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates options, add standard and specific options and returns them.
     *
     * @return The options.
     */
    protected Options buildOptions() {
        final Options options = new Options();
        addSpecificOptions(options);
        addStandardOptions(options);
        return options;
    }

    /**
     * @return Build all options and make them optional.
     */
    protected Options getOptionsAsOptional() {
        final Options options = new Options();
        for (final Option option : buildOptions().getOptions()) {
            option.setRequired(false);
            options.addOption(option);
        }
        return options;
    }

    protected void printHelp(Options options,
                             Exception e) {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.setSyntaxPrefix("USAGE\n");
        formatter.setWidth(helpWidth);
        final StringBuilder header = new StringBuilder();
        if (getHelpHeader() != null) {
            header.append("\n");
            header.append(getHelpHeader());
            header.append("\n");
        }
        header.append("\n");
        if (e != null && e.getMessage() != null) {
            header.append(e.getMessage());
            header.append("\n\n");
        }
        header.append("OPTIONS\n");

        formatter.printHelp(getMainClass().getSimpleName(),
                            header.toString(),
                            options,
                            getHelpFooter(),
                            true);
        if (e != null) {
            if (e instanceof ParseException) {
                if (getLogger().isEnabled(Level.DEBUG)) {
                    getLogger().catching(e);
                }
            } else {
                getLogger().catching(e);
            }
        }
    }

    protected void printVersion() {
        System.out.println(getVersion());
    }

    private static ParseException invalidArg(CommandLine cl,
                                             String opt,
                                             String message) {
        final String[] values = cl.getOptionValues(opt);

        final StringBuilder builder = new StringBuilder();
        if (values != null) {
            builder.append("Invalid args for ");
            builder.append(opt);
            builder.append(" args:");
            final int count = Math.min(5, values.length);
            for (int index = 0; index < count; index++) {
                builder.append(" '");
                builder.append(values[index]);
                builder.append('\'');
            }
            if (count < values.length) {
                builder.append(" ...");
            }
        } else {
            builder.append("Missing option ");
            builder.append(opt);
        }
        if (message != null) {
            builder.append(" ");
            builder.append(message);
        }
        return new ParseException(builder.toString());
    }

    /**
     * Interface used to check the validity of an option value.
     *
     * @param <T> The value type.
     */
    public interface ValueChecker<T> {
        /**
         * Checks the validity of a value and returns it or throws an exception.
         *
         * @param cl The command line.
         * @param opt The option name.
         * @param value The value to check.
         * @return the input {@code value}.
         * @throws ParseException When {@code value} is invalid.
         */
        public T check(CommandLine cl,
                       String opt,
                       T value) throws ParseException;
    }

    /**
     * Implementation of {@link ValueChecker} that always accepts and returns the input value.
     */
    public static final ValueChecker<Object> IS_TRUE = (cl,
                                                        opt,
                                                        object) -> object;
    /**
     * Implementation of {@link ValueChecker} checking that the value
     * is an existing file.
     * <p>
     * <b>Note:</b> It will fail when value is {@code null}.
     */
    public static final ValueChecker<File> IS_FILE = (cl,
                                                      opt,
                                                      file) -> {
        if (file == null || !file.isFile()) {
            throw invalidArg(cl, opt, file + " is not an existing file. Current dir: '" + Files.currentDir() + "'");
        } else {
            return file;
        }
    };

    /**
     * Implementation of {@link ValueChecker} checking that the value
     * is an existing directory.
     * <p>
     * <b>Note:</b> It will fail when value is {@code null}.
     */
    public static final ValueChecker<File> IS_DIRECTORY = (cl,
                                                           opt,
                                                           file) -> {
        if (file == null || !file.isDirectory()) {
            throw invalidArg(cl, opt, file + " is not an existing directory. Current dir: '" + Files.currentDir() + "'");
        } else {
            return file;
        }
    };

    /**
     * Implementation of {@link ValueChecker} checking that the value
     * is an existing file or directory.
     * <p>
     * <b>Note:</b> It will fail when value is {@code null}.
     */
    public static final ValueChecker<File> IS_FILE_OR_DIRECTORY = (cl,
                                                                   opt,
                                                                   file) -> {
        if (file == null || (!file.isFile() && !file.isDirectory())) {
            throw invalidArg(cl, opt, file + " is not an existing file or directory. Current dir: '" + Files.currentDir() + "'");
        } else {
            return file;
        }
    };

    /**
     * Implementation of {@link ValueChecker} checking that the value
     * is an existing file, or is {@code null}.
     * <p>
     * <b>Note:</b> It will succeed when value is {@code null}.
     */
    public static final ValueChecker<File> IS_NULL_OR_FILE = (cl,
                                                              opt,
                                                              file) -> {
        if (file != null && !file.isFile()) {
            throw invalidArg(cl, opt, file + " is not an existing file. Current dir: '" + Files.currentDir() + "'");
        } else {
            return file;
        }
    };

    /**
     * Implementation of {@link ValueChecker} checking that the value
     * is an existing directory, or is {@code null}.
     * <p>
     * <b>Note:</b> It will succeed when value is {@code null}.
     */
    public static final ValueChecker<File> IS_NULL_OR_DIRECTORY = (cl,
                                                                   opt,
                                                                   file) -> {
        if (file != null && !file.isDirectory()) {
            throw invalidArg(cl, opt, file + " is not an existing directory. Current dir: '" + Files.currentDir() + "'");
        } else {
            return file;
        }
    };

    /**
     * Implementation of {@link ValueChecker} checking that the value
     * is an existing file or directory, or is {@code null}.
     * <p>
     * <b>Note:</b> It will succeed when value is {@code null}.
     */
    public static final ValueChecker<File> IS_NULL_OR_FILE_OR_DIRECTORY = (cl,
                                                                           opt,
                                                                           file) -> {
        if (file != null && !file.isFile() && !file.isDirectory()) {
            throw invalidArg(cl, opt, file + " is not an existing file or directory. Current dir: '" + Files.currentDir() + "'");
        } else {
            return file;
        }
    };

    /**
     * Returns the checked conversion of an option value to a type, or a checked default value.
     * <p>
     * <ul>
     * <li>If option is present:
     * <ol>
     * <li>tries to convert the option value to {@code <T>} using {@code converter}.<br>
     * If conversion fails, a {@link ParseException} is thrown.
     * <li>then checks the conversion result using {@code checker}.<br>
     * If check fails, a {@link ParseException} is thrown.
     * <li>then returns the conversion result.<br>
     * </ol>
     * <li>If option is absent:
     * <ul>
     * <li>checks the {@code def} using {@code checker}.<br>
     * If check fails, a {@link ParseException} is thrown.
     * <li>returns {@code def}.
     * </ul>
     * </ul>
     * <b>Note:</b> if option is optional, {@code checker} should accept {@code def},
     * whether it is {@code null} or not.
     *
     * @param <T> The returned value type.
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value. <em>MAY BE</em> null.
     * @param converter Function used to convert a String to the expected type.
     * @param checker The value checker.
     * @return The checked conversion of the option value to {@code <T>} type or checked {@code def}.
     * @throws ParseException If option value could not be converted, or check failed.
     */
    public static <T> T getValue(CommandLine cl,
                                 String opt,
                                 T def,
                                 Function<String, T> converter,
                                 ValueChecker<? super T> checker) throws ParseException {
        if (cl.hasOption(opt)) {
            try {
                final T result = converter.apply(cl.getOptionValue(opt));
                checker.check(cl, opt, result);
                return result;
            } catch (final ParseException e) {
                throw e;
            } catch (final Exception e) {
                throw invalidArg(cl, opt, e.getMessage());
            }
        } else {
            checker.check(cl, opt, def);
            return def;
        }
    }

    /**
     * Returns the conversion of an option value to a type, or a default value.
     * <p>
     * <ul>
     * <li>If the option is present:
     * <ol>
     * <li>tries to convert the option value to {@code <T>} using {@code converter}.<br>
     * If conversion fails, a {@link ParseException} is thrown.
     * <li>then returns the conversion result.<br>
     * </ol>
     * <li>If the option is absent: returns {@code def}.
     * </ul>
     * <b>Note:</b> this is equivalent to calling {@code getValue(cl, opt, def, converter, IS_TRUE)}.
     *
     * @param <T> The returned value type.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value. <em>MAY BE</em> null.
     * @param converter Function used to convert a String to the expected type.
     * @return The conversion of the option value to {@code <T>} type or {@code def}.
     * @throws ParseException If option value could not be converted.
     */
    public static <T> T getValue(CommandLine cl,
                                 String opt,
                                 T def,
                                 Function<String, T> converter) throws ParseException {
        return getValue(cl, opt, def, converter, IS_TRUE);
    }

    /**
     * Adds option values to a collection, after converting and checking them.
     *
     * @param <T> The value type.
     * @param cl The command line.
     * @param opt The option name.
     * @param values The collection of values.
     * @param converter Function used to convert a String to the expected type.
     * @param checker The value checker.
     * @throws ParseException When a conversion or check failed.
     */
    public static <T> void fillValues(CommandLine cl,
                                      String opt,
                                      Collection<T> values,
                                      Function<String, T> converter,
                                      ValueChecker<? super T> checker) throws ParseException {
        Checks.isNotNull(values, "values");
        if (cl.hasOption(opt)) {
            try {
                for (final String s : cl.getOptionValues(opt)) {
                    final T v = converter.apply(s);
                    checker.check(cl, opt, v);
                    values.add(v);
                }
            } catch (final ParseException e) {
                throw e;
            } catch (final Exception e) {
                throw invalidArg(cl, opt, e.getMessage());
            }
        }
    }

    /**
     * Adds option values to a collection, after converting them.
     * <p>
     * <b>Note:</b> this is equivalent to calling {@code fillValues(cl, opt, values, converter, IS_TRUE)}.
     *
     * @param <T> The value type.
     * @param cl The command line.
     * @param opt The option name.
     * @param values The collection of values.
     * @param converter Function used to convert a String to the expected type.
     * @throws ParseException When a conversion failed.
     */
    public static <T> void fillValues(CommandLine cl,
                                      String opt,
                                      Collection<T> values,
                                      Function<String, T> converter) throws ParseException {
        fillValues(cl, opt, values, converter, IS_TRUE);
    }

    /**
     * Returns the values of an option, converted and checked, as a List.
     *
     * @param <T> The value type.
     * @param cl The command line.
     * @param opt The option name.
     * @param converter Function used to convert a String to the expected type.
     * @param checker The value checker.
     * @return A List of values.
     * @throws ParseException When a conversion or check failed.
     */
    public static <T> List<T> getValues(CommandLine cl,
                                        String opt,
                                        Function<String, T> converter,
                                        ValueChecker<? super T> checker) throws ParseException {
        final List<T> values = new ArrayList<>();
        fillValues(cl, opt, values, converter, checker);
        return values;
    }

    /**
     * Returns the values of an option, converted, as a List.
     * <p>
     * <b>Note:</b> this is equivalent to calling {@code getValues(cl, opt, converter, IS_TRUE)}.
     *
     * @param <T> The value type.
     * @param cl The command line.
     * @param opt The option name.
     * @param converter Function used to convert a String to the expected type.
     * @return A List of values.
     * @throws ParseException When a conversion failed.
     */
    public static <T> List<T> getValues(CommandLine cl,
                                        String opt,
                                        Function<String, T> converter) throws ParseException {
        return getValues(cl, opt, converter, IS_TRUE);
    }

    /**
     * Adds the values of an option to a String collection.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param values The collection of values.
     */
    public static void fillValues(CommandLine cl,
                                  String opt,
                                  Collection<String> values) {
        if (cl.hasOption(opt)) {
            Collections.addAll(values, cl.getOptionValues(opt));
        }
    }

    /**
     * Returns the values of an option as a String List.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @return The values of option {@code opt} as a String List.
     */
    public static List<String> getValues(CommandLine cl,
                                         String opt) {
        final List<String> values = new ArrayList<>();
        if (cl.hasOption(opt)) {
            Collections.addAll(values, cl.getOptionValues(opt));
        }
        return values;
    }

    /**
     * Returns an option as a resolved file.
     * <p>
     * <ul>
     * <li>Converts option to a File or use default value.
     * <li>If this result is {@code null}, checks it and returns {@code null}.
     * <li>Otherwise, resolve this result using {@link #getReferencePath()}, checks the resolution and returns it.
     * </ul>
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def The default value.
     * @param checker The checker.
     * @return The checked resolved option value or checked default value as a file.
     * @throws ParseException When conversion failed or check failed.
     */
    public File getValueAsResolvedFile(CommandLine cl,
                                       String opt,
                                       File def,
                                       ValueChecker<? super File> checker) throws ParseException {
        final File file = getValueAsFile(cl, opt, def);
        if (file == null) {
            checker.check(cl, opt, null);
            return null;
        } else {
            final File f = getReferencePath().resolve(file.toPath()).toFile();
            checker.check(cl, opt, f);
            return f;
        }
    }

    /**
     * Returns an option as a resolved file.
     * <p>
     * <b>Note:</b> this is equivalent to {@code getValueAsResolvedFile(cl, opt, null, checker)}.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param checker The checker.
     * @return The checked resolved option value or checked {@code null} as a file.
     * @throws ParseException When conversion failed or check failed.
     */
    public File getValueAsResolvedFile(CommandLine cl,
                                       String opt,
                                       ValueChecker<? super File> checker) throws ParseException {
        return getValueAsResolvedFile(cl, opt, null, checker);
    }

    public File getValueAsResolvedFile(CommandLine cl,
                                       String opt,
                                       File def) throws ParseException {
        return getValueAsResolvedFile(cl, opt, def, IS_TRUE);
    }

    public File getValueAsResolvedFile(CommandLine cl,
                                       String opt) throws ParseException {
        return getValueAsResolvedFile(cl, opt, null, IS_TRUE);
    }

    /**
     * Returns an option as a File.
     * <p>
     * <b>Note:</b> if option is optional, {@code checker} should accept {@code def},
     * whether it is {@code null} or not.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def The default value.
     * @param checker The checker.
     * @return The checked conversion of option to a File or the checked default value.
     * @throws ParseException If option value could not be converted to a File, or check failed.
     */
    public static File getValueAsFile(CommandLine cl,
                                      String opt,
                                      File def,
                                      ValueChecker<? super File> checker) throws ParseException {
        return getValue(cl, opt, def, File::new, checker);
    }

    /**
     * Returns an option as a File.
     * <p>
     * <b>Note:</b> if option is optional, {@code checker} should accept {@code null}.
     * <p>
     * <b>Note:</b> this is equivalent to {@code getValueAsFile(cl, opt, null, checker)}.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param checker The checker.
     * @return The checked conversion of option to a File or the checked {@code null}.
     * @throws ParseException If option value could not be converted to a File, or check failed.
     */
    public static File getValueAsFile(CommandLine cl,
                                      String opt,
                                      ValueChecker<? super File> checker) throws ParseException {
        return getValueAsFile(cl, opt, null, checker);
    }

    /**
     * Returns an option value as a File.
     * <p>
     * If option is absent, returns a default value.<br>
     * Result is {@code null} only when option does not exist and default value is {@code null}.
     * <p>
     * <b>Note:</b> this is equivalent to {@code getValueAsFile(cl, opt, def, IS_TRUE)}.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a File.
     * @throws ParseException If result could not be produced.
     */
    public static File getValueAsFile(CommandLine cl,
                                      String opt,
                                      File def) throws ParseException {
        return getValueAsFile(cl, opt, def, IS_TRUE);
    }

    /**
     * Returns an option value as a File.
     * <p>
     * If option is absent, returns {@code null}.
     * <p>
     * <b>Note:</b> this is equivalent to {@code getValueAsFile(cl, opt, null, IS_TRUE)}.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @return The option value as a File.
     * @throws ParseException If result could not be produced.
     */
    public static File getValueAsFile(CommandLine cl,
                                      String opt) throws ParseException {
        return getValueAsFile(cl, opt, null, IS_TRUE);
    }

    /**
     * Returns an option value as a Directory, and tries to create it if it does not exist.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def The default value.
     * @return The option value as a Directory, possibly creating it.
     * @throws ParseException If result could not be produced.
     */
    public static File getOrCreateValueAsDirectory(CommandLine cl,
                                                   String opt,
                                                   File def) throws ParseException {
        final File result = getValueAsFile(cl, opt, def);
        if (result == null || (result.exists() && !result.isDirectory())) {
            throw invalidArg(cl, opt, "is not a directory. Current dir: '" + Files.currentDir() + "'");
        }
        if (!result.exists()) {
            final boolean done = result.mkdirs();
            if (!done) {
                throw new ParseException("Failed to create directory: " + result);
            }
        }
        return result;
    }

    /**
     * Returns an option value as a Directory, and tries to create it if it does not exist.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @return The option value as a Directory, possibly creating it.
     * @throws ParseException If result could not be produced.
     */
    public static File getOrCreateValueAsDirectory(CommandLine cl,
                                                   String opt) throws ParseException {
        return getOrCreateValueAsDirectory(cl, opt, null);
    }

    /**
     * Returns an option value as an URL.
     * <p>
     * If option is absent, returns a default value (possibly null).<br>
     * If option is present and can be converted to an URL, returns this conversion.<br>
     * Otherwise, raises an exception.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value or default value.
     * @throws ParseException If option is present and can not be converted to a valid URL.
     */
    public static URL getValueAsURL(CommandLine cl,
                                    String opt,
                                    URL def) throws ParseException {
        if (cl.hasOption(opt)) {
            final String s = cl.getOptionValue(opt);
            try {
                return new URL(s);
            } catch (final MalformedURLException e) {
                throw new ParseException("Failed to convert '" + s + "' to URL (" + e.getMessage() + ")");
            }
        } else {
            return def;
        }
    }

    public static URL getValueAsURL(CommandLine cl,
                                    String opt) throws ParseException {
        return getValueAsURL(cl, opt, null);
    }

    /**
     * Returns an option value as a Charset.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a Charset.
     * @throws ParseException If the option value can not be parsed as a Charset.
     */
    public static Charset getValueAsCharset(CommandLine cl,
                                            String opt,
                                            Charset def) throws ParseException {
        if (cl.hasOption(opt)) {
            final String s = cl.getOptionValue(opt);
            try {
                return Charset.forName(s);
            } catch (final IllegalCharsetNameException | UnsupportedCharsetException e) {
                throw new ParseException("Failed to convert '" + s + "' to Charset (" + e.getMessage() + ")");
            }
        } else {
            return def;
        }
    }

    public static Charset getValueAsCharset(CommandLine cl,
                                            String opt) throws ParseException {
        return getValueAsCharset(cl, opt, Charset.defaultCharset());
    }

    /**
     * Returns an option value as a string.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a string.
     */
    public static String getValueAsString(CommandLine cl,
                                          String opt,
                                          String def) {
        return cl.getOptionValue(opt, def);
    }

    private static char parseChar(String s) {
        if (s != null && s.length() == 1) {
            return s.charAt(0);
        } else {
            throw new IllegalArgumentException("Invalid char '" + s + "'");
        }
    }

    /**
     * Returns an option value as a char.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a char.
     * @throws ParseException If the option value can not be parsed as a char.
     */
    public static char getValueAsChar(CommandLine cl,
                                      String opt,
                                      char def) throws ParseException {
        return getValue(cl, opt, def, AbstractMainSupport::parseChar);
    }

    public static Character getValueAsChar(CommandLine cl,
                                           String opt,
                                           Character def) throws ParseException {
        return getValue(cl, opt, def, AbstractMainSupport::parseChar);
    }

    /**
     * Returns an option value as a long.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a long.
     * @throws ParseException If the option value can not be parsed as a long.
     */
    public static long getValueAsLong(CommandLine cl,
                                      String opt,
                                      long def) throws ParseException {
        return getValue(cl, opt, def, Long::parseLong);
    }

    public static Long getValueAsLong(CommandLine cl,
                                      String opt,
                                      Long def) throws ParseException {
        return getValue(cl, opt, def, Long::parseLong);
    }

    /**
     * Returns an option value as an int.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as an int.
     * @throws ParseException If the option value can not be parsed as an int.
     */
    public static int getValueAsInt(CommandLine cl,
                                    String opt,
                                    int def) throws ParseException {
        return getValue(cl, opt, def, Integer::parseInt);
    }

    public static Integer getValueAsInt(CommandLine cl,
                                        String opt,
                                        Integer def) throws ParseException {
        return getValue(cl, opt, def, Integer::parseInt);
    }

    /**
     * Returns an option value as a short.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a short.
     * @throws ParseException If the option value can not be parsed as a short.
     */
    public static short getValueAsShort(CommandLine cl,
                                        String opt,
                                        short def) throws ParseException {
        return getValue(cl, opt, def, Short::parseShort);
    }

    public static Short getValueAsShort(CommandLine cl,
                                        String opt,
                                        Short def) throws ParseException {
        return getValue(cl, opt, def, Short::parseShort);
    }

    /**
     * Returns an option value as a byte.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a byte.
     * @throws ParseException If the option value can not be parsed as a byte.
     */
    public static byte getValueAsByte(CommandLine cl,
                                      String opt,
                                      byte def) throws ParseException {
        return getValue(cl, opt, def, Byte::parseByte);
    }

    public static Byte getValueAsByte(CommandLine cl,
                                      String opt,
                                      Byte def) throws ParseException {
        return getValue(cl, opt, def, Byte::parseByte);
    }

    /**
     * Returns an option value as a double.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a double.
     * @throws ParseException If the option value can not be parsed as a double.
     */
    public static double getValueAsDouble(CommandLine cl,
                                          String opt,
                                          double def) throws ParseException {
        return getValue(cl, opt, def, Double::parseDouble);
    }

    public static Double getValueAsDouble(CommandLine cl,
                                          String opt,
                                          Double def) throws ParseException {
        return getValue(cl, opt, def, Double::parseDouble);
    }

    /**
     * Returns an option value as a float.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param cl The command line.
     * @param opt The option name.
     * @param def Default value.
     * @return The option value as a float.
     * @throws ParseException If the option value can not be parsed as a float.
     */
    public static float getValueAsFloat(CommandLine cl,
                                        String opt,
                                        float def) throws ParseException {
        return getValue(cl, opt, def, Float::parseFloat);
    }

    public static Float getValueAsFloat(CommandLine cl,
                                        String opt,
                                        Float def) throws ParseException {
        return getValue(cl, opt, def, Float::parseFloat);
    }

    /**
     * Returns an option value as a enum.
     * <p>
     * If option is absent, returns a default value.
     *
     * @param <E> The enum type.
     * @param cl The command line.
     * @param opt The option name.
     * @param enumClass Enum class
     * @param def Default value.
     * @return The option value as an enum.
     * @throws ParseException If the option value can not be parsed as an enum value.
     */
    public static <E extends Enum<E>> E getValueAsEnum(CommandLine cl,
                                                       String opt,
                                                       Class<E> enumClass,
                                                       E def) throws ParseException {
        return getValue(cl, opt, def, s -> {
            for (final E e : enumClass.getEnumConstants()) {
                if (e.name().equals(s)) {
                    return e;
                }
            }
            throw new IllegalArgumentException("Invalid enum value");
        });
    }

    public static int getNumberOfParts(String s,
                                       String sep) {
        int count = 0;
        int from = 0;
        while (from >= 0) {
            final int pos = s.indexOf(sep, from);
            if (pos >= 0) {
                count++;
                from = pos + sep.length();
            } else {
                from = -1;
            }
        }
        return count + 1;
    }

    public static String getPart(String s,
                                 String sep,
                                 int index,
                                 String def) {
        int count = 0;
        int from = 0;
        while (from >= 0) {
            final int pos = s.indexOf(sep, from);
            if (pos >= 0) {
                if (count == index) {
                    return s.substring(from, pos);
                }
                count++;
                from = pos + sep.length();
            } else {
                if (count == index) {
                    return s.substring(from);
                }
                from = -1;
            }
        }
        return def;
    }

    public static String getPart(String s,
                                 String sep,
                                 int index) {
        return getPart(s, sep, index, null);
    }
}