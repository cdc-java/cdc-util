package cdc.util.cli;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

class AbstractMainSupportTest {
    @Test
    void testGetNumberOfParts() {
        assertSame(1, AbstractMainSupport.getNumberOfParts("Hello", "::"));
        assertSame(2, AbstractMainSupport.getNumberOfParts("Hello::World", "::"));
        assertSame(3, AbstractMainSupport.getNumberOfParts("Hello::World::", "::"));
        assertSame(2, AbstractMainSupport.getNumberOfParts("::", "::"));
        assertSame(1, AbstractMainSupport.getNumberOfParts("", "::"));
        assertSame(1, AbstractMainSupport.getNumberOfParts("3", ":::::"));
    }

    @Test
    void testGetPart() {
        assertEquals("Hello", AbstractMainSupport.getPart("Hello", "::", 0, null));
        assertEquals("World", AbstractMainSupport.getPart("Hello", "::", 1, "World"));
        assertEquals("Hello", AbstractMainSupport.getPart("Hello::World", "::", 0, null));
        assertEquals("World", AbstractMainSupport.getPart("Hello::World", "::", 1, null));
        assertEquals("Foo", AbstractMainSupport.getPart("Hello::World", "::", 2, "Foo"));

        assertEquals("", AbstractMainSupport.getPart("::Hello::World", "::", 0, null));
        assertEquals("Hello", AbstractMainSupport.getPart("::Hello::World", "::", 1, null));
        assertEquals("World", AbstractMainSupport.getPart("::Hello::World", "::", 2, null));
        assertEquals(null, AbstractMainSupport.getPart("::Hello::World", "::", 3, null));
    }

    @Test
    void testRemoveComments() {
        assertEquals("", AbstractMainSupport.removeComments(""));
        assertEquals("", AbstractMainSupport.removeComments("#"));
        assertEquals("", AbstractMainSupport.removeComments(" #"));
        assertEquals("", AbstractMainSupport.removeComments("# a"));
        assertEquals("", AbstractMainSupport.removeComments("   # a"));
        assertEquals("#", AbstractMainSupport.removeComments("\\#"));
        assertEquals("#a", AbstractMainSupport.removeComments("\\#a#b"));
        assertEquals("\\", AbstractMainSupport.removeComments("\\"));
        assertEquals("", AbstractMainSupport.removeComments("#\\"));
        assertEquals("  ", AbstractMainSupport.removeComments("  "));
        assertEquals("  #", AbstractMainSupport.removeComments("  \\#"));
    }
}