package src.util.demos;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.Test;

import cdc.util.demos.FileEncoderDemo;
import cdc.util.demos.TransformerFactoryDemo;

class DemosTest {

    @Test
    void testFileEncoderDemo() throws IOException, ParseException {
        FileEncoderDemo.main();
        assertTrue(true);
    }

    @Test
    void testTransformerFactoryyDemo() throws IOException {
        TransformerFactoryDemo.main();
        assertTrue(true);
    }
}