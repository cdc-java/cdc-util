package cdc.util.demos;

import java.io.IOException;

import javax.xml.transform.TransformerFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TransformerFactoryDemo {
    private static final Logger LOGGER = LogManager.getLogger(TransformerFactoryDemo.class);

    public static void main(String... args) throws IOException {
        System.setProperty("javax.xml.transform.TransformerFactory",
                           "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");

        // final WorkbookWriterFactory wbFactory = new WorkbookWriterFactory();
        // wbFactory.create("target/test.xlsx");
        final TransformerFactory factory = TransformerFactory.newInstance();
        // final TransformerFactory factory = TransformerFactory.newDefaultInstance();
        LOGGER.info("factory: " + factory.getClass().getCanonicalName());
        // com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl when POI is not on the class path
        // org.apache.xalan.processor.TransformerFactoryImpl when POI is on the class path
    }
}