package cdc.util.demos;

import cdc.util.tools.FileEncoder;

public final class FileEncoderDemo {
    private FileEncoderDemo() {
    }

    public static void main(String... args) {
        FileEncoder.exec("--args-file", "src/main/resources/file-encoder-args1.txt");
        FileEncoder.exec("--args-file", "src/main/resources/file-encoder-args2.txt");
    }
}