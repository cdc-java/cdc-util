package cdc.util.demos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.files.SearchPath;

public final class SearchPathDemo {
    private static final Logger LOGGER = LogManager.getLogger(SearchPathDemo.class);

    private SearchPathDemo() {
    }

    public static void main(String[] args) {
        LOGGER.info(SearchPath.SYSTEM_PATH);
        LOGGER.info("cp: {}", SearchPath.SYSTEM_PATH.resolveExe("cp"));
        LOGGER.info("rm: {}", SearchPath.SYSTEM_PATH.resolveExe("rm"));
        LOGGER.info("mv: {}", SearchPath.SYSTEM_PATH.resolveExe("mv"));
        LOGGER.info("dot: {}", SearchPath.SYSTEM_PATH.resolveExe("dot"));
    }
}