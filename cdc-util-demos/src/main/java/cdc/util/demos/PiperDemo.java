package cdc.util.demos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import cdc.util.lang.Piper;

public final class PiperDemo {
    private PiperDemo() {
    }

    @FunctionalInterface
    private static interface Worker {
        public void invoke() throws IOException, InterruptedException;
    }

    private static void wrap(String name,
                             Worker w) throws IOException, InterruptedException {
        System.out.println("BEGIN " + name + " =============================================");
        w.invoke();
        System.out.println("END " + name + " =============================================");
    }

    private static void test1() throws IOException, InterruptedException {
        wrap("TEST1", () -> {
            final Process process =
                    new ProcessBuilder("ls", "-ails").redirectOutput(Redirect.INHERIT)
                                                     .start();
            process.waitFor();
        });
    }

    private static void test1B() throws IOException, InterruptedException {
        wrap("TEST1B", () -> {
            final Process process =
                    new ProcessBuilder("ls", "-ailsR", "/").redirectOutput(Redirect.INHERIT)
                                                           .start();
            process.waitFor(1, TimeUnit.MILLISECONDS);
        });
    }

    private static void test2() throws IOException, InterruptedException {
        wrap("TEST2", () -> {
            final Runtime rt = Runtime.getRuntime();
            final Process p1 = rt.exec("ls -ails");
            final Process p2 = rt.exec("grep pom");
            // Start piping
            final InputStream in = Piper.pipeProcessesArray(p1, p2);
            // Show output of last process
            final BufferedReader r = new BufferedReader(new InputStreamReader(in));
            String s = null;
            while ((s = r.readLine()) != null) {
                System.out.println(s);
            }
        });
    }

    private static void test3() throws IOException, InterruptedException {
        wrap("TEST3", () -> {
            final Process p1 =
                    new ProcessBuilder("ls", "-ails").start();
            final Process p2 =
                    new ProcessBuilder("grep", "pom").redirectOutput(Redirect.INHERIT)
                                                     .start();
            Piper.pipeProcessesArray(p1, p2);
        });
    }

    private static void test4() throws IOException, InterruptedException {
        wrap("TEST4", () -> {
            Piper.pipeSplitArray(Arrays.asList("ls", "-ails"),
                                 Arrays.asList("grep", "pom"));
        });
    }

    private static void test4B() throws IOException, InterruptedException {
        wrap("TEST4B", () -> {
            Piper.pipeSplitArray(1,
                                 Arrays.asList("ls", "-ailsR"),
                                 Arrays.asList("grep", "pom"));
        });
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        test1();
        test1B();
        test2();
        test3();
        test4();
        test4B();
    }
}