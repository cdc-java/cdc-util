package cdc.util.demos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.events.Event;
import cdc.util.events.ProgressEvent;

public final class EventDemo {
    private static final Logger LOGGER = LogManager.getLogger(EventDemo.class);

    private EventDemo() {
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            final Event event = new Event();
            LOGGER.info("event: {}", event);
        }
        for (int i = 0; i < 10; i++) {
            final ProgressEvent event = new ProgressEvent(i, 10);
            LOGGER.info("event: {}", event);
        }
    }
}