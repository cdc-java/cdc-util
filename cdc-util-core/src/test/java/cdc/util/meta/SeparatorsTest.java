package cdc.util.meta;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SeparatorsTest {
    @Test
    void testMatcher() {
        // Should test power of 2
        assertEquals(64, Separators.STABLE.length());

        for (char c = 0; c < 65535; c++) {
            final boolean bbs = Separators.BIT_SET_MATCHER.test(c);
            final boolean bba = Separators.BOOLEAN_ARRAY_MATCHER.test(c);
            final boolean bms = Separators.MULTIPLY_SHIFT_MATCHER.test(c);
            final boolean bms2 = Separators.MULTIPLY_SHIFT_INLINE_MATCHER.test(c);
            assertEquals(bbs, bba, "'" + c + "' bit set: " + bbs + " boolean array: " + bba);
            assertEquals(bbs, bms, "'" + c + "' bit set: " + bbs + " multiply shift: " + bba);
            assertEquals(bbs, bms2, "'" + c + "' bit set: " + bbs + " multiply shift inline: " + bba);
        }
    }
}