package cdc.util.meta;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TerminatorsTest {
    @Test
    void testMatcher() {
        // Should test power of 2
        assertEquals(16, Terminators.STABLE.length());

        for (char c = 0; c < 65535; c++) {
            final boolean bbs = Terminators.BIT_SET_MATCHER.test(c);
            final boolean bba = Terminators.BOOLEAN_ARRAY_MATCHER.test(c);
            final boolean bms = Terminators.MULTIPLY_SHIFT_MATCHER.test(c);
            final boolean bms2 = Terminators.MULTIPLY_SHIFT_INLINE_MATCHER.test(c);
            assertEquals(bbs, bba, "'" + c + "' bit set: " + bbs + " boolean array: " + bba + " for " + c);
            assertEquals(bbs, bms, "'" + c + "' bit set: " + bbs + " multiply shift: " + bba + " for " + c);
            assertEquals(bbs, bms2, "'" + c + "' bit set: " + bbs + " multiply shift inline: " + bba + " for " + c);
        }
    }
}