package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.files.Files;

class FilesTest {
    @Test
    void testNormalize() {
        assertEquals("a/b", Files.normalize("a/b"));
        assertEquals("a/b", Files.normalize("a\\b"));
        assertEquals("/a/b", Files.normalize("/a/b"));
        assertEquals("//a/b", Files.normalize("//a/b"));
    }
}