package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.meta.BasicMetaData;
import cdc.util.meta.MetaData;
import cdc.util.meta.MetaDataEncoder;

class MetaDataEncoderTest {

    private static void checkEncode(String expected,
                                    MetaData meta) {
        final String result = MetaDataEncoder.encode(meta);
        assertEquals(expected, result);
    }

    private static void checkDecode(String expected,
                                    String code) {
        final MetaData meta = MetaDataEncoder.decode(code);
        final String encoded = MetaDataEncoder.encode(meta);
        assertEquals(expected, encoded);
    }

    @Test
    void testEncode() {
        final BasicMetaData meta = new BasicMetaData();
        checkEncode("", meta);

        meta.put("key1", "value1");
        checkEncode("key1:value1", meta);
        meta.put("key2", "value2");
        checkEncode("key1:value1\nkey2:value2", meta);
        meta.put("key0", "value0");
        checkEncode("key0:value0\nkey1:value1\nkey2:value2", meta);

        meta.clear();
        meta.put("k e y", "value");
        checkEncode("\"k e y\":value", meta);

        meta.clear();
        meta.put("ke\ny", "val\nue");
        checkEncode("\"ke\ny\":\"val\nue\"", meta);

        meta.clear();
        meta.put("k:e:y", "value");
        checkEncode("\"k:e:y\":value", meta);

        meta.clear();
        meta.put("key", "va:lue");
        checkEncode("key:\"va:lue\"", meta);

        meta.clear();
        meta.put("ke\"y", "va\"lue");
        checkEncode("\"ke\"\"y\":\"va\"\"lue\"", meta);

        meta.clear();
        meta.put("key", null);
        checkEncode("key:", meta);

        meta.clear();
        meta.put("k ", null);
        checkEncode("\"k \":", meta);
    }

    @Test
    void testEmptyDecode() {
        checkDecode("", "");
        checkDecode("", " ");
    }

    @Test
    void testSimpleDecode() {
        checkDecode("k:v", "k:v");
        checkDecode("k:v", "k: v");
        checkDecode("k:v", " k:v");
        checkDecode("k:v", " k : v ");
        checkDecode("k:", "k:");
    }

    @Test
    void testComplexDecode() {
        checkDecode("k1:v1\nk2:v2", "k2:v2\nk1:v1");
        checkDecode("k1:\nk2:", "k2:\nk1:");
        checkDecode("k1:\nk2:", "k2:\nk1:   ");
        checkDecode("k1:\nk2:", "\n\nk2:\nk1:   ");
        checkDecode("k1:\nk2:", "\n\n  k2  :  \n\n\n   k1  :   \n\n");
    }

    @Test
    void testEscapedDecode() {
        checkDecode("k:v", "\"k\":v");
        checkDecode("\"k \":v", "\"k \":v");
    }
}