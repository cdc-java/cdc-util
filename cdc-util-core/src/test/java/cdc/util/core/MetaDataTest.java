package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.util.meta.BasicMetaData;
import cdc.util.meta.MetaData;

class MetaDataTest {
    @Test
    void testWith() {
        final BasicMetaData md1 = new BasicMetaData();
        final BasicMetaData md2 = new BasicMetaData();
        final MetaData md12 = md1.with(md2);
        final MetaData md21 = md2.with(md1);

        md1.put("key", "value1");
        md2.put("key", "value2");
        md1.put("key1", "value1");
        md2.put("key2", "value2");

        assertEquals("value1", md12.get("key"));
        assertEquals("value2", md21.get("key"));
        assertEquals("value1", md12.get("key1"));
        assertEquals("value1", md21.get("key1"));
        assertEquals("value2", md12.get("key2"));
        assertEquals("value2", md21.get("key2"));
        assertEquals(null, md21.get("key3"));
        assertEquals(null, md12.get("key3"));
        assertEquals("VALUE3", md21.get("key3", "VALUE3"));
        assertEquals("VALUE3", md12.get("key3", "VALUE3"));
        assertEquals("value2", md21.get("key2", "VALUE3"));
        assertEquals("value2", md12.get("key2", "VALUE3"));
        assertEquals("value2", md21.get("key", "VALUE3"));
        assertEquals("value1", md12.get("key", "VALUE3"));

        assertEquals(Set.of("key", "key1", "key2"), md12.getKeysSet());
        assertEquals(Set.of("key", "key1", "key2"), md21.getKeysSet());

    }
}