package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.strings.CaseConverter;

class CaseConverterTest {

    private static void test(CaseConverter.Style style,
                             String wordPrefix,
                             String wordSeparator,
                             String wordSuffix,
                             String expected,
                             String... words) {
        final CaseConverter converter = CaseConverter.builder()
                                                     .style(style)
                                                     .wordPrefix(wordPrefix)
                                                     .wordSeparator(wordSeparator)
                                                     .wordSuffix(wordSuffix)
                                                     .build();
        assertEquals(expected, converter.convert(words));
    }

    @Test
    void testConvert() {
        final CaseConverter converter = CaseConverter.builder().build();
        assertEquals("", converter.convert());

        test(CaseConverter.Style.ORIGINAL, "", "", "", "AAAbbb", "AAA", "bbb");
        test(CaseConverter.Style.UPPER_CASE, "", "", "", "AAABBB", "AAA", "bbb");
        test(CaseConverter.Style.LOWER_CASE, "", "", "", "aaabbb", "AAA", "bbb");
        test(CaseConverter.Style.CAMEL_CASE, "", "", "", "aaaBbb", "AAA", "bbb");
        test(CaseConverter.Style.CAPITAL, "", "", "", "AaaBbb", "AAA", "bbb");

        test(CaseConverter.Style.ORIGINAL, "", "--", "", "AAA--bbb", "AAA", "bbb");
        test(CaseConverter.Style.UPPER_CASE, "", "--", "", "AAA--BBB", "AAA", "bbb");
        test(CaseConverter.Style.LOWER_CASE, "", "--", "", "aaa--bbb", "AAA", "bbb");
        test(CaseConverter.Style.CAMEL_CASE, "", "--", "", "aaa--Bbb", "AAA", "bbb");
        test(CaseConverter.Style.CAPITAL, "", "--", "", "Aaa--Bbb", "AAA", "bbb");

        test(CaseConverter.Style.ORIGINAL, "[", "--", "", "[AAA--[bbb", "AAA", "bbb");
        test(CaseConverter.Style.UPPER_CASE, "[", "--", "", "[AAA--[BBB", "AAA", "bbb");
        test(CaseConverter.Style.LOWER_CASE, "[", "--", "", "[aaa--[bbb", "AAA", "bbb");
        test(CaseConverter.Style.CAMEL_CASE, "[", "--", "", "[aaa--[Bbb", "AAA", "bbb");
        test(CaseConverter.Style.CAPITAL, "[", "--", "", "[Aaa--[Bbb", "AAA", "bbb");

        test(CaseConverter.Style.ORIGINAL, "[", "--", "]", "[AAA]--[bbb]", "AAA", "bbb");
        test(CaseConverter.Style.UPPER_CASE, "[", "--", "]", "[AAA]--[BBB]", "AAA", "bbb");
        test(CaseConverter.Style.LOWER_CASE, "[", "--", "]", "[aaa]--[bbb]", "AAA", "bbb");
        test(CaseConverter.Style.CAMEL_CASE, "[", "--", "]", "[aaa]--[Bbb]", "AAA", "bbb");
        test(CaseConverter.Style.CAPITAL, "[", "--", "]", "[Aaa]--[Bbb]", "AAA", "bbb");
    }
}