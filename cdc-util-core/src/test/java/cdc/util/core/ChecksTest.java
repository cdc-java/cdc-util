package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.Checks;

class ChecksTest {

    @Test
    void testToString() {
        assertEquals("XXX", Checks.toString("XXX"));
        assertEquals("XXX", Checks.toString("XXX", true));
        assertEquals("XXX true", Checks.toString("XXX {}", true));
        assertEquals("XXX true 10 YYY", Checks.toString("XXX {} {} YYY", true, 10));
        assertEquals("XXX true 10 YYY", Checks.toString("XXX {} {} YYY", true, 10, 11));
        assertEquals("XXX true ? YYY", Checks.toString("XXX {} {} YYY", true));
        assertEquals("XXX ? ? YYY", Checks.toString("XXX {} {} YYY"));
    }
}