package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.ShortMasks;

class ShortMasksTest {

    @Test
    void testPaddedBinString() {
        assertEquals("0000000000000000", ShortMasks.toPaddedBinString((short) 0x0));
        assertEquals("0000000000000001", ShortMasks.toPaddedBinString((short) 0x1));
        assertEquals("0000000000000010", ShortMasks.toPaddedBinString((short) 0x2));
        assertEquals("0000000000000100", ShortMasks.toPaddedBinString((short) 0x4));
        assertEquals("0000000000001000", ShortMasks.toPaddedBinString((short) 0x8));
        assertEquals("0000000000001111", ShortMasks.toPaddedBinString((short) 0xF));
        assertEquals("0000000000010000", ShortMasks.toPaddedBinString((short) 0x10));
        assertEquals("0000000000100000", ShortMasks.toPaddedBinString((short) 0x20));
        assertEquals("0000000001000000", ShortMasks.toPaddedBinString((short) 0x40));
        assertEquals("0000000010000000", ShortMasks.toPaddedBinString((short) 0x80));
        assertEquals("0000000011110000", ShortMasks.toPaddedBinString((short) 0xF0));
        assertEquals("0000000100000000", ShortMasks.toPaddedBinString((short) 0x100));
        assertEquals("0000001000000000", ShortMasks.toPaddedBinString((short) 0x200));
        assertEquals("0000010000000000", ShortMasks.toPaddedBinString((short) 0x400));
        assertEquals("0000100000000000", ShortMasks.toPaddedBinString((short) 0x800));
        assertEquals("0000111100000000", ShortMasks.toPaddedBinString((short) 0xF00));
    }

    @Test
    void testOneBitOps() {
        assertEquals((short) 0x1, ShortMasks.toMask(0));
        assertEquals((short) 0x2, ShortMasks.toMask(1));
        assertEquals((short) 0x4, ShortMasks.toMask(2));
        assertEquals((short) 0x8, ShortMasks.toMask(3));
        assertEquals((short) 0x10, ShortMasks.toMask(4));
        assertEquals((short) 0x20, ShortMasks.toMask(5));
        assertEquals((short) 0x40, ShortMasks.toMask(6));
        assertEquals((short) 0x80, ShortMasks.toMask(7));
        assertEquals((short) 0x100, ShortMasks.toMask(8));
        assertEquals((short) 0x200, ShortMasks.toMask(9));
        assertEquals((short) 0x400, ShortMasks.toMask(10));
        assertEquals((short) 0x800, ShortMasks.toMask(11));
        assertEquals((short) 0x1000, ShortMasks.toMask(12));
        assertEquals((short) 0x2000, ShortMasks.toMask(13));
        assertEquals((short) 0x4000, ShortMasks.toMask(14));
        assertEquals((short) 0x8000, ShortMasks.toMask(15));

        assertEquals((short) 0x1, ShortMasks.setEnabled((short) 0, 0, true));
        assertEquals((short) 0x2, ShortMasks.setEnabled((short) 0, 1, true));
        assertEquals((short) 0x4, ShortMasks.setEnabled((short) 0, 2, true));
        assertEquals((short) 0x8, ShortMasks.setEnabled((short) 0, 3, true));
        assertEquals((short) 0x10, ShortMasks.setEnabled((short) 0, 4, true));
        assertEquals((short) 0x20, ShortMasks.setEnabled((short) 0, 5, true));
        assertEquals((short) 0x40, ShortMasks.setEnabled((short) 0, 6, true));
        assertEquals((short) 0x80, ShortMasks.setEnabled((short) 0, 7, true));

        assertEquals(false, ShortMasks.isEnabled((short) 0, 0));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 1));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 2));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 3));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 4));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 5));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 6));
        assertEquals(false, ShortMasks.isEnabled((short) 0, 7));

        assertEquals(true, ShortMasks.isEnabled((short) 0xF, 0));
        assertEquals(true, ShortMasks.isEnabled((short) 0xF, 1));
        assertEquals(true, ShortMasks.isEnabled((short) 0xF, 2));
        assertEquals(true, ShortMasks.isEnabled((short) 0xF, 3));
        assertEquals(false, ShortMasks.isEnabled((short) 0xF, 4));
        assertEquals(false, ShortMasks.isEnabled((short) 0xF, 5));
        assertEquals(false, ShortMasks.isEnabled((short) 0xF, 6));
        assertEquals(false, ShortMasks.isEnabled((short) 0xF, 7));

        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 0));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 1));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 2));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 3));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 4));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 5));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 6));
        assertEquals(true, ShortMasks.isEnabled((short) 0xFF, 7));

        for (int index = 0; index < ShortMasks.MAX_BITS; index++) {
            final short m = ShortMasks.toMask(index);
            for (int i = 0; i < ShortMasks.MAX_BITS; i++) {
                assertEquals(ShortMasks.isEnabled(m, i), i == index);
            }
        }
    }

    @Test
    void testNBitOps() {
        assertEquals((short) 0x1, ShortMasks.toMask(0, 1));
        assertEquals((short) 0x3, ShortMasks.toMask(0, 2));
        assertEquals((short) 0x7, ShortMasks.toMask(0, 3));
        assertEquals((short) 0xF, ShortMasks.toMask(0, 4));
        assertEquals((short) 0x1F, ShortMasks.toMask(0, 5));
        assertEquals((short) 0x3F, ShortMasks.toMask(0, 6));
        assertEquals((short) 0x7F, ShortMasks.toMask(0, 7));
        assertEquals((short) 0xFF, ShortMasks.toMask(0, 8));
        assertEquals((short) 0x2, ShortMasks.toMask(1, 1));
        assertEquals((short) 0x6, ShortMasks.toMask(1, 2));
        assertEquals((short) 0xE, ShortMasks.toMask(1, 3));
        assertEquals((short) 0x1E, ShortMasks.toMask(1, 4));
        assertEquals((short) 0x3E, ShortMasks.toMask(1, 5));
        assertEquals((short) 0x7E, ShortMasks.toMask(1, 6));
        assertEquals((short) 0xFE, ShortMasks.toMask(1, 7));
        assertEquals((short) 0x4, ShortMasks.toMask(2, 1));
        assertEquals((short) 0xC, ShortMasks.toMask(2, 2));
        assertEquals((short) 0x1C, ShortMasks.toMask(2, 3));
        assertEquals((short) 0x3C, ShortMasks.toMask(2, 4));
        assertEquals((short) 0x7C, ShortMasks.toMask(2, 5));
        assertEquals((short) 0xFC, ShortMasks.toMask(2, 6));
        // ...
        assertEquals(ShortMasks.toMask(15, 1), (short) 0x8000);

        final int[] ms = { 0, 1, 0xF, 0xFF, 0xFFF, 0xFFFF };

        for (final int m : ms) {
            final short mask = (short) m;
            for (int index = 0; index < ShortMasks.MAX_BITS; index++) {
                for (int length = 1; length <= ShortMasks.MAX_BITS - index; length++) {
                    final int max = 1 << (length);
                    for (int value = 0; value < max; value++) {
                        final short smask = ShortMasks.set(mask, index, length, value);
                        final int rvalue = ShortMasks.get(smask, index, length);
                        assertEquals(value, rvalue);
                    }
                }
            }
        }
    }

    @Test
    void testOneByteOps() {
        short mask = Short.MIN_VALUE;
        do {
            byte value = Byte.MIN_VALUE;
            do {
                {
                    final byte rvalue = ShortMasks.getByte0(ShortMasks.setByte0(mask, value));
                    assertEquals(value, rvalue);
                }
                {
                    final byte rvalue = ShortMasks.getByte1(ShortMasks.setByte1(mask, value));
                    assertEquals(value, rvalue);
                }
            } while (value++ != Byte.MAX_VALUE);
        } while (mask++ != Short.MAX_VALUE);
    }
}