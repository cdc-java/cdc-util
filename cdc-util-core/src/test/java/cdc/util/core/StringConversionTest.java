package cdc.util.core;

import static cdc.util.strings.StringConversion.asBoolean;
import static cdc.util.strings.StringConversion.asByte;
import static cdc.util.strings.StringConversion.asDouble;
import static cdc.util.strings.StringConversion.asEnum;
import static cdc.util.strings.StringConversion.asFloat;
import static cdc.util.strings.StringConversion.asInt;
import static cdc.util.strings.StringConversion.asLong;
import static cdc.util.strings.StringConversion.asOptionalBoolean;
import static cdc.util.strings.StringConversion.asOptionalByte;
import static cdc.util.strings.StringConversion.asOptionalDouble;
import static cdc.util.strings.StringConversion.asOptionalEnum;
import static cdc.util.strings.StringConversion.asOptionalFloat;
import static cdc.util.strings.StringConversion.asOptionalInt;
import static cdc.util.strings.StringConversion.asOptionalLong;
import static cdc.util.strings.StringConversion.asOptionalRawEnum;
import static cdc.util.strings.StringConversion.asOptionalShort;
import static cdc.util.strings.StringConversion.asRawEnum;
import static cdc.util.strings.StringConversion.asShort;
import static cdc.util.strings.StringConversion.asString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import cdc.util.lang.FailureReaction;
import cdc.util.strings.FormatException;

class StringConversionTest {
    private static final String X = "XXX";

    @Nested
    class Doubles {
        private final double val = 10.0;
        private final Double obj = Double.valueOf(val);
        private final String str = "10.0";
        private final double eps = 1.0e-10;

        @Test
        void testToType() {
            assertEquals(val, asDouble(null, val), eps);
            assertEquals(val, asDouble("", val), eps);

            assertEquals(val, asDouble(str), eps);
            assertEquals(val, asDouble(str), eps);

            assertEquals(val, asDouble(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT), eps);
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asDouble(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalDouble(null, obj));
            assertEquals(obj, asOptionalDouble("", obj));

            assertEquals(obj, asOptionalDouble(str));
            assertEquals(null, asOptionalDouble(null));
            assertEquals(null, asOptionalDouble(""));

            assertEquals(obj, asOptionalDouble(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalDouble(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Double) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Floats {
        private final float val = 10.0F;
        private final Float obj = Float.valueOf(val);
        private final String str = "10.0";
        private final float eps = 1.0e-10F;

        @Test
        void testToType() {
            assertEquals(val, asFloat(null, val), eps);
            assertEquals(val, asFloat("", val), eps);

            assertEquals(val, asFloat(str), eps);
            assertEquals(val, asFloat(str), eps);

            assertEquals(val, asFloat(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT), eps);
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asFloat(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalFloat(null, obj));
            assertEquals(obj, asOptionalFloat("", obj));

            assertEquals(obj, asOptionalFloat(str));
            assertEquals(null, asOptionalFloat(null));
            assertEquals(null, asOptionalFloat(""));

            assertEquals(obj, asOptionalFloat(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalFloat(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Float) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Longs {
        private final long val = 10L;
        private final Long obj = Long.valueOf(val);
        private final String str = "10";

        @Test
        void testToType() {
            assertEquals(val, asLong(null, val));
            assertEquals(val, asLong("", val));

            assertEquals(val, asLong(str));
            assertEquals(val, asLong(str));

            assertEquals(val, asLong(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asLong(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalLong(null, obj));
            assertEquals(obj, asOptionalLong("", obj));

            assertEquals(obj, asOptionalLong(str));
            assertEquals(null, asOptionalLong(null));
            assertEquals(null, asOptionalLong(""));

            assertEquals(obj, asOptionalLong(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalLong(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Long) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Ints {
        private final int val = 10;
        private final Integer obj = Integer.valueOf(val);
        private final String str = "10";

        @Test
        void testToType() {
            assertEquals(val, asInt(null, val));
            assertEquals(val, asInt("", val));

            assertEquals(val, asInt(str));
            assertEquals(val, asInt(str));

            assertEquals(val, asInt(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asInt(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalInt(null, obj));
            assertEquals(obj, asOptionalInt("", obj));

            assertEquals(obj, asOptionalInt(str));
            assertEquals(null, asOptionalInt(null));
            assertEquals(null, asOptionalInt(""));

            assertEquals(obj, asOptionalInt(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalInt(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Integer) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Shorts {
        private final short val = 10;
        private final Short obj = Short.valueOf(val);
        private final String str = "10";

        @Test
        void testToType() {
            assertEquals(val, asShort(null, val));
            assertEquals(val, asShort("", val));

            assertEquals(val, asShort(str));
            assertEquals(val, asShort(str));

            assertEquals(val, asShort(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asShort(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalShort(null, obj));
            assertEquals(obj, asOptionalShort("", obj));

            assertEquals(obj, asOptionalShort(str));
            assertEquals(null, asOptionalShort(null));
            assertEquals(null, asOptionalShort(""));

            assertEquals(obj, asOptionalShort(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalShort(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Short) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Bytes {
        private final byte val = 10;
        private final Byte obj = Byte.valueOf(val);
        private final String str = "10";

        @Test
        void testToType() {
            assertEquals(val, asByte(null, val));
            assertEquals(val, asByte("", val));

            assertEquals(val, asByte(str));
            assertEquals(val, asByte(str));

            assertEquals(val, asByte(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asByte(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalByte(null, obj));
            assertEquals(obj, asOptionalByte("", obj));

            assertEquals(obj, asOptionalByte(str));
            assertEquals(null, asOptionalByte(null));
            assertEquals(null, asOptionalByte(""));

            assertEquals(obj, asOptionalByte(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalByte(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Byte) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Booleans {
        private final boolean val = true;
        private final Boolean obj = Boolean.valueOf(val);
        private final String str = "true";

        @Test
        void testToType() {
            assertEquals(val, asBoolean(null, val));
            assertEquals(val, asBoolean("", val));

            assertEquals(val, asBoolean(str));
            assertEquals(val, asBoolean(str));

            assertEquals(val, asBoolean(X, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asBoolean(X);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalBoolean(null, obj));
            assertEquals(obj, asOptionalBoolean("", obj));

            assertEquals(obj, asOptionalBoolean(str));
            assertEquals(null, asOptionalBoolean(null));
            assertEquals(null, asOptionalBoolean(""));

            assertEquals(obj, asOptionalBoolean(X, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalBoolean(X);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString((Boolean) null));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class Enums {
        private final FailureReaction val = FailureReaction.DEFAULT;
        private final FailureReaction obj = FailureReaction.DEFAULT;
        private final String str = val.name();
        private final Class<FailureReaction> cls = FailureReaction.class;

        @Test
        void testToType() {
            assertEquals(val, asEnum(null, cls, val));
            assertEquals(val, asEnum("", cls, val));

            assertEquals(val, asEnum(str, cls));
            assertEquals(val, asEnum(str, cls));

            assertEquals(val, asEnum(X, cls, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asEnum(X, cls);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalEnum(null, cls, obj));
            assertEquals(obj, asOptionalEnum("", cls, obj));

            assertEquals(obj, asOptionalEnum(str, cls));
            assertEquals(null, asOptionalEnum(null, cls));
            assertEquals(null, asOptionalEnum("", cls));

            assertEquals(obj, asOptionalEnum(X, cls, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalEnum(X, cls);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString(cls.cast(null)));
            assertEquals(str, asString(obj));
        }
    }

    @Nested
    class RawEnums {
        private final Enum<?> val = FailureReaction.DEFAULT;
        private final Enum<?> obj = FailureReaction.DEFAULT;
        private final String str = val.name();
        private final Class<? extends Enum<?>> cls = FailureReaction.class;

        @Test
        void testToType() {
            assertEquals(val, asRawEnum(null, cls, val));
            assertEquals(val, asRawEnum("", cls, val));

            assertEquals(val, asRawEnum(str, cls));
            assertEquals(val, asRawEnum(str, cls));

            assertEquals(val, asRawEnum(X, cls, val, FailureReaction.DEFAULT, FailureReaction.DEFAULT));
        }

        void testToInvalidType() {
            assertThrows(FormatException.class, () -> {
                asRawEnum(X, cls);
            });
        }

        @Test
        void testToOptionalType() {
            assertEquals(obj, asOptionalRawEnum(null, cls, obj));
            assertEquals(obj, asOptionalRawEnum("", cls, obj));

            assertEquals(obj, asOptionalRawEnum(str, cls));
            assertEquals(null, asOptionalRawEnum(null, cls));
            assertEquals(null, asOptionalRawEnum("", cls));

            assertEquals(obj, asOptionalRawEnum(X, cls, obj, FailureReaction.DEFAULT));
        }

        void testToInvalidOptionalType() {
            assertThrows(FormatException.class, () -> {
                asOptionalRawEnum(X, cls);
            });
        }

        @Test
        void testFromType() {
            assertEquals(str, asString(val));
            assertEquals(null, asString(cls.cast(null)));
            assertEquals(str, asString(obj));
        }
    }
}