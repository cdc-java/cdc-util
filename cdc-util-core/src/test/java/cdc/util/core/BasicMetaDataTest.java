package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.meta.BasicMetaData;

class BasicMetaDataTest {
    @Test
    void test() {
        final BasicMetaData md1 = new BasicMetaData();
        assertEquals(0, md1.getKeys().size());
        assertEquals(null, md1.get("Key"));
        assertEquals("VALUE", md1.get("Key", "VALUE"));

        md1.put("Key", "value");
        assertEquals(1, md1.getKeys().size());
        assertTrue(md1.hasKey("Key"));
        assertEquals("value", md1.get("Key"));
        assertEquals("value", md1.get("Key", "VALUE"));

        md1.remove("Key");
        assertEquals(null, md1.get("Key"));
        assertEquals("VALUE", md1.get("Key", "VALUE"));

        final BasicMetaData md2 = new BasicMetaData();
        md2.put("Key2", "value");

        md1.putAll(md2);
        assertEquals(1, md1.getKeys().size());

        assertEquals(md1, md1);
        assertEquals(md1, md2);
    }
}