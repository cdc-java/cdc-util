package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import cdc.util.strings.StringComparison;

class StringComparisonTest {
    @Test
    void testIndex() {
        assertSame(-1, StringComparison.indexOfFirstEndingDecimalDigit(""));
        assertSame(-1, StringComparison.indexOfFirstEndingDecimalDigit("A"));
        assertSame(1, StringComparison.indexOfFirstEndingDecimalDigit("A0"));
        assertSame(0, StringComparison.indexOfFirstEndingDecimalDigit("0"));
        assertSame(0, StringComparison.indexOfFirstEndingDecimalDigit("00"));
        assertSame(2, StringComparison.indexOfFirstEndingDecimalDigit("AA00"));
        assertSame(-1, StringComparison.indexOfFirstEndingDecimalDigit("00A"));
        assertSame(-1, StringComparison.indexOfFirstEndingDecimalDigit("("));
    }

    @Test
    void testCompare() {
        assertSame(0, StringComparison.compareDecimalDigits("", ""));
        assertSame(0, StringComparison.compareDecimalDigits("A", "A"));
        assertSame(0, StringComparison.compareDecimalDigits("A1", "A1"));
        assertTrue(StringComparison.compareDecimalDigits("A2", "A1") > 0);
        assertTrue(StringComparison.compareDecimalDigits("A10", "A1") > 0);
        assertTrue(StringComparison.compareDecimalDigits("A10", "A2") > 0);
        assertTrue(StringComparison.compareDecimalDigits("A1", "A10") < 0);
        assertTrue(StringComparison.compareDecimalDigits("A1", "B1") < 0);
        assertTrue(StringComparison.compareDecimalDigits("B1", "A1") > 0);
        assertTrue(StringComparison.compareDecimalDigits("A1", "A") > 0);
        assertTrue(StringComparison.compareDecimalDigits("A", "A1") < 0);

        assertTrue(StringComparison.compareDecimalDigits("A999999999999999998", "A999999999999999999") < 0);
        assertTrue(StringComparison.compareDecimalDigits("A99999999999999999999999998", "A99999999999999999999999999") < 0);
    }

    @Test
    void testIssue63() throws IOException {
        assertTrue(StringComparison.compareDecimalDigits("2", "10") < 0);
        assertTrue(StringComparison.compareDecimalDigits("10", "10A") < 0);
        assertTrue(StringComparison.compareDecimalDigits("10A", "2") > 0);
        assertTrue(StringComparison.compareDecimalDigits("A2", "A10") < 0);
        assertTrue(StringComparison.compareDecimalDigits("AB2", "A10") > 0);
    }
}