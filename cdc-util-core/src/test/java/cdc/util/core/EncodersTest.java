package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.util.encoding.Encoders;
import cdc.util.encoding.ExtensionEncoder;

class EncodersTest {
    enum E1 {
        A,
        B,
        C,
        D
    }

    enum E2 {
        A,
        B,
        E,
        F
    }

    @Test
    void testOrdinalEncoder() {
        final ExtensionEncoder<E1, Integer> encoder = Encoders.ordinalEncoder(E1.class);
        assertSame(0, encoder.encode(E1.A));
        assertSame(1, encoder.encode(E1.B));
        assertSame(2, encoder.encode(E1.C));
        assertSame(3, encoder.encode(E1.D));

        assertSame(E1.A, encoder.decode(0));
        assertSame(E1.B, encoder.decode(1));
        assertSame(E1.C, encoder.decode(2));
        assertSame(E1.D, encoder.decode(3));
        assertSame(null, encoder.decode(-1));
    }

    @Test
    void testSameNameEncoder() {
        final ExtensionEncoder<E1, E2> encoder = Encoders.sameNameEncoder(E1.class, E2.class);
        assertSame(E2.A, encoder.encode(E1.A));
        assertSame(E2.B, encoder.encode(E1.B));
        assertSame(null, encoder.encode(E1.C));
        assertSame(null, encoder.encode(E1.D));

        assertSame(E1.A, encoder.decode(E2.A));
        assertSame(E1.B, encoder.decode(E2.B));
        assertSame(null, encoder.decode(E2.E));
        assertSame(null, encoder.decode(E2.F));
    }
}