package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.Masks;

class MasksTest {
    @Test
    void testMasks() {
        assertEquals(0, Masks.getNumBits(0));
        assertEquals(0, Masks.getNumBits(1));
        assertEquals(1, Masks.getNumBits(2));
        assertEquals(2, Masks.getNumBits(3));
        assertEquals(2, Masks.getNumBits(4));
        assertEquals(3, Masks.getNumBits(5));
        assertEquals(3, Masks.getNumBits(6));
        assertEquals(3, Masks.getNumBits(7));
        assertEquals(3, Masks.getNumBits(8));
    }
}