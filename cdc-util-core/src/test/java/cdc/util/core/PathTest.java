package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.paths.Path;

class PathTest {

    private static Path p(String s) {
        return new Path(s);
    }

    @Test
    void testDot() {
        assertFalse(Path.DOT.isAbsolute());
        assertEquals(new Path("./.."), Path.DOT.getParent());
        assertEquals(1, Path.DOT.getNameCount());
        assertEquals(".", Path.DOT.getName(0));
    }

    @Test
    void testDotDot() {
        assertFalse(Path.DOT_DOT.isAbsolute());
        assertEquals(new Path("../.."), Path.DOT_DOT.getParent());
        assertEquals(1, Path.DOT_DOT.getNameCount());
        assertEquals("..", Path.DOT_DOT.getName(0));
    }

    @Test
    void testRoot() {
        assertTrue(Path.ROOT.isAbsolute());
        assertEquals(Path.ROOT, Path.ROOT.getParent());
        assertEquals(0, Path.ROOT.getNameCount());
    }

    @Test
    void testConstruction1() {
        final Path p = new Path("a");
        assertFalse(p.isAbsolute());
        assertEquals(1, p.getNameCount());
        assertEquals("a", p.getName(0));
        assertEquals(new Path("a/.."), p.getParent());
    }

    @Test
    void testConstruction2() {
        final Path p = new Path("/a");
        assertTrue(p.isAbsolute());
        assertEquals(1, p.getNameCount());
        assertEquals("a", p.getName(0));
        assertEquals(new Path("/a/.."), p.getParent());
    }

    void testSeparators() {
        assertEquals(new Path("/a/b"), new Path("\\a\\b"));
        assertEquals(new Path("/a/b"), new Path("|a|b"));
    }

    private static void testNormalize(String expected,
                                      String path) {
        assertEquals(expected, new Path(path).normalize().toString());
    }

    @Test
    void testNormalize() {
        testNormalize(".", ".");
        testNormalize(".", "./");
        testNormalize(".", ".//");
        testNormalize(".", "././");
        testNormalize(".", ".//.");
        testNormalize(".", "././.");
        testNormalize("..", "./..");
        testNormalize("../..", "./../..");
        testNormalize("a", "./a");
        testNormalize("a", "./a/.");
        testNormalize(".", "./a/..");
        testNormalize(".", "./a/b/../..");
        testNormalize("a", ".///a///.");
        testNormalize("a", "././/a/.//.");

        testNormalize("..", "..");
        testNormalize("../a", "../a");

        testNormalize("/", "/");
        testNormalize("/", "/..");
        testNormalize("/", "/../..");
        testNormalize("/", "/../../");
        testNormalize("/", "/a/..");
        testNormalize("/a", "/a");
        testNormalize("/a", "/a/");
        testNormalize("/a", "/a//");
        testNormalize("/a", "/a///");
        testNormalize("/a/b", "/a/b");
        testNormalize("/a/b", "///a///b");
        testNormalize("/a", "///a///b///..");
        testNormalize("/", "///a///b///..///..");

        testNormalize(".", "a/..");
        testNormalize("a", "a/.");
        testNormalize("a", "a");
        testNormalize("a", "a/");
        testNormalize("a", "a//");
        testNormalize("a", "a///");
        testNormalize("a/b", "a/b");
        testNormalize(".", "a///b///..///..");
    }

    @Test
    void testStartsWith() {
        assertTrue(p("/foo").startsWith("/"));
        assertTrue(p("/foo").startsWith("/foo"));
        assertTrue(p("/foo").startsWith("/foo/"));
        assertFalse(p("/foo").startsWith("/foo/bar"));
        assertFalse(p("/foo").startsWith("foo"));

        assertTrue(p("/foo/bar").startsWith("/"));
        assertTrue(p("/foo/bar").startsWith("/foo"));
        assertTrue(p("/foo/bar").startsWith("/foo/"));
        assertTrue(p("/foo/bar").startsWith("/foo/bar"));
        assertTrue(p("/foo/bar").startsWith("/foo/bar/"));
        assertFalse(p("/foo/bar").startsWith("/foo/bar/foo"));
    }

    @Test
    void testEndsWith() {
        assertTrue(p("/foo").endsWith("/foo"));
        assertTrue(p("/foo").endsWith("foo"));

        assertFalse(p("/foo/bar").endsWith("/bar"));
        assertTrue(p("/foo/bar").endsWith("bar"));
    }

    @Test
    void testSubPath() {
        assertEquals(p("foo"), p("foo").subpath(0, 1));
        assertEquals(p("foo"), p("/foo").subpath(0, 1));
        assertEquals(p("foo"), p("foo/bar").subpath(0, 1));
        assertEquals(p("foo"), p("/foo/bar").subpath(0, 1));
        assertEquals(p("foo/bar"), p("foo/bar").subpath(0, 2));
        assertEquals(p("foo/bar"), p("/foo/bar").subpath(0, 2));
        assertEquals(p("bar"), p("foo/bar").subpath(1, 2));
        assertEquals(p("bar"), p("/foo/bar").subpath(1, 2));
        assertEquals(p(""), p("foo/bar").subpath(1, 1));
    }
}