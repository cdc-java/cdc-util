package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URL;

import org.junit.jupiter.api.Test;

import cdc.util.files.Resources;

class ResourcesTest {
    @Test
    void test() {
        final URL url = Resources.getResource("/cdc/util/files/Resources.class");
        assertNotNull(url);
    }
}