package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.strings.StringRecognizer;

class StringRecognizerTest {
    @Test
    void testIsBoolean() {
        assertTrue(StringRecognizer.isBoolean("true"));
        assertTrue(StringRecognizer.isBoolean("false"));
        assertTrue(StringRecognizer.isBoolean("TRUE"));
        assertTrue(StringRecognizer.isBoolean("FALSE"));
        assertFalse(StringRecognizer.isBoolean("0"));
    }

    @Test
    void testIsByte() {
        assertTrue(StringRecognizer.isByte("0"));
        assertTrue(StringRecognizer.isByte("127"));
        assertFalse(StringRecognizer.isByte("128"));
        assertTrue(StringRecognizer.isByte("-128"));
        assertFalse(StringRecognizer.isByte("-129"));
    }

    @Test
    void testIsShort() {
        assertTrue(StringRecognizer.isShort("0"));
        assertTrue(StringRecognizer.isShort("32767"));
        assertFalse(StringRecognizer.isShort("32768"));
        assertTrue(StringRecognizer.isShort("-32768"));
        assertFalse(StringRecognizer.isShort("-32769"));
    }

    @Test
    void testIsInt() {
        assertTrue(StringRecognizer.isInt("0"));
        assertTrue(StringRecognizer.isInt("2147483647"));
        assertFalse(StringRecognizer.isInt("2147483648"));
        assertTrue(StringRecognizer.isInt("-2147483648"));
        assertFalse(StringRecognizer.isInt("-2147483649"));
    }

    @Test
    void testIsLong() {
        assertTrue(StringRecognizer.isLong("0"));
        assertTrue(StringRecognizer.isLong("9223372036854775807"));
        assertFalse(StringRecognizer.isLong("9223372036854775808"));
        assertTrue(StringRecognizer.isLong("-9223372036854775808"));
        assertFalse(StringRecognizer.isLong("-9223372036854775809"));
    }

    @Test
    void testIsFloat() {
        assertTrue(StringRecognizer.isFloat("0"));
        assertTrue(StringRecognizer.isFloat("0.0"));
        assertTrue(StringRecognizer.isFloat("1.0"));
        assertTrue(StringRecognizer.isFloat("1.0e10"));
        assertTrue(StringRecognizer.isFloat("1.0e10000000"));
        assertFalse(StringRecognizer.isFloat("a"));
    }

    @Test
    void testIsFiniteFloat() {
        assertTrue(StringRecognizer.isFiniteFloat("0"));
        assertTrue(StringRecognizer.isFiniteFloat("0.0"));
        assertTrue(StringRecognizer.isFiniteFloat("1.0"));
        assertTrue(StringRecognizer.isFiniteFloat("1.0e10"));
        assertTrue(StringRecognizer.isFiniteFloat("1.0e30"));
        assertFalse(StringRecognizer.isFiniteFloat("1.0e40"));
        assertFalse(StringRecognizer.isFiniteFloat("1.0e10000000"));
        assertFalse(StringRecognizer.isFiniteFloat("a"));
    }

    @Test
    void testIsDouble() {
        assertTrue(StringRecognizer.isDouble("0"));
        assertTrue(StringRecognizer.isDouble("0.0"));
        assertTrue(StringRecognizer.isDouble("1.0"));
        assertTrue(StringRecognizer.isDouble("1.0e10"));
        assertTrue(StringRecognizer.isDouble("1.0e10000000"));
        assertFalse(StringRecognizer.isDouble("a"));
    }

    @Test
    void testIsFiniteDouble() {
        assertTrue(StringRecognizer.isFiniteDouble("0"));
        assertTrue(StringRecognizer.isFiniteDouble("0.0"));
        assertTrue(StringRecognizer.isFiniteDouble("1.0"));
        assertTrue(StringRecognizer.isFiniteDouble("1.0e10"));
        assertTrue(StringRecognizer.isFiniteDouble("1.0e300"));
        assertFalse(StringRecognizer.isFiniteDouble("1.0e400"));
        assertFalse(StringRecognizer.isFiniteDouble("1.0e10000000"));
        assertFalse(StringRecognizer.isFiniteDouble("a"));
    }

    @Test
    void testGetType() {
        assertEquals(StringRecognizer.Type.STRING, StringRecognizer.getType(null));
        assertEquals(StringRecognizer.Type.STRING, StringRecognizer.getType(""));
        assertEquals(StringRecognizer.Type.STRING, StringRecognizer.getType(" "));
        assertEquals(StringRecognizer.Type.STRING, StringRecognizer.getType("Hello"));
        assertEquals(StringRecognizer.Type.BOOLEAN, StringRecognizer.getType("true"));
        assertEquals(StringRecognizer.Type.BYTE, StringRecognizer.getType("100"));
        assertEquals(StringRecognizer.Type.SHORT, StringRecognizer.getType("10000"));
        assertEquals(StringRecognizer.Type.INTEGER, StringRecognizer.getType("100000"));
        assertEquals(StringRecognizer.Type.LONG, StringRecognizer.getType("10000000000"));
        assertEquals(StringRecognizer.Type.FLOAT, StringRecognizer.getType("0.0"));
        assertEquals(StringRecognizer.Type.DOUBLE, StringRecognizer.getType("1.0e100"));
    }

}