package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.events.ProgressStack;

class ProgressStackTest {
    private static final Logger LOGGER = LogManager.getLogger(ProgressStackTest.class);

    @Test
    void test() {
        final ProgressStack stack = new ProgressStack();
        LOGGER.info(stack);
        assertEquals("1", stack.toString());
        stack.push();
        LOGGER.info(stack);
        assertEquals("1.1", stack.toString());
        stack.push();
        LOGGER.info(stack);
        assertEquals("1.1.1", stack.toString());
        stack.add();
        LOGGER.info(stack);
        assertEquals("1.1.2", stack.toString());
        stack.pop();
        stack.add();
        assertEquals("1.2", stack.toString());
        LOGGER.info(stack);
        stack.push();
        LOGGER.info(stack);
        assertEquals("1.2.1", stack.toString());
        stack.pop();
        stack.pop();
        stack.add();
        LOGGER.info(stack);
        assertEquals("2", stack.toString());
    }
}