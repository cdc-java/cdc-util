package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.util.strings.Splitter;

class SplitterTest {

    private static void testSplit(Splitter.Policy policy,
                                  String s,
                                  String... expecteds) {
        final Splitter splitter = new Splitter(policy);
        final List<String> actuals = splitter.split(s);
        assertArrayEquals(expecteds, actuals.toArray());
    }

    private static void testSplit(char separator,
                                  String s,
                                  String... expecteds) {
        final Splitter splitter = new Splitter(separator);
        final List<String> actuals = splitter.split(s);
        assertArrayEquals(expecteds, actuals.toArray());
    }

    @Test
    void testSplitSeparator() {
        testSplit(';', "");
        testSplit(';', "aa", "aa");
        testSplit(';', ";;;");
        testSplit(';', "; ;;", " ");
        testSplit(';', ";a;b;;", "a", "b");
        testSplit(';', "; a ;b;; ", " a ", "b", " ");
    }

    @Test
    void testSplitCapital() {
        testSplit(Splitter.Policy.CAPITAL, "");
        testSplit(Splitter.Policy.CAPITAL, "AB", "A", "B");
        testSplit(Splitter.Policy.CAPITAL, "  A  B  ", "A", "B");
        testSplit(Splitter.Policy.CAPITAL, "  AbcdBcd ef ", "Abcd", "Bcd", "ef");
    }

    @Test
    void testSplitJoinedCapital() {
        testSplit(Splitter.Policy.JOINED_CAPITAL, "");
        testSplit(Splitter.Policy.JOINED_CAPITAL, "ABC", "ABC");
        testSplit(Splitter.Policy.JOINED_CAPITAL, "  A  B  ", "A", "B");
        testSplit(Splitter.Policy.JOINED_CAPITAL, "  AbcdBcd  ", "Abcd", "Bcd");
        testSplit(Splitter.Policy.JOINED_CAPITAL, "ABbcd", "ABbcd");
        testSplit(Splitter.Policy.JOINED_CAPITAL, "ABbcdEFG", "ABbcd", "EFG");
    }

    @Test
    void testSplitSpace() {
        testSplit(Splitter.Policy.SPACE, "");
        testSplit(Splitter.Policy.SPACE, "A", "A");
        testSplit(Splitter.Policy.SPACE, "  A  ", "A");
        testSplit(Splitter.Policy.SPACE, "  AB  ", "AB");
        testSplit(Splitter.Policy.SPACE, "  AB CD ", "AB", "CD");
    }

    @Test
    void testSplitNone() {
        testSplit(Splitter.Policy.NONE, "");
        testSplit(Splitter.Policy.NONE, "  ", "  ");
        testSplit(Splitter.Policy.NONE, " a b ", " a b ");
    }
}