package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.events.ProgressEvent;

class ProgressEventTest {
    @Test
    void testGetPercents() {
        final double eps = 1.0e-15;
        for (int i = 0; i <= 100; i++) {
            assertEquals(i, ProgressEvent.getPercents(i, 100), eps);
        }

        for (int i = 0; i <= 1000; i++) {
            assertEquals(i / 10.0, ProgressEvent.getPercents(i, 1000), eps);
        }
    }
}