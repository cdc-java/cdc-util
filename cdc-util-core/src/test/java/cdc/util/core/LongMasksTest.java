package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.LongMasks;

class LongMasksTest {

    @Test
    void testPaddedBinString() {
        assertEquals("0000000000000000000000000000000000000000000000000000000000000000",
                     LongMasks.toPaddedBinString(0x0L));
        assertEquals("0000000000000000000000000000000000000000000000000000000000000001",
                     LongMasks.toPaddedBinString(0x1L));
        assertEquals("0000000000000000000000000000000000000000000000000000000000000010",
                     LongMasks.toPaddedBinString(0x2L));
        assertEquals("0000000000000000000000000000000000000000000000000000000000000100",
                     LongMasks.toPaddedBinString(0x4L));
        assertEquals("0000000000000000000000000000000000000000000000000000000000001000",
                     LongMasks.toPaddedBinString(0x8L));
        assertEquals("0000000000000000000000000000000000000000000000000000000000010000",
                     LongMasks.toPaddedBinString(0x10L));
        assertEquals("1111111111111111111111111111111111111111111111111111111111111111",
                     LongMasks.toPaddedBinString(0xFFFFFFFFFFFFFFFFL));
    }

    @Test
    void testIntOps() {
        assertEquals(0xB4A39281, LongMasks.getInt0(0xF8E7D6C5B4A39281L));
        assertEquals(0xF8E7D6C5, LongMasks.getInt1(0xF8E7D6C5B4A39281L));

        assertEquals(0xB4A39281L, LongMasks.setInt0(0L, 0xB4A39281));
        assertEquals(0xFFFFFFFFL, LongMasks.setInt0(0L, 0xFFFFFFFF));
        assertEquals(0xFFFFFFFFFFFFFFFFL, LongMasks.setInt0(0xFFFFFFFFFFFFFFFFL, 0xFFFFFFFF));
        assertEquals(0xFFFFFFFF00000000L, LongMasks.setInt0(0xFFFFFFFFFFFFFFFFL, 0x0));
        assertEquals(0xB4A3928100000000L, LongMasks.setInt1(0L, 0xB4A39281));
        assertEquals(0xFFFFFFFF00000000L, LongMasks.setInt1(0L, 0xFFFFFFFF));
    }

    @Test
    void testShortOps() {
        assertEquals((short) 0x9281, LongMasks.getShort0(0xF8E7D6C5B4A39281L));
        assertEquals((short) 0xB4A3, LongMasks.getShort1(0xF8E7D6C5B4A39281L));
        assertEquals((short) 0xD6C5, LongMasks.getShort2(0xF8E7D6C5B4A39281L));
        assertEquals((short) 0xF8E7, LongMasks.getShort3(0xF8E7D6C5B4A39281L));

        assertEquals(0xD2C1L, LongMasks.setShort0(0L, (short) 0xD2C1));
        assertEquals(0xD2C10000L, LongMasks.setShort1(0L, (short) 0xD2C1));
        assertEquals(0xD2C100000000L, LongMasks.setShort2(0L, (short) 0xD2C1));
        assertEquals(0xD2C1000000000000L, LongMasks.setShort3(0L, (short) 0xD2C1));
        assertEquals(0xFFFFL, LongMasks.setShort0(0L, (short) 0xFFFF));
        assertEquals(0xFFFF0000L, LongMasks.setShort1(0L, (short) 0xFFFF));
        assertEquals(0xFFFF00000000L, LongMasks.setShort2(0L, (short) 0xFFFF));
        assertEquals(0xFFFF000000000000L, LongMasks.setShort3(0L, (short) 0xFFFF));
    }

    @Test
    void testByteOps() {
        assertEquals((byte) 0x81, LongMasks.getByte0(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0x92, LongMasks.getByte1(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0xA3, LongMasks.getByte2(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0xB4, LongMasks.getByte3(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0xC5, LongMasks.getByte4(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0xD6, LongMasks.getByte5(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0xE7, LongMasks.getByte6(0xF8E7D6C5B4A39281L));
        assertEquals((byte) 0xF8, LongMasks.getByte7(0xF8E7D6C5B4A39281L));

        assertEquals(0xC1L, LongMasks.setByte0(0L, (byte) 0xC1));
        assertEquals(0xC100L, LongMasks.setByte1(0L, (byte) 0xC1));
        assertEquals(0xC10000L, LongMasks.setByte2(0L, (byte) 0xC1));
        assertEquals(0xC1000000L, LongMasks.setByte3(0L, (byte) 0xC1));
        assertEquals(0xC100000000L, LongMasks.setByte4(0L, (byte) 0xC1));
        assertEquals(0xC10000000000L, LongMasks.setByte5(0L, (byte) 0xC1));
        assertEquals(0xC1000000000000L, LongMasks.setByte6(0L, (byte) 0xC1));
        assertEquals(0xC100000000000000L, LongMasks.setByte7(0L, (byte) 0xC1));

        assertEquals(0xFFFFFFFFFFFFFFC1L, LongMasks.setByte0(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xFFFFFFFFFFFFC1FFL, LongMasks.setByte1(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xFFFFFFFFFFC1FFFFL, LongMasks.setByte2(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xFFFFFFFFC1FFFFFFL, LongMasks.setByte3(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xFFFFFFC1FFFFFFFFL, LongMasks.setByte4(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xFFFFC1FFFFFFFFFFL, LongMasks.setByte5(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xFFC1FFFFFFFFFFFFL, LongMasks.setByte6(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));
        assertEquals(0xC1FFFFFFFFFFFFFFL, LongMasks.setByte7(0xFFFFFFFFFFFFFFFFL, (byte) 0xC1));

    }

    @Test
    void testOneBitOps() {
        assertEquals(0x1L, LongMasks.toMask(0));
        assertEquals(0x2L, LongMasks.toMask(1));
        assertEquals(0x4L, LongMasks.toMask(2));
        assertEquals(0x8L, LongMasks.toMask(3));
        assertEquals(0x10L, LongMasks.toMask(4));
        assertEquals(0x20L, LongMasks.toMask(5));
        assertEquals(0x40L, LongMasks.toMask(6));
        assertEquals(0x80L, LongMasks.toMask(7));
        assertEquals(0x80000000L, LongMasks.toMask(31));
        assertEquals(0x1000000000000000L, LongMasks.toMask(60));
        assertEquals(0x2000000000000000L, LongMasks.toMask(61));
        assertEquals(0x4000000000000000L, LongMasks.toMask(62));
        assertEquals(0x8000000000000000L, LongMasks.toMask(63));

        for (int index = 0; index < LongMasks.MAX_BITS; index++) {
            final long m = LongMasks.toMask(index);
            for (int i = 0; i < LongMasks.MAX_BITS; i++) {
                assertEquals(LongMasks.isEnabled(m, i), i == index);
            }
        }
    }
}