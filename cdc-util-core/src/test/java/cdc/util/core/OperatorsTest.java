package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.lang.Operators;

class OperatorsTest {
    @Test
    void testCompareBoolean() {
        assertTrue(Operators.compare(false, false) == 0);
        assertTrue(Operators.compare(false, true) < 0);
        assertTrue(Operators.compare(true, true) == 0);
        assertTrue(Operators.compare(true, false) > 0);
    }

    @Test
    void testCompareLong() {
        assertTrue(Operators.compare(10L, 10L) == 0);
        assertTrue(Operators.compare(20L, 10L) > 0);
        assertTrue(Operators.compare(10L, 20L) < 0);
    }

    @Test
    void testCompareInteger() {
        assertTrue(Operators.compare(10, 10) == 0);
        assertTrue(Operators.compare(20, 10) > 0);
        assertTrue(Operators.compare(10, 20) < 0);
    }

    @Test
    void testCompareShort() {
        assertTrue(Operators.compare((short) 10, (short) 10) == 0);
        assertTrue(Operators.compare((short) 20, (short) 10) > 0);
        assertTrue(Operators.compare((short) 10, (short) 20) < 0);
    }

    @Test
    void testCompareByte() {
        assertTrue(Operators.compare((byte) 10, (byte) 10) == 0);
        assertTrue(Operators.compare((byte) 20, (byte) 10) > 0);
        assertTrue(Operators.compare((byte) 10, (byte) 20) < 0);
    }

    @Test
    void testCompareCharacter() {
        assertTrue(Operators.compare('A', 'A') == 0);
        assertTrue(Operators.compare('B', 'A') > 0);
        assertTrue(Operators.compare('A', 'B') < 0);
    }

    @Test
    void testCompareDouble() {
        assertTrue(Operators.compare(0.0, 0.0) == 0);
        assertTrue(Operators.compare(1.0, 0.0) > 0);
        assertTrue(Operators.compare(0.0, 1.0) < 0);
    }

    @Test
    void testCompareFloat() {
        assertTrue(Operators.compare(0.0F, 0.0F) == 0);
        assertTrue(Operators.compare(1.0F, 0.0F) > 0);
        assertTrue(Operators.compare(0.0F, 1.0F) < 0);
    }

    @Test
    void testEqualsString() {
        assertTrue(Operators.equals(null, null));
        assertFalse(Operators.equals(null, "H"));
        assertFalse(Operators.equals("H", null));
        assertTrue(Operators.equals("H", "H"));
        assertFalse(Operators.equals("H", "Hello"));
        assertFalse(Operators.equals("Hello", "H"));
    }

    @Test
    void testCompareString() {
        assertTrue(Operators.compare(null, null) == 0);
        assertTrue(Operators.compare(null, "H") < 0);
        assertTrue(Operators.compare("H", null) > 0);
        assertTrue(Operators.compare("H", "H") == 0);
        assertTrue(Operators.compare("H", "Hello") < 0);
        assertTrue(Operators.compare("Hello", "H") > 0);
    }

    @Test
    void testMinString() {
        assertTrue(Operators.min(null, null) == null);
        assertTrue(Operators.min(null, "H") == null);
        assertTrue(Operators.min("H", null) == null);
        assertTrue(Operators.min("H", "H") == "H");
        assertTrue(Operators.min("H", "Hello") == "H");
        assertTrue(Operators.min("Hello", "H") == "H");
    }

    @Test
    void testMaxString() {
        assertTrue(Operators.max(null, null) == null);
        assertTrue(Operators.max(null, "H") == "H");
        assertTrue(Operators.max("H", null) == "H");
        assertTrue(Operators.max("H", "H") == "H");
        assertTrue(Operators.max("H", "Hello") == "Hello");
        assertTrue(Operators.max("Hello", "H") == "Hello");
    }

    @Test
    void testHashCodeString() {
        assertSame(0, Operators.hashCode(null));
        assertSame(Operators.hashCode("H"), "H".hashCode());
    }

    @Test
    void testGetClass() {
        assertEquals(null, Operators.getClass(null));
        assertEquals(String.class, Operators.getClass("Hello"));
        assertEquals(Double.class, Operators.getClass(10.0));
    }

}