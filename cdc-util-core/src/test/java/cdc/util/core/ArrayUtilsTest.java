package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.ArrayUtils;

class ArrayUtilsTest {

    @Test
    void testIndexOfSmallestGreaterOrEqual() {
        final double[] values = { 0.0, 1.0, 2.0 };
        assertEquals(0, ArrayUtils.indexOfSmallestGreaterOrEqual(values, -1.0));
        assertEquals(0, ArrayUtils.indexOfSmallestGreaterOrEqual(values, 0.0));
        assertEquals(1, ArrayUtils.indexOfSmallestGreaterOrEqual(values, 0.5));
        assertEquals(1, ArrayUtils.indexOfSmallestGreaterOrEqual(values, 1.0));
        assertEquals(2, ArrayUtils.indexOfSmallestGreaterOrEqual(values, 1.5));
        assertEquals(2, ArrayUtils.indexOfSmallestGreaterOrEqual(values, 2.0));
        assertEquals(-3, ArrayUtils.indexOfSmallestGreaterOrEqual(values, 3.0));
    }

    @Test
    void testIndexOfLargestSmallerOrEqual() {
        final double[] values = { 0.0, 1.0, 2.0 };
        assertEquals(-1, ArrayUtils.indexOfLargestSmallerOrEqual(values, -1.0));
        assertEquals(0, ArrayUtils.indexOfLargestSmallerOrEqual(values, 0.0));
        assertEquals(0, ArrayUtils.indexOfLargestSmallerOrEqual(values, 0.5));
        assertEquals(1, ArrayUtils.indexOfLargestSmallerOrEqual(values, 1.0));
        assertEquals(1, ArrayUtils.indexOfLargestSmallerOrEqual(values, 1.5));
        assertEquals(2, ArrayUtils.indexOfLargestSmallerOrEqual(values, 2.0));
        assertEquals(2, ArrayUtils.indexOfLargestSmallerOrEqual(values, 3.0));
    }

    @Test
    void testSmallestSaturatedGreaterOrEqual() {
        final double[] values = { 0.0, 1.0, 2.0 };
        final double eps = 1.0e-6;
        assertEquals(0.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, -1.0), eps);
        assertEquals(0.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, 0.0), eps);
        assertEquals(1.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, 0.5), eps);
        assertEquals(1.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, 1.0), eps);
        assertEquals(2.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, 1.5), eps);
        assertEquals(2.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, 2.0), eps);
        assertEquals(2.0, ArrayUtils.smallestSaturatedGreaterOrEqual(values, 3.0), eps);
    }

    @Test
    void testLargestSaturatedSmallerOrEqual() {
        final double[] values = { 0.0, 1.0, 2.0 };
        final double eps = 1.0e-6;
        assertEquals(0.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, -1.0), eps);
        assertEquals(0.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, 0.0), eps);
        assertEquals(0.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, 0.5), eps);
        assertEquals(1.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, 1.0), eps);
        assertEquals(1.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, 1.5), eps);
        assertEquals(2.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, 2.0), eps);
        assertEquals(2.0, ArrayUtils.largestSaturatedSmallerOrEqual(values, 3.0), eps);
    }
}