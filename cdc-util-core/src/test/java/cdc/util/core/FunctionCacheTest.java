package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import cdc.util.function.FunctionCache;

class FunctionCacheTest {
    @Test
    void test() {
        final Function<Integer, Integer> f = i -> 2 * i;
        final FunctionCache<Integer, Integer> c = FunctionCache.of(f);

        assertEquals(0, c.getKeys().size());
        assertEquals(0, c.apply(0));
        assertEquals(1, c.getKeys().size());
        c.clear();
        assertEquals(0, c.getKeys().size());
        assertEquals(2, c.apply(1));
        assertEquals(4, c.apply(2));
    }
}