package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.IntMasks;

class IntMasksTest {

    @Test
    void testPaddedBinString() {
        assertEquals("00000000000000000000000000000000", IntMasks.toPaddedBinString(0x0));
        assertEquals("00000000000000000000000000000001", IntMasks.toPaddedBinString(0x1));
        assertEquals("00000000000000000000000000000010", IntMasks.toPaddedBinString(0x2));
        assertEquals("00000000000000000000000000000100", IntMasks.toPaddedBinString(0x4));
        assertEquals("00000000000000000000000000001000", IntMasks.toPaddedBinString(0x8));
        assertEquals("00000000000000000000000000010000", IntMasks.toPaddedBinString(0x10));
        assertEquals("11111111111111111111111111111111", IntMasks.toPaddedBinString(0xFFFFFFFF));
    }

    @Test
    void testShortOps() {
        assertEquals((short) 0xD2C1, IntMasks.getShort0(0xF4E3D2C1));
        assertEquals((short) 0xF4E3, IntMasks.getShort1(0xF4E3D2C1));

        assertEquals(0xD2C1, IntMasks.setShort0(0, (short) 0xD2C1));
        assertEquals(0xD2C10000, IntMasks.setShort1(0, (short) 0xD2C1));
    }

    @Test
    void testByteOps() {
        assertEquals((byte) 0xC1, IntMasks.getByte0(0xF4E3D2C1));
        assertEquals((byte) 0xD2, IntMasks.getByte1(0xF4E3D2C1));
        assertEquals((byte) 0xE3, IntMasks.getByte2(0xF4E3D2C1));
        assertEquals((byte) 0xF4, IntMasks.getByte3(0xF4E3D2C1));
        assertEquals((short) 0xD2C1, IntMasks.getShort0(0xF4E3D2C1));
        assertEquals((short) 0xF4E3, IntMasks.getShort1(0xF4E3D2C1));

        assertEquals(0xC1, IntMasks.setByte0(0, (byte) 0xC1));
        assertEquals(0xC100, IntMasks.setByte1(0, (byte) 0xC1));
        assertEquals(0xC10000, IntMasks.setByte2(0, (byte) 0xC1));
        assertEquals(0xC1000000, IntMasks.setByte3(0, (byte) 0xC1));
    }

    @Test
    void testOneBitOps() {
        assertEquals(0x1, IntMasks.toMask(0));
        assertEquals(0x2, IntMasks.toMask(1));
        assertEquals(0x4, IntMasks.toMask(2));
        assertEquals(0x8, IntMasks.toMask(3));
        assertEquals(0x10, IntMasks.toMask(4));
        assertEquals(0x20, IntMasks.toMask(5));
        assertEquals(0x40, IntMasks.toMask(6));
        assertEquals(0x80, IntMasks.toMask(7));
        assertEquals(0x80000000, IntMasks.toMask(31));

        for (int index = 0; index < IntMasks.MAX_BITS; index++) {
            final int m = IntMasks.toMask(index);
            for (int i = 0; i < IntMasks.MAX_BITS; i++) {
                assertEquals(IntMasks.isEnabled(m, i), i == index);
            }
        }
    }
}