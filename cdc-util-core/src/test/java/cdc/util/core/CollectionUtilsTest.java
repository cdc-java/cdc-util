package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.util.lang.CollectionUtils;

class CollectionUtilsTest {

    private static List<Integer> toList(int... numbers) {
        final List<Integer> list = new ArrayList<>();
        for (final int number : numbers) {
            list.add(number);
        }
        return list;
    }

    private static void testSetIndex(List<Integer> expected,
                                     List<Integer> list,
                                     int value,
                                     int to) {
        CollectionUtils.setIndex(list, value, to);
        assertEquals(expected, list);
    }

    @Test
    void testSetIndex() {

        testSetIndex(toList(0, 1, 2, 3), toList(0, 1, 2, 3), 0, 0);
        testSetIndex(toList(1, 0, 2, 3), toList(0, 1, 2, 3), 0, 1);
        testSetIndex(toList(1, 2, 0, 3), toList(0, 1, 2, 3), 0, 2);
        testSetIndex(toList(1, 2, 3, 0), toList(0, 1, 2, 3), 0, 3);

        testSetIndex(toList(1, 0, 2, 3), toList(0, 1, 2, 3), 1, 0);
        testSetIndex(toList(0, 1, 2, 3), toList(0, 1, 2, 3), 1, 1);
        testSetIndex(toList(0, 2, 1, 3), toList(0, 1, 2, 3), 1, 2);
        testSetIndex(toList(0, 2, 3, 1), toList(0, 1, 2, 3), 1, 3);

        testSetIndex(toList(2, 0, 1, 3), toList(0, 1, 2, 3), 2, 0);
        testSetIndex(toList(0, 2, 1, 3), toList(0, 1, 2, 3), 2, 1);
        testSetIndex(toList(0, 1, 2, 3), toList(0, 1, 2, 3), 2, 2);
        testSetIndex(toList(0, 1, 3, 2), toList(0, 1, 2, 3), 2, 3);

        testSetIndex(toList(3, 0, 1, 2), toList(0, 1, 2, 3), 3, 0);
        testSetIndex(toList(0, 3, 1, 2), toList(0, 1, 2, 3), 3, 1);
        testSetIndex(toList(0, 1, 3, 2), toList(0, 1, 2, 3), 3, 2);
        testSetIndex(toList(0, 1, 2, 3), toList(0, 1, 2, 3), 3, 3);
    }

    @Test
    void testSetIndexInvalidElement() {
        assertThrows(IllegalArgumentException.class, () -> {
            testSetIndex(toList(0, 1, 2, 3), toList(0, 1, 2, 3), 5, 0);
        });
    }

    void testSetIndexInvalidIndex() {
        assertThrows(IllegalArgumentException.class, () -> {
            testSetIndex(toList(0, 1, 2, 3), toList(0, 1, 2, 3), 0, 10);
        });
    }
}