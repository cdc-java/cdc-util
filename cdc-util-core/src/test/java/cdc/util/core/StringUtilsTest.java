package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.function.BiFunction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.strings.StringUtils;

class StringUtilsTest {
    private static final Logger LOGGER = LogManager.getLogger(StringUtilsTest.class);

    private static void checkFormat(String expected,
                                    String format,
                                    Object... args) {
        final BiFunction<String, Object, String> argFormater =
                (f,
                 o) -> {
                    LOGGER.info("f: '{}' o: {}", f, o);
                    return o == null ? "null" : o.toString();
                };

        final String actual = StringUtils.format(format, argFormater, args);
        LOGGER.info("format:{} expected:{} actual:{}", format, expected, actual);

        assertEquals(expected, actual);
    }

    @Test
    void testCountMatches() {
        assertEquals(0, StringUtils.countMatches(null, '/'));
        assertEquals(0, StringUtils.countMatches("", '/'));
        assertEquals(1, StringUtils.countMatches("/", '/'));
        assertEquals(2, StringUtils.countMatches("//", '/'));
        assertEquals(3, StringUtils.countMatches("A/B/C/D", '/'));
    }

    @Test
    void testExtact1() {
        assertSame(null, StringUtils.extract(null, 10));
        assertEquals("", StringUtils.extract("", 10));
        assertEquals("", StringUtils.extract("", 3));
        assertEquals("a", StringUtils.extract("a", 3));
        assertEquals("ab", StringUtils.extract("ab", 3));
        assertEquals("abc", StringUtils.extract("abc", 3));
        assertEquals("...", StringUtils.extract("abcd", 3));
        assertEquals("a...", StringUtils.extract("abcde", 4));
        assertEquals("abcde\na...", StringUtils.extract("abcde\nabcde", 10));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringUtils.extract(null, 2);
                     });
    }

    @Test
    void testExtact2() {
        assertSame(null, StringUtils.extract(null, 10, 10));
        assertEquals("abcde\nab...", StringUtils.extract("abcde\nabcdedf", 5, 2));
        assertEquals("abcde\n...", StringUtils.extract("abcde\nabcdedf\nabcdef", 5, 2));
        assertEquals("abcde", StringUtils.extract("abcde", 5, 1));
        assertEquals("ab...", StringUtils.extract("abcdef", 5, 1));
        assertEquals("...", StringUtils.extract("abcde\nabcde", 5, 1));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringUtils.extract(null, 2, 0);
                     });
    }

    @Test
    void testExtactAverage() {
        assertSame(null, StringUtils.extractAverage(null, 10, 10));
        assertEquals("abcdef", StringUtils.extractAverage("abcdef", 10, 10));
        assertEquals("ab...", StringUtils.extractAverage("abcdef", 5, 10));
        assertEquals("ab...\na...", StringUtils.extractAverage("abcdef\nabcdef", 5, 10));
        assertEquals("ab...\nab...", StringUtils.extractAverage("abcdef\nabcdef", 5, 11));
        assertEquals("ab...\nab...", StringUtils.extractAverage("abcdef\nabcdef", 5, 100));
        assertEquals("abcdef\nabcdef", StringUtils.extractAverage("abcdef\nabcdef", 6, 100));
        assertEquals("abcdef\nabc...", StringUtils.extractAverage("abcdef\nabcdefg", 6, 100));
    }

    @Test
    void testNumberOfLines() {
        assertEquals(0, StringUtils.numberOfLines(null));
        assertEquals(0, StringUtils.numberOfLines(""));
        assertEquals(1, StringUtils.numberOfLines(" "));
        assertEquals(2, StringUtils.numberOfLines(" A\n"));
    }

    @Test
    void testMaxLineLength() {
        assertEquals(0, StringUtils.maxLineLength(null));
        assertEquals(0, StringUtils.maxLineLength(""));
        assertEquals(1, StringUtils.maxLineLength("A"));
        assertEquals(4, StringUtils.maxLineLength("ABCD"));
        assertEquals(4, StringUtils.maxLineLength("ABCD\n"));
        assertEquals(5, StringUtils.maxLineLength("ABCD\nABCDE"));
    }

    @Test
    void testWrapNoStrip() {
        assertEquals("", StringUtils.wrap("", 5, false));
        assertEquals("A", StringUtils.wrap("A", 5, false));
        assertEquals("AB", StringUtils.wrap("AB", 5, false));
        assertEquals("ABC", StringUtils.wrap("ABC", 5, false));
        assertEquals("ABCD", StringUtils.wrap("ABCD", 5, false));
        assertEquals("ABCDE", StringUtils.wrap("ABCDE", 5, false));
        assertEquals("ABCDE\nF", StringUtils.wrap("ABCDEF", 5, false));
        assertEquals("ABCDE\nFG", StringUtils.wrap("ABCDEFG", 5, false));
        assertEquals("ABCDE\nFGH", StringUtils.wrap("ABCDEFGH", 5, false));
        assertEquals("ABCDE\nFGHI", StringUtils.wrap("ABCDEFGHI", 5, false));
        assertEquals("ABCDE\nFGHIJ", StringUtils.wrap("ABCDEFGHIJ", 5, false));
        assertEquals("ABCDE\nFGHIJ\nK", StringUtils.wrap("ABCDEFGHIJK", 5, false));
        assertEquals("ABC \nDEF", StringUtils.wrap("ABC DEF", 5, false));
        assertEquals("AB CD\n EF", StringUtils.wrap("AB CD EF", 5, false));
        assertEquals("A B C\n D E \nF", StringUtils.wrap("A B C D E F", 5, false));
        assertEquals("ABCDE\nFGHIJ", StringUtils.wrap("ABCDE\nFGHIJ", 5, false));
        assertEquals("ABCDE\nF\nGHIJK", StringUtils.wrap("ABCDE\nF\nGHIJK", 5, false));
    }

    @Test
    void testWrapStrip() {
        assertEquals("", StringUtils.wrap("", 5, true));
        assertEquals("A", StringUtils.wrap("A", 5, true));
        assertEquals("AB", StringUtils.wrap("AB", 5, true));
        assertEquals("ABC", StringUtils.wrap("ABC", 5, true));
        assertEquals("ABCD", StringUtils.wrap("ABCD", 5, true));
        assertEquals("ABCDE", StringUtils.wrap("ABCDE", 5, true));
        assertEquals("ABCDE\nF", StringUtils.wrap("ABCDEF", 5, true));
        assertEquals("ABCDE\nFG", StringUtils.wrap("ABCDEFG", 5, true));
        assertEquals("ABCDE\nFGH", StringUtils.wrap("ABCDEFGH", 5, true));
        assertEquals("ABCDE\nFGHI", StringUtils.wrap("ABCDEFGHI", 5, true));
        assertEquals("ABCDE\nFGHIJ", StringUtils.wrap("ABCDEFGHIJ", 5, true));
        assertEquals("ABCDE\nFGHIJ\nK", StringUtils.wrap("ABCDEFGHIJK", 5, true));
        assertEquals("ABC \nDEF", StringUtils.wrap("ABC DEF", 5, true));
        assertEquals("AB CD\nEF", StringUtils.wrap("AB CD EF", 5, true));
        assertEquals("A B C\nD E F", StringUtils.wrap("A B C D E F", 5, true));
        assertEquals("A B C\nD E F\nG H I", StringUtils.wrap("A B C D E F G H I", 5, true));
        assertEquals("A  B \nC  D \nE  F \nG  H \nI", StringUtils.wrap("A  B  C  D  E  F  G  H  I", 5, true));
        assertEquals("ABCDE", StringUtils.wrap("  ABCDE", 5, true));
        assertEquals("ABCDE\nFGHI", StringUtils.wrap("  ABCDEFGHI", 5, true));
        assertEquals("ABCDE\nFGHIJ", StringUtils.wrap("  ABCDE   FGHIJ", 5, true));
        assertEquals("ABCDE\nF  \nGHIJK", StringUtils.wrap("  ABCDE  F  GHIJK", 5, true));
    }

    @Test
    void testFormat() {
        checkFormat("aaa", "aaa");
        checkFormat("aaa1", "aaa{%s}", 1);
        checkFormat("aaa12", "aaa{%s}{%s}", 1, 2);
        checkFormat("aaa1 2", "aaa{%s} {%s}", 1, 2);
        checkFormat("aaa1 bbb2 ccc", "aaa{%s} bbb{%s} ccc", 1, 2);
        checkFormat("aaa1bbb2ccc", "aaa{%}bbb{%}ccc", 1, 2);
    }
}