package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.BiFunction;

import org.junit.jupiter.api.Test;

import cdc.util.function.BiFunctionCache;

class BiFunctionCacheTest {
    @Test
    void test() {
        final BiFunction<Integer, Integer, Integer> f = (i,
                                                         j) -> i + j;
        final BiFunctionCache<Integer, Integer, Integer> c = BiFunctionCache.of(f);

        assertEquals(0, c.getKeys().size());
        assertEquals(0, c.apply(0, 0));
        assertEquals(1, c.getKeys().size());
        c.clear();
        assertEquals(0, c.getKeys().size());
        assertEquals(2, c.apply(1, 1));
        assertEquals(4, c.apply(2, 2));
    }
}