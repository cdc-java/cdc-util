package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import cdc.util.function.Predicates;

class PredicatesTest {

    @Test
    void testIsNull() {
        final Predicate<Object> predicate = Predicates.isNull();
        assertTrue(predicate.test(null));
        assertFalse(predicate.test("Foo"));
    }

    @Test
    void testIsNotNull() {
        final Predicate<Object> predicate = Predicates.isNotNull();
        assertFalse(predicate.test(null));
        assertTrue(predicate.test("Foo"));
    }

    @Test
    void testAlswaysTrue() {
        final Predicate<Object> predicate = Predicates.alwaysTrue();
        assertTrue(predicate.test(null));
        assertTrue(predicate.test("Foo"));
    }

    @Test
    void testAlswaysFalse() {
        final Predicate<Object> predicate = Predicates.alwaysFalse();
        assertFalse(predicate.test(null));
        assertFalse(predicate.test("Foo"));
    }

    @Test
    void testIsInstanceOf() {
        final Predicate<Object> predicate = Predicates.isInstanceOf(String.class);
        assertFalse(predicate.test(null));
        assertFalse(predicate.test(10));
        assertTrue(predicate.test("Foo"));
    }

    @Test
    void testAndEmpty() {
        final Predicate<Object> predicate = Predicates.and();
        assertTrue(predicate.test(null));
        assertTrue(predicate.test(10));
        assertTrue(predicate.test("Foo"));
    }

    @Test
    void testAnd() {
        final Predicate<Object> predicate = Predicates.and(Predicates.isNotNull(),
                                                           Predicates.isInstanceOf(String.class));
        assertFalse(predicate.test(null));
        assertFalse(predicate.test(10));
        assertTrue(predicate.test("Foo"));
    }

    @Test
    void testOrEmpty() {
        final Predicate<Object> predicate = Predicates.or();
        assertFalse(predicate.test(null));
        assertFalse(predicate.test(10));
        assertFalse(predicate.test("Foo"));
    }

    @Test
    void testOr() {
        final Predicate<Object> predicate = Predicates.or(Predicates.isNotNull(),
                                                          Predicates.isInstanceOf(String.class));
        assertFalse(predicate.test(null));
        assertTrue(predicate.test(10));
        assertTrue(predicate.test("Foo"));
    }
}