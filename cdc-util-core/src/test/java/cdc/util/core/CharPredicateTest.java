package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;

class CharPredicateTest {
    @Test
    void testBitSet() {
        final CharPredicate set = new BitSetCharPredicate("abc");
        assertTrue(set.test('a'));
        assertTrue(set.test('b'));
        assertTrue(set.test('c'));
        assertFalse(set.test('d'));
    }

    @Test
    void testBooleanArray() {
        final CharPredicate set = new BooleanArrayCharPredicate("abc");
        assertTrue(set.test('a'));
        assertTrue(set.test('b'));
        assertTrue(set.test('c'));
        assertFalse(set.test('d'));
    }
}