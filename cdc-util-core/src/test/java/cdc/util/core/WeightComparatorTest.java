package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.strings.WeightComparator;

class WeightComparatorTest {
    @Test
    void test() {
        final WeightComparator c = new WeightComparator(-1, "Z", "A");
        assertTrue(c.compare("Z", "A") < 0);
        assertTrue(c.compare("B", "A") < 0);
        assertTrue(c.compare("B", "Z") < 0);
        assertTrue(c.compare("B", "C") < 0);
        assertEquals(0, c.compare("A", "A"));
        assertEquals(0, c.compare("B", "B"));
    }
}