package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.events.ProgressEvent;
import cdc.util.events.ProgressEventFilter;

class ProgressEventFilterTest {
    static final Logger LOGGER = LogManager.getLogger(ProgressEventFilterTest.class);

    @Test
    void test() {
        final List<ProgressEvent> events = new ArrayList<>();
        final Consumer<ProgressEvent> consumer = new Consumer<ProgressEvent>() {
            @Override
            public void accept(ProgressEvent event) {
                LOGGER.debug("Passed event: {}", event);
                events.add(event);
            }
        };
        final ProgressEventFilter filter = new ProgressEventFilter(consumer);
        filter.setMinDeltaMillis(0);
        filter.setMinDeltaPercents(1.0);

        for (int loops = 0; loops < 10; loops++) {
            filter.reset();
            events.clear();
            final int count = 1000;
            for (int value = 0; value <= count; value++) {
                filter.notifyProgress(value, count);
            }
            LOGGER.debug("{} events", events.size());
            assertEquals(101, events.size());
        }
    }
}