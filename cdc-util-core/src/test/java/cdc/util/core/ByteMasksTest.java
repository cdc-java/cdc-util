package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.util.lang.ByteMasks;

class ByteMasksTest {
    @Test
    void testPaddedBinString() {
        assertEquals("00000000", ByteMasks.toPaddedBinString((byte) 0x0));
        assertEquals("00000001", ByteMasks.toPaddedBinString((byte) 0x1));
        assertEquals("00000010", ByteMasks.toPaddedBinString((byte) 0x2));
        assertEquals("00000100", ByteMasks.toPaddedBinString((byte) 0x4));
        assertEquals("00001000", ByteMasks.toPaddedBinString((byte) 0x8));
        assertEquals("00001111", ByteMasks.toPaddedBinString((byte) 0xF));
        assertEquals("00010000", ByteMasks.toPaddedBinString((byte) 0x10));
        assertEquals("00100000", ByteMasks.toPaddedBinString((byte) 0x20));
        assertEquals("01000000", ByteMasks.toPaddedBinString((byte) 0x40));
        assertEquals("10000000", ByteMasks.toPaddedBinString((byte) 0x80));
        assertEquals("11110000", ByteMasks.toPaddedBinString((byte) 0xF0));
    }

    @Test
    void testOneBitOps() {
        assertEquals((byte) 0x1, ByteMasks.toMask(0));
        assertEquals((byte) 0x2, ByteMasks.toMask(1));
        assertEquals((byte) 0x4, ByteMasks.toMask(2));
        assertEquals((byte) 0x8, ByteMasks.toMask(3));
        assertEquals((byte) 0x10, ByteMasks.toMask(4));
        assertEquals((byte) 0x20, ByteMasks.toMask(5));
        assertEquals((byte) 0x40, ByteMasks.toMask(6));
        assertEquals((byte) 0x80, ByteMasks.toMask(7));

        assertEquals((byte) 0x1, ByteMasks.setEnabled((byte) 0, 0, true));
        assertEquals((byte) 0x2, ByteMasks.setEnabled((byte) 0, 1, true));
        assertEquals((byte) 0x4, ByteMasks.setEnabled((byte) 0, 2, true));
        assertEquals((byte) 0x8, ByteMasks.setEnabled((byte) 0, 3, true));
        assertEquals((byte) 0x10, ByteMasks.setEnabled((byte) 0, 4, true));
        assertEquals((byte) 0x20, ByteMasks.setEnabled((byte) 0, 5, true));
        assertEquals((byte) 0x40, ByteMasks.setEnabled((byte) 0, 6, true));
        assertEquals((byte) 0x80, ByteMasks.setEnabled((byte) 0, 7, true));

        assertEquals(false, ByteMasks.isEnabled((byte) 0, 0));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 1));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 2));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 3));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 4));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 5));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 6));
        assertEquals(false, ByteMasks.isEnabled((byte) 0, 7));

        assertEquals(true, ByteMasks.isEnabled((byte) 0xF, 0));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xF, 1));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xF, 2));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xF, 3));
        assertEquals(false, ByteMasks.isEnabled((byte) 0xF, 4));
        assertEquals(false, ByteMasks.isEnabled((byte) 0xF, 5));
        assertEquals(false, ByteMasks.isEnabled((byte) 0xF, 6));
        assertEquals(false, ByteMasks.isEnabled((byte) 0xF, 7));

        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 0));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 1));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 2));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 3));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 4));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 5));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 6));
        assertEquals(true, ByteMasks.isEnabled((byte) 0xFF, 7));

        for (int index = 0; index < ByteMasks.MAX_BITS; index++) {
            final byte m = ByteMasks.toMask(index);
            for (int i = 0; i < ByteMasks.MAX_BITS; i++) {
                assertEquals(ByteMasks.isEnabled(m, i), i == index);
            }
        }
    }

    @Test
    void testNBitOps() {
        assertEquals((byte) 0x1, ByteMasks.toMask(0, 1));
        assertEquals((byte) 0x3, ByteMasks.toMask(0, 2));
        assertEquals((byte) 0x7, ByteMasks.toMask(0, 3));
        assertEquals((byte) 0xF, ByteMasks.toMask(0, 4));
        assertEquals((byte) 0x1F, ByteMasks.toMask(0, 5));
        assertEquals((byte) 0x3F, ByteMasks.toMask(0, 6));
        assertEquals((byte) 0x7F, ByteMasks.toMask(0, 7));
        assertEquals((byte) 0xFF, ByteMasks.toMask(0, 8));
        assertEquals((byte) 0x2, ByteMasks.toMask(1, 1));
        assertEquals((byte) 0x6, ByteMasks.toMask(1, 2));
        assertEquals((byte) 0xE, ByteMasks.toMask(1, 3));
        assertEquals((byte) 0x1E, ByteMasks.toMask(1, 4));
        assertEquals((byte) 0x3E, ByteMasks.toMask(1, 5));
        assertEquals((byte) 0x7E, ByteMasks.toMask(1, 6));
        assertEquals((byte) 0xFE, ByteMasks.toMask(1, 7));
        assertEquals((byte) 0x4, ByteMasks.toMask(2, 1));
        assertEquals((byte) 0xC, ByteMasks.toMask(2, 2));
        assertEquals((byte) 0x1C, ByteMasks.toMask(2, 3));
        assertEquals((byte) 0x3C, ByteMasks.toMask(2, 4));
        assertEquals((byte) 0x7C, ByteMasks.toMask(2, 5));
        assertEquals((byte) 0xFC, ByteMasks.toMask(2, 6));
        // ...
        assertEquals((byte) 0x80, ByteMasks.toMask(7, 1));

        for (int m = 0; m <= 255; m++) {
            final byte mask = (byte) m;
            for (int index = 0; index < ByteMasks.MAX_BITS; index++) {
                for (int length = 1; length <= ByteMasks.MAX_BITS - index; length++) {
                    final int max = 1 << (length);
                    for (int value = 0; value < max; value++) {
                        final byte smask = ByteMasks.set(mask, index, length, value);
                        final int rvalue = ByteMasks.get(smask, index, length);
                        assertEquals(value, rvalue);
                    }
                }
            }
        }
    }
}