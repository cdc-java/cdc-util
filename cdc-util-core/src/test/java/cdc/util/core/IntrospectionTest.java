package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;
import cdc.util.lang.NotFoundException;

// TODO create tests for complex inheritance hierarchies
class IntrospectionTest {

    static interface I1 {
        void foo();
    }

    static interface I2 extends I1 {
        public void bar2();
    }

    static interface I3 extends I1 {
        public void bar3();
    }

    static interface I4 extends I2, I3 {
        public void bar4();
    }

    static class C1 {
        private int p0;
        protected int i0;
        public int i1;

        public C1() {
        }

        public C1(int i1) {
            this.i1 = i1;
        }

        int getI1() {
            return i1;
        }

        int getP0() {
            return p0;
        }
    }

    protected static class C2 extends C1 {
        public double d1;

        public C2() {
            super();
        }

        public C2(int i1,
                  double d1) {
            super(i1);
            this.d1 = d1;
        }

        double getD1() {
            return d1;
        }
    }

    @Test
    void testIsPrimitiveTypeClass() {
        assertTrue(Introspection.isPrimitiveTypeClass(long.class));
        assertTrue(Introspection.isPrimitiveTypeClass(int.class));
        assertTrue(Introspection.isPrimitiveTypeClass(short.class));
        assertTrue(Introspection.isPrimitiveTypeClass(byte.class));
        assertTrue(Introspection.isPrimitiveTypeClass(char.class));
        assertTrue(Introspection.isPrimitiveTypeClass(boolean.class));
        assertTrue(Introspection.isPrimitiveTypeClass(void.class));
        assertTrue(Introspection.isPrimitiveTypeClass(double.class));
        assertTrue(Introspection.isPrimitiveTypeClass(float.class));

        assertFalse(Introspection.isPrimitiveTypeClass(null));
        assertFalse(Introspection.isPrimitiveTypeClass(Long.class));
        assertFalse(Introspection.isPrimitiveTypeClass(Integer.class));
    }

    @Test
    void testIsPrimitiveTypeClassName() {
        assertTrue(Introspection.isPrimitiveTypeClassName("long"));
        assertTrue(Introspection.isPrimitiveTypeClassName("int"));
        assertTrue(Introspection.isPrimitiveTypeClassName("short"));
        assertTrue(Introspection.isPrimitiveTypeClassName("byte"));
        assertTrue(Introspection.isPrimitiveTypeClassName("char"));
        assertTrue(Introspection.isPrimitiveTypeClassName("boolean"));
        assertTrue(Introspection.isPrimitiveTypeClassName("void"));
        assertTrue(Introspection.isPrimitiveTypeClassName("double"));
        assertTrue(Introspection.isPrimitiveTypeClassName("float"));

        assertFalse(Introspection.isPrimitiveTypeClassName(null));
        assertFalse(Introspection.isPrimitiveTypeClassName("Long"));
        assertFalse(Introspection.isPrimitiveTypeClassName("Integer"));
    }

    @Test
    void testIsWrapperTypeClass() {
        assertTrue(Introspection.isWrapperTypeClass(Long.class));
        assertTrue(Introspection.isWrapperTypeClass(Integer.class));
        assertTrue(Introspection.isWrapperTypeClass(Short.class));
        assertTrue(Introspection.isWrapperTypeClass(Byte.class));
        assertTrue(Introspection.isWrapperTypeClass(Character.class));
        assertTrue(Introspection.isWrapperTypeClass(Boolean.class));
        assertTrue(Introspection.isWrapperTypeClass(Void.class));
        assertTrue(Introspection.isWrapperTypeClass(Double.class));
        assertTrue(Introspection.isWrapperTypeClass(Float.class));

        assertFalse(Introspection.isWrapperTypeClass(null));
        assertFalse(Introspection.isWrapperTypeClass(String.class));
    }

    @Test
    void testIsWrapperTypeClassName() {
        assertTrue(Introspection.isWrapperTypeClassName("Long"));
        assertTrue(Introspection.isWrapperTypeClassName("Integer"));
        assertTrue(Introspection.isWrapperTypeClassName("Short"));
        assertTrue(Introspection.isWrapperTypeClassName("Byte"));
        assertTrue(Introspection.isWrapperTypeClassName("Character"));
        assertTrue(Introspection.isWrapperTypeClassName("Boolean"));
        assertTrue(Introspection.isWrapperTypeClassName("Void"));
        assertTrue(Introspection.isWrapperTypeClassName("Double"));
        assertTrue(Introspection.isWrapperTypeClassName("Float"));

        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Long"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Integer"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Short"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Byte"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Character"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Boolean"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Void"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Double"));
        assertTrue(Introspection.isWrapperTypeClassName("java.lang.Float"));

        assertFalse(Introspection.isWrapperTypeClassName(null));
    }

    @Test
    void testWrap() {
        assertEquals(null, Introspection.wrap(null));
        assertEquals(String.class, Introspection.wrap(String.class));

        assertEquals(Void.class, Introspection.wrap(void.class));
        assertEquals(Boolean.class, Introspection.wrap(boolean.class));
        assertEquals(Character.class, Introspection.wrap(char.class));
        assertEquals(Long.class, Introspection.wrap(long.class));
        assertEquals(Integer.class, Introspection.wrap(int.class));
        assertEquals(Short.class, Introspection.wrap(short.class));
        assertEquals(Byte.class, Introspection.wrap(byte.class));
        assertEquals(Double.class, Introspection.wrap(double.class));
        assertEquals(Float.class, Introspection.wrap(float.class));

        assertEquals(Void.class, Introspection.wrap(Void.class));
        assertEquals(Boolean.class, Introspection.wrap(Boolean.class));
        assertEquals(Character.class, Introspection.wrap(Character.class));
        assertEquals(Long.class, Introspection.wrap(Long.class));
        assertEquals(Integer.class, Introspection.wrap(Integer.class));
        assertEquals(Short.class, Introspection.wrap(Short.class));
        assertEquals(Byte.class, Introspection.wrap(Byte.class));
        assertEquals(Double.class, Introspection.wrap(Double.class));
        assertEquals(Float.class, Introspection.wrap(Float.class));
    }

    @Test
    void testUnwrap() {
        assertEquals(null, Introspection.unwrap(null));
        assertEquals(String.class, Introspection.unwrap(String.class));

        assertEquals(void.class, Introspection.unwrap(void.class));
        assertEquals(boolean.class, Introspection.unwrap(boolean.class));
        assertEquals(char.class, Introspection.unwrap(char.class));
        assertEquals(long.class, Introspection.unwrap(long.class));
        assertEquals(int.class, Introspection.unwrap(int.class));
        assertEquals(short.class, Introspection.unwrap(short.class));
        assertEquals(byte.class, Introspection.unwrap(byte.class));
        assertEquals(double.class, Introspection.unwrap(double.class));
        assertEquals(float.class, Introspection.unwrap(float.class));

        assertEquals(void.class, Introspection.unwrap(Void.class));
        assertEquals(boolean.class, Introspection.unwrap(Boolean.class));
        assertEquals(char.class, Introspection.unwrap(Character.class));
        assertEquals(long.class, Introspection.unwrap(Long.class));
        assertEquals(int.class, Introspection.unwrap(Integer.class));
        assertEquals(short.class, Introspection.unwrap(Short.class));
        assertEquals(byte.class, Introspection.unwrap(Byte.class));
        assertEquals(double.class, Introspection.unwrap(Double.class));
        assertEquals(float.class, Introspection.unwrap(Float.class));
    }

    @Test
    void testHasMethod() {
        assertTrue(Introspection.hasMethod(getClass(), "testHasMethod"));
        assertTrue(Introspection.hasMethod(getClass(), "toString"));
        assertTrue(Introspection.hasMethod(getClass(), "hashCode"));
        assertFalse(Introspection.hasMethod(getClass(), "foo"));

        assertTrue(Introspection.hasMethod(C1.class, "getI1"));
        assertTrue(Introspection.hasMethod(C2.class, "getI1"));
        assertFalse(Introspection.hasMethod(C1.class, "getD1"));
        assertTrue(Introspection.hasMethod(C2.class, "getD1"));

        assertTrue(Introspection.hasMethod(I1.class, "foo"));
        assertTrue(Introspection.hasMethod(I2.class, "foo"));
        assertTrue(Introspection.hasMethod(I3.class, "foo"));
        assertTrue(Introspection.hasMethod(I4.class, "foo"));

        assertFalse(Introspection.hasMethod(I1.class, "bar2"));
        assertTrue(Introspection.hasMethod(I2.class, "bar2"));
        assertFalse(Introspection.hasMethod(I3.class, "bar2"));
        assertTrue(Introspection.hasMethod(I4.class, "bar2"));

        assertFalse(Introspection.hasMethod(I1.class, "bar3"));
        assertFalse(Introspection.hasMethod(I2.class, "bar3"));
        assertTrue(Introspection.hasMethod(I3.class, "bar3"));
        assertTrue(Introspection.hasMethod(I4.class, "bar3"));

        assertFalse(Introspection.hasMethod(I1.class, "bar4"));
        assertFalse(Introspection.hasMethod(I2.class, "bar4"));
        assertFalse(Introspection.hasMethod(I3.class, "bar4"));
        assertTrue(Introspection.hasMethod(I4.class, "bar4"));
    }

    @Test
    void testHasField() {
        assertTrue(Introspection.hasField(C1.class, "i1"));
        assertTrue(Introspection.hasField(C2.class, "i1"));
        assertFalse(Introspection.hasField(C1.class, "d1"));
        assertTrue(Introspection.hasField(C2.class, "d1"));
    }

    @Test
    void testHasConstructor() {
        assertTrue(Introspection.hasConstructor(C1.class));
        assertTrue(Introspection.hasConstructor(C1.class, int.class));
        assertFalse(Introspection.hasConstructor(C1.class, double.class));
        assertTrue(Introspection.hasConstructor(C2.class));
        assertTrue(Introspection.hasConstructor(C2.class, int.class, double.class));
    }

    @Test
    void testGetSuperClasses() {
        assertEquals(Arrays.asList(), Introspection.getSuperClasses(Object.class, false));
        assertEquals(Arrays.asList(), Introspection.getSuperClasses(Serializable.class, false));
        assertEquals(Arrays.asList(Object.class), Introspection.getSuperClasses(Void.class, false));
        assertEquals(Arrays.asList(Number.class, Object.class), Introspection.getSuperClasses(Integer.class, false));
        assertEquals(Arrays.asList(), Introspection.getSuperClasses(int.class, false));
        assertEquals(Arrays.asList(Object.class), Introspection.getSuperClasses(String.class, false));

        assertEquals(Arrays.asList(Object.class), Introspection.getSuperClasses(C1.class, false));
        assertEquals(Arrays.asList(C1.class, Object.class), Introspection.getSuperClasses(C2.class, false));
    }

    @Test
    void testGetInterfaces() {
        assertEquals(Set.of(), Introspection.getInterfaces(Object.class));
        assertTrue(Introspection.getInterfaces(String.class)
                                .containsAll(Set.of(Serializable.class, Comparable.class, CharSequence.class)));
        assertEquals(Set.of(Comparable.class), Introspection.getInterfaces(Comparable.class));

        assertEquals(Set.of(I1.class), Introspection.getInterfaces(I1.class));
        assertEquals(Set.of(I1.class, I2.class), Introspection.getInterfaces(I2.class));
        assertEquals(Set.of(I1.class, I3.class), Introspection.getInterfaces(I3.class));
        assertEquals(Set.of(I1.class, I2.class, I3.class, I4.class), Introspection.getInterfaces(I4.class));
    }

    @Test
    void testGetAllFields() {
        // Instrumentation (e.g., coverage) can change the number of fields
        assertTrue(Introspection.getAllFields(C1.class).size() >= 3);
        assertTrue(Introspection.getAllFields(C2.class).size() >= 4);
    }

    @Test
    void testGetAllMethods() {
        // Instrumentation (e.g., coverage) can change the number of fields
        assertTrue(Introspection.getAllMethods(C1.class).size() > 3);
        assertTrue(Introspection.getAllMethods(C2.class).size() >= Introspection.getAllMethods(C1.class).size() + 1);
    }

    private static void checkTraverseAllInterfaces(final Set<Class<?>> expected,
                                                   Class<?> cls) {
        final Set<Class<?>> found = new HashSet<>();
        Introspection.traverseAllInterfaces(cls, c -> found.add(c));
        assertEquals(expected, found);
    }

    @Test
    void testTraverseAllInterfaces() {
        checkTraverseAllInterfaces(Set.of(), C1.class);
        checkTraverseAllInterfaces(Set.of(), C2.class);
        checkTraverseAllInterfaces(Set.of(I1.class), I1.class);
        checkTraverseAllInterfaces(Set.of(I1.class, I2.class), I2.class);
        checkTraverseAllInterfaces(Set.of(I1.class, I3.class), I3.class);
        checkTraverseAllInterfaces(Set.of(I1.class, I2.class, I3.class, I4.class), I4.class);
    }

    private static void checkTraverseAllSuperClass(Set<Class<?>> expected,
                                                   Class<?> cls) {
        final Set<Class<?>> found = new HashSet<>();
        Introspection.traverseAllSuperClasses(cls, false, c -> found.add(c));
        assertEquals(expected, found);
    }

    @Test
    void testTraverseAllSuperClasses() {
        checkTraverseAllSuperClass(Set.of(), I1.class);
        checkTraverseAllSuperClass(Set.of(), I2.class);
        checkTraverseAllSuperClass(Set.of(), I4.class);
        checkTraverseAllSuperClass(Set.of(), Object.class);
        checkTraverseAllSuperClass(Set.of(Object.class), C1.class);
        checkTraverseAllSuperClass(Set.of(Object.class, C1.class), C2.class);
    }

    @Test
    void testGetClassPart() {
        assertEquals("String", Introspection.getClassPart(String.class));
        assertEquals("Map.Entry", Introspection.getClassPart(Map.Entry.class));

        assertEquals("Foo", Introspection.getClassPart("Foo"));
        assertEquals("Foo$Bar", Introspection.getClassPart("Foo$Bar"));
        assertEquals("Foo", Introspection.getClassPart("bar/Foo"));
        assertEquals("Foo$Bar", Introspection.getClassPart("bar/Foo$Bar"));

    }

    @Test
    void testGetPackagePart() {
        assertEquals("java.lang", Introspection.getPackagePart(String.class));
        assertEquals("java.util", Introspection.getPackagePart(Map.Entry.class));

        assertEquals("", Introspection.getPackagePart("Foo"));
        assertEquals("", Introspection.getPackagePart("Foo$Bar"));
        assertEquals("bar", Introspection.getPackagePart("bar/Foo"));
        assertEquals("bar", Introspection.getPackagePart("bar/Foo$Bar"));
    }

    @Test
    void testGetClass() {
        assertEquals(null, Introspection.getClass(""));
        assertEquals(null, Introspection.getClass(null));

        assertEquals(String.class, Introspection.getClass("String"));
        assertEquals(String.class, Introspection.getClass("java.lang.String"));
        assertEquals(Map.class, Introspection.getClass("java.util.Map"));
        assertEquals(Map.Entry.class, Introspection.getClass("java.util.Map.Entry"));
        assertEquals(Map.Entry.class, Introspection.getClass("java.util.Map$Entry"));
        assertEquals(null, Introspection.getClass("Foo"));

        assertEquals(String.class, Introspection.getClass("String", Object.class));
        assertEquals(String.class, Introspection.getClass("String", Object.class, FailureReaction.FAIL));

        assertEquals(null, Introspection.getClass("", Object.class));
        assertEquals(null, Introspection.getClass(null, Object.class));

        assertThrows(NotFoundException.class,
                     () -> {
                         Introspection.getClass("Blah", Object.class, FailureReaction.FAIL);
                     });
        assertEquals(null, Introspection.getClass("Blah", Object.class, FailureReaction.WARN));
        assertEquals(null, Introspection.getClass("Blah", Object.class, FailureReaction.DEFAULT));
    }

    @Test
    void testGetArrayClass() throws ClassNotFoundException {
        assertEquals(String[].class, Introspection.getArrayClass(String.class));
        assertEquals(String[][].class, Introspection.getArrayClass(String[].class));

        assertEquals(Long[].class, Introspection.getArrayClass(Long.class));
        assertEquals(long[].class, Introspection.getArrayClass(long.class));
        assertEquals(long[][].class, Introspection.getArrayClass(long[].class));

        assertEquals(Integer[].class, Introspection.getArrayClass(Integer.class));
        assertEquals(int[].class, Introspection.getArrayClass(int.class));

        assertEquals(Short[].class, Introspection.getArrayClass(Short.class));
        assertEquals(short[].class, Introspection.getArrayClass(short.class));

        assertEquals(Byte[].class, Introspection.getArrayClass(Byte.class));
        assertEquals(byte[].class, Introspection.getArrayClass(byte.class));

        assertEquals(Boolean[].class, Introspection.getArrayClass(Boolean.class));
        assertEquals(boolean[].class, Introspection.getArrayClass(boolean.class));

        assertEquals(Character[].class, Introspection.getArrayClass(Character.class));
        assertEquals(char[].class, Introspection.getArrayClass(char.class));

        assertEquals(Double[].class, Introspection.getArrayClass(Double.class));
        assertEquals(double[].class, Introspection.getArrayClass(double.class));

        assertEquals(Float[].class, Introspection.getArrayClass(Float.class));
        assertEquals(float[].class, Introspection.getArrayClass(float.class));

        assertEquals(Void[].class, Introspection.getArrayClass(Void.class));
        assertEquals(null, Introspection.getArrayClass(void.class));
    }

    @Test
    void testMostSpecialized() {
        assertEquals(null, Introspection.mostSpecialized(String.class, Integer.class));
        assertEquals(null, Introspection.mostSpecialized(Integer.class, String.class));
        assertEquals(Integer.class, Introspection.mostSpecialized(Number.class, Integer.class));
        assertEquals(Integer.class, Introspection.mostSpecialized(Integer.class, Number.class));
        assertEquals(Float.class, Introspection.mostSpecialized(Float.class, Float.class));
        assertEquals(float.class, Introspection.mostSpecialized(float.class, float.class));
        assertEquals(null, Introspection.mostSpecialized(Float.class, float.class));
        assertEquals(null, Introspection.mostSpecialized(float.class, Float.class));
        assertEquals(null, Introspection.mostSpecialized(Float.class, Double.class));
    }

    @Test
    void testMostSpecializedWithWrap() {
        assertEquals(null, Introspection.mostSpecializedWithWrap(String.class, Integer.class));
        assertEquals(String.class, Introspection.mostSpecializedWithWrap(String.class, Object.class));
        assertEquals(String.class, Introspection.mostSpecializedWithWrap(Object.class, String.class));
        assertEquals(Float.class, Introspection.mostSpecializedWithWrap(Float.class, Float.class));
        assertEquals(float.class, Introspection.mostSpecializedWithWrap(float.class, float.class));
        assertEquals(Float.class, Introspection.mostSpecializedWithWrap(Float.class, float.class));
        assertEquals(Float.class, Introspection.mostSpecializedWithWrap(float.class, Float.class));
        assertEquals(Float.class, Introspection.mostSpecializedWithWrap(Number.class, float.class));
        assertEquals(Float.class, Introspection.mostSpecializedWithWrap(float.class, Number.class));
    }

    @Test
    void testMostGeneralized() {
        assertEquals(null, Introspection.mostGeneralized(String.class, Integer.class));
        assertEquals(null, Introspection.mostGeneralized(Integer.class, String.class));
        assertEquals(Number.class, Introspection.mostGeneralized(Number.class, Integer.class));
        assertEquals(Number.class, Introspection.mostGeneralized(Integer.class, Number.class));
        assertEquals(Float.class, Introspection.mostGeneralized(Float.class, Float.class));
        assertEquals(float.class, Introspection.mostGeneralized(float.class, float.class));
        assertEquals(null, Introspection.mostGeneralized(Float.class, float.class));
    }

    @Test
    void testMostGeneralizedWithWrap() {
        assertEquals(null, Introspection.mostGeneralizedWithWrap(String.class, Integer.class));
        assertEquals(Object.class, Introspection.mostGeneralizedWithWrap(String.class, Object.class));
        assertEquals(Object.class, Introspection.mostGeneralizedWithWrap(Object.class, String.class));
        assertEquals(Float.class, Introspection.mostGeneralizedWithWrap(Float.class, Float.class));
        assertEquals(float.class, Introspection.mostGeneralizedWithWrap(float.class, float.class));
        assertEquals(Float.class, Introspection.mostGeneralizedWithWrap(Float.class, float.class));
    }

    @Test
    void testNewArray() {
        final int size = 10;
        final Integer[] integerArray = Introspection.newArray(Integer.class, size);
        assertEquals(size, integerArray.length);
    }
}