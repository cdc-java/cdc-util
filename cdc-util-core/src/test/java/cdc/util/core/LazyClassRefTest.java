package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.util.lang.FailureReaction;
import cdc.util.refs.ClassRef;
import cdc.util.refs.LazyClassRef;
import cdc.util.refs.ResolutionException;
import cdc.util.refs.ResolutionStatus;

class LazyClassRefTest {
    @Test
    void testValidConstructor() {
        final ClassRef x = new LazyClassRef("String");
        assertEquals(ResolutionStatus.PENDING, x.getResolutionStatus());
        assertEquals(String.class, x.get(FailureReaction.WARN));
        assertEquals(ResolutionStatus.SUCCESS, x.getResolutionStatus());
        assertEquals(String.class, x.get(FailureReaction.WARN));
        assertEquals(ResolutionStatus.SUCCESS, x.getResolutionStatus());
    }

    @Test
    void testWarnConstructor() {
        final ClassRef x = new LazyClassRef("Foo");
        assertEquals(ResolutionStatus.PENDING, x.getResolutionStatus());
        x.get(FailureReaction.WARN);
        assertEquals(ResolutionStatus.FAILURE, x.getResolutionStatus());
        x.get(FailureReaction.DEFAULT);
        assertEquals(ResolutionStatus.FAILURE, x.getResolutionStatus());
    }

    void testErrorConstructor() {
        assertThrows(ResolutionException.class, () -> {
            final ClassRef x = new LazyClassRef("Foo");
            assertEquals(ResolutionStatus.PENDING, x.getResolutionStatus());
            x.get(FailureReaction.FAIL);
        });
    }
}