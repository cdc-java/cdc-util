package cdc.util.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.util.lang.CollectionUtils;
import cdc.util.strings.KeyValue;
import cdc.util.strings.KeyValueSplitter;

class KeyValueSplitterTest {
    @SafeVarargs
    private static <T> List<T> toList(T... values) {
        return CollectionUtils.toList(values);
    }

    private static KeyValue kv(String key,
                               String value) {
        return new KeyValue(key, value);
    }

    private static void test(List<String> keys,
                             String input,
                             KeyValue... expected) {
        final List<KeyValue> found = KeyValueSplitter.split(input, keys);
        assertEquals(Arrays.asList(expected), found);
    }

    @Test
    void test0() {
        test(toList(), "hello");
    }

    @Test
    void test1() {
        test(toList("K:"), "");
        test(toList("K:"), "K:", kv("K:", ""));
        test(toList("K:"), "K:V1K:V2", kv("K:", "V1"), kv("K:", "V2"));
        test(toList("K:"), "K:V1K:V2K:V3", kv("K:", "V1"), kv("K:", "V2"), kv("K:", "V3"));
        test(toList("K:"), "K:K", kv("K:", "K"));
        test(toList("K:"), "K:K:", kv("K:", ""), kv("K:", ""));
        test(toList("K:"), "K:V1K:K:V3", kv("K:", "V1"), kv("K:", ""), kv("K:", "V3"));
    }

    @Test
    void test2() {
        test(toList("K1:", "K2:"), "");
        test(toList("K1:", "K2:"), "K1:V1K2:V2", kv("K1:", "V1"), kv("K2:", "V2"));
    }
}