package cdc.util.refs;

import cdc.util.lang.FailureReaction;

/**
 * Interface giving access to an object that is known by a key.
 * <p>
 * Implementation can resolve the key immediately or lazily.<br>
 * The associated object can not be {@code null}.
 *
 * @author Damien Carbonne
 *
 * @param <K> The key type.
 * @param <T> The object type.
 */
public interface Ref<K, T> {
    /**
     * @return The key identifying the referenced object.
     */
    public K getKey();

    /**
     * @return The current resolution status of this reference.
     */
    public ResolutionStatus getResolutionStatus();

    /**
     * Returns the object corresponding to {@link #getKey()}.
     * <p>
     * If resolution fails, then status is set to {@link ResolutionStatus#FAILURE FAILURE}.<br>
     * Then, if reaction is {@link FailureReaction#FAIL FAIL}, {@link ResolutionException}
     * is raised, otherwise {@code null} is returned.<br>
     * If resolution succeeds, status is set to {@link ResolutionStatus#SUCCESS SUCCESS}
     * and the object corresponding to {@link #getKey()} is returned.
     *
     * @param reaction The reaction to adopt in case of error.
     *
     * @return The object corresponding to {@link #getKey()}.
     * @throws ResolutionException When {@code reaction} is {@link FailureReaction#FAIL FAIL}
     *             and resolution fails.
     */
    public T get(FailureReaction reaction);

    /**
     * @return The object corresponding to {@link #getKey()}.<br>
     *         This is equivalent to {@code get(FailureReaction.FAIL)}.
     * @throws ResolutionException When resolution fails.
     */
    public default T get() {
        return get(FailureReaction.FAIL);
    }
}