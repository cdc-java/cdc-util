package cdc.util.refs;

/**
 * Enumeration of possible resolution statuses.
 *
 * @author Damien Carbonne
 */
public enum ResolutionStatus {
    /**
     * Resolution was not yet tried.
     */
    PENDING,

    /**
     * Resolution was tried and was successful.
     */
    SUCCESS,

    /**
     * Resolution was tried and failed.
     */
    FAILURE
}