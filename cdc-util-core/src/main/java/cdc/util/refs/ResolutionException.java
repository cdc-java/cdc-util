package cdc.util.refs;

public class ResolutionException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ResolutionException(String message) {
        super(message);
    }
}