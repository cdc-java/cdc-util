package cdc.util.refs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Implementation of ClassRef that can lazily resolve class.
 * <p>
 * If Eager resolution is set ({@link AbstractLazyRef#setEager}),
 * then resolution is done during ClassRef instantiation.
 *
 * @author Damien Carbonne
 */
public final class LazyClassRef extends AbstractLazyRef<String, Class<?>> implements ClassRef {
    private static final Logger LOGGER = LogManager.getLogger(LazyClassRef.class);

    public LazyClassRef(String className) {
        super(className);

        if (eager) {
            get(FailureReaction.WARN);
        }
    }

    @Override
    protected Class<?> doResolve(String key) {
        return Introspection.getClass(key, FailureReaction.DEFAULT);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public <T> Class<? extends T> get(Class<T> cls,
                                      FailureReaction reaction) {
        return ClassRef.get(this, cls, reaction, getLogger());
    }
}