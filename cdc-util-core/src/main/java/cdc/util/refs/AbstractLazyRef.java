package cdc.util.refs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

public abstract class AbstractLazyRef<K, T> extends AbstractRef<K, T> {
    private static final Logger LOGGER = LogManager.getLogger(AbstractLazyRef.class);
    private final K key;
    private T result = null;
    private ResolutionStatus status = ResolutionStatus.PENDING;
    protected static boolean eager = false;

    protected AbstractLazyRef(K key) {
        Checks.isNotNull(key, "key");
        this.key = key;
    }

    protected abstract T doResolve(K key);

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public final K getKey() {
        return key;
    }

    @Override
    public final ResolutionStatus getResolutionStatus() {
        return status;
    }

    @Override
    public final T get(FailureReaction reaction) {
        if (status == ResolutionStatus.PENDING) {
            result = doResolve(key);
            if (result == null) {
                status = ResolutionStatus.FAILURE;
            } else {
                status = ResolutionStatus.SUCCESS;
            }
        }

        if (status == ResolutionStatus.FAILURE) {
            if (reaction == FailureReaction.FAIL) {
                throw new ResolutionException("Failed to resolve " + key);
            } else if (reaction == FailureReaction.WARN) {
                LOGGER.warn("Failed to resolve {}", key);
                return null;
            } else {
                return null;
            }
        } else {
            return result;
        }
    }

    public static void setEager(boolean eager) {
        AbstractLazyRef.eager = eager;
    }

    public static boolean isEager() {
        return AbstractLazyRef.eager;
    }
}