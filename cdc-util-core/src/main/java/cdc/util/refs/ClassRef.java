package cdc.util.refs;

import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

/**
 * Interface giving access to a class in a customizable way.
 * <p>
 * Whether class resolution is immediate or deferred depends on the implementation.
 *
 * @author Damien Carbonne
 */
public interface ClassRef extends Ref<String, Class<?>> {
    /**
     * @return The name of the class to resolve.<br>
     *         This is equivalent to {@link #getKey()}.
     */
    public default String getClassName() {
        return getKey();
    }

    public <T> Class<? extends T> get(Class<T> cls,
                                      FailureReaction reaction);

    public default <T> Class<? extends T> get(Class<T> cls) {
        return get(cls, FailureReaction.FAIL);
    }

    public static <T> Class<? extends T> get(ClassRef ref,
                                             Class<T> cls,
                                             FailureReaction reaction,
                                             Logger logger) {
        Checks.isNotNull(cls, "cls");
        final Class<?> raw = ref.get(reaction);
        if (raw == null) {
            // resolve failed and reaction was WARN
            return null;
        } else if (cls.isAssignableFrom(raw)) {
            @SuppressWarnings("unchecked")
            final Class<? extends T> result = (Class<? extends T>) raw;
            return result;
        } else {
            // Cannot convert to expected class
            if (reaction == FailureReaction.DEFAULT) {
                return null;
            } else if (reaction == FailureReaction.WARN) {
                logger.warn("Can not cast {} to {}", raw.getCanonicalName(), cls.getCanonicalName());
                return null;
            } else {
                throw new ClassCastException("Can not cast " + raw.getCanonicalName() + " to " + cls.getCanonicalName());
            }
        }
    }
}