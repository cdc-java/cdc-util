package cdc.util.refs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

/**
 * Base implementation of eager reference.
 *
 * @author Damien Carbonne
 *
 * @param <K> The key type.
 * @param <T> The object type.
 */
public abstract class AbstractEagerRef<K, T> extends AbstractRef<K, T> {
    private static final Logger LOGGER = LogManager.getLogger(AbstractEagerRef.class);
    protected final T object;

    protected AbstractEagerRef(T object) {
        Checks.isNotNull(object, "object");
        this.object = object;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public final ResolutionStatus getResolutionStatus() {
        return ResolutionStatus.SUCCESS;
    }

    @Override
    public final T get(FailureReaction reaction) {
        return object;
    }
}