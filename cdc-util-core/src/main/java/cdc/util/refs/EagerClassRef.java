package cdc.util.refs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;

/**
 * Eager implementation of ClassRef.
 *
 * @author Damien Carbonne
 */
public final class EagerClassRef extends AbstractEagerRef<String, Class<?>> implements ClassRef {
    private static final Logger LOGGER = LogManager.getLogger(EagerClassRef.class);

    public EagerClassRef(Class<?> cls) {
        super(cls);
    }

    @Override
    public String getKey() {
        return object.getName();
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public <T> Class<? extends T> get(Class<T> cls,
                                      FailureReaction reaction) {
        return ClassRef.get(this, cls, reaction, getLogger());
    }
}