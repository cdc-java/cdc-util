package cdc.util.refs;

import cdc.util.lang.Checks;

/**
 * Eager implementation of {@link Ref}.
 * <p>
 * This implementation stores the key.<br>
 * If key can be extracted from object, a better implementation is possible.
 *
 * @author Damien Carbonne
 *
 * @param <K> The key type.
 * @param <T> The object type.
 */
public final class EagerRef<K, T> extends AbstractEagerRef<K, T> {
    public final K key;

    public EagerRef(K key,
                    T object) {
        super(object);
        Checks.isNotNull(key, "key");

        this.key = key;
    }

    @Override
    public K getKey() {
        return key;
    }
}