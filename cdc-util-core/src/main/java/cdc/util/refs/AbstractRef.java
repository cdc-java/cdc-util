package cdc.util.refs;

import org.apache.logging.log4j.Logger;

/**
 * Base implementation of Ref.
 *
 * @author Damien Carbonne
 *
 * @param <K> The key type.
 * @param <T> The object type.
 */
public abstract class AbstractRef<K, T> implements Ref<K, T> {
    protected abstract Logger getLogger();

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getKey())
               .append(' ')
               .append(getResolutionStatus());
        return builder.toString();
    }
}