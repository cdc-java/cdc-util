package cdc.util.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.BlackHole;

public final class Files {
    private static final Logger LOGGER = LogManager.getLogger(Files.class);
    private static final Pattern NORMALIZE_PATTERN = Pattern.compile("\\\\");

    private Files() {
    }

    /**
     * Returns a path that moves up count times.
     *
     * <pre>
     * <code>
     * toUpPath(0) = "./"
     * toUpPath(1) = "./../"
     * toUpPath(2) = "./../../"
     * ...
     * </code>
     * </pre>
     *
     * @param count The number of times path must move up.
     * @return A path that moves up count times.
     */
    public static String toUpPath(int count) {
        if (count < 0) {
            throw new IllegalArgumentException();
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append("./");
            for (int index = 0; index < count; index++) {
                builder.append("../");
            }
            return builder.toString();
        }
    }

    public static File toFile(URL url) {
        if (url == null) {
            LOGGER.error("toFile({}) FAILED", url);
            return null;
        }
        try {
            final File file = Paths.get(url.toURI()).toFile();
            if (file == null) {
                LOGGER.error("toFile({}) FAILED", url);
            }
            return file;
        } catch (final Exception e) {
            LOGGER.error("toFile({}) FAILED, {}", url, e.getMessage());
            return null;
        }
    }

    public static void deleteFile(String filename) {
        final File file = new File(filename);
        if (file.exists()) {
            final boolean success = file.delete();
            if (!success) {
                LOGGER.trace("deleteFile({}) Failed", filename);
            }
        }
    }

    public static void copyFile(String source,
                                String dir) throws IOException {
        final java.nio.file.Path src = java.nio.file.Paths.get(source);
        final java.nio.file.Path tgt = java.nio.file.Paths.get(dir);
        java.nio.file.Files.copy(src, tgt, java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }

    public static void copyFile(File source,
                                File target) throws IOException {
        LOGGER.trace("copyFile({}, {})", source, target);
        final java.nio.file.Path src = java.nio.file.Paths.get(source.toURI());
        final java.nio.file.Path tgt = java.nio.file.Paths.get(target.toURI());
        java.nio.file.Files.copy(src, tgt, java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }

    public static void copyFiles(Collection<String> sources,
                                 String dir) throws IOException {
        for (final String source : sources) {
            copyFile(source, dir);
        }
    }

    public static File currentDir() {
        return new File(Paths.get(".").toAbsolutePath().normalize().toString());
    }

    public static void mkdir(String dirname) {
        final File file = new File(dirname);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    public static String normalize(String s) {
        return NORMALIZE_PATTERN.matcher(s).replaceAll("/");
    }

    /**
     * Replaces some characters in a name to make it a <em>valid</em> base name.
     * <p>
     * <b>WARNING:</b> This does not ensure that the resulting string is really valid in all cases.<br>
     * For example, Windows specific names like COM or PRN are not detected.<br>
     * A {@code null} or empty input won't be changed, resulting in fact on an invalid base name.
     * <p>
     * Result is OS independent. Typically, on UNIX, some unnecessary changes are made.
     *
     * @param name The name.
     * @return The modified version of {@code name}.
     */
    public static String toValidBasename(String name) {
        if (name == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            for (int index = 0; index < name.length(); index++) {
                final char c = name.charAt(index);
                switch (c) {
                case '\\':
                case '/':
                case ':':
                case '*':
                case '?':
                case '"':
                case '<':
                case '>':
                case '|':
                case '\0':
                    builder.append("-");
                    break;
                default:
                    builder.append(c);
                    break;
                }
            }
            return builder.toString();
        }
    }

    public static String toValidRelativePath(String path) {
        if (path == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            for (int index = 0; index < path.length(); index++) {
                final char c = path.charAt(index);
                switch (c) {
                case ':':
                case '*':
                case '?':
                case '"':
                case '<':
                case '>':
                case '|':
                case '\0':
                    builder.append("-");
                    break;
                default:
                    builder.append(c);
                    break;
                }
            }
            return builder.toString();
        }
    }

    public static String getDirname(String filename) {
        final File file = new File(filename);
        return file.getParent();
    }

    public static String getBasename(File file) {
        return file.getName();
    }

    public static String getBasename(String filename) {
        return getBasename(new File(filename));
    }

    public static String getNakedBasename(File file) {
        final String basename = getBasename(file);
        final int pos = basename.lastIndexOf('.');
        if (pos >= 0) {
            return basename.substring(0, pos);
        } else {
            return basename;
        }
    }

    public static String getNakedBasename(String filename) {
        return getNakedBasename(new File(filename));
    }

    public static String getExtension(File file) {
        final String basename = getBasename(file);
        final int pos = basename.lastIndexOf('.');
        if (pos >= 0) {
            return basename.substring(pos + 1);
        } else {
            return null;
        }
    }

    public static String getExtension(String filename) {
        return getExtension(new File(filename));
    }

    public static List<String> split(File file) {
        final List<String> result = new ArrayList<>();
        File index = file;
        do {
            result.add(index.getName());
            index = index.getParentFile();
        } while (index != null);
        Collections.reverse(result);
        return result;
    }

    public static File toFile(List<String> names) {
        if (names == null || names.isEmpty()) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (final String name : names) {
                if (first) {
                    first = false;
                } else {
                    builder.append(File.separatorChar);
                }
                builder.append(name);
            }
            return new File(builder.toString());
        }
    }

    /**
     * Returns true if file1 is newer that file2.
     *
     * @param file1 First file.
     * @param file2 Second file.
     * @return {@code true} if {@code file1} is newer than {@code file2}.
     */
    public static boolean isNewerThan(File file1,
                                      File file2) {
        LOGGER.trace("isNewerThan({}, {})", file1, file2);
        final long modified1 = file1.lastModified();
        final long modified2 = file2.lastModified();

        LOGGER.trace("   {}", (modified1 >= modified2));
        return modified1 >= modified2;
    }

    public static long length(File file) {
        try (final RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            return raf.length();
        } catch (final IOException e) {
            LOGGER.catching(e);
            return -1;
        }
    }

    private static boolean areEqual(BufferedReader reader1,
                                    BufferedReader reader2) {
        try {
            String s1 = null;
            while ((s1 = reader1.readLine()) != null) {
                final String s2 = reader2.readLine();
                if (!s1.equals(s2)) {
                    return false;
                }
            }
            if ((s1 = reader2.readLine()) == null) {
                BlackHole.discard(s1);
                return true;
            }
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        return false;
    }

    public static boolean haveSameContent(File file1,
                                          File file2) {
        if (length(file1) == length(file2)) {
            try (final BufferedReader reader1 = new BufferedReader(new FileReader(file1));
                    final BufferedReader reader2 = new BufferedReader(new FileReader(file2))) {
                return areEqual(reader1, reader2);
            } catch (final IOException e) {
                BlackHole.discard(e);
                return false;
            }
        } else {
            return false;
        }
    }

    public static String readFileAsString(File file,
                                          Charset encoding) throws IOException {
        final byte[] encoded = java.nio.file.Files.readAllBytes(Paths.get(file.getPath()));
        return new String(encoded, encoding);
    }

    public static void cat(File file,
                           Charset encoding,
                           Logger logger,
                           Level level) throws IOException {
        if (logger.isEnabled(level)) {
            logger.log(level, "{}\n{}", file, readFileAsString(file, encoding));
        }
    }
}