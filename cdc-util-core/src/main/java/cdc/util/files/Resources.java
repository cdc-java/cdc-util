package cdc.util.files;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.debug.Debug;

public final class Resources {
    private static final Logger LOGGER = LogManager.getLogger(Resources.class);

    private Resources() {
    }

    /**
     * Return a list of all URLs corresponding to a name.
     *
     * @param name The name
     * @return A list of URLs named {@code name}.
     */
    private static List<URL> getResourcesInternal(String name) {
        LOGGER.trace("getResourcesInternal({})", name);
        final List<URL> list = new ArrayList<>();
        try {
            final Enumeration<URL> tmp = Resources.class.getClassLoader().getResources(name);
            while (tmp.hasMoreElements()) {
                list.add(tmp.nextElement());
            }
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        return list;
    }

    private static URL getResourceInternal(String name) {
        return Resources.class.getResource(name);
    }

    /**
     * Converts an existing file to an URL.
     *
     * @param file The file
     * @return The corresponding URL or {@code null} if {@code file} does not exist or conversion failed.
     */
    public static URL toURLIfExists(File file) {
        LOGGER.trace("toURL({})", file);
        if (file.exists()) {
            try {
                return file.toURI().toURL();
            } catch (final Exception e) {
                LOGGER.error("ToURL({}) FAILED, {}", file, e.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Returns the URL corresponding to a name.
     * <p>
     * Search is done in that order (name being normalized):
     * <ol>
     * <li>An existing file in the file system</li>
     * <li>A resource in class path matching name</li>
     * <li>A resource in class path matching /name</li>
     * <li>A resource in class path matching file://name</li>
     * </ol>
     * If no URL is found, a warning is printed.
     *
     * @param name The name.
     * @return An URL matching {@code name} or {@code null}.
     */
    public static URL getResource(String name) {
        return getResource(name, true);
    }

    /**
     * Returns the URL corresponding to a name.
     * <p>
     * Search is done in that order (name being normalized):
     * <ol>
     * <li>An existing file in the file system</li>
     * <li>A resource in class path matching name</li>
     * <li>A resource in class path matching /name</li>
     * <li>A resource in class path matching file://name</li>
     * </ol>
     *
     * @param name The name.
     * @param warn If true and no URL found, a warning is printed.
     * @return An URL matching {@code name} or {@code null}.
     */
    public static URL getResource(String name,
                                  boolean warn) {
        LOGGER.trace("getResource({})", name);
        final String n = Files.normalize(name);
        URL url = toURLIfExists(new File(name));
        if (url == null) {
            url = getResourceInternal(n);
        }
        if (url == null) {
            url = getResourceInternal("/" + n);
        }
        if (url == null) {
            url = getResourceInternal("file://" + n);
        }
        if (url == null) {
            if (warn) {
                LOGGER.warn("getResource({}) FAILED", name);
                Debug.printPath(LOGGER, Level.WARN);
            }
        } else {
            LOGGER.trace("   -> {}", url);
        }
        return url;
    }

    public static List<URL> getResources(String name) {
        LOGGER.trace("getResources({})", name);
        final List<URL> urls = new ArrayList<>();
        final String n = Files.normalize(name);
        final URL url = toURLIfExists(new File(name));
        if (url != null) {
            urls.add(url);
        }
        urls.addAll(getResourcesInternal(n));
        urls.addAll(getResourcesInternal("/" + n));
        urls.addAll(getResourcesInternal("file://" + n));
        if (urls.isEmpty()) {
            LOGGER.warn("getResources({}) FAILED", name);
            Debug.printPath(LOGGER, Level.WARN);
        } else {
            LOGGER.trace("   -> {}", urls);
        }
        return urls;
    }

    public static InputStream getResourceAsStream(String name) {
        LOGGER.trace("getResourceAsStream({})", name);
        final URL url = getResource(name, false);
        if (url != null) {
            try {
                return url.openStream();
            } catch (final IOException e) {
                LOGGER.warn("getResourceAsStream({}) FAILED, {}", name, e.getMessage());
                return null;
            }
        } else {
            LOGGER.warn("getResourceAsStream({}) FAILED", name);
            return null;
        }
    }

    public static String[] getResourceListing(String path) throws Exception {
        LOGGER.trace("getResourceListing({})", path);
        final List<URL> urls = getResources(path);
        final Set<String> result = new HashSet<>();
        for (final URL url : urls) {
            if ("file".equals(url.getProtocol())) {
                LOGGER.trace("File handling");
                final String[] ss = new File(url.toURI()).list();
                if (ss != null) {
                    Collections.addAll(result, ss);
                }
            } else if ("jar".equals(url.getProtocol())) {
                LOGGER.trace("Jar handling");
                final String jarPath = url.getPath().substring(5, url.getPath().indexOf('!'));
                final int pathLength = path.endsWith("/") ? path.length() : path.length() + 1;
                LOGGER.trace("Jar path: {}", jarPath);
                try (final JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"))) {
                    final Enumeration<JarEntry> entries = jar.entries();
                    while (entries.hasMoreElements()) {
                        final JarEntry entry = entries.nextElement();
                        final String name = entry.getName();
                        if (name.startsWith(path)) {
                            String s = name.substring(pathLength);
                            final int pos = s.indexOf('/');
                            if (pos >= 0) {
                                s = s.substring(0, pos);
                            }
                            if (!s.isEmpty()) {
                                result.add(s);
                            }
                        }
                    }
                }
            } else {
                LOGGER.warn("Unsupported protocol: {}", url.getProtocol());
            }
        }
        return result.toArray(new String[result.size()]);
    }

    public static void copy(String name,
                            File targetDirectory) {
        LOGGER.trace("copy({}, {})", name, targetDirectory);
        final File input = new File(name);
        final File output = new File(targetDirectory.getPath(), input.getName());

        try (final InputStream is = getResourceAsStream(name);
                final java.io.FileOutputStream fos = new java.io.FileOutputStream(output)) {
            if (is != null) {
                while (is.available() > 0) {
                    fos.write(is.read());
                }
            }
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
    }
}