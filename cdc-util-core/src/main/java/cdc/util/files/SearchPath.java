package cdc.util.files;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

/**
 * Utility used to locate files in a search path.
 *
 * @author Damien Carbonne
 *
 */
public class SearchPath {
    private static final Logger LOGGER = LogManager.getLogger(SearchPath.class);
    private final List<File> dirs;

    /**
     * Empty SearchPath.
     */
    public static final SearchPath EMPTY = new SearchPath();

    /**
     * Search path corresponding to system path.
     */
    public static final SearchPath SYSTEM_PATH = new SearchPath(System.getenv("PATH"),
                                                                File.pathSeparatorChar);

    /**
     * Creates an empty search path.
     */
    public SearchPath() {
        this.dirs = Collections.emptyList();
    }

    /**
     * Creates a SearchPath from a list of directories.
     *
     * @param dirs The directories.
     * @throws IllegalArgumentException If one passed directory exists (as a File) and is not directory.
     */
    public SearchPath(List<File> dirs) {
        final List<File> list = new ArrayList<>();
        for (final File dir : dirs) {
            if (dir.exists() && !dir.isDirectory()) {
                throw new IllegalArgumentException(dir + " exists and is not a directory");
            } else {
                list.add(dir);
            }
        }
        this.dirs = Collections.unmodifiableList(list);
    }

    /**
     * Creates a SearchPath from an array of directories.
     *
     * @param dirs The directories.
     * @throws IllegalArgumentException If one passed directory exists (as a File) and is not directory.
     */
    public SearchPath(File... dirs) {
        this(Arrays.asList(dirs));
    }

    /**
     * Creates a SearchPath from an array of directory names.
     *
     * @param dirnames The directory names.
     * @throws IllegalArgumentException If one passed directory exists (as a File) and is not directory.
     */
    public SearchPath(String... dirnames) {
        this(toFiles(dirnames));
    }

    /**
     * Creates a SearchPath initializing it with directories.
     *
     * @param dirnames String containing the list of directory names separated by {@code separator}.
     * @param separator The character used to separate directories in {@code dirnames}.
     */
    public SearchPath(String dirnames,
                      char separator) {
        this(toFiles(dirnames, separator));
    }

    /**
     * Creates a search path initializing it with directories.
     *
     * @param dirnames String containing the list of directory names separated by
     *            the OS path separator character.
     */
    public SearchPath(String dirnames) {
        this(toFiles(dirnames, File.pathSeparatorChar));
    }

    private static List<File> toFiles(String path,
                                      char separator) {
        final List<File> list = new ArrayList<>();
        final StringTokenizer tokenizer = new StringTokenizer(path, Character.toString(separator));
        while (tokenizer.hasMoreTokens()) {
            final String name = tokenizer.nextToken();
            list.add(new File(name));
        }
        return list;
    }

    private static List<File> toFiles(String... dirnames) {
        final List<File> list = new ArrayList<>();
        for (final String name : dirnames) {
            list.add(new File(name));
        }
        return list;
    }

    /**
     * Creates a new SearchPath by appending all directories of another search path.
     *
     * @param other The other search path.
     * @return A new SearchPath.
     */
    public SearchPath append(SearchPath other) {
        final List<File> list = new ArrayList<>();
        list.addAll(this.dirs);
        list.addAll(other.dirs);
        return new SearchPath(list);
    }

    /**
     * Creates a new SearchPath by appending a directory to this SearchPath.
     *
     * @param dir The directory to append.
     * @return A new SearchPath.
     * @throws IllegalArgumentException If {@code dir} is {@code null} or exists and is not a directory.
     */
    public SearchPath append(File dir) {
        return append(new SearchPath(dir));
    }

    /**
     * Creates a new SearchPath by appending all directories from a path, separating them with a separator.
     *
     * @param dirnames The directory names, separated by {@code separator}.
     * @param separator The path separator.
     * @return A new SearchPath.
     * @throws IllegalArgumentException If {@code dirnames} is null or one
     *             of designated directory names is the name of an existing
     *             file that is not a directory.
     */
    public SearchPath append(String dirnames,
                             char separator) {
        return append(new SearchPath(dirnames, separator));
    }

    /**
     * Creates a new SearchPath by appending all directories from a path, using the OS path separator.
     *
     * @param dirnames The directory names, separated by the OS path separator.
     * @return A new SearchPath.
     * @throws IllegalArgumentException If {@code dirnames} is null or one
     *             of designated directory names is the name of an existing
     *             file that is not a directory.
     */
    public SearchPath append(String dirnames) {
        return append(new SearchPath(dirnames));
    }

    /**
     * @return The list of directories of this SearchPath.
     */
    public List<File> getDirs() {
        return dirs;
    }

    /**
     * Returns the first directory in the path that contains a file.
     *
     * @param filename Name of the searched file.
     * @return The first directory that contains file named {@code filename}, or {@code null}.
     */
    public File locate(String filename) {
        if (filename != null) {
            for (final File dir : dirs) {
                final File file = new File(dir, filename);
                if (file.exists()) {
                    return dir;
                }
            }
        }
        LOGGER.warn("locate({}) failed with: {}", filename, this);
        return null;
    }

    private File resolveInt(String filename) {
        Checks.isNotNull(filename, "filename");

        for (final File dir : dirs) {
            final File file = new File(dir, filename);
            if (file.exists()) {
                return file;
            }
        }
        return null;
    }

    /**
     * Returns the first file corresponding to a file name in the path.
     * <p>
     * If none is found, returns a file corresponding to filename. If filename is {@code null},
     * returns {@code null}.
     *
     * @param filename Name of the searched file.
     * @return A matching file in the path, or a file corresponding to {@code filename}.
     */
    public File resolve(String filename) {
        if (filename == null) {
            return null;
        } else {
            File file = resolveInt(filename);
            if (file == null) {
                LOGGER.warn("resolve({}) failed with: {}", filename, this);
                file = new File(filename);
            }
            return file;
        }
    }

    /**
     * Returns the first file corresponding to a file name in this path, possibly appending extensions.
     *
     * @param filename Name of the searched file.
     * @param exts The array of optional extensions.
     * @return A matching file in the path, or a file corresponding to {@code filename}.
     */
    public File resolveExt(String filename,
                           String... exts) {
        if (filename == null) {
            return null;
        } else {
            File file = resolveInt(filename);
            int index = 0;
            while (file == null && index < exts.length) {
                final String ext = exts[index];
                file = resolveInt(filename + ext);
                index++;
            }
            if (file == null) {
                file = new File(filename);
            }
            return file;
        }
    }

    public File resolveExe(String filename) {
        return resolveExt(filename, ".exe");
    }

    public boolean findExe(String filename,
                           boolean useSystemPath) {
        if (resolveExe(filename) != null) {
            return true;
        } else {
            return useSystemPath && SYSTEM_PATH.resolveExe(filename) != null;
        }
    }

    public String toString(char separator) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final File dir : dirs) {
            if (!first) {
                builder.append(separator);
            }
            builder.append(dir.getPath());
            first = false;
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        builder.append('[');
        for (final File dir : dirs) {
            if (!first) {
                builder.append(", ");
            }
            builder.append(dir.getPath());
            first = false;
        }
        builder.append(']');
        return builder.toString();
    }
}