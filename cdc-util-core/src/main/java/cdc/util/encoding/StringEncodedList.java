package cdc.util.encoding;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StringEncodedList<E extends Encoded<String>> extends StringEncodedCollection<E> {
    protected final List<E> list = new ArrayList<>();

    public StringEncodedList() {
        super();
    }

    @Override
    public List<E> collection() {
        return list;
    }

    @Override
    public String encode() {
        return list.stream()
                   .map(e -> wrap(e.getCode()))
                   .collect(Collectors.joining(SEPARATOR));
    }
}