package cdc.util.encoding;

import java.util.HashMap;
import java.util.Map;

import cdc.util.lang.Introspection;

public final class Encoders {
    private static final Map<Class<? extends Enum<?>>, ExtensionEncoder<? extends Enum<?>, String>> ENUM_NAME_ENCODERS =
            new HashMap<>();

    public static final ExtensionEncoder<Class<?>, String> CLASS_CODE =
            new ExtensionEncoder<>(Introspection.uncheckedCast(Class.class), String.class);

    private Encoders() {
    }

    /**
     * Creates an encoder for encoded enums.
     *
     * @param <E> The encoded enum type.
     * @param <C> The code type.
     * @param enumClass The encoded enum class.
     * @param codeClass The code class.
     * @return A new Encoder for {@code enumClass}.
     */
    public static <E extends Enum<E> & Encoded<C>, C> ExtensionEncoder<E, C> encoder(Class<E> enumClass,
                                                                                     Class<C> codeClass) {
        final ExtensionEncoder<E, C> encoder = new ExtensionEncoder<>(enumClass, codeClass);
        for (final E source : enumClass.getEnumConstants()) {
            encoder.put(source, source.getCode());
        }
        encoder.lock();
        return encoder;
    }

    /**
     * Creates an encoder for enums using their name.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return A new Encoder for {@code enumClass}.
     */
    public static <E extends Enum<E>> ExtensionEncoder<E, String> nameEncoder(Class<E> enumClass) {
        final ExtensionEncoder<E, String> encoder = new ExtensionEncoder<>(enumClass, String.class);
        for (final E source : enumClass.getEnumConstants()) {
            encoder.put(source, source.name());
        }
        encoder.lock();
        return encoder;
    }

    public static ExtensionEncoder<? extends Enum<?>, String> rawNameEncoder(Class<? extends Enum<?>> enumClass) {
        final ExtensionEncoder<? extends Enum<?>, String> encoder = new ExtensionEncoder<>(enumClass, String.class);
        for (final Enum<?> source : enumClass.getEnumConstants()) {
            encoder.put(Introspection.uncheckedCast(source), source.name());
        }
        encoder.lock();
        return encoder;
    }

    public static ExtensionEncoder<? extends Enum<?>, String> getRawNameEncoder(Class<? extends Enum<?>> enumClass) {
        return ENUM_NAME_ENCODERS.computeIfAbsent(enumClass,
                                                  k -> rawNameEncoder(enumClass));
    }

    /**
     * Returns a shared name encoder instance for an enum.
     *
     * @param <E> The enum type
     * @param enumClass The enum class.
     * @return The shared name encoder instance.
     */
    public static <E extends Enum<E>> ExtensionEncoder<E, String> getNameEncoder(Class<E> enumClass) {
        final ExtensionEncoder<? extends Enum<?>, String> tmp = getRawNameEncoder(enumClass);
        @SuppressWarnings("unchecked")
        final ExtensionEncoder<E, String> result = (ExtensionEncoder<E, String>) tmp;
        return result;
    }

    /**
     * Creates an encoder for enums using their ordinal.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return A new Encoder for {@code enumClass}.
     */
    public static <E extends Enum<E>> ExtensionEncoder<E, Integer> ordinalEncoder(Class<E> enumClass) {
        final ExtensionEncoder<E, Integer> encoder = new ExtensionEncoder<>(enumClass, Integer.class);
        for (final E source : enumClass.getEnumConstants()) {
            encoder.put(source, source.ordinal());
        }
        encoder.lock();
        return encoder;
    }

    /**
     * Creates an encoder between 2 enums. Values with same names are encoded.
     *
     * @param <S> The source enum type.
     * @param <T> The target enum type.
     * @param sourceClass The source enum class.
     * @param targetClass The target enum class.
     * @return A new Encoder.
     */
    public static <S extends Enum<S>, T extends Enum<T>> ExtensionEncoder<S, T> sameNameEncoder(Class<S> sourceClass,
                                                                                                Class<T> targetClass) {
        final ExtensionEncoder<T, String> x = getNameEncoder(targetClass);

        final ExtensionEncoder<S, T> encoder = new ExtensionEncoder<>(sourceClass, targetClass);
        for (final S source : sourceClass.getEnumConstants()) {
            final T target = x.decode(source.name());
            if (target != null) {
                encoder.put(source, target);
            }
        }
        encoder.lock();
        return encoder;
    }
}