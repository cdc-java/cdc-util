package cdc.util.encoding;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class StringEncodedSet<E extends Encoded<String>> extends StringEncodedCollection<E> {
    protected final Set<E> set = new HashSet<>();

    public StringEncodedSet() {
        super();
    }

    @Override
    public Set<E> collection() {
        return set;
    }

    @Override
    public String encode() {
        return set.stream()
                  .map(e -> wrap(e.getCode()))
                  .sorted()
                  .collect(Collectors.joining(SEPARATOR));
    }
}