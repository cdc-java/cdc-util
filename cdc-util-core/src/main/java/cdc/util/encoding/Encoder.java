package cdc.util.encoding;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;

/**
 * Interface implemented by objects that can encode and decoded values.
 *
 * @author Damien Carbonne
 *
 * @param <S> The source (decoded) type.
 * @param <T> The target (encoded) type.
 */
public interface Encoder<S, T> {
    public static final Logger LOGGER = LogManager.getLogger(Encoder.class);

    /**
     * @return The source class.
     */
    public Class<S> getSourceClass();

    /**
     * @return The target class.
     */
    public Class<T> getTargetClass();

    /**
     * Encodes a source value.
     *
     * @param source The source value.
     * @return The encoded value of {@code source} or {@code null}.
     */
    public T encode(S source);

    /**
     * Encodes a source value.
     *
     * @param source The source value.
     * @param reaction The reaction to adopt if {@code source} can not be encoded.
     * @return The encoded value of {@code source} or {@code null}.
     * @throws IllegalArgumentException When {@code source} can not be encoded and {@code reaction} is {@link FailureReaction#FAIL}
     */
    public default T encode(S source,
                            FailureReaction reaction) {
        if (canEncode(source)) {
            return encode(source);
        } else {
            return FailureReaction.onError("No encoding for " + source,
                                           LOGGER,
                                           reaction,
                                           null,
                                           IllegalArgumentException::new);
        }
    }

    /**
     * Decodes an encoded value.
     *
     * @param target The encoded value.
     * @return The decoding of {@code target} or {@code null}.
     */
    public S decode(T target);

    /**
     * Decodes an encoded value.
     *
     * @param target The encoded value.
     * @param reaction The reaction to adopt if {@code target} can not be decoded.
     * @return The decoding of {@code target} or {@code null}.
     * @throws IllegalArgumentException When {@code target} can not be decoded and {@code reaction} is {@link FailureReaction#FAIL}
     */
    public default S decode(T target,
                            FailureReaction reaction) {
        if (canDecode(target)) {
            return decode(target);
        } else {
            return FailureReaction.onError("No decoding for " + target,
                                           LOGGER,
                                           reaction,
                                           null,
                                           IllegalArgumentException::new);
        }
    }

    /**
     * Returns {@code true} if a source value can be encoded.
     *
     * @param source The source value.
     * @return {@code true} if {@code source} can be encoded.
     */
    public boolean canEncode(S source);

    /**
     * Returns {@code true} if a target value can be decoded.
     *
     * @param target The target value.
     * @return {@code true} if {@code target} can be decoded.
     */
    public boolean canDecode(T target);
}