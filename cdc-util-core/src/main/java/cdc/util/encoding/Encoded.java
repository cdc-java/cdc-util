package cdc.util.encoding;

/**
 * Interface implemented by objects that can provide a coded view of themselves.
 *
 * @author Damien Carbonne
 *
 * @param <T> The encoded type.
 */
public interface Encoded<T> {

    /**
     * @return The encoded value of the object.
     */
    public T getCode();
}