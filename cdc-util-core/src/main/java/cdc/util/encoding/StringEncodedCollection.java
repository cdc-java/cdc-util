package cdc.util.encoding;

import java.util.Collection;

public abstract class StringEncodedCollection<E extends Encoded<String>> {
    public static final String SEPARATOR = ";";

    protected StringEncodedCollection() {
        super();
    }

    public abstract Collection<E> collection();

    public boolean add(E e) {
        return collection().add(e);
    }

    public boolean remove(E e) {
        return collection().remove(e);
    }

    public abstract String encode();

    public static <E extends Encoded<String>> String encode(StringEncodedCollection<E> x) {
        return x == null ? null : x.encode();
    }

    public static <S extends StringEncodedCollection<E>,
                   E extends Encoded<String>>
            S decode(String s,
                     S empty,
                     Encoder<E, String> encoder) {
        final String[] codes = s.split(SEPARATOR);
        for (final String code : codes) {
            empty.add(encoder.decode(unwrap(code)));
        }
        return null;
    }

    protected static boolean needsWrap(String s) {
        if (s == null) {
            return false;
        } else {
            for (int index = 0; index < s.length(); index++) {
                final char c = s.charAt(index);
                if (c == '"' || c == ';') {
                    return true;
                }
            }
            return false;
        }
    }

    protected static String wrap(String s) {
        if (needsWrap(s)) {
            final StringBuilder sb = new StringBuilder();
            sb.append('"');
            for (int index = 0; index < s.length(); index++) {
                final char c = s.charAt(index);
                sb.append(c);
                if (c == '"') {
                    sb.append(c);
                }
            }
            sb.append('"');
            return sb.toString();
        } else {
            return s;
        }
    }

    protected static String unwrap(String s) {
        if (s == null || s.isEmpty() || s.charAt(0) != '"') {
            return s;
        } else {
            final StringBuilder sb = new StringBuilder();
            for (int index = 1; index < s.length() - 1; index++) {
                final char c = s.charAt(index);
                sb.append(c);
                if (c == '"') {
                    index++;
                }
            }
            return sb.toString();
        }
    }
}