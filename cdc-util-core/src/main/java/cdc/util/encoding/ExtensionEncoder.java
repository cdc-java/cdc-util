package cdc.util.encoding;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of Encoder that is defined in extension.
 *
 * @author Damien Carbonne
 *
 * @param <S> The source (decoded) type.
 * @param <T> The target (encoded) type.
 */
public class ExtensionEncoder<S, T> implements Encoder<S, T> {
    private final Class<S> sourceClass;
    private final Class<T> targetClass;
    private final Map<S, T> forward = new HashMap<>();
    private final Map<T, S> backward = new HashMap<>();
    private boolean locked = false;

    public ExtensionEncoder(Class<S> sourceClass,
                            Class<T> targetClass) {
        this.sourceClass = sourceClass;
        this.targetClass = targetClass;
    }

    @Override
    public Class<S> getSourceClass() {
        return sourceClass;
    }

    @Override
    public Class<T> getTargetClass() {
        return targetClass;
    }

    public Set<S> getSourceValues() {
        return forward.keySet();
    }

    public Set<T> getTargetValues() {
        return backward.keySet();
    }

    public ExtensionEncoder<S, T> put(S source,
                                      T target) {
        if (isLocked()) {
            throw new IllegalStateException(getClass().getCanonicalName() + " is locked");
        }
        if (forward.containsKey(source)) {
            throw new IllegalArgumentException("'" + source + "' is already mapped to '" + forward.get(source) + "'");
        }
        if (backward.containsKey(target)) {
            throw new IllegalArgumentException("'" + target + "' is already mapped from '" + backward.get(target) + "'");
        }
        forward.put(source, target);
        backward.put(target, source);
        return this;
    }

    public void lock() {
        this.locked = true;
    }

    public boolean isLocked() {
        return locked;
    }

    @Override
    public T encode(S source) {
        return forward.get(source);
    }

    @Override
    public S decode(T target) {
        return backward.get(target);
    }

    @Override
    public boolean canEncode(S source) {
        return forward.containsKey(source);
    }

    @Override
    public boolean canDecode(T target) {
        return backward.containsKey(target);
    }
}