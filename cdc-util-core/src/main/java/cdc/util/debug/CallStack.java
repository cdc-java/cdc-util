package cdc.util.debug;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utility that can be used to print indented messages depending on call stack depth.
 * <p>
 * An indentation counter is associated to each thread.<br>
 * On should call leave in a finally block.
 *
 * @author Damien Carbonne
 */
public final class CallStack {
    private static final Logger LOGGER = LogManager.getLogger(CallStack.class);
    private static final ThreadLocal<Integer> DEPTH = ThreadLocal.withInitial(() -> 0);

    private CallStack() {
    }

    private static String indent(int delta) {
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < DEPTH.get() + delta; index++) {
            builder.append("  ");
        }
        return builder.toString();
    }

    public static int getDepth() {
        return DEPTH.get();
    }

    public static void enter(Logger logger,
                             Level level,
                             String message) {
        DEPTH.set(DEPTH.get() + 1);
        log(logger, level, message, 0);
    }

    public static void enter(String message) {
        enter(LOGGER, Level.TRACE, message);
    }

    public static void log(Logger logger,
                           Level level,
                           String message,
                           int delta) {
        logger.log(level, indent(delta) + message);
    }

    public static void log(String message,
                           int delta) {
        log(LOGGER, Level.TRACE, message, delta);
    }

    public static void leave(Logger logger,
                             Level level,
                             String message) {
        log(logger, level, message, 0);
        DEPTH.set(DEPTH.get() - 1);
    }

    public static void leave(String message) {
        leave(LOGGER, Level.TRACE, message);
    }

    public static void leave() {
        DEPTH.set(DEPTH.get() - 1);
    }
}