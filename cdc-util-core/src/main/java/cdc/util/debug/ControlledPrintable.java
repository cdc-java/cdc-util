package cdc.util.debug;

import java.io.PrintStream;

/**
 * Specialization of Printable that allows control of verbosity.
 *
 * @author Damien Carbonne
 */
public interface ControlledPrintable extends Printable {
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity);

    public default void print(PrintStream out,
                              Verbosity verbosity) {
        print(out, 0, verbosity);
    }

    @Override
    public default void print(PrintStream out,
                              int level) {
        print(out, level, Verbosity.NORMAL);
    }
}