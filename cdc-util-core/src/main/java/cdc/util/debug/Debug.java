package cdc.util.debug;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.RuntimeMXBean;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.files.Files;
import cdc.util.lang.CollectionUtils;

/**
 * Set of debug utilities.
 *
 * @author Damien Carbonne
 *
 */
public final class Debug {
    private static final Logger LOGGER = LogManager.getLogger(Debug.class);

    private Debug() {
    }

    /**
     * Prints the stack trace using a logger and a log level.
     *
     * @param logger The logger.
     * @param level The log level.
     * @param depth The maximum depth to print.
     */
    public static void printStackTrace(Logger logger,
                                       Level level,
                                       int depth) {
        final StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        final int max = depth < 0 ? Integer.MAX_VALUE : depth;
        int count = 0;
        for (final StackTraceElement element : elements) {
            if (count >= max) {
                return;
            }
            logger.log(level,
                       "        at {}.{} ({}:{})",
                       element.getClassName(),
                       element.getMethodName(),
                       element.getFileName(),
                       element.getLineNumber());
            count++;
        }
    }

    public static void printStackTrace(Logger logger,
                                       Level level) {
        printStackTrace(logger, level, -1);
    }

    public static void printStackTrace(int depth) {
        printStackTrace(LOGGER, Level.ERROR, depth);
    }

    /**
     * Prints the stack trace using local logger and error level.
     */
    public static void printStackTrace() {
        printStackTrace(-1);
    }

    /**
     * Returns {@code true} when current method is called by another method.
     * <p>
     * Note that one can not distinguish overloaded methods.
     *
     * @param className The class name of the candidate calling method.
     * @param methodName The method name of the candidate calling method.
     * @return {@code true} when current method is called by {@code methodName} declared in {@code className}.
     */
    public static boolean isCalledBy(String className,
                                     String methodName) {
        final StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (final StackTraceElement element : elements) {
            if (className.equals(element.getClassName())
                    && methodName.equals(element.getMethodName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return The class path.
     */
    public static URL[] getClassPath() {
        final ClassLoader cl = ClassLoader.getSystemClassLoader();
        if (cl instanceof URLClassLoader) {
            return ((URLClassLoader) cl).getURLs();
        } else {
            final String classpath = System.getProperty("java.class.path");
            final String separator = System.getProperty("path.separator");
            final List<URL> list = new ArrayList<>();
            final StringTokenizer tk = new StringTokenizer(classpath, separator);
            while (tk.hasMoreElements()) {
                try {
                    list.add(new File((String) tk.nextElement()).toURI().toURL());
                } catch (final MalformedURLException e) {
                    LOGGER.catching(e);
                }
            }
            final URL[] urls = new URL[list.size()];
            return list.toArray(urls);
        }
    }

    /**
     * Prints the class path using a logger and a log level.
     *
     * @param logger The logger.
     * @param level The log level.
     */
    public static void printClassPath(Logger logger,
                                      Level level) {
        logger.log(level, "Class path:");
        for (final URL url : getClassPath()) {
            logger.log(level, "    {}", url.getFile());
        }
    }

    /**
     * Prints class path using local logger and debug level.
     */
    public static void printClassPath() {
        printClassPath(LOGGER, Level.DEBUG);
    }

    /**
     * Prints current directory and class path using a logger and a log level.
     *
     * @param logger The logger.
     * @param level The log level.
     */
    public static void printPath(Logger logger,
                                 Level level) {
        logger.log(level, "Current dir: {}", Files.currentDir());
        printClassPath(logger, level);
    }

    /**
     * Prints current directory and class path using local logger and debug level.
     */
    public static void printPath() {
        printPath(LOGGER, Level.DEBUG);
    }

    public static List<String> getVMArgs() {
        return ManagementFactory.getRuntimeMXBean().getInputArguments();
    }

    public static void printVMArgs(Logger logger,
                                   Level level) {
        logger.log(level, "VM args:");
        for (final String s : getVMArgs()) {
            logger.log(level, "    {}", s);
        }
    }

    public static void printVMArgs() {
        printVMArgs(LOGGER, Level.DEBUG);
    }

    public static void printSystemProperties(Logger logger,
                                             Level level) {
        final RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
        final Map<String, String> map = bean.getSystemProperties();
        logger.log(level, "System properties:");
        for (final String key : CollectionUtils.toSortedList(map.keySet())) {
            logger.log(level, "    {}: {}", key, map.get(key));
        }
    }

    public static void printSystemProperties() {
        printSystemProperties(LOGGER, Level.DEBUG);
    }

    public static void printInputArguments(Logger logger,
                                           Level level) {
        final RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
        logger.log(level, "Input arguments:");
        for (final String arg : bean.getInputArguments()) {
            logger.log(level, "    {}", arg);
        }
    }

    public static void printInputArguments() {
        printInputArguments(LOGGER, Level.DEBUG);
    }

    public static void printSystemEnvironment(Logger logger,
                                              Level level) {
        final Map<String, String> map = System.getenv();
        logger.log(level, "System environment:");
        for (final String key : CollectionUtils.toSortedList(map.keySet())) {
            logger.log(level, "    {}: {}", key, System.getenv(key));
        }
    }

    public static void printSystemEnvironment() {
        printSystemEnvironment(LOGGER, Level.DEBUG);
    }

    private static void print(String title,
                              MemoryUsage usage,
                              Logger logger,
                              Level level) {
        logger.log(level, "   {}", title);
        logger.log(level, "      init: {}", usage.getInit());
        logger.log(level, "      max: {}", usage.getMax());
        logger.log(level, "      used: {}", usage.getUsed());
        logger.log(level, "      commited: {}", usage.getCommitted());
    }

    public static void printMemoryUsage(Logger logger,
                                        Level level) {
        final MemoryMXBean bean = ManagementFactory.getMemoryMXBean();
        logger.log(level, "Memory usage:");
        print("Heap", bean.getHeapMemoryUsage(), logger, level);
        print("Non heap", bean.getNonHeapMemoryUsage(), logger, level);
    }

    public static void printMemoryUsage() {
        printMemoryUsage(LOGGER, Level.DEBUG);
    }
}