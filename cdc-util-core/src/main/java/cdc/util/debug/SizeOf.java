package cdc.util.debug;

public final class SizeOf {
    private SizeOf() {
    }

    @FunctionalInterface
    public static interface Allocator {
        public Object allocate(int index);
    }

    public static void check(Allocator allocator) {
        // Warm up all classes/methods we will use
        Memory.warmUp();

        // Array to keep strong references to allocated objects
        final int count = 100000;
        final Object[] objects = new Object[count];

        long heap1 = 0;

        // Allocate count+1 objects, discard the first one
        for (int index = -1; index < count; ++index) {
            Object object = null;

            object = allocator.allocate(index);

            if (index == -1) {
                // object = null; // Discard the warm up object
                Memory.runGC();
                heap1 = Memory.usedMemory(); // Take a before heap snapshot
            } else {
                objects[index] = object;
            }
        }
        Memory.runGC();
        final long heap2 = Memory.usedMemory(); // Take an after heap snapshot:

        final int size = Math.round(((float) (heap2 - heap1)) / count);
        System.out.println(objects[0].getClass().getCanonicalName() + " size = " + size + " bytes");
        for (int i = 0; i < count; ++i) {
            objects[i] = null;
        }
    }
}