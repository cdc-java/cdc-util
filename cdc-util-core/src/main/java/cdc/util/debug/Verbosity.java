package cdc.util.debug;

public enum Verbosity {
    ESSENTIAL,
    NORMAL,
    DETAILED
}