package cdc.util.debug;

import java.util.ArrayList;
import java.util.List;

public class Indenter {
    private int depth = 0;
    private final String indent;
    private final List<String> values = new ArrayList<>();

    public Indenter(String indent) {
        this.indent = indent;
        // For 0
        values.add("");
    }

    public void enter() {
        depth++;
    }

    public void leave() {
        depth--;
    }

    private static String build(String s,
                                int count) {
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < count; index++) {
            builder.append(s);
        }
        return builder.toString();
    }

    public String indent() {
        if (depth < 0) {
            return "";
        } else {
            while (depth > values.size() + 1) {
                values.add(build(indent, values.size()));
            }
            return values.get(depth);
        }
    }
}