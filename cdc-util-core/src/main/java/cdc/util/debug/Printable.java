package cdc.util.debug;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

/**
 * Interface implemented by objects the can print their content.
 * <p>
 * This is more useful for objects whose content is complex.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface Printable {
    /**
     * Prints this object at an indentation level.
     *
     * @param out The PrintStream to use.
     * @param level The indentation level.
     */
    public void print(PrintStream out,
                      int level);

    /**
     * Prints this object at indentation level 0.
     *
     * @param out The PrintStream to use.
     */
    public default void print(PrintStream out) {
        print(out, 0);
    }

    /**
     * Indents at a given level.
     *
     * @param out The PrintStream to use.
     * @param level The indentation level.
     */
    public default void indent(PrintStream out,
                               int level) {
        Printables.indent(out, level);
    }

    public static void append(StringBuilder builder,
                              Printable printable,
                              int level) {
        try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                final PrintStream out = new PrintStream(baos, true, StandardCharsets.UTF_8.name())) {
            printable.print(out, level);
            builder.append(baos.toString(StandardCharsets.UTF_8.name()));
        } catch (final IOException e) {
            // Ignore
        }
    }

    public static void append(StringBuilder builder,
                              Printable printable) {
        append(builder, printable, 0);
    }
}