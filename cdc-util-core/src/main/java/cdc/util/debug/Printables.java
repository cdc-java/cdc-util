package cdc.util.debug;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Registry of Printable objects.
 * <p>
 * This is mainly used for debug purpose.
 *
 * @author Damien Carbonne
 *
 */
public final class Printables {
    private static final Logger LOGGER = LogManager.getLogger(Printables.class);
    private static final int INDENT_SIZE = 3;
    private static final Map<String, Printable> MAP = new HashMap<>();

    private Printables() {
    }

    private static void printBlanks(PrintStream out,
                                    int count) {
        for (int i = 0; i < count; i++) {
            out.print(' ');
        }
    }

    public static void indent(PrintStream out,
                              int level) {
        printBlanks(out, level * INDENT_SIZE);
    }

    /**
     * Registers a Printable with a name.
     *
     * @param name The name.
     * @param printable The printable.
     */
    public static void register(String name,
                                Printable printable) {
        if (name != null && !MAP.containsKey(name) && printable != null) {
            MAP.put(name, printable);
        } else {
            LOGGER.error("register({}, {}) FAILED", name, printable);
        }
    }

    /**
     * Registers a printable using a class canonical name as name.
     *
     * @param cls The class.
     * @param printable The printable.
     */
    public static void register(Class<?> cls,
                                Printable printable) {
        register(cls.getCanonicalName(), printable);
    }

    /**
     * Registers a printable using its class canonical name as name.
     *
     * @param printable The printable.
     */
    public static void register(Printable printable) {
        register(printable.getClass(), printable);
    }

    /**
     * @return A Set of names of registered Printable.
     */
    public static Set<String> getNames() {
        return MAP.keySet();
    }

    /**
     *
     * @param name The name.
     * @return The Printable named {@code name}.
     */
    public static Printable getPrintable(String name) {
        return MAP.get(name);
    }
}