package cdc.util.debug;

public final class Memory {
    private static final Runtime RUNTIME = Runtime.getRuntime();

    private Memory() {
    }

    public static long warmUp() {
        runGC();
        return usedMemory();
    }

    public static void runGC() {
        for (int index = 0; index < 4; ++index) {
            runGCInt();
        }
    }

    private static void runGCInt() {
        long usedMem1 = usedMemory();
        long usedMem2 = Long.MAX_VALUE;
        for (int i = 0; (usedMem1 < usedMem2) && (i < 500); ++i) {
            RUNTIME.runFinalization();
            RUNTIME.gc();
            Thread.yield();
            usedMem2 = usedMem1;
            usedMem1 = usedMemory();
        }
    }

    public static long usedMemory() {
        return RUNTIME.totalMemory() - RUNTIME.freeMemory();
    }
}