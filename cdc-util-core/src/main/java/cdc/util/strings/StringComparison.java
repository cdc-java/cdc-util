package cdc.util.strings;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import cdc.util.lang.CollectionUtils;

public final class StringComparison {
    private StringComparison() {
    }

    public static int indexOfFirstEndingDecimalDigit(String s) {
        final int len = s.length();
        for (int index = len - 1; index >= 0; index--) {
            final char c = s.charAt(index);
            if (c < '0' || c > '9') {
                if (index < len - 1) {
                    return index + 1;
                } else {
                    return -1;
                }
            }
        }
        return len > 0 ? 0 : -1;
    }

    /**
     * Compares 2 strings using numeric comparison of decimal sections.
     * <ul>
     * <li>{@code 2 < 10}
     * <li>{@code A2 < A10}
     * <li>{@code A2A < A10}
     * <li>{@code AB2 > A10}
     * </ul>
     *
     * @param s1 The first string
     * @param s2 The second string.
     * @return The comparison of {@code s1} and {@code s2}.
     */
    public static int compareDecimalDigits(String s1,
                                           String s2) {
        final List<Part> parts1 = split(s1);
        final List<Part> parts2 = split(s2);
        return CollectionUtils.compareLexicographic(parts1, parts2);
    }

    private static List<Part> split(String text) {
        if (text == null || text.isEmpty()) {
            return Collections.emptyList();
        } else {
            final List<Part> parts = new ArrayList<>();
            final int length = text.length();
            int first = 0;
            // 1 = number, 2 = string
            int state = 0;
            for (int i = 0; i < length; i++) {
                final char c = text.charAt(i);
                if ('0' <= c && c <= '9') {
                    // current char is a digit
                    if (state == 2) {
                        // previous char existed and was a non-digit
                        parts.add(new Part(text.substring(first, i), false));
                        first = i;
                    }
                    state = 1;
                } else {
                    // current char is NOT a digit
                    if (state == 1) {
                        // previous char existed and was a digit
                        parts.add(new Part(text.substring(first, i), true));
                        first = i;
                    }
                    state = 2;
                }
            }
            parts.add(new Part(text.substring(first), state == 1));
            return parts;
        }
    }

    private static final class Part implements Comparable<Part> {
        private final String text;
        private final boolean isNumber;

        public Part(String text,
                    boolean isNumber) {
            this.text = text;
            this.isNumber = isNumber;
        }

        @Override
        public int hashCode() {
            return Objects.hash(text,
                                isNumber);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Part)) {
                return false;
            }
            final Part other = (Part) object;
            return isNumber == other.isNumber
                    && Objects.equals(text, other.text);
        }

        @Override
        public int compareTo(Part other) {
            if (isNumber) {
                if (other.isNumber) {
                    // The maximum number that can be represented by a Long is 9_223_372_036_854_775_807
                    // So 18 digits always hold in a Long
                    if (text.length() <= 18 && other.text.length() <= 18) {
                        final long n1 = Long.parseLong(text);
                        final long n2 = Long.parseLong(other.text);
                        return Long.compare(n1, n2);
                    } else {
                        final BigInteger n1 = new BigInteger(text);
                        final BigInteger n2 = new BigInteger(other.text);
                        return n1.compareTo(n2);
                    }
                } else {
                    return 1;
                }
            } else {
                if (other.isNumber) {
                    return -1;
                } else {
                    return text.compareTo(other.text);
                }
            }
        }
    }
}