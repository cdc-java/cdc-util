package cdc.util.strings;

import java.util.Objects;

public class KeyValue {
    private final String key;
    private final String value;

    public KeyValue(String key,
                    String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key,
                            value);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof KeyValue)) {
            return false;
        }
        final KeyValue other = (KeyValue) object;
        return Objects.equals(key, other.key)
                && Objects.equals(value, other.value);
    }

    @Override
    public String toString() {
        return "[" + key + ", " + value + "]";
    }
}