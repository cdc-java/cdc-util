package cdc.util.strings;

import cdc.util.lang.BlackHole;

public final class StringRecognizer {
    private StringRecognizer() {
    }

    public enum Type {
        BOOLEAN,
        BYTE,
        SHORT,
        INTEGER,
        LONG,
        FLOAT,
        DOUBLE,
        STRING
    }

    public static Type getType(String s) {
        if (s == null) {
            return Type.STRING;
        } else if (isByte(s)) {
            return Type.BYTE;
        } else if (isShort(s)) {
            return Type.SHORT;
        } else if (isInt(s)) {
            return Type.INTEGER;
        } else if (isLong(s)) {
            return Type.LONG;
        } else if (isFiniteFloat(s)) {
            return Type.FLOAT;
        } else if (isFiniteDouble(s)) {
            return Type.DOUBLE;
        } else if (isBoolean(s)) {
            return Type.BOOLEAN;
        } else {
            return Type.STRING;
        }
    }

    public static boolean isBoolean(String s) {
        try {
            StringConversion.asBoolean(s);
            return true;
        } catch (final Exception e) {
            BlackHole.discard(e);
            return false;
        }
    }

    public static boolean isLong(String s) {
        try {
            Long.parseLong(s);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isInt(String s) {
        try {
            BlackHole.discard(Integer.parseInt(s));
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isShort(String s) {
        try {
            BlackHole.discard(Short.parseShort(s));
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isByte(String s) {
        try {
            BlackHole.discard(Byte.parseByte(s));
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isDouble(String s) {
        try {
            BlackHole.discard(Double.parseDouble(s));
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isFiniteDouble(String s) {
        try {
            final double x = Double.parseDouble(s);
            return Double.isFinite(x);
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isFloat(String s) {
        try {
            BlackHole.discard(Float.parseFloat(s));
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public static boolean isFiniteFloat(String s) {
        try {
            final float x = Float.parseFloat(s);
            return Float.isFinite(x);
        } catch (final NumberFormatException e) {
            return false;
        }
    }
}