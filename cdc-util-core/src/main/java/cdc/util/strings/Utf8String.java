package cdc.util.strings;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Arrays;

public class Utf8String implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    private final byte[] data;

    public Utf8String(String s) {
        data = s.getBytes(UTF8_CHARSET);
    }

    @Override
    public int hashCode() {
        return java.util.Arrays.hashCode(data);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (other instanceof Utf8String) {
            return Arrays.equals(data, ((Utf8String) other).data);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return new String(data, UTF8_CHARSET);
    }
}