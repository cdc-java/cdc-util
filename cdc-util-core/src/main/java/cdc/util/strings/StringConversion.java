package cdc.util.strings;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.encoding.Encoders;
import cdc.util.lang.FailureReaction;

/**
 * Utilities for conversion between strings and primitive types or enums.
 * <p>
 * <b>Note:</b> {@code asString} is used instead of {@code toString} to allow static import.<br>
 * For consistency reasons, asBoolean is used instead of toBoolean, ...
 * <p>
 * {@code asType} is used to convert a string to a primitive type or a non null enum.<br>
 * {@code asOptionalType} is used to convert a string to a possibly null object type or enum.<br>
 * <p>
 * For each of the following <b>{@code primitive types}</b> or <b>{@code object Types}</b>:
 * <ul>
 * <li>Boolean and boolean
 * <li>Long and long
 * <li>Integer and int
 * <li>Short and short
 * <li>Byte and byte
 * <li>Double and double
 * <li>Float and float
 * <li>Enum
 * </ul>
 * , the following functions are provided:
 * <ul>
 * <li><b>{@code type asType(String s, type def, missingReaction, errorReaction)}</b><br>
 * Converts a string to a <b>{@code primitive type}</b> value.<br>
 * If string is {@code null} or empty, the default value is returned or an exception is raised, depending on missing reaction.<br>
 * If string can not be converted, the default value is returned or an exception is raised, depending on error reaction.<br>
 *
 * <li><b>{@code type asType(String s, type def)}</b><br>
 * Converts a string to a <b>{@code primitive type}</b> value.<br>
 * If string is {@code null} or empty, the default value is returned.<br>
 * If string can not be converted, an exception is raised.
 *
 * <li><b>{@code type asType(String s)}</b><br>
 * Converts a string to a <b>{@code primitive type}</b> value.<br>
 * If string is {@code null}, empty or can not be converted, an exception is raised.
 *
 * <li><b>{@code Type asOptionalType(String s, Type def, errorReaction)}</b><br>
 * Converts a string to an <b>{@code object Type}</b> value.<br>
 * If string is {@code null} or empty, the default value is returned.<br>
 * If string can not be converted, the default value is returned or an exception is raised, depending on error reaction.
 *
 * <li><b>{@code Type asOptionalType(String s, Type def)}</b><br>
 * Converts a string to an <b>{@code object Type}</b> value.<br>
 * If string is {@code null} or empty, the default value is returned.<br>
 * If string can not be converted, an exception is raised.
 *
 * <li><b>{@code Type asOptionalType(String s)}</b><br>
 * Converts a string to an <b>{@code object Type}</b> value.<br>
 * If string is {@code null} or empty, {@code null} is returned.<br>
 * If string can not be converted, an exception is raised.
 *
 * <li><b>{@code String asString(type value)}</b><br>
 * Converts a <b>{@code primitive type}</b> value to String.
 *
 * <li><b>{@code String asString(Type value)}</b><br>
 * Converts an <b>{@code object Type}</b> value to String.<br>
 * {@code null} is converted to {@code null}.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class StringConversion {
    private static final Logger LOGGER = LogManager.getLogger(StringConversion.class);

    private StringConversion() {
    }

    private static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }

    private static String message(String s,
                                  String type,
                                  Throwable cause) {
        final StringBuilder builder = new StringBuilder();
        builder.append("Can not parse '");
        builder.append(s);
        builder.append("' as ");
        builder.append(type);
        if (cause != null) {
            builder.append(" (");
            builder.append(cause.getMessage());
            builder.append(")");
        }
        return builder.toString();
    }

    /**
     * Handles an error when conversion failed.
     *
     * @param <T> Class of the returned value.
     * @param s The string whose conversion failed.
     * @param def The default return value.
     * @param type The conversion target type.
     * @param reaction The reaction to adopt in case of conversion failure.
     * @param cause The optional associated exception.
     * @return The default value, when reaction is not {@link FailureReaction#FAIL}.
     * @throws FormatException When reaction is {@link FailureReaction#FAIL}
     */
    private static <T> T onError(String s,
                                 T def,
                                 String type,
                                 FailureReaction reaction,
                                 Throwable cause) {
        switch (reaction) {
        case FAIL:
            // Fails
            throw new FormatException(message(s, type, null), cause);
        case WARN:
            // Warns and returns the default value
            LOGGER.warn(message(s, type, cause));
            return def;
        default:
            // Silently returns the default value
            return def;
        }
    }

    /**
     * Converts a String to a boolean.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static boolean asBoolean(String s,
                                    boolean def,
                                    FailureReaction missingReaction,
                                    FailureReaction errorReaction) {
        final String tmp = s == null ? null : s.toLowerCase();

        if (isNullOrEmpty(tmp)) {
            return onError(s, def, "boolean", missingReaction, null);
        } else if ("true".equals(tmp)) {
            return true;
        } else if ("false".equals(tmp)) {
            return false;
        } else {
            return onError(s, def, "boolean", errorReaction, null);
        }
    }

    /**
     * Converts a String to a boolean.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static boolean asBoolean(String s,
                                    boolean def) {
        return asBoolean(s,
                         def,
                         FailureReaction.DEFAULT,
                         FailureReaction.FAIL);
    }

    /**
     * Converts a String to a boolean.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static boolean asBoolean(String s) {
        return asBoolean(s,
                         false, // Ignored
                         FailureReaction.FAIL,
                         FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Boolean.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Boolean asOptionalBoolean(String s,
                                            Boolean def,
                                            FailureReaction errorReaction) {
        final String tmp = s == null ? null : s.toLowerCase();

        if (isNullOrEmpty(s)) {
            return def;
        } else if ("true".equals(tmp)) {
            return Boolean.TRUE;
        } else if ("false".equals(tmp)) {
            return Boolean.FALSE;
        } else {
            return onError(s, def, "Boolean", errorReaction, null);
        }
    }

    /**
     * Converts a String to an optional Boolean.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Boolean asOptionalBoolean(String s,
                                            Boolean def) {
        return asOptionalBoolean(s,
                                 def,
                                 FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Boolean.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Boolean asOptionalBoolean(String s) {
        return asOptionalBoolean(s,
                                 null);
    }

    /**
     * Converts a String to a char.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static char asChar(String s,
                              char def,
                              FailureReaction missingReaction,
                              FailureReaction errorReaction) {

        if (isNullOrEmpty(s)) {
            return onError(s, def, "char", missingReaction, null);
        } else if (s.length() == 1) {
            return s.charAt(0);
        } else {
            return onError(s, def, "char", errorReaction, null);
        }
    }

    /**
     * Converts a String to a char.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static char asChar(String s,
                              char def) {
        return asChar(s,
                      def,
                      FailureReaction.DEFAULT,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to a char.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static char asChar(String s) {
        return asChar(s,
                      ' ', // Ignored
                      FailureReaction.FAIL,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Character.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Character asOptionalChar(String s,
                                           Character def,
                                           FailureReaction errorReaction) {
        if (isNullOrEmpty(s)) {
            return def;
        } else if (s.length() == 1) {
            return Character.valueOf(s.charAt(0));
        } else {
            return onError(s, def, "Character", errorReaction, null);
        }
    }

    /**
     * Converts a String to an optional Character.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Character asOptionalChar(String s,
                                           Character def) {
        return asOptionalChar(s,
                              def,
                              FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Character.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Character asOptionalChar(String s) {
        return asOptionalChar(s,
                              null);
    }

    /**
     * Converts a boolean to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(boolean value) {
        return Boolean.toString(value);
    }

    /**
     * Converts an optional Boolean to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Boolean value) {
        if (value == null) {
            return null;
        } else {
            return Boolean.toString(value.booleanValue());
        }
    }

    /**
     * Converts a char to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(char value) {
        return Character.toString(value);
    }

    /**
     * Converts an optional Character to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Character value) {
        if (value == null) {
            return null;
        } else {
            return Character.toString(value.charValue());
        }
    }

    /**
     * Converts a String to a long.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static long asLong(String s,
                              long def,
                              FailureReaction missingReaction,
                              FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "long", missingReaction, null);
            } else {
                return Long.parseLong(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "long", errorReaction, e);
        }
    }

    /**
     * Converts a String to a long.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static long asLong(String s,
                              long def) {
        return asLong(s,
                      def,
                      FailureReaction.DEFAULT,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to a long.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static long asLong(String s) {
        return asLong(s,
                      0L, // Ignored
                      FailureReaction.FAIL,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Long.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Long asOptionalLong(String s,
                                      Long def,
                                      FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return def;
            } else {
                return Long.parseLong(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "Long", errorReaction, e);
        }
    }

    /**
     * Converts a String to an optional Long.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Long asOptionalLong(String s,
                                      Long def) {
        return asOptionalLong(s,
                              def,
                              FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Long.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Long asOptionalLong(String s) {
        return asOptionalLong(s,
                              null);
    }

    /**
     * Converts a long to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(long value) {
        return Long.toString(value);
    }

    /**
     * Converts an optional Long to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Long value) {
        if (value == null) {
            return null;
        } else {
            return Long.toString(value.longValue());
        }
    }

    /**
     * Converts a String to an int.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static int asInt(String s,
                            int def,
                            FailureReaction missingReaction,
                            FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "int", missingReaction, null);
            } else {
                return Integer.parseInt(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "int", errorReaction, e);
        }
    }

    /**
     * Converts a String to an int.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static int asInt(String s,
                            int def) {
        return asInt(s,
                     def,
                     FailureReaction.DEFAULT,
                     FailureReaction.FAIL);
    }

    /**
     * Converts a String to an int.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static int asInt(String s) {
        return asInt(s,
                     0, // Ignored
                     FailureReaction.FAIL,
                     FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Integer.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Integer asOptionalInt(String s,
                                        Integer def,
                                        FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return def;
            } else {
                return Integer.parseInt(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "Integer", errorReaction, e);
        }
    }

    /**
     * Converts a String to an optional Integer.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Integer asOptionalInt(String s,
                                        Integer def) {
        return asOptionalInt(s,
                             def,
                             FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Integer.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Integer asOptionalInt(String s) {
        return asOptionalInt(s,
                             null);
    }

    /**
     * Converts an int to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(int value) {
        return Integer.toString(value);
    }

    /**
     * Converts an optional Integer to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Integer value) {
        if (value == null) {
            return null;
        } else {
            return Integer.toString(value.intValue());
        }
    }

    /**
     * Converts a String to a short.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static short asShort(String s,
                                short def,
                                FailureReaction missingReaction,
                                FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "short", missingReaction, null);
            } else {
                return Short.parseShort(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "short", errorReaction, e);
        }
    }

    /**
     * Converts a String to a short.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static short asShort(String s,
                                short def) {
        return asShort(s,
                       def,
                       FailureReaction.DEFAULT,
                       FailureReaction.FAIL);
    }

    /**
     * Converts a String to a short.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static short asShort(String s) {
        return asShort(s,
                       (short) 0, // Ignored
                       FailureReaction.FAIL,
                       FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Short.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Short asOptionalShort(String s,
                                        Short def,
                                        FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return def;
            } else {
                return Short.parseShort(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "Short", errorReaction, e);
        }
    }

    /**
     * Converts a String to an optional Short.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Short asOptionalShort(String s,
                                        Short def) {
        return asOptionalShort(s,
                               def,
                               FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Short.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Short asOptionalShort(String s) {
        return asOptionalShort(s,
                               null);
    }

    /**
     * Converts a short to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(short value) {
        return Short.toString(value);
    }

    /**
     * Converts an optional Short to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Short value) {
        if (value == null) {
            return null;
        } else {
            return Short.toString(value.shortValue());
        }
    }

    /**
     * Converts a String to a byte.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static byte asByte(String s,
                              byte def,
                              FailureReaction missingReaction,
                              FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "byte", missingReaction, null);
            } else {
                return Byte.parseByte(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "byte", errorReaction, e);
        }
    }

    /**
     * Converts a String to a byte.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static byte asByte(String s,
                              byte def) {
        return asByte(s,
                      def,
                      FailureReaction.DEFAULT,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to a byte.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static byte asByte(String s) {
        return asByte(s,
                      (byte) 0, // Ignored
                      FailureReaction.FAIL,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Byte.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Byte asOptionalByte(String s,
                                      Byte def,
                                      FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return def;
            } else {
                return Byte.parseByte(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "Byte", errorReaction, e);
        }
    }

    /**
     * Converts a String to an optional Byte.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Byte asOptionalByte(String s,
                                      Byte def) {
        return asOptionalByte(s,
                              def,
                              FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Byte.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Byte asOptionalByte(String s) {
        return asOptionalByte(s,
                              null);
    }

    /**
     * Converts a byte to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(byte value) {
        return Byte.toString(value);
    }

    /**
     * Converts an optional Byte to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Byte value) {
        if (value == null) {
            return null;
        } else {
            return Byte.toString(value.byteValue());
        }
    }

    /**
     * Converts a String to a double.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static double asDouble(String s,
                                  double def,
                                  FailureReaction missingReaction,
                                  FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "double", missingReaction, null);
            } else {
                return Double.parseDouble(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "double", errorReaction, e);
        }
    }

    /**
     * Converts a String to a double.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static double asDouble(String s,
                                  double def) {
        return asDouble(s,
                        def,
                        FailureReaction.DEFAULT,
                        FailureReaction.FAIL);
    }

    /**
     * Converts a String to a double.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static double asDouble(String s) {
        return asDouble(s,
                        0.0, // Ignored
                        FailureReaction.FAIL,
                        FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Double.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Double asOptionalDouble(String s,
                                          Double def,
                                          FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return def;
            } else {
                return Double.parseDouble(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "Double", errorReaction, e);
        }
    }

    /**
     * Converts a String to an optional Double.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Double asOptionalDouble(String s,
                                          Double def) {
        return asOptionalDouble(s,
                                def,
                                FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Double.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Double asOptionalDouble(String s) {
        return asOptionalDouble(s,
                                null);
    }

    /**
     * Converts a double to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(double value) {
        return Double.toString(value);
    }

    /**
     * Converts an optional Double to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Double value) {
        if (value == null) {
            return null;
        } else {
            return Double.toString(value.doubleValue());
        }
    }

    /**
     * Converts a String to a float.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static float asFloat(String s,
                                float def,
                                FailureReaction missingReaction,
                                FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "float", missingReaction, null);
            } else {
                return Float.parseFloat(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "float", errorReaction, e);
        }
    }

    /**
     * Converts a String to a float.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static float asFloat(String s,
                                float def) {
        return asFloat(s,
                       def,
                       FailureReaction.DEFAULT,
                       FailureReaction.FAIL);
    }

    /**
     * Converts a String to a float.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static float asFloat(String s) {
        return asFloat(s,
                       0.0F, // Ignored
                       FailureReaction.FAIL,
                       FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Float.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Float asOptionalFloat(String s,
                                        Float def,
                                        FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return def;
            } else {
                return Float.parseFloat(s);
            }
        } catch (final Exception e) {
            return onError(s, def, "Float", errorReaction, e);
        }
    }

    /**
     * Converts a String to an optional Float.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Float asOptionalFloat(String s,
                                        Float def) {
        return asOptionalFloat(s,
                               def,
                               FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional Float.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Float asOptionalFloat(String s) {
        return asOptionalFloat(s,
                               null);
    }

    /**
     * Converts a float to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     */
    public static String asString(float value) {
        return Float.toString(value);
    }

    /**
     * Converts an optional Float to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Float value) {
        if (value == null) {
            return null;
        } else {
            return Float.toString(value.floatValue());
        }
    }

    /**
     * Converts a String to a raw enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static Enum<?> asRawEnum(String s,
                                    Class<? extends Enum<?>> enumClass,
                                    Enum<?> def,
                                    FailureReaction missingReaction,
                                    FailureReaction errorReaction) {
        try {
            if (isNullOrEmpty(s)) {
                return onError(s, def, "enum", missingReaction, null);
            } else {
                final Enum<?> result = Encoders.getRawNameEncoder(enumClass).decode(s);
                if (result == null) {
                    throw new FormatException();
                } else {
                    return result;
                }
            }
        } catch (final FormatException e) {
            return onError(s, def, "enum", errorReaction, null);
        }
    }

    /**
     * Converts a String to a raw enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static Enum<?> asRawEnum(String s,
                                    Class<? extends Enum<?>> enumClass,
                                    Enum<?> def) {
        return asRawEnum(s,
                         enumClass,
                         def,
                         FailureReaction.DEFAULT,
                         FailureReaction.FAIL);
    }

    /**
     * Converts a String to a raw enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param s The String.
     * @param enumClass The enum class.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static Enum<?> asRawEnum(String s,
                                    Class<? extends Enum<?>> enumClass) {
        return asRawEnum(s,
                         enumClass,
                         null, // Ignored
                         FailureReaction.FAIL,
                         FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional raw enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static Enum<?> asOptionalRawEnum(String s,
                                            Class<? extends Enum<?>> enumClass,
                                            Enum<?> def,
                                            FailureReaction errorReaction) {
        if (isNullOrEmpty(s)) {
            return def;
        } else {
            return asRawEnum(s, enumClass, def, FailureReaction.DEFAULT, errorReaction);
        }
    }

    /**
     * Converts a String to an optional raw enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static Enum<?> asOptionalRawEnum(String s,
                                            Class<? extends Enum<?>> enumClass,
                                            Enum<?> def) {
        return asOptionalRawEnum(s,
                                 enumClass,
                                 def,
                                 FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional raw enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param s The String.
     * @param enumClass The enum class.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static Enum<?> asOptionalRawEnum(String s,
                                            Class<? extends Enum<?>> enumClass) {
        return asOptionalRawEnum(s, enumClass, null);
    }

    /**
     * Converts an optional raw enum to a String.
     *
     * @param value The value.
     * @return The string corresponding to {@code value}.
     *         {@code null} is returned when {@code value} is {@code null}.
     */
    public static String asString(Enum<?> value) {
        if (value == null) {
            return null;
        } else {
            return value.name();
        }
    }

    /**
     * Converts a String to an enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned or an exception is raised, depending on
     * {@code missingReaction}.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param <E> The enum type.
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when {@code s} is {@code null} or empty.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and missing or error reaction is {@link FailureReaction#FAIL}.
     */
    public static <E extends Enum<E>> E asEnum(String s,
                                               Class<E> enumClass,
                                               E def,
                                               FailureReaction missingReaction,
                                               FailureReaction errorReaction) {
        final Enum<?> e = asRawEnum(s, enumClass, def, missingReaction, errorReaction);
        return enumClass.cast(e);
    }

    /**
     * Converts a String to an enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param <E> The enum type.
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When conversion fails.
     */
    public static <E extends Enum<E>> E asEnum(String s,
                                               Class<E> enumClass,
                                               E def) {
        return asEnum(s,
                      enumClass,
                      def,
                      FailureReaction.DEFAULT,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to an enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty or cannot be converted, throws an exception.
     * </ol>
     *
     * @param <E> The enum type.
     * @param s The String.
     * @param enumClass The enum class.
     * @return The value corresponding to {@code s}.
     * @throws FormatException When {@code s} is {@code null} of empty or conversion fails.
     */
    public static <E extends Enum<E>> E asEnum(String s,
                                               Class<E> enumClass) {
        return asEnum(s,
                      enumClass,
                      null, // Ignored
                      FailureReaction.FAIL,
                      FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, {@code def} is returned or an exception is raised, depending on {@code errorReaction}.
     * </ol>
     *
     * @param <E> The enum type.
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @param errorReaction The reaction to adopt when conversion of {@code s} fails.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails and {@code errorReaction} is {@link FailureReaction#FAIL}.
     */
    public static <E extends Enum<E>> E asOptionalEnum(String s,
                                                       Class<E> enumClass,
                                                       E def,
                                                       FailureReaction errorReaction) {
        if (isNullOrEmpty(s)) {
            return def;
        } else {
            return asEnum(s, enumClass, def, FailureReaction.DEFAULT, errorReaction);
        }
    }

    /**
     * Converts a String to an optional enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code def} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param <E> The enum type.
     * @param s The String.
     * @param enumClass The enum class.
     * @param def The default value.
     * @return The value corresponding to {@code s} or {@code def}.
     * @throws FormatException When conversion fails.
     */
    public static <E extends Enum<E>> E asOptionalEnum(String s,
                                                       Class<E> enumClass,
                                                       E def) {
        return asOptionalEnum(s,
                              enumClass,
                              def,
                              FailureReaction.FAIL);
    }

    /**
     * Converts a String to an optional enum.
     * <ol>
     * <li>If {@code s} is {@code null} or empty, {@code null} is returned.
     * <li>If {@code s} cannot be converted, an exception is raised.
     * </ol>
     *
     * @param <E> The enum type.
     * @param s The String.
     * @param enumClass The enum class.
     * @return The value corresponding to {@code s}.
     *         {@code null} when {@code s} is {@code null} or empty.
     * @throws FormatException When conversion fails.
     */
    public static <E extends Enum<E>> E asOptionalEnum(String s,
                                                       Class<E> enumClass) {
        return asOptionalEnum(s,
                              enumClass,
                              null);
    }

    public static <E extends Enum<E>> E valueOf(Class<E> cls,
                                                String name) {
        return Enum.valueOf(cls, name);
    }
}