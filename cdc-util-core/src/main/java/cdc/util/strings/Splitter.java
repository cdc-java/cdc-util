package cdc.util.strings;

import java.util.ArrayList;
import java.util.List;

import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

/**
 * Class that can split a string into parts following different policies.
 * <p>
 * <b>Note:</b> No empty parts are generated.
 * <p>
 * <b>WARNING:</b> This class is not thread safe.
 *
 * @author Damien Carbonne
 */
public class Splitter {
    private final Policy policy;
    private final char separator;
    private String s = null;
    private int from = -1;
    private int to = -1;

    /**
     * Enumeration of possible splitting policies.
     *
     * @author Damien Carbonne
     */
    public enum Policy {
        /** No split. */
        NONE,

        /**
         * Parts are separated by a specific separator character.
         * <p>
         * Several consecutive separators are equivalent to 1 separator.
         */
        SEPARATOR,

        /**
         * Parts are separated by white spaces.
         *
         * @see Character#isWhitespace(char)
         */
        SPACE,

        /**
         * Each upper case letter, or letter following a space char, starts a new
         * part.
         */
        CAPITAL,

        /**
         * An upper case letter following a lower case letter starts a new part.
         * <p>
         * Or a non space char following a space char.
         */
        JOINED_CAPITAL
    }

    private Splitter(Policy policy,
                     char separator) {
        Checks.isNotNull(policy, "policy");
        this.policy = policy;
        this.separator = separator;
        reset(s);
    }

    /**
     * Creates a splitter with a policy.
     * <p>
     * If {@code policy == Policy.SEPARATOR} then separator is set to ' '.
     *
     * @param policy The policy.
     * @throws IllegalArgumentException When {@code policy} is {@code null}.
     */
    public Splitter(Policy policy) {
        this(policy, ' ');
    }

    /**
     * Creates a separator policy.
     *
     * @param separator The separator.
     */
    public Splitter(char separator) {
        this(Policy.SEPARATOR, separator);
    }

    /**
     * Creates a splitter with Policy.NONE policy.
     */
    public Splitter() {
        this(Policy.NONE);
    }

    /**
     * @return The policy.
     */
    public final Policy getPolicy() {
        return policy;
    }

    /**
     * @return The separator.
     */
    public final char getSeparator() {
        return separator;
    }

    /**
     * Resets this splitter to slit a new string.
     *
     * @param s The string to split.
     */
    public final void reset(String s) {
        this.s = s;
        if (s == null || s.length() == 0) {
            from = -1;
            to = -1;
        } else {
            to = 0;
            advance();
        }
    }

    /**
     * @return {@code true} if there are more parts to retrieve.
     */
    public final boolean hasNext() {
        return from < to;
    }

    /**
     * @return The next string part and advance to next part.
     */
    public final String next() {
        if (!hasNext()) {
            throw new IllegalStateException();
        }
        final String result = s.substring(from, to);
        advance();
        return result;
    }

    /**
     * Uses this splitter to split a string into a list of parts.
     *
     * @param s The string to split.
     * @return A list of parts.
     */
    public final List<String> split(String s) {
        final List<String> result = new ArrayList<>();
        reset(s);
        while (hasNext()) {
            result.add(next());
        }
        return result;
    }

    /**
     * Returns a list of parts by splitting a string using a policy.
     *
     * @param s The string.
     * @param policy The splitting policy.
     * @return The list of words in {@code s} splitting it with {@code policy}.
     *         If policy is SEPARATOR, uses ' ' as separator.
     */
    public static List<String> split(String s,
                                     Policy policy) {
        final Splitter splitter = new Splitter(policy);
        return splitter.split(s);
    }

    /**
     * Returns a list of parts by splitting a string using a separator.
     *
     * @param s The string.
     * @param separator The splitting separator.
     * @return The list of words in {@code s} separated by {@code separator}.
     */
    public static List<String> split(String s,
                                     char separator) {
        final Splitter splitter = new Splitter(separator);
        return splitter.split(s);
    }

    private static boolean isWitheSpace(char c) {
        return Character.isWhitespace(c);
    }

    private static boolean isUpperCase(char c) {
        return Character.isUpperCase(c);
    }

    private boolean isSeparator(char c) {
        return c == separator;
    }

    private void skipSeparators() {
        while (from < s.length()
                && isSeparator(s.charAt(from))) {
            from++;
        }
    }

    private void skipWhiteSpaces() {
        while (from < s.length()
                && isWitheSpace(s.charAt(from))) {
            from++;
        }
    }

    private void advanceNone() {
        from = to;
        to = s.length();
    }

    private void advanceSeparator() {
        from = to;
        skipSeparators();
        if (from < s.length()) {
            to = from + 1;
            while (to < s.length()
                    && !isSeparator(s.charAt(to))) {
                to++;
            }
        } else {
            to = from;
        }
    }

    private void advanceSpace() {
        from = to;
        skipWhiteSpaces();
        if (from < s.length()) {
            to = from + 1;
            while (to < s.length()
                    && !isWitheSpace(s.charAt(to))) {
                to++;
            }
        } else {
            to = from;
        }
    }

    private void advanceCapital() {
        from = to;
        skipWhiteSpaces();
        if (from < s.length()) {
            to = from + 1;
            while (to < s.length()
                    && !isUpperCase(s.charAt(to))
                    && !isWitheSpace(s.charAt(to))) {
                to++;
            }
        } else {
            to = from;
        }
    }

    private void advanceJoinedCapital() {
        from = to;
        skipWhiteSpaces();
        if (from < s.length()) {
            to = from + 1;

            while (to < s.length()
                    && isUpperCase(s.charAt(to))) {
                to++;
            }
            while (to < s.length()
                    && !isWitheSpace(s.charAt(to))
                    && !isUpperCase(s.charAt(to))) {
                to++;
            }
        } else {
            to = from;
        }
    }

    private void advance() {
        switch (policy) {
        case NONE:
            advanceNone();
            break;

        case SEPARATOR:
            advanceSeparator();
            break;

        case SPACE:
            advanceSpace();
            break;

        case CAPITAL:
            advanceCapital();
            break;

        case JOINED_CAPITAL:
            advanceJoinedCapital();
            break;

        default:
            throw new UnexpectedValueException(policy);
        }
    }
}