package cdc.util.strings;

import java.util.Random;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cdc.util.lang.Checks;

public final class StringUtils {
    private static final String LETTERS_OR_DIGITS = "abcedfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final Random RANDOM = new Random();
    private static final Pattern NATIVE_TO_STRING_PATTERN = Pattern.compile("^.*@[0-9a-fA-F]+$");
    private static final Pattern NL_PATTERN = Pattern.compile("\\R");
    private static final Pattern ARG_PATTERN = Pattern.compile("\\{%[^}]*}");

    /**
     * A default argument formatter that can be used with {@link #format(String, BiFunction, Object...)}.
     * Mainly intended for tests.
     */
    public static final BiFunction<String, Object, String> DEFAULT_ARG_FORMATTER =
            (f,
             o) -> (o == null ? "null" : o.toString());

    private StringUtils() {
    }

    /**
     * Returns {@code true} if a String is {@code null} or empty.
     *
     * @param s The string.
     * @return {@code true} if {@code s} is {@code null} or empty.
     */
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }

    /**
     * Returns the numbers of matching characters in a string.
     *
     * @param s The string.
     * @param c The searched character.
     * @return The number of occurrences of {@code c} in {@code s}.
     */
    public static int countMatches(String s,
                                   char c) {
        if (s == null) {
            return 0;
        } else {
            int count = 0;
            for (int index = 0; index < s.length(); index++) {
                final char x = s.charAt(index);
                if (x == c) {
                    count++;
                }
            }
            return count;
        }
    }

    /**
     * Returns the number of lines in a string.
     *
     * @param s The string.
     * @return The number of lines in {@code s}.<br>
     *         If {@code s} is {@code null} or empty, returns 0,
     *         otherwise returns the number of occurrences of {@code '\n' + 1}.
     */
    public static int numberOfLines(String s) {
        if (isNullOrEmpty(s)) {
            return 0;
        } else {
            final Matcher m = NL_PATTERN.matcher(s);
            int lines = 1;
            while (m.find()) {
                lines++;
            }
            return lines;
        }
    }

    /**
     * Splits a string into lines.
     *
     * @param s The string.
     * @return {@code null} or an array of lines.
     */
    public static String[] splitLines(String s) {
        return s == null ? null : NL_PATTERN.split(s);
    }

    /**
     * @param s The string.
     * @return The maximum line length of lines in {@code s}.
     */
    public static int maxLineLength(String s) {
        if (s == null) {
            return 0;
        } else {
            int maxLength = 0;
            int length = 0;
            for (int index = 0; index < s.length(); index++) {
                final char x = s.charAt(index);
                if (x == '\n') {
                    maxLength = Math.max(maxLength, length);
                    length = 0;
                } else {
                    length++;
                }
            }
            maxLength = Math.max(maxLength, length);
            return maxLength;
        }
    }

    /**
     * Returns the largest character in a string.
     *
     * @param s The string.
     * @return The largest character in {@code s},
     *         or {@code -1} if {@code s} is {@code null} or empty.
     */
    public static int maxChar(String s) {
        int max = -1;
        if (s != null) {
            for (int index = 0; index < s.length(); index++) {
                max = Math.max(max, s.charAt(index));
            }
        }
        return max;
    }

    /**
     * @return A random letter or digit character.
     */
    public static char randomLetterOrDigit() {
        return LETTERS_OR_DIGITS.charAt(RANDOM.nextInt(LETTERS_OR_DIGITS.length()));
    }

    /**
     * Returns a string filled with random letters or digits.
     *
     * @param length The string length.
     * @return A string of length random letters or digits characters.
     */
    public static String scrambleWithLettersOrDigits(int length) {
        final StringBuilder sb = new StringBuilder();
        for (int index = 0; index < length; index++) {
            sb.append(randomLetterOrDigit());
        }
        return sb.toString();
    }

    /**
     * Return a string that has the same length, but is filled with random letters or digits.
     *
     * @param s The string.
     * @param preserveSpaces If {@code true}, spaces are preserved.
     * @return {@code null} or a String with same length and filled with random letters or digits.
     */
    public static String scrambleWithLettersOrDigits(String s,
                                                     boolean preserveSpaces) {
        if (s == null) {
            return null;
        } else {
            final StringBuilder sb = new StringBuilder();
            for (int index = 0; index < s.length(); index++) {
                final char c = s.charAt(index);
                if (preserveSpaces && Character.isWhitespace(c)) {
                    sb.append(c);
                } else {
                    sb.append(randomLetterOrDigit());
                }
            }
            return sb.toString();
        }
    }

    /**
     * Replaces all characters of a string by one character.
     *
     * @param s The string.
     * @param preserveSpaces If {@code true}, spaces are preserved.
     * @param c The replacement character.
     * @return A String that has the same length as s
     *         and with all characters equal to {@code c} or spaces.
     */
    public static String fillWithCharacter(String s,
                                           boolean preserveSpaces,
                                           char c) {
        if (s == null) {
            return s;
        } else {
            final StringBuilder sb = new StringBuilder();
            for (int index = 0; index < s.length(); index++) {
                if (preserveSpaces && Character.isWhitespace(s.charAt(index))) {
                    sb.append(s.charAt(index));
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }
    }

    /**
     * Extracts at most max characters from a string.
     * <p>
     * If some characters of the input string where removed, appends "..." at the end.
     * The returned string contains at most {@code max} characters.
     *
     * @param s The string.
     * @param max The max number of characters to extract.
     * @return A string containing at most {@code max} characters from {@code s}.
     * @throws IllegalArgumentException When {@code max < 3}.
     */
    public static String extract(String s,
                                 int max) {
        Checks.isTrue(max >= 3, "max (" + max + ") is too small");

        if (s == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            final int m;
            if (s.length() <= max) {
                m = s.length();
            } else {
                m = max - 3;
            }
            for (int index = 0; index < m; index++) {
                final char c = s.charAt(index);
                builder.append(c);
            }
            if (s.length() > max) {
                builder.append("...");
            }
            return builder.toString();
        }
    }

    /**
     * Extracts at most {@code maxNumberOfLines} lines so that each line is at {@code mostmaxLineLength} long.
     *
     * @param s The string.
     * @param maxNumberOfLines The maximum number of lines.
     * @param maxLineLength The maximum line length.
     * @return A string containing at most {@code maxNumberOfLines} lines that are at most {@code mostmaxLineLength} long.
     * @throws IllegalArgumentException When {@code maxNumberOfLines < 1} or {@code maxLineLength < 3}.
     */
    public static String extract(String s,
                                 int maxLineLength,
                                 int maxNumberOfLines) {
        Checks.isTrue(maxNumberOfLines >= 1, "maxNumberOfLines is too small");

        if (s == null) {
            return null;
        } else {
            final String[] lines = s.split("\n");
            final StringBuilder builder = new StringBuilder();
            final int m;
            if (lines.length <= maxNumberOfLines) {
                m = lines.length;
            } else {
                m = maxNumberOfLines - 1;
            }
            for (int index = 0; index < m; index++) {
                if (index > 0) {
                    builder.append("\n");
                }
                builder.append(extract(lines[index], maxLineLength));
            }
            if (lines.length > maxNumberOfLines) {
                if (maxNumberOfLines > 1) {
                    builder.append('\n');
                }
                builder.append("...");
            }
            return builder.toString();
        }
    }

    public static String extractAverage(String s,
                                        int maxLineLength,
                                        int maxSize) {
        if (s == null) {
            return null;
        } else {
            final int lines = countMatches(s, '\n') + 1;
            final int maxLines = 1 + maxSize / (maxLineLength - 1);
            final int maxNumberOfLines = Math.min(lines, maxLines);
            return extract(extract(s, maxLineLength, maxNumberOfLines), maxSize);
        }
    }

    /**
     * Inserts new lines characters so that each line is not longer than a max length.
     * <p>
     * If possible, new lines are inserted at space locations.
     *
     * @param s The string.
     * @param maxLineLength The maximum line length.
     * @param strip If {@code true}, leading spaces are stripped on each line.
     * @return The wrapped version of {@code s}.
     */
    public static String wrap(String s,
                              int maxLineLength,
                              boolean strip) {
        if (isNullOrEmpty(s)) {
            return s;
        }
        final StringBuilder builder = new StringBuilder();
        int lineStart = 0;
        int lastSpace = -1;
        int index = 0;
        if (strip) {
            while (index < s.length() && Character.isWhitespace(s.charAt(index))) {
                index++;
            }
            lineStart = index;
        }
        do {
            final char c = s.charAt(index);
            if (Character.isWhitespace(c)) {
                lastSpace = index;
            }
            if (c == '\n') {
                builder.append(s, lineStart, index + 1);
                lineStart = index + 1;
            } else if (index - lineStart + 1 == maxLineLength) {
                final char nc = index + 1 < s.length() ? s.charAt(index + 1) : ' ';
                final boolean nextIsSpace = Character.isWhitespace(nc);
                final boolean nextIsNL = nc == '\n';

                if (!nextIsSpace && lastSpace >= lineStart) {
                    builder.append(s, lineStart, lastSpace + 1);
                    if (!nextIsNL && index + 1 < s.length()) {
                        builder.append('\n');
                    }
                    lineStart = lastSpace + 1;
                } else {
                    builder.append(s, lineStart, index + 1);
                    if (!nextIsNL && index + 1 < s.length()) {
                        builder.append('\n');
                    }
                    lineStart = index + 1;
                }
                if (nextIsSpace && strip) {
                    index++;
                    while (index < s.length() && Character.isWhitespace(s.charAt(index))) {
                        index++;
                    }
                    lineStart = index;
                    index--;
                }
            }
            index++;
        } while (index < s.length());
        if (lineStart < s.length()) {
            builder.append(s, lineStart, s.length());
        }
        return builder.toString();
    }

    /**
     * Returns a string that contains the same character count times.
     *
     * @param c The character.
     * @param count Size of the resulting string.
     * @return A String that contains only {@code c} character {@code count} times.
     */
    public static String toString(char c,
                                  int count) {
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < count; index++) {
            builder.append(c);
        }
        return builder.toString();
    }

    /**
     * Returns {@code true} if a string looks like the string returned by {@link Object#toString()}.
     *
     * @param s The string to test.
     * @return {@code true} if {@code s} looks like the string returned by {@link Object#toString()}.
     */
    public static boolean looksLikeNativeToString(String s) {
        if (s != null) {
            final Matcher m = NATIVE_TO_STRING_PATTERN.matcher(s);
            return m.matches();
        } else {
            return false;
        }
    }

    /**
     * If a string is empty, returns {@code null}, otherwise returns the string.
     *
     * @param s The string.
     * @return {@code null} if {@code s} is {@code null} or empty, otherwise returns {@code s}.
     */
    public static String nullify(String s) {
        return isNullOrEmpty(s) ? null : s;
    }

    /**
     * Yields a String from a format, elementary formatters, and arguments.
     * <p>
     * Format specifiers are defined with <code>{%specifier}</code>.
     * <p>
     * It is the responsibility of {@code argsFormatter} to convert each (specifier, object) pair
     * to a string.
     * <p>
     * Examples with {@link #DEFAULT_ARG_FORMATTER}:
     * <ul>
     * <li>format("aaa", DEFAULT_ARG_FORMATTER) : "aaa"
     * <li>format("aaa{%}bbb", DEFAULT_ARG_FORMATTER, 1) : "aaa1bbb"
     * </ul>
     *
     * @param format The format.
     * @param argsFormatter The argument formatter.<br>
     *            The first passed arg is the format specifier.
     *            The second passed arg is the argument.
     * @param args The arguments.
     * @return The formatted string.
     */
    public static String format(String format,
                                BiFunction<String, Object, String> argsFormatter,
                                Object... args) {
        final StringBuilder builder = new StringBuilder();
        final Matcher matcher = ARG_PATTERN.matcher(format);
        int from = 0;
        int argIndex = 0;
        while (matcher.find()) {
            final String text = format.substring(from, matcher.start());
            builder.append(text);
            final String argGroup = matcher.group();
            final String argFormat = argGroup.substring(2, argGroup.length() - 1);
            from = matcher.end();
            final Object arg = argIndex < args.length
                    ? args[argIndex]
                    : null;
            final String formattedArg = argsFormatter.apply(argFormat, arg);
            builder.append(formattedArg);
            argIndex++;
        }
        final String text = format.substring(from);
        builder.append(text);

        return builder.toString();
    }
}