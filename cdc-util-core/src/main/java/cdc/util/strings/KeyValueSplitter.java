package cdc.util.strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * Utility used to split a string as a list of (key, value) pairs.
 * <p>
 * <pre>
 * <code>
 *    KeyValueSplitter splitter = new KeyValueSplitter("key1", "key2, ...);
 *    splitter.reset("....");
 *    while (splitter.hasMore()) {
 *       // do something with splitter.getValue() and splitter.getkey();
 *       splitter.next();
 *    }
 * </code>
 * </pre>
 *
 * @author Damien Carbonne
 */
public class KeyValueSplitter {
    private final List<String> keys;
    private String input = null;
    private int currentStart = -1;
    private String currentKey = null;

    private static class Node {
        final int pos;
        final String key;

        public static final Comparator<Node> COMPARATOR =
                Comparator.comparingInt(n -> n.pos);

        public Node(int pos,
                    String key) {
            this.pos = pos;
            this.key = key;
        }
    }

    private final List<Node> nexts = new ArrayList<>();

    /**
     * Creates a splitter.
     *
     * @param keys The recognized keys.
     */
    public KeyValueSplitter(List<String> keys) {
        this.keys = Collections.unmodifiableList(new ArrayList<>(keys));
    }

    /**
     * Creates a splitter.
     *
     * @param keys The recognized keys.
     */
    public KeyValueSplitter(String... keys) {
        this(Arrays.asList(keys));
    }

    /**
     * @return The list of recognized keys.
     */
    public List<String> getKeys() {
        return keys;
    }

    /**
     * Resets recognition.
     *
     * @param input The input string.
     */
    public void reset(String input) {
        this.input = input;
        this.currentKey = null;
        this.currentStart = -1;
        nexts.clear();

        if (input != null) {
            initNexts();
            advance();
        }
    }

    /**
     * @return The input string.
     */
    public String getInput() {
        return input;
    }

    private void initNexts() {
        for (int index = 0; index < keys.size(); index++) {
            final int pos = input.indexOf(keys.get(index));
            if (pos >= 0) {
                nexts.add(new Node(pos, keys.get(index)));
            }
        }
        Collections.sort(nexts, Node.COMPARATOR);
    }

    private void refreshNexts(int from,
                              String key) {
        final int pos = input.indexOf(key, from);
        if (pos >= 0) {
            final Node node = new Node(pos, key);
            int index = Collections.binarySearch(nexts, node, Node.COMPARATOR);
            if (index < 0) {
                // That should always be true
                index = -(index + 1);
            }
            nexts.add(index, node);
        }
    }

    private void advance() {
        if (nexts.isEmpty()) {
            currentKey = null;
            currentStart = -1;
        } else {
            currentKey = nexts.get(0).key;
            currentStart = nexts.get(0).pos;
            nexts.remove(0);
            refreshNexts(currentStart + currentKey.length(), currentKey);
        }
    }

    /**
     * @return {@code true} if the input string contains more keys.
     */
    public boolean hasMore() {
        return currentKey != null;
    }

    private void checkHasMore() {
        Checks.assertTrue(hasMore(), "no more");
    }

    public void next() {
        checkHasMore();
        advance();
    }

    /**
     * @return The key at current position.
     */
    public String getKey() {
        checkHasMore();
        return currentKey;
    }

    /**
     * @return The value at current position.
     */
    public String getValue() {
        checkHasMore();
        if (nexts.isEmpty()) {
            return input.substring(currentStart + currentKey.length());
        } else {
            return input.substring(currentStart + currentKey.length(), nexts.get(0).pos);
        }
    }

    public static List<KeyValue> split(String input,
                                       List<String> keys) {
        final List<KeyValue> list = new ArrayList<>();
        final KeyValueSplitter splitter = new KeyValueSplitter(keys);
        splitter.reset(input);
        while (splitter.hasMore()) {
            list.add(new KeyValue(splitter.getKey(), splitter.getValue()));
            splitter.next();
        }
        return list;
    }

    public static List<KeyValue> split(String input,
                                       String... keys) {
        return split(input, Arrays.asList(keys));
    }
}