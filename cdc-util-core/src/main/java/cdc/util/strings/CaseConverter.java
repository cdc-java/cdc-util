package cdc.util.strings;

import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

/**
 * Utility used to change case of words.
 * <p>
 * Words may be recognized by splitting the input string.<br>
 * Conversion can add prefix or suffix to each word, and a separator between words.
 * <p>
 * <b>WARNING:</b> This class is not thread safe.
 *
 * @author Damien Carbonne
 *
 * @see Splitter
 */
public final class CaseConverter {
    private final Style style;
    private final String wordPrefix;
    private final String wordSuffix;
    private final String wordSeparator;
    private final Splitter splitter;
    private final boolean active;

    /**
     * Enumeration if the supported case styles.
     *
     * @author Damien Carbonne
     */
    public enum Style {
        /**
         * Words are kept unchanged.
         * <p>
         * AAA bbb: AAA bbb
         */
        ORIGINAL,

        /**
         * All letters are transformed to upper case.
         * AAA bbb: AAA BBB
         */
        UPPER_CASE,

        /**
         * All letters are transformed to lower case.
         * AAA bbb: aaa bbb
         */
        LOWER_CASE,

        /**
         * First letter is transformed to Upper case, others to lower case.
         * <p>
         * AAA bbb: Aaa Bbb<br>
         * Synonym of Pascal case.
         */
        CAPITAL,

        /**
         * First letter is transformed to Upper case, others to lower case, except
         * for first word which is totally lower case.
         * <p>
         * AAA bbb: aaa Bbb
         */
        CAMEL_CASE
    }

    private CaseConverter(Builder builder) {
        this.style = Checks.isNotNull(builder.style, "style");
        this.wordPrefix = validate(builder.wordPrefix);
        this.wordSeparator = validate(builder.wordSeparator);
        this.wordSuffix = validate(builder.wordSuffix);
        this.splitter = Checks.isNotNull(builder.splitter, "splitter");

        active = splitter.getPolicy() != Splitter.Policy.NONE
                || this.style != Style.ORIGINAL
                || this.wordPrefix.length() > 0
                || this.wordSuffix.length() > 0
                || this.wordSeparator.length() > 0;
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    /**
     * @return The result style.
     */
    public Style getStyle() {
        return style;
    }

    /**
     * @return The word prefix.
     */
    public String getWordPrefix() {
        return wordPrefix;
    }

    /**
     * @return The word suffix.
     */
    public String getWordSuffix() {
        return wordSuffix;
    }

    /**
     * @return The word separator.
     */
    public String getWordSeparator() {
        return wordSeparator;
    }

    /**
     * @return The splitter.
     */
    public Splitter getSplitter() {
        return splitter;
    }

    /**
     * Converts an array of words (already split).
     *
     * @param words The words.
     * @return The conversion of {@code words} using current settings.
     */
    public String convert(String... words) {
        final StringBuilder builder = new StringBuilder();
        int index = 0;
        for (final String word : words) {
            if (index > 0) {
                builder.append(wordSeparator);
            }
            builder.append(wordPrefix);
            builder.append(convertWord(word, index));
            builder.append(wordSuffix);
            index++;
        }

        return builder.toString();
    }

    /**
     * Splits a string into words and converts them.
     *
     * @param words The string that is first split, then converted.
     * @return The conversion of {@code words} using current settings.
     */
    public String splitAndConvert(String words) {
        if (active) {
            final StringBuilder builder = new StringBuilder();
            int index = 0;
            splitter.reset(words);
            while (splitter.hasNext()) {
                if (index > 0) {
                    builder.append(wordSeparator);
                }
                builder.append(wordPrefix);
                builder.append(convertWord(splitter.next(), index));
                builder.append(wordSuffix);
                index++;
            }
            return builder.toString();
        } else {
            return words;
        }
    }

    /**
     * Converts a string by splitting it and converting each word in accordance with a style.
     *
     * @param words The string.
     * @param style The result style.
     * @param wordPrefix The word prefix.
     * @param wordSeparator The word separator.
     * @param wordSuffix The word suffix.
     * @param splitter The splitter.
     * @return The conversion of {@code words}.
     */
    public static String splitAndConvert(String words,
                                         Style style,
                                         String wordPrefix,
                                         String wordSeparator,
                                         String wordSuffix,
                                         Splitter splitter) {
        final CaseConverter converter = builder().style(style)
                                                 .wordPrefix(wordPrefix)
                                                 .wordSeparator(wordSeparator)
                                                 .wordSuffix(wordSuffix)
                                                 .splitter(splitter)
                                                 .build();
        return converter.splitAndConvert(words);
    }

    /**
     * Converts a string by splitting it and converting each word in accordance with a style.
     *
     * @param words The string.
     * @param style The result style.
     * @param splitter The splitter.
     * @return The conversion of {@code words}.
     */
    public static String splitAndConvert(String words,
                                         Style style,
                                         Splitter splitter) {
        return splitAndConvert(words,
                               style,
                               "",
                               "",
                               "",
                               splitter);
    }

    /**
     * Converts a string by splitting it and converting each word in accordance with a style.
     *
     * @param words The string.
     * @param style The result style.
     * @param splitterPolicy The splitter policy used to split {@code words}.
     * @return The conversion of {@code words}.
     */
    public static String splitAndConvert(String words,
                                         Style style,
                                         Splitter.Policy splitterPolicy) {
        return splitAndConvert(words,
                               style,
                               new Splitter(splitterPolicy));
    }

    /**
     * Converts a string by splitting it using a SEPARATOR policy
     * and converting each word in accordance with a style.
     *
     * @param words The string.
     * @param style The result style.
     * @param splitterSeparator The splitter separator.
     * @return The conversion of {@code words}.
     */
    public static String splitAndConvert(String words,
                                         Style style,
                                         char splitterSeparator) {
        return splitAndConvert(words,
                               style,
                               new Splitter(splitterSeparator));
    }

    private String convertWord(String word,
                               int index) {
        switch (style) {
        case CAPITAL:
            return toCapital(word);
        case LOWER_CASE:
            return word.toLowerCase();
        case UPPER_CASE:
            return word.toUpperCase();
        case ORIGINAL:
            return word;
        case CAMEL_CASE:
            if (index == 0) {
                return word.toLowerCase();
            } else {
                return toCapital(word);
            }
        default:
            throw new UnexpectedValueException(style);
        }
    }

    public static String toCapital(String word) {
        if (word.length() <= 1) {
            return word.toUpperCase();
        } else {
            final StringBuilder builder = new StringBuilder(word.length());
            builder.append(Character.toUpperCase(word.charAt(0)));
            for (int index = 1; index < word.length(); index++) {
                builder.append(Character.toLowerCase(word.charAt(index)));
            }
            return builder.toString();
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Style style = Style.ORIGINAL;
        private String wordPrefix = "";
        private String wordSuffix = "";
        private String wordSeparator = "";
        private Splitter splitter = new Splitter();

        protected Builder() {
        }

        public Builder style(Style style) {
            this.style = style;
            return this;
        }

        public Builder wordPrefix(String wordPrefix) {
            this.wordPrefix = wordPrefix;
            return this;
        }

        public Builder wordSuffix(String wordSuffix) {
            this.wordSuffix = wordSuffix;
            return this;
        }

        public Builder wordSeparator(String wordSeparator) {
            this.wordSeparator = wordSeparator;
            return this;
        }

        public Builder splitter(Splitter splitter) {
            this.splitter = splitter;
            return this;
        }

        public CaseConverter build() {
            return new CaseConverter(this);
        }
    }
}