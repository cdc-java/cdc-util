package cdc.util.strings;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Utility that can anonymize strings.
 * <ul>
 * <li>All characters can be replaced by one replacement character or jammed.
 * <li>Specified characters can be preserved.
 * <li>White spaces can be preserved.
 * <li>String size can be limited.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class StringAnonymizer implements UnaryOperator<String> {
    private static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final Random RANDOM = new Random();
    private final Set<Feature> features = new HashSet<>();
    /** Replacement character to use if no jam */
    private char replacement = 'X';
    private String preservedChars = null;
    private final Set<Character> preservedCharsSet = new HashSet<>();
    private int maxLength = -1;

    public enum Feature {
        PRESERVE_WHITESPACES,
        JAM
    }

    public StringAnonymizer() {
        super();
    }

    public StringAnonymizer(char replacement) {
        setReplacement(replacement);
    }

    public void setEnabled(Feature feature,
                           boolean enabled) {
        if (enabled) {
            features.add(feature);
        } else {
            features.remove(feature);
        }
    }

    public boolean isEnabled(Feature feature) {
        return features.contains(feature);
    }

    /**
     * @return The replacement character.
     */
    public char getReplacement() {
        return replacement;
    }

    public void setReplacement(char replacement) {
        this.replacement = replacement;
    }

    /**
     * @return The preserved characters.
     */
    public String getPreservedCharacters() {
        return preservedChars;
    }

    public void setPreservedCharacters(String chars) {
        this.preservedChars = null;
        this.preservedCharsSet.clear();
        if (chars != null) {
            for (int index = 0; index < chars.length(); index++) {
                this.preservedCharsSet.add(chars.charAt(index));
            }
        }
    }

    /**
     * @return The maximum length of the anonymized strings.
     */
    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int value) {
        this.maxLength = value;
    }

    private char anonymize(char c) {
        return anonymize(c,
                         replacement,
                         preservedCharsSet,
                         isEnabled(Feature.PRESERVE_WHITESPACES),
                         isEnabled(Feature.JAM));
    }

    /**
     * Anonymizes a string with using settings.
     *
     * @param s The string.
     * @return The anomymized version of {@code s}.
     */
    public String anonymize(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        } else {
            final StringBuilder builder = new StringBuilder();
            final int max = maxLength < 0 ? s.length() : Math.min(maxLength, s.length());
            for (int index = 0; index < max; index++) {
                final char c = s.charAt(index);
                builder.append(anonymize(c));
            }
            return builder.toString();
        }
    }

    @Override
    public String apply(String t) {
        return anonymize(t);
    }

    /**
     * @return A random character among: {@code [A-Z0-9]}.
     */
    public static char randomChar() {
        final int num = RANDOM.nextInt(LETTERS.length());
        return LETTERS.charAt(num);
    }

    /**
     * Jam a string by replacing all its characters by random ones.
     *
     * @param s The string.
     * @return A string that ha the same length as {@code s} made of random characters.
     */
    public static String jam(String s) {
        if (s == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            for (int index = 0; index < s.length(); index++) {
                builder.append(randomChar());
            }
            return builder.toString();
        }
    }

    /**
     * Anonymizes a character.
     * <ul>
     * <li>If {@code c} belongs to {@code preservedChars}, returns {@code c}.
     * <li>Else if {@code c} is a white space and {@code preserveWhiteSpaces} is {@code true}, returns {@code c}.
     * <li>Else if {@code jam} is {@code true}, returns a random character.
     * <li>Else returns {@code replacement}.
     * </ul>
     *
     * @param c The character.
     * @param replacement The replacement character to use. Meaningful if {@code jam} is {@code false}.
     * @param preservedChars A set of characters to preserve. May by {@code null}.
     * @param preserveWhiteSpaces If {@code true}, spaces are preserved.
     * @param jam If {@code true}, characters are replaced by random ones.
     * @return An anonymized version of {@code c}.
     */
    public static char anonymize(char c,
                                 char replacement,
                                 Set<Character> preservedChars,
                                 boolean preserveWhiteSpaces,
                                 boolean jam) {
        if (preservedChars != null && preservedChars.contains(c)) {
            return c;
        } else if (preserveWhiteSpaces && Character.isWhitespace(c)) {
            return c;
        } else {
            return jam ? randomChar() : replacement;
        }
    }

    /**
     * Anonymizes a string.
     *
     * @param s The string.
     * @param replacement The replacement character to use. Meaningful if {@code jam} is {@code false}.
     * @param preservedChars A set of characters to preserve. May by {@code null}.
     * @param preserveWhiteSpaces If {@code true}, spaces are preserved.
     * @param jam If {@code true}, characters are replaced by random ones.
     * @param maxLength The maximum length of the result.
     * @return An anonymized version of {@code s}.
     */
    public static String anonymize(String s,
                                   char replacement,
                                   Set<Character> preservedChars,
                                   boolean preserveWhiteSpaces,
                                   boolean jam,
                                   int maxLength) {
        if (s == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            final int max = maxLength < 0 ? s.length() : Math.min(maxLength, s.length());
            for (int index = 0; index < max; index++) {
                final char c = s.charAt(index);
                builder.append(anonymize(c, replacement, preservedChars, preserveWhiteSpaces, jam));
            }
            return builder.toString();
        }
    }

    /**
     * @return A Jamming function.
     */
    public static Function<String, String> jammer() {
        return StringAnonymizer::jam;
    }

    /**
     * Returns a function that replaces all characters by a character.
     *
     * @param replacement The replacement character
     * @return A function that replaces all characters by {@code replacement}.
     */
    public static Function<String, String> replacer(char replacement) {
        return new StringAnonymizer(replacement);
    }

    /**
     * Returns an anonymizing function.
     *
     * @param replacement The replacement character to use. Meaningful if {@code jam} is {@code false}.
     * @param preservedChars A set of characters to preserve. May by {@code null}.
     * @param maxLength The maximum length of the result.
     * @param features The features to enable.
     * @return An anonymizing function.
     */
    public static Function<String, String> anonymizer(char replacement,
                                                      String preservedChars,
                                                      int maxLength,
                                                      Feature... features) {
        final StringAnonymizer result = new StringAnonymizer(replacement);
        result.setPreservedCharacters(preservedChars);
        result.setMaxLength(maxLength);
        for (final Feature feature : features) {
            result.setEnabled(feature, true);
        }
        return result;
    }

    private static class Numberer implements Function<String, String> {
        private final String prefix;
        private int next;

        public Numberer(String prefix,
                        int from) {
            this.prefix = prefix;
            this.next = from;
        }

        @Override
        public String apply(String t) {
            final String result = prefix + next;
            next++;
            return result;
        }
    }

    /**
     * Returns a function that appends a new number each time it is called to a string.
     *
     * @param prefix The prefix.
     * @param from The initial number.
     * @return A string of the form {@code prefix} + {@code number}.
     */
    public static Function<String, String> numberer(String prefix,
                                                    int from) {
        return new Numberer(prefix, from);
    }

    /**
     * Returns a function that appends a new number each time it is called to a string.
     * <p>
     * Numbering starts at 0.
     *
     * @param prefix The prefix.
     * @return A string of the form {@code prefix} + {@code number}.
     */
    public static Function<String, String> numberer(String prefix) {
        return numberer(prefix, 0);
    }
}