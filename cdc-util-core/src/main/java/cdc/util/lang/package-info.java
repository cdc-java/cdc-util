/**
 * Low level utilities related to Java.
 *
 * @author Damien Carbonne
 */
package cdc.util.lang;