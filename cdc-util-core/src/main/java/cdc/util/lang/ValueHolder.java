package cdc.util.lang;

public class ValueHolder<T> {
    public T value;

    public ValueHolder() {
        this.value = null;
    }

    public ValueHolder(T value) {
        this.value = value;
    }
}