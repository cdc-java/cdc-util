package cdc.util.lang;

import java.util.Arrays;

/**
 * Utilities using short as mask.<br>
 * Bits are numbered from 0 (Less Significant) to 15 (Most Significant).<br>
 * Bytes are numbered from 0 (Less Significant) to 1 (Most Significant).
 *
 * @author Damien Carbonne
 *
 */
public final class ShortMasks {
    private static final int FULL_MASK = 0xFFFF;

    /**
     * The maximum number of bits that can be encoded with a short.
     */
    public static final int MAX_BITS = 16;

    private ShortMasks() {
    }

    private static void checkIndex(int index) {
        if (index < 0 || index >= MAX_BITS) {
            throw new IllegalArgumentException("Invalid index: " + index);
        }
    }

    private static void checkIndexLength(int index,
                                         int length) {
        checkIndex(index);
        if (length <= 0 || length > MAX_BITS) {
            throw new IllegalArgumentException("Invalid length: " + length);
        }
        if (index + length > MAX_BITS) {
            throw new IllegalArgumentException("Invalid index/length " + index + " " + length);
        }
    }

    private static void checkValueLength(int value,
                                         int length) {
        if ((value & toMask(0, length)) != value) {
            throw new IllegalArgumentException("Invalid value: " + value + ", can not be coded on " + length + " bit(s)");
        }
    }

    public static byte getByte0(short mask) {
        return (byte) mask;
    }

    public static short setByte0(short mask,
                                 byte value) {
        return (short) ((mask & 0xFF00) | (value & 0xFF));
    }

    public static byte getByte1(short mask) {
        return (byte) (mask >>> 8);
    }

    public static short setByte1(short mask,
                                 byte value) {
        return (short) ((mask & 0x00FF) | ((value & 0xFF) << 8));
    }

    /**
     * Return the short mask corresponding to one bit at a given index.
     * - toMask (0) == 1
     * - toMask (1) == 10
     * - toMask (2) == 100 - ...
     *
     * @param index The index. Must be in range [0, 15] inclusive
     * @return The short mask corresponding to one bit at index.
     */
    public static short toMask(int index) {
        checkIndex(index);
        return (short) (1 << index);
    }

    /**
     * Return whether the index-th bit of a mask is set or not.
     *
     * @param mask The tested mask.
     * @param index The tested index. Must be in range [0, 31] inclusive.
     * @return Whether index-th bit of mask is set or not.
     */
    public static boolean isEnabled(short mask,
                                    int index) {
        return (mask & toMask(index)) != 0;
    }

    /**
     * Set the index-th bit of a mask to 0 or 1.
     *
     * @param mask The mask to set.
     * @param index Index of the bit to set. Must be in range [0, 15] inclusive.
     * @param enabled If true, set the index-th bit to 1, otherwise set it to 0.
     * @return The input mask with its index-th bit modified.
     */
    public static short setEnabled(short mask,
                                   int index,
                                   boolean enabled) {
        if (enabled) {
            return (short) (mask | toMask(index));
        } else {
            return (short) (mask & ~toMask(index));
        }
    }

    /**
     * Return the maximum possible index that can be used to encode length bits.
     * If length is invalid, return -1;
     *
     * @param length The number of bits to encode. Must be in range [1..16] to
     *            obtain a valid result.
     * @return The maximum possible index that can be used to encode length
     *         bits, or -1.
     */
    public static int getMaxIndex(int length) {
        if (length <= 0 || length > MAX_BITS) {
            return -1;
        } else {
            return MAX_BITS - length;
        }
    }

    /**
     * Return the short mask corresponding to length bits with a given Least
     * Significant bit.
     * <ul>
     * <li>toMask(index, 1) = toMask(index)</li>
     * <li>toMask(0, 2) = 11</li>
     * <li>toMask(1, 2) = 110</li>
     * <li>toMask(2, 3) = 11100</li>
     * </ul>
     *
     * @param index Index of the Least Significant bit.
     * @param length Number of bits.
     * @return The corresponding int mask;
     */
    public static short toMask(int index,
                               int length) {
        checkIndexLength(index, length);
        return (short) ((FULL_MASK >>> (MAX_BITS - length)) << index);
    }

    /**
     * Set bits in range [index, index + length[ to a given value.
     * <br>
     * Constraints:
     * <ul>
     * <li>{@literal index >= 0}</li>
     * <li>{@literal length > 0}</li>
     * <li>{@literal index + length <= MAX_BITS}</li>
     * </ul>
     *
     * @param mask The mask whose bits must be modified.
     * @param index Index of the Least Significant bit to set.
     * @param length Number of bits to set.
     * @param value The value to set. Must fit in length bit(s).
     * @return The modified input mask.
     */
    public static short set(short mask,
                            int index,
                            int length,
                            int value) {
        checkIndexLength(index, length);
        checkValueLength(value, length);
        final short m = toMask(index, length);
        return (short) ((mask & ~m) | ((value << index) & m));
    }

    /**
     * Return the value corresponding to bits in range [index, index + length[.
     * <br>
     * Constraints:
     * <ul>
     * <li>{@literal index >= 0}</li>
     * <li>{@literal length > 0}</li>
     * <li>{@literal index + length <= MAX_BITS}</li>
     * </ul>
     *
     * @param mask The mask whose bits must be extracted.
     * @param index Index of the Least Significant bit to set.
     * @param length Number of bits to set.
     * @return The value corresponding to bits in range [index, index + length[.
     */
    public static int get(short mask,
                          int index,
                          int length) {
        checkIndexLength(index, length);
        return (mask >> index) & (FULL_MASK >>> (MAX_BITS - length));
    }

    public static <E extends Enum<E>> short set(short mask,
                                                int index,
                                                E value,
                                                Class<E> enumClass,
                                                boolean nullable) {
        return set(mask, index, Masks.getLength(enumClass, nullable), value == null ? 0 : value.ordinal() + 1);
    }

    public static <E extends Enum<E>> E get(short mask,
                                            int index,
                                            Class<E> enumClass,
                                            boolean nullable) {
        final int i = get(mask, index, Masks.getLength(enumClass, nullable));
        return i == 0 ? null : enumClass.getEnumConstants()[i - 1];
    }

    public static <E extends Enum<E>> short toMask(E value) {
        return toMask(Masks.toIndex(value));
    }

    @SafeVarargs
    public static <E extends Enum<E>> short toMask(E... values) {
        short mask = 0;
        for (final E value : values) {
            mask = setEnabled(mask, value, true);
        }
        return mask;
    }

    public static <E extends Enum<E>> short setEnabled(short mask,
                                                       E value,
                                                       boolean enabled) {
        return setEnabled(mask, Masks.toIndex(value), enabled);
    }

    public static <E extends Enum<E>> boolean isEnabled(short mask,
                                                        E value) {
        return isEnabled(mask, Masks.toIndex(value));
    }

    public static <E extends Enum<E>> String toString(short mask,
                                                      Class<E> enumClass) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final E value : enumClass.getEnumConstants()) {
            if (isEnabled(mask, value)) {
                if (!first) {
                    builder.append("|");
                }
                first = false;
                builder.append(value.name());
            }
        }
        return builder.toString();
    }

    public static String toPaddedBinString(short val) {
        final char[] buffer = new char[MAX_BITS];
        Arrays.fill(buffer, '0');
        for (int i = 0; i < MAX_BITS; ++i) {
            final int mask = 1 << i;
            if ((val & mask) == mask) {
                buffer[MAX_BITS - 1 - i] = '1';
            }
        }
        return new String(buffer);
    }
}