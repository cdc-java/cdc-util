package cdc.util.lang;

public class DataException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DataException() {
        super();
    }

    public DataException(String message) {
        super(message);
    }

    public DataException(String message,
                         Throwable cause) {
        super(message, cause);
    }
}