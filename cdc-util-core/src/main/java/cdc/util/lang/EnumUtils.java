package cdc.util.lang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Set of utilities related to enumerated types.
 *
 * @author Damien Carbonne
 *
 */
public final class EnumUtils {
    private EnumUtils() {
    }

    /**
     * Converts source enums to target ones with same names.
     * <p>
     * Example:
     * <ul>
     * <li>E1: A, B, C, D
     * <li>E2: A, B, F, G
     * <li>getEnumsWithMatchingNames(E2.class, [E1.A, E1.C]) : [E2.A]
     * </ul>
     *
     * @param <S> The source enum type.
     * @param <T> The target enum type.
     * @param targetClass The source enum class.
     * @param sources The source enums.
     * @return An Iterable of target enums that have the same names as
     *         source enums. Source enums that dson't have matching targets
     *         are ignored.
     */
    public static <S extends Enum<S>, T extends Enum<T>> List<T> getEnumsWithMatchingNames(Class<T> targetClass,
                                                                                           Iterable<S> sources) {
        final Map<String, T> map = new HashMap<>();
        for (final T target : targetClass.getEnumConstants()) {
            map.put(target.name(), target);
        }

        final List<T> targets = new ArrayList<>();
        for (final S source : sources) {
            final T target = map.get(source.name());
            if (target != null) {
                targets.add(target);
            }
        }
        return targets;
    }

    @SafeVarargs
    public static <S extends Enum<S>, T extends Enum<T>> List<T> getEnumsWithMatchingNames(Class<T> targetClass,
                                                                                           S... sources) {
        return getEnumsWithMatchingNames(targetClass, Arrays.asList(sources));
    }

    public static <S extends Enum<S>, T extends Enum<T>> List<S> getSourceEnumsWithMatchingNames(Class<S> sourceClass,
                                                                                                 Class<T> targetClass) {
        return getEnumsWithMatchingNames(sourceClass, targetClass.getEnumConstants());
    }

    public static <S extends Enum<S>, T extends Enum<T>> List<T> getTargetEnumsWithMatchingNames(Class<S> sourceClass,
                                                                                                 Class<T> targetClass) {
        return getEnumsWithMatchingNames(targetClass, sourceClass.getEnumConstants());
    }

    /**
     * Returns the enum literal that has the smaller ordinal.
     *
     * @param <E> Class of the enum literals.
     * @param left The first literal.
     * @param right The second literal.
     * @return The literal among left and right that has the smaller ordinal.
     */
    public static <E extends Enum<E>> E min(E left,
                                            E right) {
        return left.ordinal() <= right.ordinal() ? left : right;
    }

    /**
     * Returns the enum literal that has the smaller ordinal.
     *
     * @param <E> Class of the enum literals.
     * @param values The literals. Must not be empty.
     * @return The literal among values that has the smaller ordinal.
     */
    @SafeVarargs
    public static <E extends Enum<E>> E min(E... values) {
        if (values.length == 0) {
            throw new IllegalArgumentException();
        } else {
            E min = values[0];
            for (int index = 1; index < values.length; index++) {
                final E value = values[index];
                if (value.ordinal() < min.ordinal()) {
                    min = value;
                }
            }
            return min;
        }
    }

    /**
     * Returns the enum literal that has the greater ordinal.
     *
     * @param <E> Class of the enum literals.
     * @param left The first literal.
     * @param right The second literal.
     * @return The literal among left and right that has the greater ordinal.
     */
    public static <E extends Enum<E>> E max(E left,
                                            E right) {
        return left.ordinal() >= right.ordinal() ? left : right;
    }

    /**
     * Returns the enum literal that has the smaller ordinal.
     *
     * @param <E> Class of the enum literals.
     * @param values The literals. Must not be empty.
     * @return The literal among values that has the greater ordinal.
     */
    @SafeVarargs
    public static <E extends Enum<E>> E max(E... values) {
        if (values.length == 0) {
            throw new IllegalArgumentException();
        } else {
            E max = values[0];
            for (int index = 1; index < values.length; index++) {
                final E value = values[index];
                if (value.ordinal() > max.ordinal()) {
                    max = value;
                }
            }
            return max;
        }
    }
}