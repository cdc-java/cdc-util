package cdc.util.lang;

import java.util.Comparator;

public final class ComparatorUtils {
    public static final Comparator<Class<?>> CLASS_SIMPLE_NAME_COMPARATOR =
            Comparator.comparing(Class::getSimpleName);

    public static final Comparator<Class<?>> CLASS_CANONICAL_NAME_COMPARATOR =
            Comparator.comparing(Class::getCanonicalName);

    private ComparatorUtils() {
    }

    private static class Adapter<T, D> implements Comparator<T> {
        private final Class<D> delegateClass;
        private final Comparator<D> delegate;

        public Adapter(Class<D> delegateClass,
                       Comparator<D> delegate) {
            this.delegateClass = delegateClass;
            this.delegate = delegate;
        }

        @Override
        public int compare(T o1,
                           T o2) {
            final D d1 = delegateClass.cast(o1);
            final D d2 = delegateClass.cast(o2);
            return delegate.compare(d1, d2);
        }
    }

    public static <T, D> Comparator<T> adapt(Class<D> delegateClass,
                                             Comparator<D> delegate) {
        return new Adapter<>(delegateClass, delegate);
    }
}