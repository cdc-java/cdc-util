package cdc.util.lang;

import java.util.Arrays;

/**
 * Utilities using long as mask.<br>
 * Bits are numbered from 0 (Less Significant) to 63 (Most Significant).<br>
 * Bytes are numbered from 0 (Less Significant) to 7 (Most Significant).<br>
 * Shorts are numbered from 0 (Less Significant) to 3 (Most Significant).<br>
 * Ints are numbered from 0 (Less Significant) to 1 (Most Significant).
 *
 * @author Damien Carbonne
 *
 */
public final class LongMasks {
    private static final long FULL_MASK = 0xFFFFFFFFFFFFFFFFL;

    /**
     * The maximum number of bits that can be encoded with an long.
     */
    public static final int MAX_BITS = 64;

    private LongMasks() {
    }

    private static void checkIndex(int index) {
        if (index < 0 || index >= MAX_BITS) {
            throw new IllegalArgumentException("Invalid index: " + index);
        }
    }

    private static void checkIndexLength(int index,
                                         int length) {
        checkIndex(index);
        if (length <= 0 || length > MAX_BITS) {
            throw new IllegalArgumentException("Invalid length: " + length);
        }
        if (index + length > MAX_BITS) {
            throw new IllegalArgumentException("Invalid index/length " + index + " " + length);
        }
    }

    private static void checkValueLength(long value,
                                         int length) {
        if ((value & toMask(0, length)) != value) {
            throw new IllegalArgumentException("Invalid value: " + value + ", can not be coded on " + length + " bit(s)");
        }
    }

    public static int getInt0(long mask) {
        return (int) (mask & 0xFFFFFFFFL);
    }

    public static long setInt0(long mask,
                               int value) {
        return (mask & 0xFFFFFFFF00000000L) | (value & 0xFFFFFFFFL);
    }

    public static int getInt1(long mask) {
        return (int) ((mask & 0xFFFFFFFF00000000L) >>> 32);
    }

    public static long setInt1(long mask,
                               int value) {
        return (mask & 0xFFFFFFFFL) | ((value & 0xFFFFFFFFL) << 32);
    }

    public static short getShort0(long mask) {
        return (short) (mask & 0xFFFFL);
    }

    public static long setShort0(long mask,
                                 short value) {
        return (mask & 0xFFFFFFFFFFFF0000L) | (value & 0xFFFFL);
    }

    public static short getShort1(long mask) {
        return (short) ((mask & 0xFFFF0000L) >>> 16);
    }

    public static long setShort1(long mask,
                                 short value) {
        return (mask & 0xFFFFFFFF0000FFFFL) | ((value & 0xFFFFL) << 16);
    }

    public static short getShort2(long mask) {
        return (short) ((mask & 0xFFFF00000000L) >>> 32);
    }

    public static long setShort2(long mask,
                                 short value) {
        return (mask & 0xFFFF0000FFFFFFFFL) | ((value & 0xFFFFL) << 32);
    }

    public static short getShort3(long mask) {
        return (short) ((mask & 0xFFFF000000000000L) >>> 48);
    }

    public static long setShort3(long mask,
                                 short value) {
        return (mask & 0xFFFF0000FFFFFFFFL) | ((value & 0xFFFFL) << 48);
    }

    public static byte getByte0(long mask) {
        return (byte) (mask & 0xFFL);
    }

    public static long setByte0(long mask,
                                byte value) {
        return (mask & 0xFFFFFFFFFFFFFF00L) | (value & 0xFFL);
    }

    public static byte getByte1(long mask) {
        return (byte) ((mask & 0xFF00L) >>> 8);
    }

    public static long setByte1(long mask,
                                byte value) {
        return (mask & 0xFFFFFFFFFFFF00FFL) | ((value & 0xFFL) << 8);
    }

    public static byte getByte2(long mask) {
        return (byte) ((mask & 0xFF0000L) >>> 16);
    }

    public static long setByte2(long mask,
                                byte value) {
        return (mask & 0xFFFFFFFFFF00FFFFL) | ((value & 0xFFL) << 16);
    }

    public static byte getByte3(long mask) {
        return (byte) ((mask & 0xFF000000L) >>> 24);
    }

    public static long setByte3(long mask,
                                byte value) {
        return (mask & 0xFFFFFFFF00FFFFFFL) | ((value & 0xFFL) << 24);
    }

    public static byte getByte4(long mask) {
        return (byte) ((mask & 0xFF00000000L) >>> 32);
    }

    public static long setByte4(long mask,
                                byte value) {
        return (mask & 0xFFFFFF00FFFFFFFFL) | ((value & 0xFFL) << 32);
    }

    public static byte getByte5(long mask) {
        return (byte) ((mask & 0xFF0000000000L) >>> 40);
    }

    public static long setByte5(long mask,
                                byte value) {
        return (mask & 0xFFFF00FFFFFFFFFFL) | ((value & 0xFFL) << 40);
    }

    public static byte getByte6(long mask) {
        return (byte) ((mask & 0xFF000000000000L) >>> 48);
    }

    public static long setByte6(long mask,
                                byte value) {
        return (mask & 0xFF00FFFFFFFFFFFFL) | ((value & 0xFFL) << 48);
    }

    public static byte getByte7(long mask) {
        return (byte) ((mask & 0xFF00000000000000L) >>> 56);
    }

    public static long setByte7(long mask,
                                byte value) {
        return (mask & 0xFFFFFFFFFFFFFFL) | ((value & 0xFFL) << 56);
    }

    /**
     * Return the long mask corresponding to one bit at a given index. - toMask
     * (0) == 1 - toMask (1) == 10 - toMask (2) == 100 - ...
     *
     * @param index The index. Must be in range [0, 31] inclusive
     * @return The long mask corresponding to one bit at index.
     */
    public static long toMask(int index) {
        checkIndex(index);
        return 1L << index;
    }

    /**
     * Return whether the index-th bit of a mask is set or not.
     *
     * @param mask The tested mask.
     * @param index The tested index. Must be in range [0, 31] inclusive.
     * @return Whether index-th bit of mask is set or not.
     */
    public static boolean isEnabled(long mask,
                                    int index) {
        return (mask & toMask(index)) != 0L;
    }

    /**
     * Set the index-th bit of a mask to 0 or 1.
     *
     * @param mask The mask to set.
     * @param index Index of the bit to set. Must be in range [0, 31] inclusive.
     * @param enabled If true, set the index-th bit to 1, otherwise set it to 0.
     * @return The input mask with its index-th bit modified.
     */
    public static long setEnabled(long mask,
                                  int index,
                                  boolean enabled) {
        if (enabled) {
            return mask | toMask(index);
        } else {
            return mask & ~toMask(index);
        }
    }

    /**
     * Return the maximum possible index that can be used to encode length bits.
     * If length is invalid, return -1;
     *
     * @param length The number of bits to encode. Must be in range [1..32] to
     *            obtain a valid result.
     * @return The maximum possible index that can be used to encode length
     *         bits, or -1.
     */
    public static int getMaxIndex(int length) {
        if (length <= 0 || length > MAX_BITS) {
            return -1;
        } else {
            return MAX_BITS - length;
        }
    }

    /**
     * Return the long mask corresponding to length bits with a given Least
     * Significant bit.
     * <ul>
     * <li>toMask(index, 1) = toMask(index)</li>
     * <li>toMask(0, 2) = 11</li>
     * <li>toMask(1, 2) = 110</li>
     * <li>toMask(2, 3) = 11100</li>
     * </ul>
     *
     * @param index Index of the Least Significant bit.
     * @param length Number of bits.
     * @return The corresponding long mask;
     */
    public static long toMask(int index,
                              int length) {
        checkIndexLength(index, length);
        return (FULL_MASK >>> (MAX_BITS - length)) << index;
    }

    /**
     * Set bits in range [index, index + length[ to a given value.
     * <br>
     * Constraints:
     * <ul>
     * <li>{@literal index >= 0}</li>
     * <li>{@literal length > 0}</li>
     * <li>{@literal index + length <= MAX_BITS}</li>
     * </ul>
     *
     * @param mask The mask whose bits must be modified.
     * @param index Index of the Least Significant bit to set.
     * @param length Number of bits to set.
     * @param value The value to set. Must fit in length bit(s).
     * @return The modified input mask.
     */
    public static long set(long mask,
                           int index,
                           int length,
                           long value) {
        checkIndexLength(index, length);
        checkValueLength(value, length);
        final long m = toMask(index, length);
        return (mask & ~m) | ((value << index) & m);
    }

    /**
     * Return the value corresponding to bits in range [index, index + length[.
     * <br>
     * Constraints:
     * <ul>
     * <li>{@literal index >= 0}</li>
     * <li>{@literal length > 0}</li>
     * <li>{@literal index + length <= MAX_BITS}</li>
     * </ul>
     *
     * @param mask The mask whose bits must be extracted.
     * @param index Index of the Least Significant bit to set.
     * @param length Number of bits to set.
     * @return The value corresponding to bits in range [index, index + length[.
     */
    public static long get(long mask,
                           int index,
                           int length) {
        checkIndexLength(index, length);
        return (mask >> index) & (FULL_MASK >>> (MAX_BITS - length));
    }

    public static <E extends Enum<E>> long set(long mask,
                                               int index,
                                               E value,
                                               Class<E> enumClass,
                                               boolean nullable) {
        return set(mask, index, Masks.getLength(enumClass, nullable), value == null ? 0 : value.ordinal() + 1);
    }

    public static <E extends Enum<E>> E get(long mask,
                                            int index,
                                            Class<E> enumClass,
                                            boolean nullable) {
        final long i = get(mask, index, Masks.getLength(enumClass, nullable));
        return i == 0 ? null : enumClass.getEnumConstants()[(int) (i - 1)];
    }

    public static <E extends Enum<E>> long toMask(E value) {
        return toMask(Masks.toIndex(value));
    }

    @SafeVarargs
    public static <E extends Enum<E>> long toMask(E... values) {
        long mask = 0L;
        for (final E value : values) {
            mask = setEnabled(mask, value, true);
        }
        return mask;
    }

    public static <E extends Enum<E>> long setEnabled(long mask,
                                                      E value,
                                                      boolean enabled) {
        return setEnabled(mask, Masks.toIndex(value), enabled);
    }

    public static <E extends Enum<E>> boolean isEnabled(long mask,
                                                        E value) {
        return isEnabled(mask, Masks.toIndex(value));
    }

    public static <E extends Enum<E>> String toString(long mask,
                                                      Class<E> enumClass) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final E value : enumClass.getEnumConstants()) {
            if (isEnabled(mask, value)) {
                if (!first) {
                    builder.append("|");
                }
                first = false;
                builder.append(value.name());
            }
        }
        return builder.toString();
    }

    public static String toPaddedBinString(long val) {
        final char[] buffer = new char[MAX_BITS];
        Arrays.fill(buffer, '0');
        for (int i = 0; i < MAX_BITS; ++i) {
            final long mask = 1L << i;
            if ((val & mask) == mask) {
                buffer[MAX_BITS - 1 - i] = '1';
            }
        }
        return new String(buffer);
    }
}