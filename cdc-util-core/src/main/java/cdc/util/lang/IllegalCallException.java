package cdc.util.lang;

public class IllegalCallException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public IllegalCallException() {
        super();
    }

    public IllegalCallException(String message) {
        super(message);
    }

    public IllegalCallException(String message,
                                Throwable cause) {
        super(message, cause);
    }

    public IllegalCallException(Throwable cause) {
        super(cause);
    }
}