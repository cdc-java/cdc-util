package cdc.util.lang;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Set of utilities to check arguments validity
 *
 * @author Damien Carbonne
 *
 */
public final class Checks {
    private static final String NULL = "Null ";
    private static final String NOT_NULL = "Not null ";
    private static final String EMPTY = "Empty ";
    private static final String NOT_IN = ") not in [";

    private Checks() {
    }

    /**
     * Replaces occurrences of <code>{}</code> by conversion of objects to strings.
     * <p>
     * The number of occurrences of <code>{}</code> in {@code format} should match the length of {@code args}.<br>
     * Additional {@code args} are ignored.<br>
     * Missing {@code args} are replaced by {@code '?'}.
     * <p>
     * Examples:
     * <ul>
     * <li><code>toString("xxx {} yyy {}", o1, o2) : "xxx " + o1 + " yyy " + o2</code>
     * <li><code>toString("xxx {} yyy {}", o1) : "xxx " + o1 + " yyy " + "?"</code>
     * <li><code>toString("xxx {} yyy {}", o1, o2, o3) : "xxx " + o1 + " yyy " + o2</code>
     * </ul>
     *
     * @param format The format.
     * @param args The arguments.
     * @return A formatted string, where successive occurrences of <code>{}</code>
     *         are replaced by conversion of {@code args} to strings.
     */
    public static String toString(String format,
                                  Object... args) {
        final StringBuilder builder = new StringBuilder();
        final int formatLength = format.length();
        final int argsLength = args.length;
        int argIndex = 0;
        int from = 0;

        while (from < formatLength) {
            final int to = format.indexOf("{}", from);
            if (to >= 0) {
                builder.append(format, from, to);
                // append next arg
                if (argIndex < argsLength) {
                    builder.append(args[argIndex]);
                } else {
                    builder.append('?');
                }
                argIndex++;
                from = to + 2;
            } else {
                builder.append(format, from, formatLength);
                from = formatLength;
            }
        }
        return builder.toString();
    }

    /**
     * Checks that an argument is not {@code null}.
     *
     * @param <T> The object type.
     *
     * @param arg The argument.
     * @param argName The argument name.
     * @return {@code arg} if not {@code null}.
     * @throws IllegalArgumentException When {@code arg} is {@code null}.
     */
    public static <T> T isNotNull(T arg,
                                  String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else {
            return arg;
        }
    }

    /**
     * Checks that an argument is {@code null}.
     *
     * @param <T> The object type.
     *
     * @param arg The argument.
     * @param argName The argument name.
     * @return {@code arg} if {@code null}.
     * @throws IllegalArgumentException When {@code arg} is not {@code null}.
     */
    public static <T> T isNull(T arg,
                               String argName) {
        if (arg != null) {
            throw new IllegalArgumentException(NOT_NULL + argName);
        } else {
            return arg;
        }
    }

    /**
     * Checks that a string argument is not null or empty.
     *
     * @param arg The argument.
     * @param argName The argument name.
     * @return {@code arg} if its is neither {@code null} nor empty.
     * @throws IllegalArgumentException When {@code arg} is {@code null} or empty.
     */
    public static String isNotNullOrEmpty(String arg,
                                          String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (arg.isEmpty()) {
            throw new IllegalArgumentException(EMPTY + argName);
        } else {
            return arg;
        }
    }

    /**
     * Checks that a collection argument is not {@code null} or empty.
     *
     * @param <C> The collection type.
     * @param <E> The element type.
     *
     * @param arg The argument.
     * @param argName The argument name.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is {@code null} or empty.
     */
    public static <C extends Collection<E>, E> C isNotNullOrEmpty(C arg,
                                                                  String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (arg.isEmpty()) {
            throw new IllegalArgumentException(EMPTY + argName);
        } else {
            return arg;
        }
    }

    /**
     * Checks that an array argument is not {@code null} or empty.
     *
     * @param <T> The array element type.
     *
     * @param arg The argument.
     * @param argName The argument name.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is {@code null} or empty.
     */
    public static <T> T[] isNotNullOrEmpty(T[] arg,
                                           String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (arg.length == 0) {
            throw new IllegalArgumentException(EMPTY + argName);
        } else {
            return arg;
        }
    }

    /**
     * Checks that an argument is an instance of a class.
     *
     * @param <T> The argument type.
     * @param arg The argument.
     * @param cls The class.
     * @param argName The argument name.
     * @return {@code arg}.
     * @throws IllegalArgumentException When {@code arg} is not an instance of {@code cls}.
     */
    public static <T> T isInstance(T arg,
                                   Class<?> cls,
                                   String argName) {
        if (!cls.isInstance(arg)) {
            throw new IllegalArgumentException(argName + " is not an instance of " + cls.getCanonicalName());
        }
        return arg;
    }

    /**
     * Checks that collection does not contain an element.
     *
     * @param <C> The collection type.
     * @param <E> The collection element type.
     * @param arg The argument.
     * @param object The element.
     * @param argName The argument name.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} contains {@code object}.
     */
    public static <C extends Collection<? extends E>, E> C doesNotContain(C arg,
                                                                          Object object,
                                                                          String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (arg.contains(object)) {
            throw new IllegalArgumentException(argName + " already contains '" + object + "'");
        } else {
            return arg;
        }
    }

    public static <C extends Collection<? extends E>, E> C contain(C arg,
                                                                   Object object,
                                                                   String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (!arg.contains(object)) {
            throw new IllegalArgumentException(argName + " does not contain '" + object + "'");
        } else {
            return arg;
        }
    }

    /**
     * Checks that map does not contain an key.
     *
     * @param <M> The map type.
     * @param <K> The key type.
     * @param <V> The value type.
     * @param arg The map argument.
     * @param key The key.
     * @param argName The argument name.
     * @return {@code arg}.
     * @throws IllegalArgumentException When {@code arg} contains key {@code key}.
     */
    public static <M extends Map<K, V>, K, V> M doesNotContainKey(M arg,
                                                                  Object key,
                                                                  String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (arg.containsKey(key)) {
            throw new IllegalArgumentException(argName + " already contains key '" + key + "'");
        } else {
            return arg;
        }
    }

    public static <M extends Map<K, V>, K, V> M containKey(M arg,
                                                           Object key,
                                                           String argName) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL + argName);
        } else if (!arg.containsKey(key)) {
            throw new IllegalArgumentException(argName + " does not contains key '" + key + "'");
        } else {
            return arg;
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     *
     * @param message The exception message.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              String message) {
        if (!value) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     * @param messageSupplier The exception message supplier.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              Supplier<String> messageSupplier) {
        if (!value) {
            throw new IllegalArgumentException(messageSupplier.get());
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param args The array of exception message arguments.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              String format,
                              Object... args) {
        if (!value) {
            throw new IllegalArgumentException(toString(format, args));
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              String format,
                              Object arg0) {
        if (!value) {
            throw new IllegalArgumentException(toString(format, arg0));
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              String format,
                              Object arg0,
                              Object arg1) {
        if (!value) {
            throw new IllegalArgumentException(toString(format, arg0, arg1));
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              String format,
                              Object arg0,
                              Object arg1,
                              Object arg2) {
        if (!value) {
            throw new IllegalArgumentException(toString(format, arg0, arg1, arg2));
        }
    }

    /**
     * Checks that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @param arg3 The exception message 4th argument.
     * @throws IllegalArgumentException When {@code value} is {@code false}.
     */
    public static void isTrue(boolean value,
                              String format,
                              Object arg0,
                              Object arg1,
                              Object arg2,
                              Object arg3) {
        if (!value) {
            throw new IllegalArgumentException(toString(format, arg0, arg1, arg2, arg3));
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param message The exception message.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               String message) {
        if (value) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param messageSupplier The exception message supplier.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               Supplier<String> messageSupplier) {
        if (value) {
            throw new IllegalArgumentException(messageSupplier.get());
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param args The array of exception message arguments.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               String format,
                               Object... args) {
        if (value) {
            throw new IllegalArgumentException(toString(format, args));
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               String format,
                               Object arg0) {
        if (value) {
            throw new IllegalArgumentException(toString(format, arg0));
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               String format,
                               Object arg0,
                               Object arg1) {
        if (value) {
            throw new IllegalArgumentException(toString(format, arg0, arg1));
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               String format,
                               Object arg0,
                               Object arg1,
                               Object arg2) {
        if (value) {
            throw new IllegalArgumentException(toString(format, arg0, arg1, arg2));
        }
    }

    /**
     * Checks that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @param arg3 The exception message 4th argument.
     * @throws IllegalArgumentException When {@code value} is {@code true}.
     */
    public static void isFalse(boolean value,
                               String format,
                               Object arg0,
                               Object arg1,
                               Object arg2,
                               Object arg3) {
        if (value) {
            throw new IllegalArgumentException(toString(format, arg0, arg1, arg2, arg3));
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param message The exception message.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  String message) {
        if (!value) {
            throw new AssertionError(message);
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param messageSupplier The exception message supplier.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  Supplier<String> messageSupplier) {
        if (!value) {
            throw new AssertionError(messageSupplier.get());
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param args The array of exception message arguments.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  String format,
                                  Object... args) {
        if (!value) {
            throw new AssertionError(toString(format, args));
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  String format,
                                  Object arg0) {
        if (!value) {
            throw new AssertionError(toString(format, arg0));
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  String format,
                                  Object arg0,
                                  Object arg1) {
        if (!value) {
            throw new AssertionError(toString(format, arg0, arg1));
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  String format,
                                  Object arg0,
                                  Object arg1,
                                  Object arg2) {
        if (!value) {
            throw new AssertionError(toString(format, arg0, arg1, arg2));
        }
    }

    /**
     * Asserts that a value is {@code true}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @param arg3 The exception message 4th argument.
     * @throws AssertionError When {@code value} is {@code false}.
     */
    public static void assertTrue(boolean value,
                                  String format,
                                  Object arg0,
                                  Object arg1,
                                  Object arg2,
                                  Object arg3) {
        if (!value) {
            throw new AssertionError(toString(format, arg0, arg1, arg2, arg3));
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param message The exception message.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   String message) {
        if (value) {
            throw new AssertionError(message);
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param messageSupplier The exception message supplier.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   Supplier<String> messageSupplier) {
        if (value) {
            throw new AssertionError(messageSupplier.get());
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param args The array of exception message arguments.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   String format,
                                   Object... args) {
        if (value) {
            throw new AssertionError(toString(format, args));
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   String format,
                                   Object arg0) {
        if (value) {
            throw new AssertionError(toString(format, arg0));
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   String format,
                                   Object arg0,
                                   Object arg1) {
        if (value) {
            throw new AssertionError(toString(format, arg0, arg1));
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   String format,
                                   Object arg0,
                                   Object arg1,
                                   Object arg2) {
        if (value) {
            throw new AssertionError(toString(format, arg0, arg1, arg2));
        }
    }

    /**
     * Asserts that a value is {@code false}.
     *
     * @param value The value.
     * @param format The exception message format.
     * @param arg0 The exception message 1st argument.
     * @param arg1 The exception message 2nd argument.
     * @param arg2 The exception message 3rd argument.
     * @param arg3 The exception message 4th argument.
     * @throws AssertionError When {@code value} is {@code true}.
     */
    public static void assertFalse(boolean value,
                                   String format,
                                   Object arg0,
                                   Object arg1,
                                   Object arg2,
                                   Object arg3) {
        if (value) {
            throw new AssertionError(toString(format, arg0, arg1, arg2, arg3));
        }
    }

    /**
     * Throws an exception.
     * <p>
     * This may be used in switch cases:
     *
     * <pre>
     * <code>
     * switch (e) {
     * case ...:
     *    break;
     * case ...:
     *    break;
     * default:
     *    unexpectedValue(e, "e");
     *    break;
     * }
     * </code>
     * </pre>
     *
     * @param value The unexpected value.
     * @param valueName The value name
     * @throws UnexpectedValueException In all cases.
     */
    public static void unexpectedValue(Object value,
                                       String valueName) {
        throw new UnexpectedValueException(valueName + " '" + value + "'");
    }

    /**
     * Checks that an argument is in a range.
     *
     * @param arg The argument.
     * @param min The min value (inclusive).
     * @param max The max value (inclusive)
     * @param argName The argument name.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is not in [min, max].
     */
    public static long isInRange(long arg,
                                 String argName,
                                 long min,
                                 long max) {
        if (arg < min || arg > max) {
            throw new IllegalArgumentException(argName + "(" + arg + NOT_IN + min + ", " + max + "]");
        } else {
            return arg;
        }
    }

    /**
     * Checks that an argument is in a range.
     *
     * @param arg The argument.
     * @param min The min value (inclusive).
     * @param max The max value (inclusive)
     * @param argName The argument name.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is not in [min, max].
     */
    public static int isInRange(int arg,
                                String argName,
                                int min,
                                int max) {
        if (arg < min || arg > max) {
            throw new IllegalArgumentException(argName + "(" + arg + NOT_IN + min + ", " + max + "]");
        } else {
            return arg;
        }
    }

    /**
     * Checks that an argument is in a range.
     *
     * @param arg The argument.
     * @param min The min value (inclusive).
     * @param max The max value (inclusive)
     * @param argName The argument name.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is not in [min, max].
     */
    public static double isInRange(double arg,
                                   String argName,
                                   double min,
                                   double max) {
        if (arg < min || arg > max) {
            throw new IllegalArgumentException(argName + "(" + arg + NOT_IN + min + ", " + max + "]");
        } else {
            return arg;
        }
    }

    /**
     * Checks that the length of a String has a given value.
     *
     * @param arg The string to check.
     * @param argName The argument name.
     * @param length The expected length.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is {@code null},
     *             or its length is different from {@code length}.
     */
    public static String hasFixedLength(String arg,
                                        String argName,
                                        int length) {
        isNotNull(arg, argName);
        if (arg.length() != length) {
            throw new IllegalArgumentException(argName + " length != " + length + " (" + arg + ")");
        }
        return arg;
    }

    /**
     * Checks that the length of a string is in a given range.
     *
     * @param arg The string to check.
     * @param argName The argument name.
     * @param minLength The min length (inclusive).
     * @param maxLength The max length (inclusive)
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is {@code null},
     *             or its length is not in {@code [minLength, maxLength]}.
     */
    public static String hasLengthInRange(String arg,
                                          String argName,
                                          int minLength,
                                          int maxLength) {
        isNotNull(arg, argName);
        if (arg.length() < minLength || arg.length() > maxLength) {
            throw new IllegalArgumentException(argName + " length not in [" + minLength + ", " + maxLength + "] (" + arg + ")");
        }
        return arg;
    }

    /**
     * Checks that an enum argument belongs to a set of values.
     *
     * @param <E> The enum type.
     * @param arg The argument.
     * @param argName The argument name.
     * @param expected The list of valid values.
     * @return {@code arg}
     * @throws IllegalArgumentException When {@code arg} is not one of {@code expected}.
     */
    @SafeVarargs
    public static <E extends Enum<E>> E isAmong(E arg,
                                                String argName,
                                                E... expected) {
        for (final E e : expected) {
            if (arg == e) {
                return arg;
            }
        }
        throw new IllegalArgumentException(argName + " (" + arg + ") is not among " + Arrays.toString(expected));
    }

    /**
     * Checks that an array of enum values match a predicate.
     *
     * @param <E> The enum type.
     * @param predicate The predicate.
     * @param argName The argument name.
     * @param values The enum values to test.
     * @return {@code values}.
     * @throws IllegalArgumentException When one value of {@code values} is not accepted by {@code predicate}.
     */
    @SafeVarargs
    public static <E> E[] areAccepted(Predicate<? super E> predicate,
                                      String argName,
                                      E... values) {
        for (final E value : values) {
            if (!predicate.test(value)) {
                throw new IllegalArgumentException(argName + " '" + value + "' is not accepted");
            }
        }
        return values;
    }

    /**
     * Checks that a set of collection of values match a predicate.
     *
     * @param <E> The enum type.
     * @param <C> The collection type.
     * @param predicate The predicate.
     * @param argName The argument name.
     * @param values The enum values to test.
     * @return {@code values}.
     * @throws IllegalArgumentException When one value of {@code values} is not accepted by {@code predicate}.
     */
    public static <E, C extends Collection<? extends E>> C areAccepted(Predicate<? super E> predicate,
                                                                       String argName,
                                                                       C values) {
        for (final E value : values) {
            if (!predicate.test(value)) {
                throw new IllegalArgumentException(argName + " '" + value + "' is not accepted");
            }
        }
        return values;
    }
}