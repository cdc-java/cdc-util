package cdc.util.lang;

import java.util.Objects;

public final class Operators {
    private Operators() {
    }

    /**
     * Safely compares two objects (possibly null).
     *
     * @param o1 first object
     * @param o2 second object
     * @return whether o1 equals o2
     */
    public static boolean equals(Object o1,
                                 Object o2) {
        return Objects.equals(o1, o2);
    }

    public static boolean notEquals(Object o1,
                                    Object o2) {
        return !equals(o1, o2);
    }

    public static Class<?> getClass(Object o) {
        return o == null ? null : o.getClass();
    }

    public static String getClassSimpleName(Object o) {
        return o == null ? "null" : o.getClass().getSimpleName();
    }

    public static String getClassCanonicalName(Object o) {
        return o == null ? "null" : o.getClass().getCanonicalName();
    }

    public static String toString(Object o) {
        return Objects.toString(o);
    }

    public static String toStringWithClass(Object o) {
        return o == null ? "null" : o.toString() + " [" + o.getClass().getCanonicalName() + "]";
    }

    public static <T extends Comparable<? super T>> int compare(T l,
                                                                T r) {
        if (l == null) {
            if (r == null) {
                return 0;
            } else {
                return -1;
            }
        } else {
            if (r == null) {
                return 1;
            } else {
                return l.compareTo(r);
            }
        }
    }

    public static <T extends Comparable<? super T>> T min(T l,
                                                          T r) {
        return compare(l, r) < 0 ? l : r;
    }

    public static <T extends Comparable<? super T>> T max(T l,
                                                          T r) {
        return compare(l, r) > 0 ? l : r;
    }

    public static int compare(long l,
                              long r) {
        return Long.compare(l, r);
    }

    public static int compare(int l,
                              int r) {
        return Integer.compare(l, r);
    }

    public static int compare(short l,
                              short r) {
        return Short.compare(l, r);
    }

    public static int compare(byte l,
                              byte r) {
        return Byte.compare(l, r);
    }

    public static int compare(char l,
                              char r) {
        return Character.compare(l, r);
    }

    public static int compare(boolean l,
                              boolean r) {
        return Boolean.compare(l, r);
    }

    public static int compare(double l,
                              double r) {
        return Double.compare(l, r);
    }

    public static int compare(float l,
                              float r) {
        return Float.compare(l, r);
    }

    public static int hashCode(Object o) {
        return Objects.hashCode(o);
    }

    public static int hashCode(long value) {
        return Long.hashCode(value);
    }

    public static int hashCode(int value) {
        return Integer.hashCode(value);
    }

    public static int hashCode(short value) {
        return Short.hashCode(value);
    }

    public static int hashCode(byte value) {
        return Byte.hashCode(value);
    }

    public static int hashCode(char value) {
        return Character.hashCode(value);
    }

    public static int hashCode(boolean value) {
        return Boolean.hashCode(value);
    }

    public static int hashCode(double value) {
        return Double.hashCode(value);
    }

    public static int hashCode(float value) {
        return Float.hashCode(value);
    }
}