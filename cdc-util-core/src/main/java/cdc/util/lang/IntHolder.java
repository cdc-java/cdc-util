package cdc.util.lang;

public class IntHolder {
    public int value;

    public IntHolder() {
        this.value = 0;
    }

    public IntHolder(int value) {
        this.value = value;
    }
}