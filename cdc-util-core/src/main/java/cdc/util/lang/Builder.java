package cdc.util.lang;

/**
 * Base interface of builders.
 *
 * @param <B> The Builder type.
 * @param <T> The built type.
 */
public interface Builder<B, T> extends Building<B> {
    /**
     * @return a new instance of the built type.
     */
    public T build();
}