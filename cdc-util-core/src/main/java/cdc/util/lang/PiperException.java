package cdc.util.lang;

public class PiperException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PiperException() {
        super();
    }

    public PiperException(String message) {
        super(message);
    }

    public PiperException(String message,
                          Throwable cause) {
        super(message,
              cause);
    }
}