package cdc.util.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class CollectionUtils {
    private CollectionUtils() {
    }

    /**
     * Changes the index of an element in a list.
     *
     * @param <T> The type of elements in the list.
     * @param list The list.
     * @param element The element. It <em>MUST</em> belong to the list.
     * @param to The new index of element. {@code 0 <= to < list.size()}
     * @throws IllegalArgumentException When {@code list} is {@code null}<br>
     *             or {@code element} does not belong to {@code list}<br>
     *             or {@code to < 0 or  to >= list.size()}
     */
    public static <T> void setIndex(List<? extends T> list,
                                    T element,
                                    int to) {
        Checks.isNotNull(list, "list");
        Checks.isInRange(to, "to", 0, list.size() - 1);
        final int from = list.indexOf(element);
        if (from >= 0) {
            final int shift = to - from;
            if (shift > 0) {
                Collections.rotate(list.subList(from, to + 1), -1);
            } else if (shift < 0) {
                Collections.rotate(list.subList(to, from + 1), 1);
            }
        } else {
            throw new IllegalArgumentException("Invalid element");
        }
    }

    public static <T extends Comparable<? super T>> List<T> toSortedList(Collection<? extends T> values) {
        final List<T> result = new ArrayList<>(values);
        Collections.sort(result);
        return result;
    }

    public static <T> List<T> toSortedList(Collection<? extends T> values,
                                           Comparator<? super T> comparator) {
        final List<T> result = new ArrayList<>(values);
        Collections.sort(result, comparator);
        return result;
    }

    /**
     * Converts an array of values to a modifiable list.
     * <p>
     * <b>WARNING:</b> use {@link List#of(Object...)} if a unmodifiable list is expected.
     *
     * @param <T> The element type.
     * @param values The values.
     * @return A modifiable list containing {@code values}.
     */
    @SafeVarargs
    public static <T> List<T> toList(T... values) {
        final List<T> result = new ArrayList<>();
        Collections.addAll(result, values);
        return result;
    }

    /**
     * Converts a list to an unmodifiable list.
     *
     * @param <T> The element type.
     * @param list The list.
     * @return An unmodifiable list that has the same content as {@code list}.
     */
    public static <T> List<T> seal(List<? extends T> list) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        } else {
            return Collections.unmodifiableList(list);
        }
    }

    /**
     * Converts an array of values to a modifiable set.
     * <p>
     * <b>WARNING:</b> use {@link Set#of(Object...)} if a unmodifiable set is expected.
     *
     * @param <T> The element type.
     * @param values The values.
     * @return A modifiable set containing {@code values}.
     */
    @SafeVarargs
    public static <T> Set<T> toSet(T... values) {
        final Set<T> result = new HashSet<>();
        Collections.addAll(result, values);
        return result;
    }

    /**
     * Converts a collection to a set.
     * <p>
     * If the collection is a set, simply converts the collection.<br>
     * Otherwise, constructs a new set.
     * <p>
     * <b>WARNING:</b> use {@link Set#of(Object...)} if possible.
     *
     * @param <T> The element type.
     * @param values The collection.
     * @return A set containing elements of {@code values}.
     */
    public static <T> Set<T> toSet(Collection<T> values) {
        if (values instanceof Set) {
            return (Set<T>) values;
        } else {
            return new HashSet<>(values);
        }
    }

    /**
     * Converts a set to an unmodifiable set.
     *
     * @param <T> The element type.
     * @param set The set.
     * @return An unmodifiable set that has the same content as {@code set}.
     */
    public static <T> Set<T> seal(Set<? extends T> set) {
        if (set == null || set.isEmpty()) {
            return Collections.emptySet();
        } else {
            return Collections.unmodifiableSet(set);
        }
    }

    /**
     * Compares lexicographically 2 lists using a specific comparator.
     *
     * @param <T> The object type.
     * @param a The first list.
     * @param b The second list.
     * @param comparator The comparator.
     * @return {@code 0} if {@code a} and {@code b} are equal and
     *         contain the same elements in the same order;<br>
     *         A value less than {@code 0} if {@code a} is
     *         lexicographically less than {@code b};<br>
     *         And a value greater than {@code 0} if {@code a} is
     *         lexicographically greater than {@code b}.
     */
    public static <T> int compareLexicographic(List<? extends T> a,
                                               List<? extends T> b,
                                               Comparator<? super T> comparator) {
        if (a == b) {
            return 0;
        }

        if (a == null || b == null) {
            return a == null ? -1 : 1;
        }

        final int length = Math.min(a.size(), b.size());
        for (int index = 0; index < length; index++) {
            final T ta = a.get(index);
            final T tb = b.get(index);
            // Avoid ta == tb test
            final int cmp = comparator.compare(ta, tb);
            if (cmp != 0) {
                return cmp;
            }
        }

        return a.size() - b.size();
    }

    /**
     * Compares lexicographically 2 lists of comparable elements.
     *
     * @param <T> The object type.
     * @param a The first list.
     * @param b The second list.
     * @return {@code 0} if {@code a} and {@code b} are equal and
     *         contain the same elements in the same order;<br>
     *         A value less than {@code 0} if {@code a} is
     *         lexicographically less than {@code b};<br>
     *         And a value greater than {@code 0} if {@code a} is
     *         lexicographically greater than {@code b}.
     */
    public static <T extends Comparable<? super T>> int compareLexicographic(List<? extends T> a,
                                                                             List<? extends T> b) {
        if (a == b) {
            return 0;
        }

        if (a == null || b == null) {
            return a == null ? -1 : 1;
        }

        final int length = Math.min(a.size(), b.size());
        for (int index = 0; index < length; index++) {
            final T ta = a.get(index);
            final T tb = b.get(index);
            if (ta == null || tb == null) {
                return ta == null ? -1 : 1;
            }
            // Avoid ta == tb test
            final int cmp = ta.compareTo(tb);
            if (cmp != 0) {
                return cmp;
            }
        }

        return a.size() - b.size();
    }
}