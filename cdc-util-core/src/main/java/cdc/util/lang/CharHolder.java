package cdc.util.lang;

public class CharHolder {
    public char value;

    public CharHolder() {
        this.value = 0;
    }

    public CharHolder(char value) {
        this.value = value;
    }
}