package cdc.util.lang;

/**
 * Base interface of building.
 *
 * @param <B> The Builder type.
 */
public interface Building<B> {
    /**
     * @return This builder.
     */
    public B self();
}