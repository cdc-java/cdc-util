package cdc.util.lang;

import java.io.InputStream;
import java.net.URL;
import java.util.Optional;
import java.util.jar.Manifest;

/**
 * Utilities related to {@link Manifest}.
 *
 * @author Damien Carbonne
 */
public final class ManifestUtils {
    private ManifestUtils() {
    }

    public static final String IMPLEMENTATION_TITLE = "Implementation-Title";
    public static final String IMPLEMENTATION_VERSION = "Implementation-Version";
    public static final String IMPLEMENTATION_VENDOR = "Implementation-Vendor";
    public static final String IMPLEMENTATION_VENDOR_ID = "Implementation-Vendor-Id";
    public static final String IMPLEMENTATION_URL = "Implementation-URL";

    public static final String SPECIFICATION_TITLE = "Specification-Title";
    public static final String SPECIFICATION_VERSION = "Specification-Version";
    public static final String SPECIFICATION_VENDOR = "Specification-Vendor";
    public static final String SPECIFICATION_VENDOR_ID = "Specification-Vendor-Id";
    public static final String SPECIFICATION_URL = "Specification-URL";

    public static final String SEALED = "Sealed";

    /**
     * Returns the Manifest of the jar containing a class.
     *
     * @param cls The class.
     * @return The Manifest of the jar containing {@code cls}.
     */
    public static Optional<Manifest> getManifest(Class<?> cls) {
        final String resource = "/" + cls.getName().replace(".", "/") + ".class";
        final String fullPath = cls.getResource(resource).toString();
        String archivePath = fullPath.substring(0, fullPath.length() - resource.length());
        if (archivePath.endsWith("\\WEB-INF\\classes") || archivePath.endsWith("/WEB-INF/classes")) {
            archivePath = archivePath.substring(0, archivePath.length() - "/WEB-INF/classes".length()); // Required for wars
        }

        try (InputStream input = new URL(archivePath + "/META-INF/MANIFEST.MF").openStream()) {
            return Optional.of(new Manifest(input));
        } catch (final Exception e) {
            return Optional.empty();
        }
    }

    /**
     * Returns the value of a main attribute, or a default value.
     *
     * @param manifest The manifest.
     * @param name The main attribute name.
     * @param def The default value.
     * @return The value of the specified attribute {@code name} or {@code def}.
     */
    public static String getValue(Optional<Manifest> manifest,
                                  String name,
                                  String def) {
        final String value;
        if (manifest.isPresent()) {
            value = manifest.get().getMainAttributes().getValue(name);
        } else {
            value = null;
        }
        return value == null ? def : value;
    }
}