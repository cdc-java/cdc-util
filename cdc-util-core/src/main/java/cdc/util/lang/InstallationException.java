package cdc.util.lang;

public class InstallationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InstallationException() {
        super();
    }

    public InstallationException(String message) {
        super(message);
    }

    public InstallationException(Throwable cause) {
        super(cause);
    }

    public InstallationException(String message,
                                 Throwable cause) {
        super(message, cause);
    }
}