package cdc.util.lang;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cache of elements.
 *
 * @author Damien Carbonne
 *
 * @param <T> The element type.
 */
public class Cache<T> {
    private final Class<T> elementClass;
    private final Map<T, T> map = new HashMap<>();

    public Cache(Class<T> elementClass) {
        Checks.isNotNull(elementClass, "elementClass");
        this.elementClass = elementClass;
    }

    /**
     * Returns the cached value equals to an element.
     * <p>
     * If no cached value is found, then the passed element is cached and returned.
     *
     * @param x The element.
     * @return The cached value corresponding to {@code x} or {@code x}.
     */
    public final T get(T x) {
        final T result = map.putIfAbsent(x, x);
        return result == null ? x : result;
    }

    /**
     * @return The element class.
     */
    public final Class<T> getElementClass() {
        return elementClass;
    }

    /**
     * Clears this cache.
     */
    public void clear() {
        map.clear();
    }

    public void print(PrintStream out,
                      Comparator<T> comparator) {
        out.println(map.size() + " elements");
        final List<T> keys = new ArrayList<>();
        keys.addAll(map.keySet());
        if (comparator != null) {
            Collections.sort(keys, comparator);
        }
        for (final T key : keys) {
            out.println("   '" + key + "'");
        }
    }
}