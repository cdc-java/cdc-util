package cdc.util.lang;

import java.util.Arrays;
import java.util.Comparator;

public final class ArrayUtils {
    private ArrayUtils() {
    }

    /**
     * Search the index of the smallest value in a sorted array of doubles that
     * is greater or equal to input value.
     * <p>
     * If no such value exists, returns -(values.length)
     *
     * @param values The sorted array of values.
     * @param value The value to be searched.
     * @return The index of the smallest value in values that is greater or equal to value.
     */
    public static int indexOfSmallestGreaterOrEqual(double[] values,
                                                    double value) {
        final int index = Arrays.binarySearch(values, value);

        if (index >= 0) {
            return index;
        } else {
            // index == -(insertion point) - 1
            final int tmp = -(index + 1);
            return tmp < values.length ? tmp : -values.length;
        }
    }

    public static double smallestSaturatedGreaterOrEqual(double[] values,
                                                         double value) {
        final int index = indexOfSmallestGreaterOrEqual(values, value);
        return index >= 0 ? values[index] : values[values.length - 1];
    }

    public static int indexOfLargestSmallerOrEqual(double[] values,
                                                   double value) {
        final int index = Arrays.binarySearch(values, value);

        if (index >= 0) {
            return index;
        } else {
            // index == -(insertion point) - 1
            final int tmp = -(index + 1);
            return tmp - 1;
        }
    }

    public static double largestSaturatedSmallerOrEqual(double[] values,
                                                        double value) {
        final int index = indexOfLargestSmallerOrEqual(values, value);
        return index >= 0 ? values[index] : values[0];
    }

    /**
     * Compares lexicographically 2 arrays using a specific comparator.
     * <p>
     * <b>Note</b>This is available in Java 9.
     *
     * @param <T> The object type.
     * @param a The first array.
     * @param b The second array.
     * @param comparator The comparator.
     * @return {@code 0} if {@code a} and {@code b} are equal and
     *         contain the same elements in the same order;<br>
     *         A value less than {@code 0} if {@code a} is
     *         lexicographically less than {@code b};<br>
     *         And a value greater than {@code 0} if {@code a} is
     *         lexicographically greater than {@code b}.
     */
    public static <T> int compareLexicographic(T[] a,
                                               T[] b,
                                               Comparator<? super T> comparator) {
        if (a == b) {
            return 0;
        }

        if (a == null || b == null) {
            return a == null ? -1 : 1;
        }

        final int length = Math.min(a.length, b.length);
        for (int index = 0; index < length; index++) {
            final T ta = a[index];
            final T tb = b[index];
            // Avoid ta == tb test
            final int cmp = comparator.compare(ta, tb);
            if (cmp != 0) {
                return cmp;
            }
        }

        return a.length - b.length;
    }

    /**
     * Compares lexicographically 2 arrays of comparable elements.
     * <p>
     * <b>Note</b>This is available in Java 9.
     *
     * @param <T> The object type.
     * @param a The first array.
     * @param b The second array.
     * @return {@code 0} if {@code a} and {@code b} are equal and
     *         contain the same elements in the same order;<br>
     *         A value less than {@code 0} if {@code a} is
     *         lexicographically less than {@code b};<br>
     *         And a value greater than {@code 0} if {@code a} is
     *         lexicographically greater than {@code b}.
     */
    public static <T extends Comparable<? super T>> int compareLexicographic(T[] a,
                                                                             T[] b) {
        if (a == b) {
            return 0;
        }

        if (a == null || b == null) {
            return a == null ? -1 : 1;
        }

        final int length = Math.min(a.length, b.length);
        for (int index = 0; index < length; index++) {
            final T ta = a[index];
            final T tb = b[index];
            if (ta == null || tb == null) {
                return ta == null ? -1 : 1;
            }
            // Avoid ta == tb test
            final int cmp = ta.compareTo(tb);
            if (cmp != 0) {
                return cmp;
            }
        }

        return a.length - b.length;
    }
}