package cdc.util.lang;

import java.util.Arrays;

/**
 * Utilities using byte as mask.
 * <p>
 * Bits are numbered from 0 (Less Significant) to 7 (Most Significant).
 *
 * <pre>
 *   MSB    LSB
 *    7......0
 *      ^^^^
 *      |  |
 *      |  + index (2)
 *      + index (2) + length (4) - 1 = (5)
 * </pre>
 *
 * Those constraints always hold:
 * <ul>
 * <li>{@code 0 <=  index <= 7}</li>
 * <li>{@code 1 <= length (<= 8)}</li>
 * <li>{@code (1 <=) index + length <= 8}</li>
 * <li>{@code value} must fit in {@code length} bits</li>
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class ByteMasks {
    private static final int FULL_MASK = 0xFF;

    /**
     * The maximum number of bits that can be encoded with a byte.
     */
    public static final int MAX_BITS = 8;

    private ByteMasks() {
    }

    private static void checkIndex(int index) {
        if (index < 0 || index >= MAX_BITS) {
            throw new IllegalArgumentException("Invalid index: " + index);
        }
    }

    private static void checkIndexAndLength(int index,
                                            int length) {
        checkIndex(index);
        if (length <= 0 || length > MAX_BITS) {
            throw new IllegalArgumentException("Invalid length: " + length);
        }
        if (index + length > MAX_BITS) {
            throw new IllegalArgumentException("Invalid index/length " + index + " " + length);
        }
    }

    private static void checkValueLength(int value,
                                         int length) {
        if ((value & toMask(0, length)) != value) {
            throw new IllegalArgumentException("Invalid value: " + value + ", can not be coded on " + length + " bit(s)");
        }
    }

    /**
     * Returns the byte mask corresponding to one bit at a given index.
     * <p>
     * Examples:
     * <ul>
     * <li>{@code toMask(0) == 00000001}</li>
     * <li>{@code toMask(1) == 00000010}</li>
     * <li>{@code toMask(2) == 00000100}</li>
     * <li>...</li>
     * <li>{@code toMask(7) == 10000000}</li>
     * </ul>
     *
     * @param index The index.
     * @return The byte mask corresponding to one bit at index.
     * @throws IllegalArgumentException When index is invalid.
     */
    public static byte toMask(int index) {
        checkIndex(index);
        return (byte) (1 << index);
    }

    /**
     * Returns whether the index-th bit of a byte mask is set or not.
     *
     * @param mask The mask.
     * @param index The index.
     * @return Whether index-th bit of mask is set or not.
     * @throws IllegalArgumentException When index is invalid.
     */
    public static boolean isEnabled(byte mask,
                                    int index) {
        return (mask & toMask(index)) != 0;
    }

    /**
     * Sets the index-th bit of a byte mask to 0 or 1.
     * <p>
     * Examples:
     * <ul>
     * <li>{@code setEnabled(........, 0, true) == .......1}</li>
     * <li>{@code setEnabled(........, 0, false) == .......0}</li>
     * <li>...</li>
     * <li>{@code setEnabled(........, 7, true) == 1.......}</li>
     * <li>{@code setEnabled(........, 7, false) == 0.......}</li>
     * </ul>
     *
     * @param mask The mask.
     * @param index The bit index.
     * @param enabled If true, set the index-th bit to 1, otherwise set it to 0.
     * @return The input mask with its index-th bit modified.
     * @throws IllegalArgumentException When index is invalid.
     */
    public static byte setEnabled(byte mask,
                                  int index,
                                  boolean enabled) {
        if (enabled) {
            return (byte) (mask | toMask(index));
        } else {
            return (byte) (mask & ~toMask(index));
        }
    }

    /**
     * Returns the maximum possible index that can be used to encode length bits.
     * <p>
     * If length is invalid, returns -1;
     *
     * @param length The number of bits to encode.
     * @return The maximum possible index that can be used to encode length
     *         bits, or -1.
     */
    public static int getMaxIndex(int length) {
        if (length <= 0 || length > MAX_BITS) {
            return -1;
        } else {
            return MAX_BITS - length;
        }
    }

    /**
     * Returns the byte mask corresponding to length bits with a given Least
     * Significant bit.
     * <p>
     * Examples:
     * <ul>
     * <li>{@code toMask(index, 1) = toMask(index)}</li>
     * <li>{@code toMask(0, 2) = 00000011}</li>
     * <li>{@code toMask(1, 2) = 00000110}</li>
     * <li>{@code toMask(2, 3) = 00011100}</li>
     * <li>...</li>
     * </ul>
     *
     * @param index The index of the Least Significant bit.
     * @param length The number of bits.
     * @return The corresponding byte mask;
     * @throws IllegalArgumentException When index, length, or their combination is invalid.
     */
    public static byte toMask(int index,
                              int length) {
        checkIndexAndLength(index, length);
        return (byte) ((FULL_MASK >>> (MAX_BITS - length)) << index);
    }

    /**
     * Sets bits in range [index, index + length[ to a given value.
     *
     * @param mask The mask.
     * @param index The index of the Least Significant bit to set.
     * @param length The number of bits to set.
     * @param value The value to set.
     * @return The modified input mask.
     * @throws IllegalArgumentException When index, length, or their combination is invalid, or value does not fit in length bits.
     */
    public static byte set(byte mask,
                           int index,
                           int length,
                           int value) {
        checkIndexAndLength(index, length);
        checkValueLength(value, length);
        final byte m = toMask(index, length);
        return (byte) ((mask & ~m) | ((value << index) & m));
    }

    /**
     * Returns the value corresponding to bits in range [index, index + length[.
     *
     * @param mask The mask whose bits must be extracted.
     * @param index Index of the Least Significant bit to set.
     * @param length Number of bits to set.
     * @return The value corresponding to bits in range [index, index + length[.
     * @throws IllegalArgumentException When index, length, or their combination is invalid.
     */
    public static int get(byte mask,
                          int index,
                          int length) {
        checkIndexAndLength(index, length);
        return (mask >> index) & (FULL_MASK >>> (MAX_BITS - length));
    }

    public static <E extends Enum<E>> byte set(byte mask,
                                               int index,
                                               E value,
                                               Class<E> enumClass,
                                               boolean nullable) {
        Masks.checkNullableValue(value, nullable);
        return set(mask,
                   index,
                   Masks.getLength(enumClass, nullable),
                   Masks.toValue(value, nullable));
    }

    public static <E extends Enum<E>> byte set(byte mask,
                                               int index,
                                               E value,
                                               E[] values,
                                               boolean nullable) {
        Masks.checkNullableValue(value, nullable);
        return set(mask,
                   index,
                   Masks.getLength(values, nullable),
                   Masks.toValue(value, nullable));
    }

    public static <E extends Enum<E>> E get(byte mask,
                                            int index,
                                            Class<E> enumClass,
                                            boolean nullable) {
        final int i = get(mask, index, Masks.getLength(enumClass, nullable));
        return Masks.toEnum(enumClass, i, nullable);
    }

    public static <E extends Enum<E>> E get(byte mask,
                                            int index,
                                            E[] values,
                                            boolean nullable) {
        final int i = get(mask, index, Masks.getLength(values, nullable));
        return Masks.toEnum(values, i, nullable);
    }

    /**
     * Converts an enum value to a byte mask.
     *
     * @param <E> The enum type.
     * @param value the enum value.
     * @return The mask corresponding to value.
     * @throws IllegalArgumentException When value is null.
     */
    public static <E extends Enum<E>> byte toMask(E value) {
        return toMask(Masks.toIndex(value));
    }

    /**
     * Converts a set of enum values to a byte mask.
     *
     * @param <E> The enum type.
     * @param values The values.
     * @return The mask corresponding to values.
     * @throws IllegalArgumentException When a value is null.
     */
    @SafeVarargs
    public static <E extends Enum<E>> byte toMask(E... values) {
        byte mask = 0;
        for (final E value : values) {
            mask = setEnabled(mask, value, true);
        }
        return mask;
    }

    /**
     * Sets, in a byte mask, the bit corresponding to an enum value.
     *
     * @param <E> The enum type.
     * @param mask The mask.
     * @param value The enum value.
     * @param enabled If true, set the bit corresponding to value to 1, otherwise set it to 0.
     * @return The input mask with its bit corresponding to value modified.
     */
    public static <E extends Enum<E>> byte setEnabled(byte mask,
                                                      E value,
                                                      boolean enabled) {
        return setEnabled(mask, Masks.toIndex(value), enabled);
    }

    /**
     * Returns whether the bit corresponding to an enum value is set or not in a byte mask.
     *
     * @param <E> The enum type.
     * @param mask The mask.
     * @param value The enum value.
     * @return Whether the bit of mask corresponding to value is set or not.
     */
    public static <E extends Enum<E>> boolean isEnabled(byte mask,
                                                        E value) {
        return isEnabled(mask, Masks.toIndex(value));
    }

    /**
     * Returns a mask where each bit corresponding to an enum value is set.
     * <p>
     * The number of bits that are set equals the number of values of the enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return A mask where each bit corresponding to a value of enum is set.
     */
    public static <E extends Enum<E>> int toAllMask(Class<E> enumClass) {
        byte mask = 0;
        for (final E e : enumClass.getEnumConstants()) {
            mask = setEnabled(mask, e, true);
        }
        return mask;
    }

    public static <E extends Enum<E>> String toString(byte mask,
                                                      Class<E> enumClass) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final E value : enumClass.getEnumConstants()) {
            if (isEnabled(mask, value)) {
                if (!first) {
                    builder.append("|");
                }
                first = false;
                builder.append(value.name());
            }
        }
        return builder.toString();
    }

    public static String toPaddedBinString(byte val) {
        final char[] buffer = new char[MAX_BITS];
        Arrays.fill(buffer, '0');
        for (int i = 0; i < MAX_BITS; ++i) {
            final int mask = 1 << i;
            if ((val & mask) == mask) {
                buffer[MAX_BITS - 1 - i] = '1';
            }
        }
        return new String(buffer);
    }
}