package cdc.util.lang;

public final class SystemProperties {

    /**
     * Character that separates components of a file path. This is "/" on UNIX and "\" on Windows.
     */
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     * Sequence used by operating system to separate lines in text files.
     */
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    /**
     * Path separator character used in java.class.path.
     */
    public static final String PATH_SEPARATOR = System.getProperty("path.separator");

    /**
     * Path used to find directories and JAR archives containing class files.
     * Elements of the class path are separated by a platform-specific character
     * specified in the path.separator property.
     */
    public static final String JAVA_CLASS_PATH = System.getProperty("java.class.path");

    private static final String[] JAVA_CLASS_PATH_ARRAY = JAVA_CLASS_PATH.split(PATH_SEPARATOR);

    private SystemProperties() {
    }

    public static final String[] javaClassPath() {
        return JAVA_CLASS_PATH_ARRAY.clone();
    }
}