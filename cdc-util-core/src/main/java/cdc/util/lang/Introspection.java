package cdc.util.lang;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Set of utilities dedicated to introspection.
 * <p>
 * Primitive types are and their corresponding wrapper type are:
 * <ul>
 * <li>{@code char} - {@code Character}
 * <li>{@code long} - {@code Long}
 * <li>{@code int} - {@code Integer}
 * <li>{@code short} - {@code Short}
 * <li>{@code byte} - {@code Byte}
 * <li>{@code double} - {@code Double}
 * <li>{@code float} - {@code Float}
 * <li>{@code boolean} - {@code Boolean}
 * <li>{@code void} - {@code Void}
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class Introspection {
    private static final Logger LOGGER = LogManager.getLogger(Introspection.class);
    private static final Map<String, Class<?>> PRIMITIVE_TYPES = new HashMap<>();
    private static final Map<String, Class<?>> WRAPPER_TYPES = new HashMap<>();
    private static final Map<Class<?>, Class<?>> PRIMITIVE_TO_WRAPPER = new HashMap<>();
    private static final Map<Class<?>, Class<?>> WRAPPER_TO_PRIMITIVE = new HashMap<>();

    static {
        registerPrimitiveAndWrapperType(long.class, Long.class);
        registerPrimitiveAndWrapperType(int.class, Integer.class);
        registerPrimitiveAndWrapperType(short.class, Short.class);
        registerPrimitiveAndWrapperType(byte.class, Byte.class);
        registerPrimitiveAndWrapperType(char.class, Character.class);
        registerPrimitiveAndWrapperType(boolean.class, Boolean.class);
        registerPrimitiveAndWrapperType(double.class, Double.class);
        registerPrimitiveAndWrapperType(float.class, Float.class);
        registerPrimitiveAndWrapperType(void.class, Void.class);
    }

    private Introspection() {
    }

    private static void registerPrimitiveAndWrapperType(Class<?> primitiveClass,
                                                        Class<?> wrapperClass) {
        PRIMITIVE_TYPES.put(primitiveClass.getCanonicalName(), primitiveClass);
        PRIMITIVE_TO_WRAPPER.put(primitiveClass, wrapperClass);
        WRAPPER_TO_PRIMITIVE.put(wrapperClass, primitiveClass);
        WRAPPER_TYPES.put(wrapperClass.getSimpleName(), wrapperClass);
        WRAPPER_TYPES.put(wrapperClass.getCanonicalName(), wrapperClass);
    }

    /**
     * Returns {@code true} when a class is a primitive type class.
     *
     * @param cls The class
     * @return {@code true} when {@code cls} is a primitive type class.
     */
    public static boolean isPrimitiveTypeClass(Class<?> cls) {
        return cls != null && PRIMITIVE_TYPES.containsKey(cls.getCanonicalName());
    }

    /**
     * Returns {@code true} when a string is the class name of a primitive type.
     *
     * @param name The string.
     * @return {@code true} when {@code name} is the class name of a primitive type.
     */
    public static boolean isPrimitiveTypeClassName(String name) {
        return PRIMITIVE_TYPES.containsKey(name);
    }

    /**
     * Returns {@code true} when a class is a wrapper type class.
     *
     * @param cls The class
     * @return {@code true} when {@code cls} is a wrapper type class.
     */
    public static boolean isWrapperTypeClass(Class<?> cls) {
        return cls != null && WRAPPER_TYPES.containsKey(cls.getCanonicalName());
    }

    /**
     * Returns {@code true} when a string is the class name of a wrapper type.
     *
     * @param name The string.
     * @return {@code true} when {@code name} is the class name of a wrapper type.
     */
    public static boolean isWrapperTypeClassName(String name) {
        return WRAPPER_TYPES.containsKey(name);
    }

    /**
     * If input is a primitive type class, returns the corresponding wrapper class, otherwise returns the input.
     * <p>
     * This is similar to boxing.<br>
     * Note that {@code int.class} is declared as {@code Class<Integer>} even if {@code int.class != Integer.class}.<br>
     * This is the same for other primitive types. This is why the returned type is the same as the input one.
     *
     * @param <T> The type.
     * @param cls The class
     * @return The wrapper class corresponding to {@code cls} if {@code cls} is a primitive type class, or {@code cls}.
     */
    public static <T> Class<T> wrap(Class<T> cls) {
        @SuppressWarnings("unchecked")
        final Class<T> tmp = (Class<T>) PRIMITIVE_TO_WRAPPER.get(cls);
        return tmp == null ? cls : tmp;
    }

    /**
     * If input is a wrapper type class, returns the corresponding unwrapped class, otherwise returns the input.
     * <p>
     * Explanations on returned type are the same as for {@link #wrap}.
     *
     * @param <T> The type.
     * @param cls The class
     * @return The unwrapped class corresponding to {@code cls} if {@code cls} is a wrapped type class, or {@code cls}.
     */
    public static <T> Class<T> unwrap(Class<T> cls) {
        @SuppressWarnings("unchecked")
        final Class<T> tmp = (Class<T>) WRAPPER_TO_PRIMITIVE.get(cls);
        return tmp == null ? cls : tmp;
    }

    /**
     * Returns the field that has a given name.
     * <p>
     * This will search on the initial class and all its ancestors.
     *
     * @param cls The class.
     * @param name The field name.
     * @return The field named {@code name} or null.
     */
    public static Field getField(Class<?> cls,
                                 String name) {
        Checks.isNotNull(cls, "cls");

        try {
            return cls.getDeclaredField(name);
        } catch (final SecurityException | NoSuchFieldException e) {
            // Ignore
        }

        final Class<?> sup = cls.getSuperclass();
        if (sup != null) {
            return getField(sup, name);
        } else {
            return null;
        }
    }

    /**
     * Returns {@code true} when a class has a field.
     *
     * @param cls The class.
     * @param name The name of the searched field.
     * @return Whether {@code cls} has a field named {@code name} or not.
     */
    public static boolean hasField(Class<?> cls,
                                   String name) {
        return getField(cls, name) != null;
    }

    public static Set<Field> getFieldsMatching(Class<?> cls,
                                               Predicate<Field> predicate) {
        final Set<Field> result = new HashSet<>();
        traverseAllFields(cls,
                          f -> {
                              if (predicate.test(f)) {
                                  result.add(f);
                              }
                          });
        return result;
    }

    public static Set<Field> getAllFields(Class<?> cls) {
        return getFieldsMatching(cls, f -> true);
    }

    /**
     * Returns the method that has a given name and signature.
     * <p>
     * This will search on the initial class, all its ancestors and implemented / extended interfaces.
     *
     * @param cls The class.
     * @param name The method name.
     * @param signature The method signature.
     * @return The method named {@code name} and with signature {@code signature} or {@code null}.
     */
    public static Method getMethod(Class<?> cls,
                                   String name,
                                   Class<?>... signature) {
        Checks.isNotNull(cls, "cls");

        // Search on the class
        Method result = null;
        try {
            result = cls.getDeclaredMethod(name, signature);
        } catch (final SecurityException | NoSuchMethodException e) {
            // Ignore
        }

        if (result != null) {
            return result;
        } else {
            // Search on its interfaces (and their ancestors)
            for (final Class<?> xface : cls.getInterfaces()) {
                result = getMethod(xface, name, signature);
                if (result != null) {
                    return result;
                }
            }
            // Search on its ancestor
            final Class<?> sup = cls.getSuperclass();
            if (sup != null) {
                return getMethod(sup, name, signature);
            } else {
                return null;
            }
        }
    }

    public static Method getMethod(String className,
                                   String methodName,
                                   Class<?>... signature) {
        final Class<?> cls = getClass(className);
        if (cls == null) {
            return null;
        } else {
            return getMethod(cls, methodName, signature);
        }
    }

    /**
     * Returns {@code true} when a class has a method.
     *
     * @param cls The class.
     * @param name The method name.
     * @param signature The method signature.
     * @return Whether {@code cls} has a method named {@code name} with signature {@code signature} or not.
     */
    public static boolean hasMethod(Class<?> cls,
                                    String name,
                                    Class<?>... signature) {
        return getMethod(cls, name, signature) != null;
    }

    public static Set<Method> getMethodsMatching(Class<?> cls,
                                                 Predicate<Method> predicate) {
        final Set<Method> result = new HashSet<>();
        traverseAllMethods(cls,
                           m -> {
                               if (predicate.test(m)) {
                                   result.add(m);
                               }
                           });
        return result;
    }

    public static Set<Method> getAllMethods(Class<?> cls) {
        return getMethodsMatching(cls, m -> true);
    }

    public static <T> Constructor<T> getConstructor(Class<T> cls,
                                                    FailureReaction reaction,
                                                    Class<?>... parameterTypes) {
        try {
            return cls.getConstructor(parameterTypes);
        } catch (NoSuchMethodException | SecurityException e) {
            return NotFoundException.onError(cls.getName() + Arrays.toString(parameterTypes) + " contructor not found",
                                             LOGGER,
                                             reaction,
                                             null);
        }
    }

    public static <T> Constructor<T> getConstructor(Class<T> cls,
                                                    Class<?>... parameterTypes) {
        return getConstructor(cls, FailureReaction.DEFAULT, parameterTypes);
    }

    public static boolean hasConstructor(Class<?> cls,
                                         Class<?>... parameterTypes) {
        return getConstructor(cls, parameterTypes) != null;
    }

    /**
     * Creates a new instance.
     *
     * @param <T> The target type.
     * @param cls The target class.
     * @param parameterTypes The types of constructor arguments.
     * @param reaction The reaction to adopt in case of error.
     * @param parameters The constructor arguments.
     * @return A new instance of type {@code <T>} or {@code null}.
     * @throws IllegalArgumentException When creation fails and {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <T> T newInstance(Class<T> cls,
                                    Class<?>[] parameterTypes,
                                    FailureReaction reaction,
                                    Object... parameters) {
        final Constructor<T> constructor = getConstructor(cls, reaction, parameterTypes);
        if (constructor == null) {
            return null;
        } else {
            try {
                return constructor.newInstance(parameters);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                return FailureReaction.onError("newInstance(" + cls.getCanonicalName() + Arrays.toString(parameterTypes)
                        + " with " + Arrays.toString(parameters) + ") failed, "
                        + e // e.getClass().getSimpleName() + ", message: " + e.getMessage()
                        + ", cause: " + e.getCause(),
                                               LOGGER,
                                               reaction,
                                               null,
                                               IllegalArgumentException::new);
            }
        }
    }

    public static <T> T newInstance(Class<T> cls,
                                    FailureReaction reaction,
                                    Object... parameters) {
        final Class<?>[] parameterTypes = new Class<?>[parameters.length];
        for (int index = 0; index < parameters.length; index++) {
            if (parameters[index] == null) {
                throw new IllegalArgumentException("null parameter at " + index);
            } else {
                parameterTypes[index] = parameters[index].getClass();
            }
        }
        return newInstance(cls, parameterTypes, reaction, parameters);
    }

    /**
     * Returns the class that is the most specialized of 2 classes.
     * <p>
     * If {@code cls1} or {@code cls2} is a primitive type class, result won't be {@code null} if they are equal.
     * <ul>
     * <li>{@code mostSpecialized(String.class, Object.class) == String.class}
     * <li>{@code mostSpecialized(Float.class, float.class) == null}
     * <li>{@code mostSpecialized(float.class, float.class) == float.class}
     * </ul>
     *
     * @param cls1 The first class.
     * @param cls2 The second class.
     * @return The class that is most specialized of {@code cls1} or {@code cls2}, or {@code null}.
     */
    public static Class<?> mostSpecialized(Class<?> cls1,
                                           Class<?> cls2) {
        Checks.isNotNull(cls1, "cls1");
        Checks.isNotNull(cls2, "cls2");

        if (cls1.isAssignableFrom(cls2)) {
            return cls2;
        } else if (cls2.isAssignableFrom(cls1)) {
            return cls1;
        } else {
            return null;
        }
    }

    /**
     * Returns the class that is the most specialized of 2 classes with a special handling of primitive types.
     * <p>
     * If {@code cls1} or {@code cls2} is a primitive type class, result won't be {@code null}
     * if their wrapped types have an inheritance relationship.
     * <ul>
     * <li>{@code mostSpecializedWithWrap(String.class, Object.class) == String.class}
     * <li>{@code mostSpecializedWithWrap(Float.class, float.class) == Float.class}
     * <li>{@code mostSpecializedWithWrap(float.class, float.class) == float.class}
     * <li>{@code mostSpecializedWithWrap(Number.class, float.class) == Float.class}
     * </ul>
     *
     * @param cls1 The first class.
     * @param cls2 The second class.
     * @return The class that is most specialized (after a possible wrap) of {@code cls1} or {@code cls2}, or {@code null}.
     */
    public static Class<?> mostSpecializedWithWrap(Class<?> cls1,
                                                   Class<?> cls2) {
        Checks.isNotNull(cls1, "cls1");
        Checks.isNotNull(cls2, "cls2");

        final Class<?> tmp = mostSpecialized(cls1, cls2);
        if (tmp == null && (isPrimitiveTypeClass(cls1) != isPrimitiveTypeClass(cls2))) {
            return mostSpecialized(wrap(cls1), wrap(cls2));
        } else {
            return tmp;
        }
    }

    /**
     * Returns the class that is the most generalized of 2 classes.
     * <p>
     * If {@code cls1} or {@code cls2} is a primitive type class, result won't be {@code null} if they are equal.
     * <ul>
     * <li>{@code mostGeneralized(String.class, Object.class) == Object.class}
     * <li>{@code mostGeneralized(Float.class, float.class) == null}
     * <li>{@code mostGeneralized(float.class, float.class) == float.class}
     * </ul>
     *
     * @param cls1 The first class.
     * @param cls2 The second class.
     * @return The class that is most generalized of {@code cls1} or {@code cls2}, or {@code null}.
     */
    public static Class<?> mostGeneralized(Class<?> cls1,
                                           Class<?> cls2) {
        Checks.isNotNull(cls1, "cls1");
        Checks.isNotNull(cls2, "cls2");

        if (cls1.isAssignableFrom(cls2)) {
            return cls1;
        } else if (cls2.isAssignableFrom(cls1)) {
            return cls2;
        } else {
            return null;
        }
    }

    /**
     * Returns the class that is the most generalized of 2 classes with a special handling of primitive types.
     * <p>
     * If {@code cls1} or {@code cls2} is a primitive type class, result won't be {@code null}
     * if their wrapped types have an inheritance relationship.
     * <ul>
     * <li>{@code mostGeneralizedWithWrap(String.class, Object.class) == Object.class}
     * <li>{@code mostGeneralizedWithWrap(Float.class, float.class) == Float.class}
     * <li>{@code mostGeneralizedWithWrap(float.class, float.class) == float.class}
     * <li>{@code mostGeneralizedWithWrap(Number.class, float.class) == Number.class}
     * </ul>
     *
     * @param cls1 The first class.
     * @param cls2 The second class.
     * @return The class that is most generalized (after a possible wrap) of {@code cls1} or {@code cls2}, or {@code null}.
     */
    public static Class<?> mostGeneralizedWithWrap(Class<?> cls1,
                                                   Class<?> cls2) {
        Checks.isNotNull(cls1, "cls1");
        Checks.isNotNull(cls2, "cls2");

        final Class<?> tmp = mostGeneralized(cls1, cls2);
        if (tmp == null && (isPrimitiveTypeClass(cls1) != isPrimitiveTypeClass(cls2))) {
            return mostGeneralized(wrap(cls1), wrap(cls2));
        } else {
            return tmp;
        }
    }

    /**
     * Traverses all super classes of a class, optionally itself.
     *
     * @param cls The initial traversal class .
     * @param self If {@code true} and {@code cls} is a class, theb {@code cls} is also traversed.
     * @param consumer The consumer of the traversed classes. {@code cls} is <em>NOT</em> traversed.
     */
    public static void traverseAllSuperClasses(Class<?> cls,
                                               boolean self,
                                               Consumer<Class<?>> consumer) {
        Checks.isNotNull(cls, "cls");
        Checks.isNotNull(consumer, "consumer");

        if (self && !cls.isInterface()) {
            consumer.accept(cls);
        }

        Class<?> index = cls.getSuperclass();
        while (index != null) {
            consumer.accept(index);
            index = index.getSuperclass();
        }
    }

    /**
     * Traverse all interfaces implemented or extended by a class.
     * If the class is an interface,it is also traversed.
     *
     * @param cls The initial traversal class.
     * @param consumer The consumer of traversed classes.
     */
    public static void traverseAllInterfaces(Class<?> cls,
                                             Consumer<Class<?>> consumer) {
        Checks.isNotNull(cls, "cls");
        Checks.isNotNull(consumer, "consumer");

        traverseHierarchy(cls,
                          c -> {
                              if (c.isInterface()) {
                                  consumer.accept(c);
                              }
                          });
    }

    /**
     * Traverse a class and its hierarchy (super classes and interfaces).
     *
     * @param cls The initial traversal class.
     * @param consumer The consumer of traversed classes.
     */
    public static void traverseHierarchy(Class<?> cls,
                                         Consumer<Class<?>> consumer) {
        Checks.isNotNull(cls, "cls");
        Checks.isNotNull(consumer, "consumer");

        // Both sets contains classes and interfaces
        final Set<Class<?>> todo = new HashSet<>();
        final Set<Class<?>> done = new HashSet<>();

        // Add cls that may correspond to a class or interface.
        todo.add(cls);
        // Add ancestors
        if (!cls.isInterface()) {
            todo.addAll(getSuperClasses(cls, false));
        }

        while (!todo.isEmpty()) {
            // Extract the next class to process
            final Class<?> next = todo.iterator().next();
            todo.remove(next);

            // fire an event
            consumer.accept(next);
            // We don't want to analyze next again
            done.add(next);

            // Add interfaces of next for future processing, if they have not been processed yet
            for (final Class<?> xface : next.getInterfaces()) {
                if (!done.contains(xface)) {
                    todo.add(xface);
                }
            }
        }
    }

    public static void traverseAllMethods(Class<?> cls,
                                          Consumer<Method> consumer) {
        final Set<Method> done = new HashSet<>();
        traverseHierarchy(cls,
                          c -> {
                              for (final Method method : c.getDeclaredMethods()) {
                                  if (!done.contains(method)) {
                                      consumer.accept(method);
                                      done.add(method);
                                  }
                              }
                          });
    }

    public static void traverseAllFields(Class<?> cls,
                                         Consumer<Field> consumer) {
        final Set<Field> done = new HashSet<>();
        traverseHierarchy(cls,
                          c -> {
                              for (final Field field : c.getDeclaredFields()) {
                                  if (!done.contains(field)) {
                                      consumer.accept(field);
                                      done.add(field);
                                  }
                              }
                          });
    }

    public static void traverseAllConstructors(Class<?> cls,
                                               Consumer<Constructor<?>> consumer) {
        final Set<Constructor<?>> done = new HashSet<>();
        traverseHierarchy(cls,
                          c -> {
                              for (final Constructor<?> field : c.getDeclaredConstructors()) {
                                  if (!done.contains(field)) {
                                      consumer.accept(field);
                                      done.add(field);
                                  }
                              }
                          });
    }

    /**
     * Returns a list of all super classes of a class, including optionally itself.
     * <p>
     * If {@code cls} represents an interface, then the result is empty.
     *
     * @param cls The class.
     * @param self If {@code true} and {@code cls} is a class,
     *            then {@code cls} is also included in result.
     * @return The list of super classes of {@code cls},
     *         including itself if {@code cls} is a class and {@code self} is {@code true}.
     */
    public static List<Class<?>> getSuperClasses(Class<?> cls,
                                                 boolean self) {
        Checks.isNotNull(cls, "cls");

        final List<Class<?>> result = new ArrayList<>();
        traverseAllSuperClasses(cls, self, result::add);
        return result;
    }

    /**
     * Returns a set of interfaces implemented or extended by a class or interface.
     * <p>
     * If {@code cls} is an interface, it is contained in result.
     *
     * @param cls The class.
     * @return A set of all interfaces implemented or extends by {@code cls}.
     */
    public static Set<Class<?>> getInterfaces(Class<?> cls) {
        Checks.isNotNull(cls, "cls");

        final Set<Class<?>> result = new HashSet<>();
        traverseAllInterfaces(cls, result::add);
        return result;
    }

    /**
     * Returns the class object corresponding to a name.
     * <p>
     * When no class is found, returns {@code null}.
     *
     * @param name Fully qualified name of the desired class.
     * @param initialize If {@code true} the class will be initialized.
     *            See Section 12.4 of <em>The Java Language Specification</em>.
     * @param loader Class loader from which the class must be loaded.
     * @return The class object representing the desired class or {@code null}.
     */
    private static Class<?> getClassInt(String name,
                                        boolean initialize,
                                        ClassLoader loader) {
        if (name != null && !name.isEmpty()) {
            try {
                return Class.forName(name, initialize, loader);
            } catch (final ClassNotFoundException e) {
                // Ignore
                BlackHole.discard(e);
            }
        }
        return null;
    }

    /**
     * Tries to find a class with a name.
     * <p>
     * If that fails, replaces last '.' by '$' and tries to find a class with that new name.
     * Do this recursively.
     *
     * @param name Fully qualified name of the desired class.
     * @param initialize If {@code true} the class will be initialized.
     *            See Section 12.4 of <em>The Java Language Specification</em>.
     * @param loader Class loader from which the class must be loaded
     * @return The class or null.
     */
    private static Class<?> getClassIntDollars(String name,
                                               boolean initialize,
                                               ClassLoader loader) {
        final Class<?> result = getClassInt(name, initialize, loader);
        if (result == null && name != null) {
            final int pos = name.lastIndexOf('.');
            if (pos >= 0) {
                final StringBuilder sb = new StringBuilder(name);
                sb.setCharAt(pos, '$');
                return getClassIntDollars(sb.toString(), initialize, loader);
            } else {
                return null;
            }
        } else {
            return result;
        }
    }

    /**
     * Returns the class corresponding to a name.
     * <p>
     * If no matching class is found, '.' are replaced by '$', starting from the last one to check
     * whether a class can be found.
     * <p>
     * So, for the nested class "a.B.C", one can either pass "a.B.C" or a.B$C".<br>
     * In the first case, "a.B.C", "A.B$C" and "a$B$C" will be tested.
     * <p>
     * Classes in java.lang package can be searched without passing {@code java.lang}.<br>
     * So, {@code getClass(String, ...)} will produce "java.lang.String". This check is done in last position.
     *
     * @param name Fully qualified name of the desired class.
     * @param initialize If {@code true} the class will be initialized.
     *            See Section 12.4 of <em>The Java Language Specification</em>.
     * @param loader Class loader from which the class must be loaded
     * @param reaction The reaction to adopt when class could not be found.
     * @return The class corresponding to name, or null.
     * @throws NotFoundException When the class could not be found and reaction is FAIL.
     */
    public static Class<?> getClass(String name,
                                    boolean initialize,
                                    ClassLoader loader,
                                    FailureReaction reaction) {
        // Check normal name
        Class<?> result = getClassIntDollars(name, initialize, loader);

        // Check with java.lang. If not handled in first check
        if (result == null) {
            result = getClassInt("java.lang." + name, initialize, loader);
        }

        if (result == null) {
            if (reaction == FailureReaction.DEFAULT) {
                return null;
            } else if (reaction == FailureReaction.WARN) {
                LOGGER.warn("Could not find class '{}'", name);
                return null;
            } else {
                throw new NotFoundException("Could not find class '" + name + "'");
            }
        } else {
            return result;
        }
    }

    public static <C> Class<? extends C> getClass(String name,
                                                  Class<C> cls,
                                                  boolean initialize,
                                                  ClassLoader loader,
                                                  FailureReaction reaction) {
        final Class<?> tmp = getClass(name, initialize, loader, reaction);
        if (tmp == null) {
            return null;
        } else {
            return tmp.asSubclass(cls);
        }
    }

    /**
     * Returns the class corresponding to a name.
     * <p>
     * If no matching class is found, '.' are replaced by '$', starting from the last one to check
     * whether a class can be found.
     * <p>
     * So, for the nested class "a.B.C", one can either pass "a.B.C" or a.B$C".<br>
     * In the first case, "a.B.C", "A.B$C" and "a$B$C" will be tested.
     * <p>
     * Classes in java.lang package can be searched without passing {@code java.lang}.<br>
     * So, {@code getClass(String, ...)} will produce "java.lang.String". This check is done in last position.
     * <p>
     * The class loader is used to load {@link Introspection} is used.
     *
     * @param name Fully qualified name of the desired class.
     * @param initialize If {@code true} the class will be initialized.
     *            See Section 12.4 of <em>The Java Language Specification</em>.
     * @param reaction The reaction to adopt when class could not be found.
     * @return The class corresponding to {@code name}, or {@code null}.
     * @throws NotFoundException When the class could not be found and reaction is FAIL.
     */
    public static Class<?> getClass(String name,
                                    boolean initialize,
                                    FailureReaction reaction) {
        return getClass(name, initialize, Introspection.class.getClassLoader(), reaction);
    }

    public static <C> Class<? extends C> getClass(String name,
                                                  Class<C> cls,
                                                  boolean initialize,
                                                  FailureReaction reaction) {
        return getClass(name, cls, initialize, Introspection.class.getClassLoader(), reaction);
    }

    /**
     * Returns the class corresponding to a name.
     * <p>
     * If no matching class is found, '.' are replaced by '$', starting from the last one to check
     * whether a class can be found.
     * <p>
     * So, for the nested class "a.B.C", one can either pass "a.B.C" or a.B$C".<br>
     * In the first case, "a.B.C", "A.B$C" and "a$B$C" will be tested.
     * <p>
     * Classes in java.lang package can be searched without passing {@code java.lang}.<br>
     * So, {@code getClass(String, ...)} will produce "java.lang.String". This check is done in last position.
     * <p>
     * The system class loader is used and class is NOT initialized.
     *
     * @param name Fully qualified name of the desired class.
     * @param reaction The reaction to adopt when class could not be found.
     * @return The class corresponding to {@code name}, or {@code null}.
     * @throws NotFoundException When the class could not be found and reaction is FAIL.
     */
    public static Class<?> getClass(String name,
                                    FailureReaction reaction) {
        return getClass(name, false, reaction);
    }

    public static <C> Class<? extends C> getClass(String name,
                                                  Class<C> cls,
                                                  FailureReaction reaction) {
        return getClass(name, cls, false, reaction);
    }

    /**
     * Returns the class corresponding to a name.
     * <p>
     * If no matching class is found, '.' are replaced by '$', starting from the last one to check
     * whether a class can be found.
     * <p>
     * So, for the nested class "a.B.C", one can either pass "a.B.C" or a.B$C".<br>
     * In the first case, "a.B.C", "A.B$C" and "a$B$C" will be tested.
     * <p>
     * Classes in java.lang package can be searched without passing {@code java.lang}.<br>
     * So, {@code getClass(String, ...)} will produce "java.lang.String". This check is done in last position.
     * <p>
     * The system class loader is used and class is NOT initialized.
     *
     * @param name Fully qualified name of the desired class.
     * @return The class corresponding to {@code name}, or {@code null}.
     */
    public static Class<?> getClass(String name) {
        return getClass(name, false, FailureReaction.DEFAULT);
    }

    public static <C> Class<? extends C> getClass(String name,
                                                  Class<C> cls) {
        return getClass(name, cls, false, FailureReaction.DEFAULT);
    }

    /**
     * Returns the package name of a class.
     *
     * @param cls The class.
     * @return The name of the package of {@code cls}.
     */
    public static String getPackagePart(Class<?> cls) {
        Checks.isNotNull(cls, "cls");

        final Package p = cls.getPackage();
        if (p == null) {
            return "";
        } else {
            return p.getName();
        }
    }

    /**
     * Returns the class name (without package) of a class.
     *
     * @param cls The class.
     * @return The class part of the name of {@code cls}.
     */
    public static String getClassPart(Class<?> cls) {
        Checks.isNotNull(cls, "cls");

        final String full = cls.getCanonicalName();
        final String pack = getPackagePart(cls);
        if (pack.isEmpty()) {
            return full;
        } else {
            return full.substring(pack.length() + 1);
        }
    }

    /**
     * Returns the package part of the internal name of a class.
     * <p>
     * Internal name has the form p1/p2/Class1$Class2.
     * <ul>
     * <li>{@code getPackagePart("C") = ""}
     * <li>{@code getPackagePart("C$D") = ""}
     * <li>{@code getPackagePart("a/C") = "a"}
     * <li>{@code getPackagePart("a/C$D") = "a"}
     * </ul>
     *
     * @param internalName The class internal name.
     * @return The package part of {@code internalName}.
     */
    public static String getPackagePart(String internalName) {
        final int pos = internalName.lastIndexOf('/');
        return pos >= 0 ? internalName.substring(0, pos) : "";
    }

    /**
     * Returns the class part of the internal name of a class.
     * <p>
     * Internal name has the form p1/p2/Class1$Class2.
     * <ul>
     * <li>{@code getClassPart("C") = "C"}
     * <li>{@code getClassPart("C$D") = "C$D"}
     * <li>{@code getClassPart("a/C") = "C"}
     * <li>{@code getClassPart("a/C$D") = "C$D"}
     * </ul>
     *
     * @param internalName The class internal name.
     * @return The class part of {@code internalName}.
     */
    public static String getClassPart(String internalName) {
        final int pos = internalName.lastIndexOf('/');
        return pos >= 0 ? internalName.substring(pos + 1) : internalName;
    }

    /**
     * Returns the array class of a class.
     * <p>
     * This works for any class, including array classes.<br>
     * <b>WARNING:</b> There is no {@code void[]} class.
     *
     * @param <T> The class type.
     * @param cls The class
     * @return The class corresponding to T[] or {@code null} if {code cls} is {@code void.class}.
     */
    public static <T> Class<T[]> getArrayClass(Class<T> cls) {
        Checks.isNotNull(cls, "cls");

        // This seems the simpler way of doing things.
        // There may exist more efficient solutions.
        if (void.class.equals(cls)) {
            return null;
        } else {
            @SuppressWarnings("unchecked")
            final Class<T[]> result = (Class<T[]>) Array.newInstance(cls, 0).getClass();
            return result;
        }
    }

    public static <T> T[] newArray(Class<T> cls,
                                   int length) {
        @SuppressWarnings("unchecked")
        final T[] result = (T[]) Array.newInstance(cls, length);
        return result;
    }

    public static <T> Class<T> uncheckedCast(Class<?> cls) {
        @SuppressWarnings("unchecked")
        final Class<T> result = (Class<T>) cls;
        return result;
    }

    public static <T> T uncheckedCast(Object object) {
        @SuppressWarnings("unchecked")
        final T result = (T) object;
        return result;
    }
}