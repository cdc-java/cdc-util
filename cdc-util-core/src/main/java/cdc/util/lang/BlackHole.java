package cdc.util.lang;

public final class BlackHole {
    private BlackHole() {
    }

    public static void discard(long value) {
        // Ignore
    }

    public static void discard(int value) {
        // Ignore
    }

    public static void discard(short value) {
        // Ignore
    }

    public static void discard(byte value) {
        // Ignore
    }

    public static void discard(char value) {
        // Ignore
    }

    public static void discard(boolean value) {
        // Ignore
    }

    public static void discard(double value) {
        // Ignore
    }

    public static void discard(float value) {
        // Ignore
    }

    public static void discard(Object value) {
        // Ignore
    }
}