package cdc.util.lang;

public class DoubleHolder {
    public double value;

    public DoubleHolder() {
        this.value = 0.0;
    }

    public DoubleHolder(double value) {
        this.value = value;
    }
}