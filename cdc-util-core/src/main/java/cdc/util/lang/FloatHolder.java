package cdc.util.lang;

public class FloatHolder {
    public float value;

    public FloatHolder() {
        this.value = 0.0F;
    }

    public FloatHolder(float value) {
        this.value = value;
    }
}