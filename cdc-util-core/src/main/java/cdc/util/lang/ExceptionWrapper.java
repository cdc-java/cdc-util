package cdc.util.lang;

public class ExceptionWrapper extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ExceptionWrapper() {
        super();
    }

    public ExceptionWrapper(String message) {
        super(message);
    }

    public ExceptionWrapper(String message,
                            Throwable cause) {
        super(message, cause);
    }

    public ExceptionWrapper(Throwable cause) {
        super(cause);
    }

    public static RuntimeException wrap(Exception e) {
        if (e instanceof RuntimeException) {
            return (RuntimeException) e;
        } else {
            return new ExceptionWrapper(e);
        }
    }

    @FunctionalInterface
    public static interface Procedure {
        void invoke() throws Exception;
    }

    @FunctionalInterface
    public static interface Supplier<T> {
        T get() throws Exception;
    }

    public static void wrap(Procedure procedure) {
        try {
            procedure.invoke();
        } catch (final Exception e) {
            throw wrap(e);
        }
    }

    public static <T> T wrap(Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (final Exception e) {
            throw wrap(e);
        }
    }
}