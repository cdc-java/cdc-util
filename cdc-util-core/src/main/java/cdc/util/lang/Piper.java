package cdc.util.lang;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Piper {
    private static final Logger LOGGER = LogManager.getLogger(Piper.class);

    private Piper() {
    }

    private static final class Internal implements Runnable {
        private final InputStream input;
        private final OutputStream output;

        public Internal(InputStream input,
                        OutputStream output) {
            this.input = input;
            this.output = output;
        }

        @Override
        public void run() {
            try {
                // Create 512 bytes buffer
                final byte[] b = new byte[512];
                int read = 1;
                // As long as data is read; -1 means EOF
                while (read > -1) {
                    // Read bytes into buffer
                    read = input.read(b, 0, b.length);
                    if (read > -1) {
                        // Write bytes to output
                        output.write(b, 0, read);
                    }
                }
            } catch (final Exception e) {
                // Something happened while reading or writing streams; pipe is broken
                throw new PiperException("Broken pipe", e);
            } finally {
                try {
                    input.close();
                } catch (final Exception e) {
                    // Ignore
                }
                try {
                    output.close();
                } catch (final Exception e) {
                    // Ignore
                }
            }
        }
    }

    /**
     * Pipes an array of processes.
     *
     * @param timeout The timeout (milliseconds) before killing the processes. Ignored if {@code <= 0}.
     * @param processes The processes.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     */
    public static InputStream pipeProcessesArray(long timeout,
                                                 Process... processes) throws InterruptedException {
        Checks.isTrue(processes.length > 0, "not enough processes");

        for (int i = 0; i < processes.length - 1; i++) {
            final Process p1 = processes[i];
            final Process p2 = processes[i + 1];
            new Thread(new Internal(p1.getInputStream(), p2.getOutputStream())).start();
        }
        final Process last = processes[processes.length - 1];
        // Wait for last process in chain
        if (timeout <= 0) {
            last.waitFor();
        } else {
            last.waitFor(timeout, TimeUnit.MILLISECONDS);
            if (last.isAlive()) {
                last.destroy();
                if (last.isAlive()) {
                    last.destroyForcibly();
                }
            }
        }
        // Return its InputStream
        return last.getInputStream();
    }

    /**
     * Pipes an array of processes.
     *
     * @param processes The processes.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     */
    public static InputStream pipeProcessesArray(Process... processes) throws InterruptedException {
        return pipeProcessesArray(-1L, processes);
    }

    /**
     * Pipes a list of processes.
     *
     * @param timeout The timeout (milliseconds) before killing the processes. Ignored if {@code <= 0}.
     * @param processes The processes.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     */
    public static InputStream pipeProcessesList(long timeout,
                                                List<Process> processes) throws InterruptedException {
        return pipeProcessesArray(timeout, processes.toArray(new Process[processes.size()]));
    }

    /**
     * Pipes a list of processes.
     *
     * @param processes The processes.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     */
    public static InputStream pipeProcessesList(List<Process> processes) throws InterruptedException {
        return pipeProcessesList(-1L, processes);
    }

    /**
     * Creates processes and pipes them.
     * <p>
     * Arguments of each command are explicitly defined.
     * <p>
     * Example (pseudo code):
     * <pre>
     * {@code
     * pipeSplitArray(List.of("ls", "-ails"), List.of("grep", "foo"));}
     * </pre>
     *
     * @param timeout The timeout (milliseconds) before killing the processes. Ignored if {@code <= 0}.
     * @param commands The array of lists of split commands arguments.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     * @throws IOException When an IO error occurs starting the processes.
     */
    @SafeVarargs
    public static InputStream pipeSplitArray(long timeout,
                                             List<String>... commands) throws InterruptedException, IOException {
        final Process[] processes = new Process[commands.length];
        for (int index = 0; index < commands.length; index++) {
            final List<String> command = commands[index];

            final Process process;
            if (index == commands.length - 1) {
                process = new ProcessBuilder(command).redirectErrorStream(true)
                                                     .redirectOutput(Redirect.INHERIT)
                                                     .start();
            } else {
                process = new ProcessBuilder(command).redirectErrorStream(true)
                                                     .start();
            }
            processes[index] = process;

            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            try (final InputStream is = process.getErrorStream()) {
                int b;
                while ((b = is.read()) != -1) {
                    os.write(b);
                }
            } catch (final IOException e) {
                LOGGER.error("Error in process {}", command);
                LOGGER.catching(e);
            } finally {
                if (os.size() > 0) {
                    LOGGER.error("Error in process {}", command);
                    LOGGER.error(os);
                }
            }
        }
        return pipeProcessesArray(timeout, processes);
    }

    /**
     * Creates processes and pipes them.
     * <p>
     * Arguments of each command are explicitly defined.
     * <p>
     * Example (pseudo code):
     * <pre>
     * {@code
     * pipeSplitArray(List.of("ls", "-ails"), List.of("grep", "foo"));}
     * </pre>
     *
     * @param commands The array of lists of split commands arguments.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     * @throws IOException When an IO error occurs starting the processes.
     */
    @SafeVarargs
    public static InputStream pipeSplitArray(List<String>... commands) throws InterruptedException, IOException {
        return pipeSplitArray(-1L, commands);
    }

    /**
     * Creates processes and pipes them.
     * <p>
     * Arguments of each command are explicitly defined.
     * <p>
     * Example (pseudo code):
     * <pre>
     * {@code
     * pipeSplitList(List.of(List.of("ls", "-ails"), List.of("grep", "foo")));}
     * </pre>
     *
     * @param timeout The timeout (milliseconds) before killing the processes. Ignored if {@code <= 0}.
     * @param commands The list of lists of split commands arguments.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     * @throws IOException When an IO error occurs starting the processes.
     */
    public static InputStream pipeSplitList(long timeout,
                                            List<List<String>> commands) throws InterruptedException, IOException {
        @SuppressWarnings("unchecked")
        final List<String>[] array = commands.toArray(new List[commands.size()]);
        return pipeSplitArray(timeout, array);
    }

    /**
     * Creates processes and pipes them.
     * <p>
     * Arguments of each command are explicitly defined.
     * <p>
     * Example (pseudo code):
     * <pre>
     * {@code
     * pipeSplitList(List.of(List.of("ls", "-ails"), List.of("grep", "foo")));}
     * </pre>
     *
     * @param commands The list of lists of split commands arguments.
     * @return The InputStream of the last process.
     * @throws InterruptedException If the current thread is interrupted
     *             by another thread while it is waiting, then the wait
     *             is ended and an InterruptedException is thrown.
     * @throws IOException When an IO error occurs starting the processes.
     */
    public static InputStream pipeSplitList(List<List<String>> commands) throws InterruptedException, IOException {
        return pipeSplitList(-1L, commands);
    }
}