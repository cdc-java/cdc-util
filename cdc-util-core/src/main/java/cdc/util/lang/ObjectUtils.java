package cdc.util.lang;

/**
 * Utilities for Objects.
 *
 * @author Damien Carbonne
 */
public final class ObjectUtils {
    private ObjectUtils() {
    }

    public static String identityHashString(Object object) {
        return object == null
                ? "null"
                : Integer.toHexString(System.identityHashCode(object));
    }

    /**
     * Returns the same string for the given object as would be returned by the default method toString(),
     * whether or not the given object's class overrides hashCode() and toString().
     *
     * @param object The object.
     * @return The string representation of {@code object},
     *         or {@code "null"} if {@code object} is {@code null}.
     */
    public static String identityToString(Object object) {
        return object == null
                ? "null"
                : object.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(object));
    }
}