package cdc.util.lang;

public class LongHolder {
    public long value;

    public LongHolder() {
        this.value = 0L;
    }

    public LongHolder(long value) {
        this.value = value;
    }
}