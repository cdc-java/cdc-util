package cdc.util.lang;

public class BooleanHolder {
    public boolean value;

    public BooleanHolder() {
        this.value = false;
    }

    public BooleanHolder(boolean value) {
        this.value = value;
    }
}