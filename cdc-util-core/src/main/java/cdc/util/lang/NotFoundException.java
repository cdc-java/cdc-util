package cdc.util.lang;

import org.apache.logging.log4j.Logger;

/**
 * Exception that can be raised when an error occurs and {@link FailureReaction#FAIL} is required.
 *
 * @author Damien Carbonne
 *
 */
public class NotFoundException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public NotFoundException(String message) {
        super(message);
    }

    /**
     * A function invoked in case of failure and that reacts as described by a failure reaction.
     * <p>
     * Depending on {@code reaction}:
     * <ul>
     * <li>{@link FailureReaction#DEFAULT}: nothing is done.
     * <li>{@link FailureReaction#WARN}: a warning is logged with {@code message}.
     * <li>{@link FailureReaction#FAIL}: an error is logged with {@code message} and an exception is raised with {@code message}.
     * </ul>
     *
     * @param message The message.
     * @param logger The logger.
     * @param reaction The failure reaction.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static void onError(String message,
                               Logger logger,
                               FailureReaction reaction) {
        FailureReaction.onError(message, logger, reaction, NotFoundException::new);
    }

    /**
     * A function invoked in case of failure and that reacts as described by a failure reaction.
     * <p>
     * Depending on {@code reaction}:
     * <ul>
     * <li>{@link FailureReaction#DEFAULT}: silently returns {@code def}.
     * <li>{@link FailureReaction#WARN}: a warning is logged with {@code message} and returns {@code def}.
     * <li>{@link FailureReaction#FAIL}: an error is logged with {@code message} and NotFoundException is raised with
     * {@code message}.
     * </ul>
     *
     * @param <T> The return type.
     * @param message The message.
     * @param logger The logger.
     * @param reaction The failure reaction.
     * @param def The default value to return.
     * @return The default value {@code def}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <T> T onError(String message,
                                Logger logger,
                                FailureReaction reaction,
                                T def) {
        return FailureReaction.onError(message, logger, reaction, def, NotFoundException::new);
    }

    /**
     * A function used to react to a result.
     * <p>
     * If {@code result} is not {@code null}, returns {@code result}.<br>
     * Otherwise, on {@code reaction}:
     * <ul>
     * <li>{@link FailureReaction#DEFAULT}: silently returns {@code def}.
     * <li>{@link FailureReaction#WARN}: a warning is logged with {@code message} and returns {@code def}.
     * <li>{@link FailureReaction#FAIL}: an error is logged with {@code message} and NotFoundException is raised with
     * {@code message}.
     * </ul>
     *
     * @param <T> The return type.
     * @param result The result to analyze.
     * @param message The message.
     * @param logger The logger.
     * @param reaction The failure reaction.
     * @param def The default value to return.
     * @return {@code result} if not {@code null}, or {@code def}.
     * @throws NotFoundException When {@code result} is {@code null} and {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <T> T onResult(T result,
                                 String message,
                                 Logger logger,
                                 FailureReaction reaction,
                                 T def) {
        return FailureReaction.onResult(result, message, logger, reaction, def, NotFoundException::new);
    }

    public static <T> T onResult(T result,
                                 String message) {
        return FailureReaction.onResult(result, message, NotFoundException::new);
    }
}