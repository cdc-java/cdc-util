package cdc.util.lang;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public final class MapUtils {
    private MapUtils() {
    }

    public static <K, V> Map<K, V> filterKeys(Map<K, V> map,
                                              Predicate<K> predicate) {
        final Map<K, V> result = new HashMap<>();
        for (final Map.Entry<K, V> entry : map.entrySet()) {
            if (predicate.test(entry.getKey())) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }
}
