package cdc.util.lang;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Date utilities.
 * <p>
 * It provides conversions between:
 * <ul>
 * <li>{@link java.util.Date}
 * <li>{@link java.util.Calendar}
 * <li>{@link java.time.LocalDateTime}
 * <li>{@link java.time.LocalDate}
 * <li>{@link java.time.LocalTime}
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class DateUtils {
    private DateUtils() {
    }

    private static final LocalDate REF = LocalDate.of(1970, 1, 1);

    /**
     * Converts a Calendar to Date.
     *
     * @param calendar The Calendar.
     * @return The conversion of {@code calendar} to Date.
     */
    public static Date asDate(Calendar calendar) {
        return calendar.getTime();
    }

    /**
     * Converts a LocalDateTime to Date.
     *
     * @param localDateTime The LocalDateTime.
     * @return The conversion of {@code localDateTime} to Date.
     */
    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Converts a LocalDate to Date.
     *
     * @param localDate The LocalDate.
     * @return The conversion of {@code localDate} to Date.
     */
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Converts a LocalTime to Date.
     *
     * @param localTime The LocalTime.
     * @return The conversion of {@code localTime} to Date.
     */
    public static Date asDate(LocalTime localTime) {
        final Instant instant = localTime.atDate(REF).atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * Converts a Date to Calendar.
     *
     * @param date The Date.
     * @return The conversion of {@code date} to Calendar.
     */
    public static Calendar asCalendar(Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Converts a LocalDateTime to Calendar.
     *
     * @param localDateTime The LocalDateTime.
     * @return The conversion of {@code localDateTime} to Calendar.
     */
    public static Calendar asCalendar(LocalDateTime localDateTime) {
        return asCalendar(asDate(localDateTime));
    }

    /**
     * Converts a LocalDate to Calendar.
     *
     * @param localDate The LocalDate.
     * @return The conversion of {@code localDate} to Calendar.
     */
    public static Calendar asCalendar(LocalDate localDate) {
        return asCalendar(asDate(localDate));
    }

    /**
     * Converts a LocalTime to Calendar.
     *
     * @param localTime The LocalTime.
     * @return The conversion of {@code localTime} to Calendar.
     */
    public static Calendar asCalendar(LocalTime localTime) {
        return asCalendar(asDate(localTime));
    }

    /**
     * Converts a Date to LocalDateTime.
     *
     * @param date The Date.
     * @return The conversion of {@code calendar} to LocalDateTime.
     */
    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * Converts a Calendar to LocalDateTime.
     *
     * @param calendar The Calendar.
     * @return The conversion of {@code calendar} to LocalDateTime.
     */
    public static LocalDateTime asLocalDateTime(Calendar calendar) {
        return LocalDateTime.ofInstant(calendar.toInstant(),
                                       calendar.getTimeZone().toZoneId());
    }

    /**
     * Converts a Date to LocalDate.
     *
     * @param date The Date.
     * @return The conversion of {@code date} to LocalDate.
     */
    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Converts a Calendar to LocalDate.
     *
     * @param calendar The Calendar.
     * @return The conversion of {@code calendar} to LocalDate.
     */
    public static LocalDate asLocalDate(Calendar calendar) {
        return LocalDateTime.ofInstant(calendar.toInstant(),
                                       calendar.getTimeZone().toZoneId())
                            .toLocalDate();
    }

    /**
     * Converts a Date to LocalTime.
     *
     * @param date The Date.
     * @return The conversion of {@code date} to LocalTime.
     */
    public static LocalTime asLocalTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalTime();
    }

    /**
     * Converts a Calendar to LocalTime.
     *
     * @param calendar The Calendar.
     * @return The conversion of {@code calendar} to LocalTime.
     */
    public static LocalTime asLocalTime(Calendar calendar) {
        return LocalDateTime.ofInstant(calendar.toInstant(),
                                       calendar.getTimeZone().toZoneId())
                            .toLocalTime();
    }
}