package cdc.util.lang;

public final class Masks {
    private Masks() {
    }

    public static void checkNullableValue(Object value,
                                          boolean nullable) {
        if (value == null && !nullable) {
            throw new IllegalArgumentException("Invalid non nullable value");
        }
    }

    /**
     * Returns the number of bits necessary to encode a number of values.
     *
     * @param number The number of values.
     * @return The number of bits necessary to encode {@code number} values.
     */
    public static int getNumBits(int number) {
        if (number <= 1) {
            return 0;
        } else {
            return 32 - Integer.numberOfLeadingZeros(number - 1);
        }
    }

    /**
     * Returns the number of bits necessary to encode any value of an Enum class.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @param nullable If true, null value should also be encodable.
     * @return The number of bits necessary to encode any value of enumclass,
     *         including optionally null.
     */
    public static <E extends Enum<E>> int getLength(Class<E> enumClass,
                                                    boolean nullable) {
        if (nullable) {
            return getNumBits(enumClass.getEnumConstants().length + 1);
        } else {
            return getNumBits(enumClass.getEnumConstants().length);
        }
    }

    public static <E extends Enum<E>> int getLength(E[] values,
                                                    boolean nullable) {
        if (nullable) {
            return getNumBits(values.length + 1);
        } else {
            return getNumBits(values.length);
        }
    }

    public static <E extends Enum<E>> int toValue(E value,
                                                  boolean nullable) {
        if (nullable) {
            return value == null ? 0 : value.ordinal() + 1;
        } else {
            return value.ordinal();
        }
    }

    public static <E extends Enum<E>> E toEnum(Class<E> enumClass,
                                               int value,
                                               boolean nullable) {
        if (nullable) {
            return value == 0 ? null : enumClass.getEnumConstants()[value - 1];
        } else {
            return enumClass.getEnumConstants()[value];
        }
    }

    public static <E extends Enum<E>> E toEnum(E[] values,
                                               int value,
                                               boolean nullable) {
        if (nullable) {
            return value == 0 ? null : values[value - 1];
        } else {
            return values[value];
        }
    }

    /**
     * Returns the bit index used to encode an enum value.
     * <p>
     * This is the ordinal of the value.
     *
     * @param <E> Enum class.
     * @param value The enum value to encode.
     * @return The bit index used to encode value.
     * @throws IllegalArgumentException When value is null.
     */
    public static <E extends Enum<E>> int toIndex(E value) {
        Checks.isNotNull(value, "value");
        return value.ordinal();
    }
}