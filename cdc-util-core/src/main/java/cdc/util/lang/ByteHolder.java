package cdc.util.lang;

public class ByteHolder {
    public byte value;

    public ByteHolder() {
        this.value = 0;
    }

    public ByteHolder(byte value) {
        this.value = value;
    }
}