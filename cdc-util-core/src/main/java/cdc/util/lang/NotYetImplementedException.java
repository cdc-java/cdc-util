package cdc.util.lang;

public class NotYetImplementedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NotYetImplementedException() {
        super();
    }

    public NotYetImplementedException(String message) {
        super(message);
    }

    public NotYetImplementedException(String message,
                                Throwable cause) {
        super(message, cause);
    }

    public NotYetImplementedException(Throwable cause) {
        super(cause);
    }
}