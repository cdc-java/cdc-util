package cdc.util.lang;

public class ShortHolder {
    public short value;

    public ShortHolder() {
        this.value = 0;
    }

    public ShortHolder(short value) {
        this.value = value;
    }
}