package cdc.util.lang;

import java.util.function.Function;

import org.apache.logging.log4j.Logger;

/**
 * Enumeration of possible reactions when an operation fails.
 *
 * @author Damien Carbonne
 *
 */
public enum FailureReaction {
    /**
     * Keeps silent, and returns a default value (typically {@code null} or do nothing.
     */
    DEFAULT,

    /**
     * Indicates that the operation failed, and returns a default value (typically {@code null} or do nothing.
     */
    WARN,

    /**
     * Raises an exception when the operation failed.
     */
    FAIL;

    /**
     * A function invoked in case of failure and that reacts as described by a failure reaction.
     * <p>
     * Depending on {@code reaction}:
     * <ul>
     * <li>{@link FailureReaction#DEFAULT}: nothing is done.
     * <li>{@link FailureReaction#WARN}: a warning is logged with {@code message}.
     * <li>{@link FailureReaction#FAIL}: an error is logged with {@code message} and an exception is raised with {@code message}.
     * </ul>
     *
     * @param <E> The raised exception type.
     * @param message The message.
     * @param logger The logger.
     * @param reaction The failure reaction.
     * @param newException The exception builder.
     * @throws E When {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <E extends Exception> void onError(String message,
                                                     Logger logger,
                                                     FailureReaction reaction,
                                                     Function<String, E> newException) throws E {
        switch (reaction) {
        case FAIL:
            logger.error(message);
            throw newException.apply(message);
        case WARN:
            logger.warn(message);
            break;
        case DEFAULT:
        default:
            break;
        }
    }

    /**
     * A function invoked in case of failure and that reacts as described by a failure reaction.
     * <p>
     * Depending on {@code reaction}:
     * <ul>
     * <li>{@link FailureReaction#DEFAULT}: silently returns {@code def}.
     * <li>{@link FailureReaction#WARN}: a warning is logged with {@code message} and returns {@code def}.
     * <li>{@link FailureReaction#FAIL}: an error is logged with {@code message} and an exception is raised with {@code message}.
     * </ul>
     *
     * @param <T> The return type.
     * @param <E> The raised exception type.
     * @param message The message.
     * @param logger The logger.
     * @param reaction The failure reaction.
     * @param def The default value to return.
     * @param newException The exception builder.
     * @return The default value {@code def}.
     * @throws E When {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <T, E extends Exception> T onError(String message,
                                                     Logger logger,
                                                     FailureReaction reaction,
                                                     T def,
                                                     Function<String, E> newException) throws E {
        switch (reaction) {
        case FAIL:
            logger.error(message);
            throw newException.apply(message);
        case WARN:
            logger.warn(message);
            return def;
        case DEFAULT:
        default:
            return def;
        }
    }

    public static <T, E extends Exception> T onError(String message,
                                                     Function<String, E> newException) throws E {
        throw newException.apply(message);
    }

    /**
     * A function used to react to a result.
     * <p>
     * If {@code result} is not {@code null}, returns {@code result}.<br>
     * Otherwise, on {@code reaction}:
     * <ul>
     * <li>{@link FailureReaction#DEFAULT}: silently returns {@code def}.
     * <li>{@link FailureReaction#WARN}: a warning is logged with {@code message} and returns {@code def}.
     * <li>{@link FailureReaction#FAIL}: an error is logged with {@code message} and an exception is raised with {@code message}.
     * </ul>
     *
     * @param <T> The return type.
     * @param <E> The raised exception type.
     * @param result The result to analyze.
     * @param message The message.
     * @param logger The logger.
     * @param reaction The failure reaction.
     * @param def The default value to return.
     * @param newException The exception builder.
     * @return {@code result} if not {@code null}, or {@code def}.
     * @throws E When {@code result} is {@code null} and {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <T, E extends Exception> T onResult(T result,
                                                      String message,
                                                      Logger logger,
                                                      FailureReaction reaction,
                                                      T def,
                                                      Function<String, E> newException) throws E {
        if (result == null) {
            return onError(message, logger, reaction, def, newException);
        } else {
            return result;
        }
    }

    public static <T, E extends Exception> T onResult(T result,
                                                      String message,
                                                      Function<String, E> newException) throws E {
        if (result == null) {
            return onError(message, newException);
        } else {
            return result;
        }
    }
}