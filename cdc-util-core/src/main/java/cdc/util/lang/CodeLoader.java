package cdc.util.lang;

import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utility class used to force loading of classes.
 *
 * @author Damien Carbonne
 */
public abstract class CodeLoader {
    // TODO move this code to specific project. Add measures ? Use annotations.
    private static final Logger LOGGER = LogManager.getLogger(CodeLoader.class);

    public static boolean load(String className) {
        LOGGER.info("load({})", className);
        final Class<? extends CodeLoader> cls = Introspection.getClass(className, CodeLoader.class);
        if (cls != null) {
            try {
                final CodeLoader loader = cls.getDeclaredConstructor().newInstance();
                loader.load();
                return true;
            } catch (IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException
                    | NoSuchMethodException | SecurityException e) {
                LOGGER.catching(e);
            }
        } else {
            LOGGER.warn("Failed to find {}", className);
        }
        return false;
    }

    public final void load() {
        LOGGER.info("load({})", getClass().getCanonicalName());
        loadAll();
    }

    /**
     * Implemented by concrete classes to load a set of classes.
     */
    protected abstract void loadAll();

    /**
     * Forces loading of a class.
     *
     * @param cls The class to load.
     */
    protected void load(Class<?> cls) {
        // Ignore
    }
}