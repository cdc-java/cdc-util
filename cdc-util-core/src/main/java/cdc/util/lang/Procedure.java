package cdc.util.lang;

@FunctionalInterface
public interface Procedure {
    public void invoke();
}