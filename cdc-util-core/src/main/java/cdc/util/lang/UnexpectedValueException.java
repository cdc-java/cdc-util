package cdc.util.lang;

public class UnexpectedValueException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UnexpectedValueException() {
        super();
    }

    public UnexpectedValueException(String message) {
        super(message);
    }

    public UnexpectedValueException(Enum<?> value) {
        super("Unexpected enum value: " + value);
    }
}