package cdc.util.lang;

public final class SystemUtils {
    private SystemUtils() {
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis, 0);
        } catch (final InterruptedException e) {
            BlackHole.discard(e);
        }
    }

    public static void sleepInSeconds(double seconds) {
        try {
            Thread.sleep((int) (1000.0 * seconds), 0);
        } catch (final InterruptedException e) {
            BlackHole.discard(e);
        }
    }
}