package cdc.util.lang;

public class ImplementationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ImplementationException() {
        super();
    }

    public ImplementationException(String message) {
        super(message);
    }

    public ImplementationException(String message,
                                   Throwable cause) {
        super(message, cause);
    }

    public ImplementationException(Throwable cause) {
        super(cause);
    }
}