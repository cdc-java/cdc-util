package cdc.util.paths;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * Representation of an absolute or relative path.
 * <p>
 * This implementation is immutable.
 * <p>
 * One of the following characters can be used a separator: '/', '\\' or '|'.
 * <p>
 * A path is a succession of names separated by a separator.<br>
 * A name can not contain any separator<br>
 * An absolute path starts by a separator.<br>
 * '.' and '..' are special names.
 * <p>
 * It is possible to mix separators.<br>
 * Separator does not matter in comparisons.
 *
 * @author Damien Carbonne
 */
public class Path implements Comparable<Path> {
    public static final char SLASH = '/';
    public static final char BACKSLASH = '\\';
    public static final char PIPE = '|';

    /**
     * Enumeration of possible separators.
     */
    public enum Separator {
        SLASH(Path.SLASH),
        BACKSLASH(Path.BACKSLASH),
        PIPE(Path.PIPE);

        private final char c;

        private Separator(char c) {
            this.c = c;
        }

        public char getChar() {
            return c;
        }
    }

    /** The root path. */
    public static final Path ROOT = new Path(Character.toString(SLASH));
    /** The local path. */
    public static final Path DOT = new Path(".");
    /** The parent path. */
    public static final Path DOT_DOT = new Path("..");
    private final boolean absolute;
    private final List<String> names;

    private Path(boolean absolute,
                 List<String> names) {
        this.absolute = absolute;
        this.names = Collections.unmodifiableList(names);
    }

    public Path(String path) {
        this(isAbsolute(path),
             toNames(path));
    }

    public static Path of(String path) {
        return new Path(path);
    }

    /**
     * @param c The char.
     * @return {@code true} if {@code c} is a separator char.
     */
    private static boolean isSeparator(char c) {
        return c == SLASH || c == BACKSLASH || c == PIPE;
    }

    /**
     * @param s The string.
     * @param from The start index.
     * @return The index of the next separator, or -1.
     */
    private static int separatorIndex(String s,
                                      int from) {
        for (int index = from; index < s.length(); index++) {
            final char c = s.charAt(index);
            if (isSeparator(c)) {
                return index;
            }
        }
        return -1;
    }

    /**
     *
     * @param path The string.
     * @return {@code true} if {@code path} starts with a separator.
     */
    private static boolean isAbsolute(String path) {
        Checks.isNotNull(path, "path");
        return !path.isEmpty() && isSeparator(path.charAt(0));
    }

    private static List<String> toNames(String path) {
        Checks.isNotNull(path, "path");
        final List<String> tmp = new ArrayList<>();
        int from = 0;
        while (from < path.length()) {
            int to = separatorIndex(path, from);
            if (to == -1) {
                final String next = path.substring(from);
                tmp.add(next);
                to = path.length();
            } else if (to == 0) {
                // Ignore: absolute path
            } else if (to == from) {
                // two consecutive separators
                tmp.add("");
            } else {
                tmp.add(path.substring(from, to));
            }
            from = to + 1;
        }
        return tmp;
    }

    /**
     * @param s The string.
     * @return {@code true} if {@code s} is a valid string as a name.
     */
    public static boolean isValidName(String s) {
        return s != null
                && !s.isEmpty()
                && separatorIndex(s, 0) < 0;
    }

    /**
     * @return {@code true} if this path is absolute.
     */
    public boolean isAbsolute() {
        return absolute;
    }

    /**
     * @return {@code true} if this path is relative.
     */
    public boolean isRelative() {
        return !absolute;
    }

    /**
     * Returns the parent path of this path.
     * <ul>
     * <li>{@code "/".getParent() = "/"}
     * <li>{@code "".getParent() = ".."}
     * <li>{@code "foo/x".getParent() = "foo"}
     * </ul>
     *
     * @return The parent path of this path.
     */
    public Path getParent() {
        if (getNameCount() == 0) {
            if (isAbsolute()) {
                return ROOT;
            } else {
                return DOT_DOT;
            }
        } else {
            return resolve(DOT_DOT);
        }
    }

    /**
     * Returns the name at a position.
     *
     * @param index The index.
     * @return The name located at {@code index}.
     */
    public String getName(int index) {
        return names.get(index);
    }

    /**
     * @return The number of names of this path.
     */
    public int getNameCount() {
        return names.size();
    }

    /**
     * Returns a path that is this path with redundant names eliminated.
     * <p>
     * For example:
     * <ul>
     * <li>{@code ["/a/b"].normalize} yields {@code ["/a/b"]}.
     * <li>{@code ["/a/b/.."].normalize} yields {@code ["/a"]}.
     * <li>{@code ["/a/b/."].normalize} yields {@code ["/a/b"]}.
     * <li>{@code ["/a/.."].normalize} yields {@code ["/"]}.
     * <li>{@code ["/a/../.."].normalize} yields {@code ["/.."]}.
     * <li>{@code ["a/.."].normalize} yields {@code [""]}.
     * <li>{@code ["a/../.."].normalize} yields {@code [".."]}.
     * </ul>
     *
     * @return A path that is this path with redundant names eliminated.
     */
    public Path normalize() {
        final List<String> tmp = new ArrayList<>();
        tmp.addAll(names);

        int index = 0;

        while (index < tmp.size()) {
            final String name = tmp.get(index);
            if (".".equals(name) || name.isEmpty()) {
                if (tmp.size() > 1) {
                    // Remove it if it is not the only name
                    tmp.remove(index);
                } else {
                    index++;
                }
            } else if ("..".equals(name)) {
                if (index == 0) {
                    if (absolute) {
                        tmp.remove(index);
                    } else {
                        index++;
                    }
                } else {
                    if ("..".equals(tmp.get(index - 1))) {
                        index++;
                    } else {
                        tmp.subList(index - 1, index + 1).clear();
                        index--;
                    }
                }
            } else {
                index++;
            }
        }
        return new Path(absolute, tmp);
    }

    /**
     * @return A path with same names as this one, and that is relative.
     */
    public Path toRelative() {
        if (isRelative()) {
            return this;
        } else {
            return new Path(false, names);
        }
    }

    /**
     * @return A path with same names as this one, and that is absolute.
     */
    public Path toAbsolute() {
        if (isAbsolute()) {
            return this;
        } else {
            return new Path(true, names);
        }
    }

    /**
     * Resolves the given path against this path.
     * <p>
     * If {@code other} is an {@link #isAbsolute() absolute} path then returns {@code other}.<br>
     * Otherwise, created a new path by appending names of {@code other} to names of this {@code Path}.
     * <p>
     * For example:
     * <ul>
     * <li>{@code ["/a/b"].resolve(["/c"])} yields {@code ["/c"]}.
     * <li>{@code ["/a/b"].resolve(["c"])} yields {@code ["/a/b/c"]}.
     * <li>{@code ["a/b"].resolve(["c"])} yields {@code ["a/b/c"]}.
     * </ul>
     *
     * @param other The path to resolve against this one.
     * @return The resulting Path.
     */
    public Path resolve(Path other) {
        if (other.isAbsolute()) {
            return other;
        } else {
            final List<String> tmp = new ArrayList<>();
            tmp.addAll(names);
            tmp.addAll(other.names);
            return new Path(isAbsolute(), tmp);
        }
    }

    /**
     * Converts a given path string to a {@code Path} and resolves it against
     * this {@code Path} in exactly the manner specified by the {@link #resolve(Path) resolve} method.
     *
     * @param other The path string.
     * @return The resulting Path.
     */
    public Path resolve(String other) {
        return resolve(new Path(other));
    }

    /**
     * Returns a relative Path that is a subsequence of the name elements of this path.
     * <p>
     * The beginIndex and endIndex parameters specify the subsequence of name elements. The name that is closest to the root in
     * the
     * hierarchy has index 0. The name that is farthest from the root has index count-1. The returned Path has the
     * name elements that begin at beginIndex and extend to the element at index endIndex-1.
     *
     * @param beginIndex The begin index (inclusive).
     * @param endIndex the end index (exclusive).
     * @return A new Path that is a subsequence of the name elements in this Path.
     */
    public Path subpath(int beginIndex,
                        int endIndex) {
        return new Path(false, names.subList(beginIndex, endIndex));
    }

    /**
     * Tests if this path starts with a given path.
     * <p>
     * This is the case when this path and {@code other} path are
     * both {@link #isAbsolute() absolute} or {@link #isRelative() relative},
     * and this path starts with all names of {@code other}.
     * <p>
     * <b>WARNING:</b> no {@link #normalize() normalization} is applied.
     *
     * @param other The given path.
     * @return {@code true} if this path starts with {@code other}; otherwise
     *         {@code false}.
     */
    public boolean startsWith(Path other) {
        if (absolute == other.absolute && names.size() >= other.names.size()) {
            return names.subList(0, other.names.size()).equals(other.names);
        } else {
            return false;
        }
    }

    /**
     * Converts a given path string to a {@code Path} and tests if
     * this path starts with it.
     *
     * @param other The path string.
     * @return {@code true} if this path starts with {@code new Path(other)}; otherwise
     *         {@code false}.
     */
    public boolean startsWith(String other) {
        return startsWith(new Path(other));
    }

    /**
     * Tests if this path ends with a given path.
     * <p>
     * If both paths have the same number of names, they must both
     * be {@link #isAbsolute() absolute} or {@link #isRelative() relative},
     * and have the same names.
     * <p>
     * If this path has more names than {@code other}, {@code other} must be
     * {@link #isRelative() relative} and names must match starting from the last one.
     * <p>
     * <b>WARNING:</b> no {@link #normalize() normalization} is applied.
     *
     * @param other The given path.
     * @return {@code true} if this path ends with {@code other}; otherwise
     *         {@code false}.
     */
    public boolean endsWith(Path other) {
        if (names.size() == other.names.size()) {
            if (!absolute && other.absolute) {
                return false;
            } else {
                return names.equals(other.names);
            }
        } else if (names.size() > other.names.size()) {
            if (other.absolute) {
                return false;
            } else {
                return names.subList(names.size() - other.names.size(), names.size()).equals(other.names);
            }
        } else {
            return false;
        }
    }

    /**
     * Converts a given path string to a {@code Path} and tests if
     * this path ends with it.
     *
     * @param other The path string.
     * @return {@code true} if this path ends with {@code new Path(other)}; otherwise
     *         {@code false}.
     */
    public boolean endsWith(String other) {
        return endsWith(new Path(other));
    }

    @Override
    public int hashCode() {
        return names.hashCode()
                + Boolean.hashCode(absolute);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Path)) {
            return false;
        }
        final Path other = (Path) object;
        return absolute == other.absolute
                && names.equals(other.names);
    }

    /**
     * @param separator The separator.
     * @return A String representation of this path using {@code separator}.
     */
    public String toString(Separator separator) {
        final char sep = separator.getChar();
        final StringBuilder builder = new StringBuilder();
        if (isAbsolute()) {
            builder.append(sep);
        }
        for (int index = 0; index < getNameCount(); index++) {
            if (index != 0) {
                builder.append(sep);
            }
            builder.append(getName(index));
        }
        if (builder.length() == 0) {
            builder.append('.');
        }
        return builder.toString();
    }

    /**
     * @return A String representation using the slash separator.
     */
    public String toStringSlash() {
        return toString(Separator.SLASH);
    }

    /**
     * @return A String representation using the backslash separator.
     */
    public String toStringBackslash() {
        return toString(Separator.BACKSLASH);
    }

    /**
     * @return A String representation using the pipe separator.
     */
    public String toStringPipe() {
        return toString(Separator.PIPE);
    }

    @Override
    public String toString() {
        return toString(Separator.SLASH);
    }

    @Override
    public int compareTo(Path o) {
        return toString().compareTo(o.toString());
    }
}