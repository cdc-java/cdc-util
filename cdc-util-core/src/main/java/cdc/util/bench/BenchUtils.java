package cdc.util.bench;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class BenchUtils {
    private BenchUtils() {
    }

    public static String basename(Class<?> benchClass) {
        final Date date = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return benchClass.getSimpleName() + "-" + format.format(date) + "-bench";
    }

    public static String filename(File folder,
                                  Class<?> benchClass,
                                  String ext) {
        return new File(folder, basename(benchClass) + ext).getPath();
    }

    public static String filename(String folder,
                                  Class<?> benchClass,
                                  String ext) {
        return filename(new File(folder), benchClass, ext);
    }
}