package cdc.util.events;

import java.util.function.Consumer;

/**
 * Class used to filter progress events.
 * <p>
 * Time based and percentage base filtering are possible.<br>
 * They can be enabled individually.
 *
 * @author Damien Carbonne
 */
public class ProgressEventFilter {
    private Consumer<ProgressEvent> consumer;
    private int minDeltaMillis = 250;
    private double minDeltaPercents = 1.0;

    private long previousTotal;
    private double previousPercents;
    private long previousMillis;

    /**
     * Creates a ProgressEventFilter with an optional ProgressEvent consumer.
     * <p>
     * Refresh time interval is 250 ms and percentage interval is 1 percent.
     *
     * @param consumer The optional ProgressEvent consumer.
     */
    public ProgressEventFilter(Consumer<ProgressEvent> consumer) {
        this.consumer = consumer;
        this.previousTotal = -1L;
        this.previousPercents = -1000.0;
        this.previousMillis = 0L;
    }

    /**
     * Creates a ProgressEventFilter with no ProgressEvent consumer.
     * <p>
     * Refresh time interval is 250 ms and percentage interval is 1 percent.
     */
    public ProgressEventFilter() {
        this(null);
    }

    private static long getMillis() {
        return System.currentTimeMillis();
    }

    /**
     * Resets memorization (percentage and time) of previous fired event.
     */
    public void reset() {
        this.previousTotal = -1L;
        this.previousPercents = -1000.0;
        this.previousMillis = 0L;
    }

    /**
     * @return The ProgressEvent consumer.
     */
    public Consumer<ProgressEvent> getConsumer() {
        return consumer;
    }

    /**
     * Sets the ProgressEvent consumer.
     *
     * @param consumer The ProgressEvent consumer.
     */
    public void setConsumer(Consumer<ProgressEvent> consumer) {
        this.consumer = consumer;
    }

    /**
     * @return The minimum time change (milliseconds) between 2 accepted events. Enabled if positive.
     */
    public final int getMinDeltaMillis() {
        return minDeltaMillis;
    }

    /**
     * Sets the minimum time increment between 2 accepted events.
     *
     * @param value The minimum time increment (in mill seconds) between 2 accepted events.
     *            If {@code value < 0}, time filtering is disabled.
     */
    public void setMinDeltaMillis(int value) {
        this.minDeltaMillis = value;
    }

    /**
     * @return The minimum percentage change between 2 accepted events. Enabled if positive.
     */
    public final double getMinDeltaPercents() {
        return minDeltaPercents;
    }

    /**
     * Sets the minimum percentage change between 2 accepted events.
     * <p>
     * If {@code value <= 0.0}, percentage filtering is disabled.
     *
     * @param value The minimum percentage change between 2 accepted events.
     */
    public void setMinDeltaPercents(double value) {
        this.minDeltaPercents = value;
    }

    /**
     * Checks percentage criterion.
     * <p>
     * If percentage criterion passes, then current event parameters are memorized.<br>
     * <b>WARNING:</b> This can be only be called if percentage filtering is enabled.
     *
     * @param value The current value.
     * @param total The total value.
     * @param force If {@code true}, filtering is bypassed.
     * @return {@code true} if percentage criterion is accepted.
     */
    private boolean checkPercentage(long value,
                                    long total,
                                    boolean force) {
        // Be careful if progress is going backward
        if (force || total > 0) {
            final double percents = ProgressEvent.getPercents(value, total);
            if (force
                    || total != previousTotal
                    || total == value
                    || (Math.abs(percents - previousPercents) >= minDeltaPercents)) {
                previousPercents = percents;
                previousMillis = getMillis();
                previousTotal = total;
                return true;
            }
        }
        return false;
    }

    /**
     * Checks time criterion.
     * <p>
     * If time criterion passes, then current event parameters are memorized.<br>
     * <b>WARNING:</b> This can be only be called if time filtering is enabled.
     *
     * @param value The current value.
     * @param total The total value.
     * @param force If {@code true}, filtering is bypassed.
     * @return {@code true} if time criterion is accepted.
     */
    private boolean checkTime(long value,
                              long total,
                              boolean force) {
        final long millis = getMillis();
        if (force
                || (millis - previousMillis >= minDeltaMillis)) {
            previousPercents = ProgressEvent.getPercents(value, total);
            previousMillis = millis;
            previousTotal = total;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns {@code true} if an event is accepted.
     *
     * @param value The current value.
     * @param total The total value.
     * @param force If {@code true}, filtering is bypassed.
     * @return {@code true} if the event corresponding to {@code (value, total)} is accepted.
     */
    private boolean canFire(long value,
                            long total,
                            boolean force) {
        if (minDeltaPercents <= 0.0) {
            // No percentage filtering
            if (minDeltaMillis <= 0) {
                // No filtering at all
                return true;
            } else {
                // Time filtering only
                return checkTime(value, total, force);
            }
        } else {
            // Percentage filtering
            if (checkPercentage(value, total, force)) {
                return true;
            } else if (minDeltaMillis > 0) {
                // Percentage does not pass, but time may pass
                return checkTime(value, total, force);
            } else {
                return force;
            }
        }
    }

    /**
     * Returns {@code true} if an event is accepted.
     * <p>
     * An important event is always accepted.
     *
     * @param event The event.
     * @param force If {@code true}, filtering is bypassed.
     * @return {@code true} if {@code event} is accepted.
     */
    private boolean canFire(ProgressEvent event,
                            boolean force) {
        return canFire(event.getValue(), event.getTotal(), force || event.isImportant());
    }

    /**
     * Notifies a progress event.
     * <p>
     * The event is dispatched to consumer if it passes filtering
     * and consumer is not {@code null}.
     *
     * @param event The progress event.
     * @param force If {@code true}, filtering is bypassed.
     */
    public void notifyProgress(ProgressEvent event,
                               boolean force) {
        // Call canFire before comparing consumer, as it will update counters.
        if (canFire(event, force) && consumer != null) {
            consumer.accept(event);
        }
    }

    /**
     * Notifies a progress event.
     * <p>
     * The event is dispatched to consumer if it passes filtering
     * and consumer is not {@code null}.
     *
     * @param event The progress event.
     */
    public void notifyProgress(ProgressEvent event) {
        notifyProgress(event, false);
    }

    /**
     * Notifies a progress event.
     * <p>
     * The event is dispatched to consumer if it passes filtering
     * and consumer is not {@code null}.
     *
     * @param value The current value.
     * @param total The total value.
     * @param detail The progress detail.
     * @param force If {@code true}, filtering is bypassed.
     */
    public void notifyProgress(long value,
                               long total,
                               String detail,
                               boolean force) {
        // Call canFire before comparing consumer, as it will update counters.
        if (canFire(value, total, force) && consumer != null) {
            consumer.accept(new ProgressEvent(value, total, detail, force));
        }
    }

    /**
     * Notifies a progress event.
     * <p>
     * The event is dispatched to consumer if it passes filtering
     * and consumer is not {@code null}.
     *
     * @param value The current value.
     * @param total The total value.
     * @param detail The progress detail.
     */
    public void notifyProgress(long value,
                               long total,
                               String detail) {
        notifyProgress(value, total, detail, false);
    }

    /**
     * Notifies a progress event.
     * <p>
     * The event is dispatched to consumer if it passes filtering
     * and consumer is not {@code null}.
     *
     * @param value The current value.
     * @param total The total value.
     * @param force If {@code true}, filtering is bypassed.
     */
    public void notifyProgress(long value,
                               long total,
                               boolean force) {
        notifyProgress(value, total, null, force);
    }

    /**
     * Notifies a progress event.
     * <p>
     * The event is dispatched to consumer if it passes filtering
     * and consumer is not {@code null}.
     *
     * @param value The current value.
     * @param total The total value.
     */
    public void notifyProgress(long value,
                               long total) {
        notifyProgress(value, total, false);
    }
}