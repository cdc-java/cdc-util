package cdc.util.events;

public class ProgressCancelledException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ProgressCancelledException() {
        super();
    }

    public ProgressCancelledException(String message) {
        super(message);
    }

    public ProgressCancelledException(Throwable cause) {
        super(cause);
    }

    public ProgressCancelledException(String message,
                                      Throwable cause) {
        super(message, cause);
    }
}