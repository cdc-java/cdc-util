package cdc.util.events;

import java.time.format.DateTimeFormatter;

import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Class describing a progress event, by:
 * <ul>
 * <li>A total value. When {@code total <= 0} the progress status is undefined.
 * <li>A current value that must be {@code >= 0}. It should be {@code <= total}.
 * <li>A detail describing the event.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public class ProgressEvent extends Event {
    private final long value;
    private final long total;
    private final String detail;
    private final boolean important;

    /**
     * Creates a ProgressEvent.
     *
     * @param value The current value ({@code >= 0}).
     * @param total The total value (may be negative).
     * @param detail The detail (may be {@code null}).
     * @param important If {@code true}, this event is important and should not be filtered.
     */
    public ProgressEvent(long value,
                         long total,
                         String detail,
                         boolean important) {
        Checks.isTrue(value >= 0, "value must be >= 0");
        this.value = value;
        this.total = total;
        this.detail = detail;
        this.important = important;
    }

    /**
     * Creates a non-important ProgressEvent.
     *
     * @param value The current value ({@code >= 0}).
     * @param total The total value (may be negative).
     * @param detail The detail (may be {@code null}).
     */
    public ProgressEvent(long value,
                         long total,
                         String detail) {
        this(value, total, detail, false);
    }

    /**
     * Creates a non-important ProgressEvent with {@code null} detail.
     *
     * @param value The current value ({@code >= 0}).
     * @param total The total value (may be negative).
     */
    public ProgressEvent(long value,
                         long total) {
        this(value, total, null);
    }

    /**
     * @return {@code true} when progress is undefined.
     *         It is the case when {@code total <= 0}.
     */
    public boolean isUndefined() {
        return total <= 0;
    }

    public boolean isImportant() {
        return important;
    }

    /**
     * @return The current value ({@code >= 0}).
     */
    public final long getValue() {
        return value;
    }

    /**
     * @return The total. When {@code <= 0}, the progress is undefined.
     */
    public final long getTotal() {
        return total;
    }

    /**
     * @return The associated detail (possibly {@code null}).
     */
    public final String getDetail() {
        return detail;
    }

    /**
     * Converts a (value, total) pair to a percentage.
     * <p>
     * When {@code total <= 0}, -1.0 is returned.
     * <p>
     * Result is computed with a precision of 6 digits.
     *
     * @param value The value.
     * @param total The total.
     * @return The percentage corresponding to {@code value} and {@code total }.
     */
    public static double getPercents(long value,
                                     long total) {
        if (total > 0) {
            final double x = Math.round(((double) value / total) * 1.0e8);
            return x / 1.0e6;
        } else {
            return -1.0;
        }
    }

    /**
     * @return The current progress percent.
     *         Meaningful when {@code total > 0}.
     */
    public double getPercents() {
        return getPercents(value, total);
    }

    @Override
    public String toString() {
        if (getPercents() >= 0.0) {
            if (StringUtils.isNullOrEmpty(detail)) {
                return String.format("[%s %5.2f%%]",
                                     getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                                     getPercents());
            } else {
                return String.format("[%s %5.2f%% %s]",
                                     getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                                     getPercents(),
                                     detail);
            }
        } else {
            if (StringUtils.isNullOrEmpty(detail)) {
                return String.format("[%s ???]",
                                     getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            } else {
                return String.format("[%s ??? %s]",
                                     getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                                     detail);
            }
        }
    }
}