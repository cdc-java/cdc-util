package cdc.util.events;

import java.util.ArrayList;
import java.util.List;

import cdc.util.lang.Checks;

public class ProgressStack {
    private final List<Integer> stack = new ArrayList<>();

    public ProgressStack(int number) {
        stack.add(number);
    }

    public ProgressStack() {
        this(1);
    }

    public void push(int number) {
        stack.add(number);
    }

    public void push() {
        push(1);
    }

    public void pop() {
        Checks.assertFalse(stack.isEmpty(), "empty stack");
        stack.remove(stack.size() - 1);
    }

    public void add(int increment) {
        final int index = stack.size() - 1;
        stack.set(index, stack.get(index) + increment);
    }

    public void add() {
        add(1);
    }

    public int[] getNumbers() {
        final int[] array = new int[stack.size()];
        for (int index = 0; index < array.length; index++) {
            array[index] = stack.get(index);
        }
        return array;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final int value : stack) {
            if (first) {
                first = false;
            } else {
                builder.append('.');
            }
            builder.append(value);
        }
        return builder.toString();
    }
}