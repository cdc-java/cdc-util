package cdc.util.events;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import cdc.util.time.RefTime;

/**
 * Base class for events.
 *
 * @author Damien Carbonne
 *
 */
public class Event {
    private final long nanos;

    public Event() {
        this.nanos = System.nanoTime();
    }

    public long getNanos() {
        return nanos;
    }

    public long getUptimeNanos() {
        return nanos - RefTime.REF_NANOS;
    }

    public LocalDateTime getLocalDateTime() {
        return RefTime.nanosToLocalDataTime(nanos);
    }

    @Override
    public String toString() {
        return "[" + getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "]";
    }
}