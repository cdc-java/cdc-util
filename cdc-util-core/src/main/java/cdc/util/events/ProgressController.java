package cdc.util.events;

/**
 * Interface implemented by objects that can monitor and control progress.
 *
 * @author Damien Carbonne
 *
 */
public interface ProgressController {
    /**
     * Implementation of ProgressController that does nothing.
     */
    public static final ProgressController VOID = new ProgressController() {
        @Override
        public void onProgress(ProgressEvent event) {
            // Ignore
        }

        @Override
        public boolean isCancelled() {
            return false;
        }
    };

    public static final ProgressController VERBOSE = VerboseProgressController.DEFAULT;

    /**
     * Invoked by the controlled object to notify the controller of a progress change.
     *
     * @param event The event.
     */
    public void onProgress(ProgressEvent event);

    /**
     * Invoked by the controlled object to ask the controller if action must be interrupted or not.
     *
     * @return {@code true} if action must be cancelled.
     */
    public boolean isCancelled();
}