package cdc.util.events;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VerboseProgressController implements ProgressController {
    private static final Logger LOGGER = LogManager.getLogger(VerboseProgressController.class);
    public static final VerboseProgressController DEFAULT = new VerboseProgressController(ProgressController.VOID);
    private final ProgressController delegate;

    public VerboseProgressController(ProgressController delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onProgress(ProgressEvent event) {
        LOGGER.info("onProgress({})", event);
        delegate.onProgress(event);
    }

    @Override
    public boolean isCancelled() {
        return delegate.isCancelled();
    }
}