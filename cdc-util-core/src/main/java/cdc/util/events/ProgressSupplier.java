package cdc.util.events;

import java.io.IOException;
import java.util.function.Supplier;

import cdc.util.lang.Checks;

/**
 * Helper class used to generate progress events.
 * <p>
 * It can filter events.
 *
 * @author Damien Carbonne
 */
public class ProgressSupplier {
    private ProgressController controller;
    private long total = -1L;
    private long value = 0L;
    private String detail = null;
    private final ProgressEventFilter filter = new ProgressEventFilter();

    /**
     * Creates a progress supplier with a default controller,
     * that does nothing and never interrupts advance.
     */
    public ProgressSupplier() {
        this(ProgressController.VOID);
    }

    /**
     * Creates a progress supplier with a progress controller.
     *
     * @param controller The progress controller.
     */
    public ProgressSupplier(ProgressController controller) {
        Checks.isNotNull(controller, "controller");
        setController(controller);
    }

    /**
     * Sets the progress controller.
     *
     * @param controller The progress controller.
     */
    public final void setController(ProgressController controller) {
        Checks.isNotNull(controller, "controller");
        this.controller = controller;
        if (controller != null) {
            this.filter.setConsumer(controller::onProgress);
        } else {
            this.filter.setConsumer(null);
        }
        total = -1L;
        value = 0L;
    }

    /**
     * Fires a progress event.
     * <p>
     * This should rarely be necessary.
     *
     * @param force If {@code true} an event is generated, even if not necessary.
     */
    public void fireProgress(boolean force) {
        filter.notifyProgress(value, total, detail, force);
    }

    private void fireProgress() {
        fireProgress(false);
    }

    /**
     * @return The associated progress controller.
     */
    public ProgressController getController() {
        return controller;
    }

    /**
     * @return The associated event filter.
     *         It can be used to configure events filtering.
     */
    public ProgressEventFilter getFilter() {
        return filter;
    }

    /**
     * Resets filter, total, value and detail, and fires an event.
     * <p>
     * Value is set to 0.
     *
     * @param total The total value.
     * @param detail The detail.
     */
    public void reset(long total,
                      String detail) {
        filter.reset();
        this.total = total;
        value = 0L;
        this.detail = detail;
        fireProgress();
    }

    /**
     * Resets filter, total, value and detail, and fires an event.
     * <p>
     * Total is set to -1, value to 0 and detail to {@code null}.
     */
    public void reset() {
        reset(-1L, null);
    }

    /**
     * @return The total value.
     */
    public long getTotal() {
        return total;
    }

    /**
     * Sets the total value, and fires an event.
     *
     * @param total The total value.
     */
    public void setTotal(long total) {
        this.total = total;
        fireProgress();
    }

    /**
     * @return The current value.
     */
    public long getValue() {
        return value;
    }

    /**
     * Increments the current value and fires an event.
     *
     * @param by The increment (may be negative).
     */
    public void incrementValue(long by) {
        value += by;
        fireProgress();
    }

    /**
     * Increments the current value by one.
     */
    public void incrementValue() {
        incrementValue(1);
    }

    /**
     * Sets the current value and fires an event.
     * <p>
     * The new value may be less than the previous one.
     *
     * @param value The current value.
     */
    public void setValue(long value) {
        Checks.isTrue(value >= 0L, "value < 0");
        this.value = value;
        fireProgress();
    }

    /**
     * Sets the progress detail.
     *
     * @param detail The detail.
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * @return The progress detail.
     */
    public String getDetail() {
        return detail;
    }

    public void close() {
        if (value > 0) {
            setTotal(value);
        } else {
            setTotal(100);
            setValue(100);
        }
    }

    /**
     * If controller is cancelled, throws an exception.
     *
     * @param <E> The exception type.
     * @param supplier The exception supplier.
     * @throws E When the controller is cancelled.
     */
    public <E extends Exception> void checkCancelled(Supplier<E> supplier) throws E {
        if (controller.isCancelled()) {
            throw supplier.get();
        }
    }

    /**
     * If controller is cancelled, throws an IOEXception.
     *
     * @throws IOException When the controller is cancelled.
     */
    public void checkCancelled() throws IOException {
        checkCancelled(() -> new IOException("Interrupted"));
    }
}