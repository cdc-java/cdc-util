package cdc.util.function;

import java.util.BitSet;

import cdc.util.strings.StringUtils;

/**
 * Implementation of {@link CharPredicate} based on {@link BitSet}.
 * <p>
 * This predicate returns the belonging of a character to a set.
 * <p>
 * The implementation has a constant response time, slightly worse than {@link BooleanArrayCharPredicate}.
 * It is 8 times more memory efficient (1 bit per character.
 *
 * @author Damien Carbonne
 */
public final class BitSetCharPredicate implements CharPredicate {
    private final BitSet set;

    public BitSetCharPredicate(String chars) {
        this.set = new BitSet(StringUtils.maxChar(chars) + 1);
        for (int index = 0; index < chars.length(); index++) {
            this.set.set(chars.charAt(index));
        }
    }

    @Override
    public boolean test(char value) {
        return set.get(value);
    }
}