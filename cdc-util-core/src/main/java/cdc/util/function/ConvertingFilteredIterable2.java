package cdc.util.function;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Iterable (of target type) built over an Iterable (of source type) and a
 * predicate (of target type).
 *
 * @author Damien Carbonne
 *
 * @param <S> Type of the source elements returned by the delagate.
 * @param <T> Type of the target elements returned by the iterator.
 */
public class ConvertingFilteredIterable2<S, T> implements Iterable<T> {
    private final Function<S, T> converter;
    private final Iterable<? extends S> delegate;
    private final Predicate<? super T> predicate;

    /**
     * Creates a ConvertingFilteredIterable using a filter.
     *
     * @param converter The converter.
     * @param delegate The delegate iterable.
     * @param predicate The filtering predicate.
     */
    public ConvertingFilteredIterable2(Function<S, T> converter,
                                       Iterable<? extends S> delegate,
                                       Predicate<? super T> predicate) {
        Checks.isNotNull(converter, "converter");
        Checks.isNotNull(delegate, "delegate");
        Checks.isNotNull(predicate, "predicate");

        this.converter = converter;
        this.delegate = delegate;
        this.predicate = predicate;
    }

    /**
     * Creates a ConvertingFilteredIterable without filter.
     *
     * @param converter The converter.
     * @param delegate The delegate iterable.
     */
    public ConvertingFilteredIterable2(Function<S, T> converter,
                                       Iterable<? extends S> delegate) {
        this(converter, delegate, Predicates.alwaysTrue());
    }

    @Override
    public Iterator<T> iterator() {
        return new ConvertingFilteredIterator2<>(converter, delegate.iterator(), predicate);
    }
}