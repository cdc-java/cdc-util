package cdc.util.function;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * Implementation of {@link Function} that caches results.
 * <p>
 * All computations are cached and never released, till {@link #clear()} is called.
 *
 * @param <T> The type of the input to the function.
 * @param <R> The type of the result of the function.
 */
public final class FunctionCache<T, R> implements Function<T, R> {
    private final Function<T, R> delegate;
    private final Map<T, R> cache = new HashMap<>();

    private FunctionCache(Function<T, R> delegate) {
        this.delegate = delegate;
    }

    /**
     * Creates a cached version of a function.
     *
     * @param <T> The type of the input to the function.
     * @param <R> The type of the result of the function.
     * @param function The function.
     * @return A cached version of {@code function}.
     */
    public static <T, R> FunctionCache<T, R> of(Function<T, R> function) {
        return new FunctionCache<>(function);
    }

    @Override
    public R apply(T t) {
        return cache.computeIfAbsent(t, delegate);
    }

    /**
     * Clears this cache.
     */
    public void clear() {
        cache.clear();
    }

    /**
     * @return A set of caches keys.
     */
    public Set<T> getKeys() {
        return cache.keySet();
    }
}