package cdc.util.function;

import java.util.Objects;

/**
 * Represents a predicate (boolean-valued function) of one {@code char}-valued
 * argument.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface CharPredicate {
    /**
     * Evaluates this predicate on the given argument.
     *
     * @param value The input argument
     * @return {@code true} if the input argument matches the predicate,
     *         otherwise {@code false}
     */
    public boolean test(char value);

    /**
     * Returns a composed predicate that represents a short-circuiting logical
     * AND of this predicate and another. When evaluating the composed
     * predicate, if this predicate is {@code false}, then the {@code other}
     * predicate is not evaluated.
     *
     * <p>
     * Any exceptions thrown during evaluation of either predicate are relayed
     * to the caller; if evaluation of this predicate throws an exception, the
     * {@code other} predicate will not be evaluated.
     *
     * @param other A predicate that will be logically-ANDed with this
     *            predicate
     * @return A composed predicate that represents the short-circuiting logical
     *         AND of this predicate and the {@code other} predicate
     * @throws NullPointerException if other is null
     */
    default CharPredicate and(CharPredicate other) {
        Objects.requireNonNull(other);
        return value -> test(value) && other.test(value);
    }

    /**
     * Returns a predicate that represents the logical negation of this
     * predicate.
     *
     * @return A predicate that represents the logical negation of this
     *         predicate
     */
    default CharPredicate negate() {
        return value -> !test(value);
    }

    /**
     * Returns a composed predicate that represents a short-circuiting logical
     * OR of this predicate and another. When evaluating the composed
     * predicate, if this predicate is {@code true}, then the {@code other}
     * predicate is not evaluated.
     *
     * <p>
     * Any exceptions thrown during evaluation of either predicate are relayed
     * to the caller; if evaluation of this predicate throws an exception, the
     * {@code other} predicate will not be evaluated.
     *
     * @param other a predicate that will be logically-ORed with this
     *            predicate
     * @return A composed predicate that represents the short-circuiting logical
     *         OR of this predicate and the {@code other} predicate
     * @throws NullPointerException if other is null
     */
    default CharPredicate or(CharPredicate other) {
        Objects.requireNonNull(other);
        return value -> test(value) || other.test(value);
    }
}