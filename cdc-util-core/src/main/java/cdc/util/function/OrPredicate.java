package cdc.util.function;

import java.util.List;
import java.util.function.Predicate;

public final class OrPredicate<T> extends AbstractCompositePredicate<T> {

    @SafeVarargs
    public OrPredicate(Predicate<? super T>... delegates) {
        super(delegates);
    }

    public OrPredicate(final List<Predicate<? super T>> delegates) {
        super(delegates);
    }

    @Override
    public boolean test(T input) {
        boolean result = false;
        for (final Predicate<? super T> delegate : delegates) {
            result = result || delegate.test(input);
        }
        return result;
    }
}