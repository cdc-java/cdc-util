package cdc.util.function;

import java.util.List;
import java.util.function.Predicate;

public abstract class AbstractCompositePredicate<T> implements Predicate<T> {
    protected final Predicate<? super T>[] delegates;

    @SafeVarargs
    protected AbstractCompositePredicate(Predicate<? super T>... delegates) {
        this.delegates = delegates.clone();
    }

    protected AbstractCompositePredicate(List<Predicate<? super T>> delegates) {
        @SuppressWarnings("unchecked")
        final Predicate<T>[] tmp = (Predicate<T>[]) new Object[delegates.size()];
        this.delegates = tmp;
    }
}