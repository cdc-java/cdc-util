package cdc.util.function;

import cdc.util.strings.StringUtils;

/**
 * Implementation of {@link CharPredicate} based on boolean array.
 * <p>
 * This predicate returns the belonging of a character to a set.
 * <p>
 * This is the fastest implementation, but the worst memory efficient.
 *
 * @author Damien Carbonne
 */
public final class BooleanArrayCharPredicate implements CharPredicate {
    private final boolean[] mask;
    private final int maskLength;

    public BooleanArrayCharPredicate(String chars) {
        mask = build(chars);
        maskLength = mask.length;
    }

    private static boolean[] build(String chars) {
        final int max = StringUtils.maxChar(chars);
        final boolean[] tmp = new boolean[max + 1];
        for (int index = 0; index < chars.length(); index++) {
            tmp[chars.charAt(index)] = true;
        }
        return tmp;
    }

    @Override
    public boolean test(char value) {
        return value < maskLength ? mask[value] : false;
    }
}