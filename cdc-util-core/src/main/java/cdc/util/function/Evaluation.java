package cdc.util.function;

/**
 * Enumeration of possible evaluation results (CONTINUE, PRUNE).
 * This is used to control traversal termination.
 *
 * @author Damien Carbonne
 *
 */
public enum Evaluation {
    /** Traversal must continue. */
    CONTINUE,

    /** Traversal must stop. */
    PRUNE;

    public boolean isContinue() {
        return this == CONTINUE;
    }

    public boolean isPrune() {
        return this == PRUNE;
    }

    public static Evaluation toEvaluation(boolean value) {
        return value ? CONTINUE : PRUNE;
    }
}