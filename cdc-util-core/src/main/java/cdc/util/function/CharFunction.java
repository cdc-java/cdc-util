package cdc.util.function;

import java.util.function.Function;

/**
 * Represents a function that accepts a char-valued argument and produces a
 * result. This is the {@code char}-consuming primitive specialization for
 * {@link Function}.
 *
 * @param <R> The type of the result of the function.
 */
@FunctionalInterface
public interface CharFunction<R> {
    /**
     * Applies this function to the given argument.
     *
     * @param value The function argument.
     * @return The function result.
     */
    public R apply(char value);
}