package cdc.util.function;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Consumers<T> {
    private final List<Consumer<T>> consumers = new ArrayList<>();

    public void add(Consumer<T> consumer) {
        consumers.add(consumer);
    }

    public void remove(Consumer<T> consumer) {
        consumers.remove(consumers.lastIndexOf(consumer));
    }

    public void accept(T e) {
        for (final Consumer<T> consumer : consumers) {
            consumer.accept(e);
        }
    }
}