package cdc.util.function;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Iterator (of target type) built over an iterator (of source type) and a
 * predicate (of target type).
 *
 * @author Damien Carbonne
 *
 * @param <S> Type of the source elements.
 * @param <T> Type of the target elements returned by the iterator.
 */
public class ConvertingFilteredIterator2<S, T> implements Iterator<T> {
    private final Function<S, T> converter;
    private final Iterator<? extends S> delegate;
    private final Predicate<? super T> predicate;

    /**
     * Next element.
     * Meaningful when hasNext is true.
     */
    private T next = null;

    private boolean hasNext = false;

    public ConvertingFilteredIterator2(Function<S, T> converter,
                                       Iterator<? extends S> delegate,
                                       Predicate<? super T> predicate) {
        Checks.isNotNull(converter, "converter");
        Checks.isNotNull(delegate, "delegate");
        Checks.isNotNull(predicate, "predicate");

        this.converter = converter;
        this.delegate = delegate;
        this.predicate = predicate;
        advance();
    }

    public ConvertingFilteredIterator2(Function<S, T> converter,
                                       Iterator<? extends S> delegate) {
        this(converter, delegate, Predicates.alwaysTrue());
    }

    private void advance() {
        hasNext = false;
        while (delegate.hasNext() && !hasNext) {
            final S object = delegate.next();
            if (object == null) {
                next = converter.apply(object);
                hasNext = predicate.test(next);
            }
        }
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public T next() {
        if (hasNext) {
            final T result = next;
            advance();
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void remove() {
        delegate.remove();
    }
}