package cdc.util.function;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * Implementation of {@link BiFunction} that caches results.
 * <p>
 * All computations are cached and never released, till {@link #clear()} is called.
 *
 * @param <T> The type of the first argument to the function.
 * @param <U> The type of the second argument to the function.
 * @param <R> The type of the result of the function.
 */
public final class BiFunctionCache<T, U, R> implements BiFunction<T, U, R> {
    private final BiFunction<T, U, R> delegate;
    private final Map<Key<T, U>, R> cache = new HashMap<>();

    public record Key<T, U>(T t,
                            U u) {
    }

    private BiFunctionCache(BiFunction<T, U, R> delegate) {
        this.delegate = delegate;
    }

    /**
     * Creates a cached version of a function.
     *
     * @param <T> The type of the first argument to the function.
     * @param <U> The type of the second argument to the function.
     * @param <R> The type of the result of the function.
     * @param function The function.
     * @return A cached version of {@code function}.
     */
    public static <T, U, R> BiFunctionCache<T, U, R> of(BiFunction<T, U, R> function) {
        return new BiFunctionCache<>(function);
    }

    @Override
    public R apply(T t,
                   U u) {
        final Key<T, U> key = new Key<>(t, u);
        return cache.computeIfAbsent(key, k -> delegate.apply(k.t, k.u));
    }

    /**
     * Clears this cache.
     */
    public void clear() {
        cache.clear();
    }

    /**
     * @return A set of caches keys.
     */
    public Set<Key<T, U>> getKeys() {
        return cache.keySet();
    }
}