package cdc.util.function;

/**
 * Interface implemented by classes that need to be notified when an item is
 * visited.
 *
 * @author Damien Carbonne
 *
 * @param <T> Class of the visited item.
 */
@FunctionalInterface
public interface Visitor<T> {
    /**
     * Invoked when an item is visited.
     *
     * @param item The visited item.
     */
    public void visit(T item);

    /**
     * @param <U> The visited item class.
     * @return A Visitor that does nothing.
     */
    public static <U> Visitor<U> ignore() {
        return n -> {
            //Ignore
        };
    }
}