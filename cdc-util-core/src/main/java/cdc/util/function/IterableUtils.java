package cdc.util.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Iterable utilities.
 *
 * @author Damien Carbonne
 *
 */
public final class IterableUtils {
    private IterableUtils() {
    }

    /**
     * Returns the size of an Iterable.
     *
     * @param iterable The Iterable.
     * @return The size of {@code iterable}.
     */
    public static int size(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>) iterable).size();
        } else {
            int size = 0;
            for (final Iterator<?> iterator = iterable.iterator(); iterator.hasNext();) {
                iterator.next();
                size++;
            }
            return size;
        }
    }

    /**
     * Returns true when an Iterable is empty.
     *
     * @param iterable The Iterable.
     * @return {@code true} when {@code iterable} is empty.
     */
    public static boolean isEmpty(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>) iterable).isEmpty();
        } else {
            return !iterable.iterator().hasNext();
        }
    }

    /**
     * Returns the first element of an Iterable, or {@code null}.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @return The first element of {@code iterable}, if {@code iterable} is not empty, or {@code null}.
     */
    public static <E> E getFirstOrNull(Iterable<? extends E> iterable) {
        final Iterator<? extends E> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            return null;
        }
    }

    /**
     * Returns the last element of an Iterable, or {@code null}.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @return The last element of {@code iterable}, if {@code iterable} is not empty, or {@code null}.
     */
    public static <E> E getLastOrNull(Iterable<? extends E> iterable) {
        if (iterable instanceof List) {
            final List<? extends E> list = (List<? extends E>) iterable;
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(list.size() - 1);
            }
        } else {
            final Iterator<? extends E> iterator = iterable.iterator();
            E result = null;
            while (iterator.hasNext()) {
                result = iterator.next();
            }
            return result;
        }
    }

    /**
     * Returns the element of an Iterable at a given index or {@code null}.
     * <p>
     * {@code null} is returned when index is not valid.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @param index The index.
     * @return The element at {@code index} in {@code iterable} or {@code null}.
     */
    public static <E> E getAtOrNull(Iterable<? extends E> iterable,
                                    int index) {
        if (iterable instanceof List) {
            final List<? extends E> list = (List<? extends E>) iterable;
            if (index >= 0 && index < list.size()) {
                return list.get(index);
            } else {
                return null;
            }
        } else {
            final Iterator<? extends E> iterator = iterable.iterator();
            E result = null;
            int count = 0;
            while (iterator.hasNext()) {
                result = iterator.next();
                if (count == index) {
                    return result;
                } else {
                    count++;
                }
            }
            return null;
        }
    }

    /**
     * Returns the index of the first matching element or -1.
     * <p>
     * Matching is checked with {@code ==}.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @param element The searched element.
     * @return The index of the first matching element in {@code iterable}.
     */
    public static <E> int indexOf(Iterable<? extends E> iterable,
                                  E element) {
        int index = 0;
        for (final Iterator<? extends E> iterator = iterable.iterator(); iterator.hasNext();) {
            final E next = iterator.next();
            if (next == element) {
                return index;
            }
            index++;
        }
        return -1;
    }

    /**
     * Add all elements contained in a Iterable to a collection.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @param collection The collection.
     */
    public static <E> void addAll(Iterable<? extends E> iterable,
                                  Collection<E> collection) {
        for (final E next : iterable) {
            collection.add(next);
        }
    }

    public static <E> Collection<E> toCollection(Iterable<? extends E> iterable) {
        if (iterable instanceof Collection) {
            @SuppressWarnings("unchecked")
            final Collection<E> result = (Collection<E>) iterable;
            return result;
        } else {
            final List<E> result = new ArrayList<>();
            addAll(iterable, result);
            return result;
        }
    }

    /**
     * Converts the content of an Iterable to a list.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @return A list containing elements of {@code iterable}.
     */
    public static <E> List<E> toList(Iterable<? extends E> iterable) {
        if (iterable instanceof List) {
            @SuppressWarnings("unchecked")
            final List<E> result = (List<E>) iterable;
            return result;
        } else {
            final List<E> result = new ArrayList<>();
            addAll(iterable, result);
            return result;
        }
    }

    public static <E> List<E> toUnmodifiableList(Iterable<? extends E> iterable) {
        return Collections.unmodifiableList(toList(iterable));
    }

    public static <E> List<E> toUnmodifiableList(Iterable<? extends E> iterable,
                                                 Predicate<? super E> predicate) {
        return Collections.unmodifiableList(toList(filter(iterable, predicate)));
    }

    /**
     * Converts the content of an Iterable to a sorted list.
     * <p>
     * Elements must be comparable.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @return A sorted list containing elements of {@code iterable}.
     */
    public static <E extends Comparable<? super E>> List<E> toSortedList(Iterable<? extends E> iterable) {
        final List<E> result = new ArrayList<>();
        addAll(iterable, result);
        Collections.sort(result);
        return result;
    }

    /**
     * Converts the content of an Iterable to a sorted list.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @param comparator The comparator to use for sorting.
     * @return A sorted list containing elements of {@code iterable}.
     */
    public static <E> List<E> toSortedList(Iterable<? extends E> iterable,
                                           Comparator<? super E> comparator) {
        final List<E> result = new ArrayList<>();
        addAll(iterable, result);
        Collections.sort(result, comparator);
        return result;
    }

    /**
     * Converts the content of an Iterable to a set.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @return A set containing elements of {@code iterable}.
     */
    public static <E> Set<E> toSet(Iterable<? extends E> iterable) {
        if (iterable instanceof Set) {
            @SuppressWarnings("unchecked")
            final Set<E> result = (Set<E>) iterable;
            return result;
        } else {
            final Set<E> result = new HashSet<>();
            addAll(iterable, result);
            return result;
        }
    }

    public static <E> Set<E> toUnmodifiableSet(Iterable<? extends E> iterable) {
        return Collections.unmodifiableSet(toSet(iterable));
    }

    public static <E> Set<E> toUnmodifiableSet(Iterable<? extends E> iterable,
                                               Predicate<? super E> predicate) {
        return Collections.unmodifiableSet(toSet(filter(iterable, predicate)));
    }

    /**
     * Returns a filtered view of an Iterable.
     *
     * @param <E> The type of elements returned by the iterator.
     * @param iterable The Iterable.
     * @param predicate The predicate to use for filtering.
     * @return An Iterable that contains elements of {@code iterable} and are accepted by {@code predicate}.
     */
    public static <E> Iterable<E> filter(Iterable<? extends E> iterable,
                                         Predicate<? super E> predicate) {
        return new FilteredIterable<>(iterable, predicate);
    }

    /**
     * Returns a converted view of an Iterable.
     *
     * @param <T> The type of elements returned by returned Iterable.
     * @param targetClass The class of elements returned by returned Iterable.
     * @param iterable The Iterable.
     * @return An converted view of {@code iterable}.
     */
    public static <T> Iterable<T> convert(Class<T> targetClass,
                                          Iterable<?> iterable) {
        return new ConvertingFilteredIterable<>(targetClass, iterable);
    }

    public static <S, T> Iterable<T> convert(Function<S, T> converter,
                                             Iterable<? extends S> iterable) {
        return new ConvertingFilteredIterable2<>(converter, iterable);
    }

    /**
     * Returns a converted and filtered view of an Iterable.
     *
     * @param <T> The type of elements returned by returned Iterable.
     * @param targetClass The class of elements returned by returned Iterable.
     * @param iterable The Iterable.
     * @param predicate The predicate to use for filtering.
     * @return An converted and filtered view of {@code iterable}.
     */
    public static <T> Iterable<T> filterAndConvert(Class<T> targetClass,
                                                   Iterable<?> iterable,
                                                   Predicate<? super T> predicate) {
        return new ConvertingFilteredIterable<>(targetClass, iterable, predicate);
    }

    public static <T> Iterable<T> filterAndConvert(Class<T> targetClass,
                                                   Iterable<?> iterable) {
        return filterAndConvert(targetClass, iterable, Predicates.alwaysTrue());
    }

    public static <S, T> Iterable<T> filterAndConvert(Function<S, T> converter,
                                                      Iterable<? extends S> iterable,
                                                      Predicate<? super T> predicate) {
        return new ConvertingFilteredIterable2<>(converter, iterable, predicate);
    }

    public static <S, T> Iterable<T> filterAndConvert(Function<S, T> converter,
                                                      Iterable<? extends S> iterable) {
        return filterAndConvert(converter, iterable, Predicates.alwaysTrue());
    }

    public static <T> Iterable<T> join(Iterable<T> iterable1,
                                       Iterable<T> iterable2) {
        return new MergedIterable<>(iterable1, iterable2);
    }

    public static <T> Iterable<T> join(Iterable<T> iterable,
                                       T value) {
        return join(iterable, Arrays.asList(value));
    }
}