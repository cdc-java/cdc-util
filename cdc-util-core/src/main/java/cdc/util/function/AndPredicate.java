package cdc.util.function;

import java.util.List;
import java.util.function.Predicate;

public final class AndPredicate<T> extends AbstractCompositePredicate<T> {

    @SafeVarargs
    public AndPredicate(Predicate<? super T>... delegates) {
        super(delegates);
    }

    public AndPredicate(final List<Predicate<? super T>> delegates) {
        super(delegates);
    }

    @Override
    public boolean test(T input) {
        boolean result = true;
        for (final Predicate<? super T> delegate : delegates) {
            result = result && delegate.test(input);
        }
        return result;
    }
}