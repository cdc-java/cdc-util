package cdc.util.function;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Iterator (of target type) built over an iterator (of source type) and a
 * predicate (of target type).
 * <p>
 * Only source objects that are null or match the target class are kept and converted to target class.<br>
 * If null objects should be ignored, an appropriate predicate should be used.
 *
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of the target elements returned by the iterator.
 */
public class ConvertingFilteredIterator<T> implements Iterator<T> {
    private final Class<T> targetClass;
    private final Iterator<?> delegate;
    private final Predicate<? super T> predicate;

    /**
     * Next element.
     * Meaningful when hasNext is true.
     */
    private T next = null;

    private boolean hasNext = false;

    public ConvertingFilteredIterator(Class<T> targetClass,
                                      Iterator<?> delegate,
                                      Predicate<? super T> predicate) {
        Checks.isNotNull(targetClass, "targetClass");
        Checks.isNotNull(delegate, "delegate");
        Checks.isNotNull(predicate, "predicate");

        this.targetClass = targetClass;
        this.delegate = delegate;
        this.predicate = predicate;
        advance();
    }

    public ConvertingFilteredIterator(Class<T> targetClass,
                                      Iterator<?> delegate) {
        this(targetClass, delegate, Predicates.alwaysTrue());
    }

    private void advance() {
        hasNext = false;
        while (delegate.hasNext() && !hasNext) {
            final Object object = delegate.next();
            if (object == null || targetClass.isInstance(object)) {
                next = targetClass.cast(object);
                hasNext = predicate.test(next);
            }
        }
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public T next() {
        if (hasNext) {
            final T result = next;
            advance();
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void remove() {
        delegate.remove();
    }
}