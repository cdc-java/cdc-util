package cdc.util.function;

import java.util.Iterator;
import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Iterable built over another Iterable and a Predicate.
 *
 * @author Damien Carbonne
 *
 * @param <E> Type of the element returned by the iterator.
 */
public class FilteredIterable<E> implements Iterable<E> {
    private final Iterable<? extends E> delegate;
    private final Predicate<? super E> predicate;

    public FilteredIterable(Iterable<? extends E> delegate,
                            Predicate<? super E> predicate) {
        Checks.isNotNull(delegate, "delegate");
        Checks.isNotNull(predicate, "predicate");

        this.delegate = delegate;
        this.predicate = predicate;
    }

    @Override
    public Iterator<E> iterator() {
        return new FilteredIterator<>(delegate.iterator(), predicate);
    }
}