package cdc.util.function;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MergedIterator<E> implements Iterator<E> {
    private final Iterator<E> iterator1;
    private final Iterator<E> iterator2;

    public MergedIterator(Iterator<E> iterator1,
                          Iterator<E> iterator2) {
        this.iterator1 = iterator1;
        this.iterator2 = iterator2;
    }

    @Override
    public boolean hasNext() {
        return iterator1.hasNext() || iterator2.hasNext();
    }

    @Override
    public E next() {
        if (iterator1.hasNext()) {
            return iterator1.next();
        } else if (iterator2.hasNext()) {
            return iterator2.next();
        } else {
            throw new NoSuchElementException();
        }
    }
}