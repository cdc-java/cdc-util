package cdc.util.function;

/**
 * Interface implement by classes that can control termination of a visit.
 * <p>
 * This is used in conjunction with Visitor.<br>
 * Visit and termination must be handled separately.
 *
 * @author Damien Carbonne
 *
 * @param <T> The item type.
 */
@FunctionalInterface
public interface Evaluator<T> {
    /**
     * Evaluates situation on an item.
     * <p>
     * If result is {@link Evaluation#CONTINUE}, then traversal must continue.<br>
     * If result is {@link Evaluation#PRUNE}, then traversal must be stopped.
     *
     * @param item The item for which an evaluation must be done.
     * @return The evaluation result.
     */
    public Evaluation evaluate(T item);

    /**
     * Creates an Evaluator that always returns {@link Evaluation#CONTINUE}.
     *
     * @param <T> The Evaluator item type.
     * @return A new Evaluator that always returns {@link Evaluation#CONTINUE}.
     */
    public static <T> Evaluator<T> continueTraversal() {
        return i -> Evaluation.CONTINUE;
    }

    /**
     * Creates an Evaluator that always returns {@link Evaluation#PRUNE}.
     *
     * @param <T> The Evaluator item type.
     * @return A new Evaluator that always returns {@link Evaluation#PRUNE}.
     */
    public static <T> Evaluator<T> stopTraversal() {
        return i -> Evaluation.PRUNE;
    }

    /**
     * Returns {@code true} if an Evaluator is {@code null} or
     * evaluates an item to {@link Evaluation#CONTINUE}.
     *
     * @param <T> The Evaluator item type.
     * @param evaluator The Evaluator.
     * @param item The item.
     * @return {@code true} if {@code evaluator} is {@code null} or
     *         evaluates {@code item} to {@link Evaluation#CONTINUE}.
     */
    public static <T> boolean continueTraversal(Evaluator<T> evaluator,
                                                T item) {
        return evaluator == null || evaluator.evaluate(item) == Evaluation.CONTINUE;
    }
}