package cdc.util.function;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public final class Predicates {
    private Predicates() {
    }

    /**
     * Returns a Predicate that returns {@code true} when input value is not null.
     *
     * @param <T> The type of the input of the predicate.
     * @return A Predicate that returns {@code true} when input value is not null.
     */
    public static <T> Predicate<T> isNotNull() {
        return Objects::nonNull;
    }

    /**
     * Returns a Predicate that returns {@code true} when input value is null.
     *
     * @param <T> The type of the input of the predicate.
     * @return A Predicate that returns {@code true} when input value is null.
     */
    public static <T> Predicate<T> isNull() {
        return Objects::isNull;
    }

    /**
     * Returns a Predicate that always returns {@code true}.
     *
     * @param <T> The type of the input of the predicate.
     * @return A Predicate that always returns {@code true}.
     */
    public static <T> Predicate<T> alwaysTrue() {
        return o -> true;
    }

    /**
     * Returns a Predicate that always returns {@code false}.
     *
     * @param <T> The type of the input of the predicate.
     * @return A Predicate that always returns {@code false}.
     */
    public static <T> Predicate<T> alwaysFalse() {
        return o -> false;
    }

    /**
     * Returns a Predicate that returns {@code true} when input value is an instance of a target class.
     * <p>
     * If input value is null, returns {@code false}.
     *
     * @param <T> The type of the input of the predicate.
     * @param targetClass The target class.
     * @return A Predicate that returns {@code true} when input value is an instance of {@code targetClass}.
     */
    public static <T> Predicate<T> isInstanceOf(Class<? extends T> targetClass) {
        return targetClass::isInstance;
    }

    @SafeVarargs
    public static <T> Predicate<T> and(Predicate<? super T>... delegates) {
        return new AndPredicate<>(delegates);
    }

    public static <T> Predicate<T> and(List<Predicate<? super T>> delegates) {
        return new AndPredicate<>(delegates);
    }

    @SafeVarargs
    public static <T> Predicate<T> or(Predicate<? super T>... delegates) {
        return new OrPredicate<>(delegates);
    }

    public static <T> Predicate<T> or(List<Predicate<? super T>> delegates) {
        return new OrPredicate<>(delegates);
    }
}