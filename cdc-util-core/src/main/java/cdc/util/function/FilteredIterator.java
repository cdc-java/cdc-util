package cdc.util.function;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Iterator built over another Iterator and a Predicate.
 *
 * @author Damien Carbonne
 *
 * @param <E> Type if the element returned by the iterator.
 */
public class FilteredIterator<E> implements Iterator<E> {
    private final Iterator<? extends E> delegate;
    private final Predicate<? super E> predicate;
    private E next = null;
    private boolean hasNext = false;

    public FilteredIterator(Iterator<? extends E> delegate,
                            Predicate<? super E> predicate) {
        Checks.isNotNull(delegate, "delegate");
        Checks.isNotNull(predicate, "predicate");

        this.delegate = delegate;
        this.predicate = predicate;
        advance();
    }

    private void advance() {
        hasNext = false;
        while (delegate.hasNext() && !hasNext) {
            next = delegate.next();
            hasNext = predicate == null || predicate.test(next);
        }
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public E next() {
        if (hasNext) {
            final E result = next;
            advance();
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void remove() {
        delegate.remove();
    }
}