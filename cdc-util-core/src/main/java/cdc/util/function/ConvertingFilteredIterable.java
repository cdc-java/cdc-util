package cdc.util.function;

import java.util.Iterator;
import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Iterable (of target type) built over an Iterable (of source type) and a
 * predicate (of target type).
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of the target elements returned by the iterator.
 */
public class ConvertingFilteredIterable<T> implements Iterable<T> {
    private final Class<T> targetClass;
    private final Iterable<?> delegate;
    private final Predicate<? super T> predicate;

    /**
     * Creates a ConvertingFilteredIterable using a filter.
     *
     * @param targetClass The target class.
     * @param delegate The delegate iterable.
     * @param predicate The filtering predicate.
     */
    public ConvertingFilteredIterable(Class<T> targetClass,
                                      Iterable<?> delegate,
                                      Predicate<? super T> predicate) {
        Checks.isNotNull(targetClass, "targetClass");
        Checks.isNotNull(delegate, "delegate");
        Checks.isNotNull(predicate, "predicate");

        this.targetClass = targetClass;
        this.delegate = delegate;
        this.predicate = predicate;
    }

    /**
     * Creates a ConvertingFilteredIterable without filter.
     *
     * @param targetClass The target class.
     * @param delegate The delegate iterable.
     */
    public ConvertingFilteredIterable(Class<T> targetClass,
                                      Iterable<?> delegate) {
        this(targetClass, delegate, Predicates.alwaysTrue());
    }

    @Override
    public Iterator<T> iterator() {
        return new ConvertingFilteredIterator<>(targetClass, delegate.iterator(), predicate);
    }
}