package cdc.util.function;

import java.util.Iterator;

public class MergedIterable<E> implements Iterable<E> {
    private final Iterable<E> iterable1;
    private final Iterable<E> iterable2;

    public MergedIterable(Iterable<E> iterable1,
                          Iterable<E> iterable2) {
        this.iterable1 = iterable1;
        this.iterable2 = iterable2;
    }

    @Override
    public Iterator<E> iterator() {
        return new MergedIterator<>(iterable1.iterator(), iterable2.iterator());
    }
}