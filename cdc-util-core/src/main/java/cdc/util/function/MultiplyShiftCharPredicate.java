package cdc.util.function;

/**
 * Implementation of {@link CharPredicate} based on a table, and basic operations.
 * <p>
 * This predicate returns the belonging of a character to a set.
 * <p>
 * This is a good tradeoff between speed an memory efficiency.<br>
 * One needs to carefully compute parameters using <em>MultiplyShiftHashSearcher</em>.
 *
 * @author Damien Carbonne
 * @see <a href=
 *      "https://gitlab.com/cdc-java/cdc-office/-/blob/master/cdc-office-tools/src/main/java/cdc/office/tools/MultiplyShiftHashSearcher.java">MultiplyShiftHashSearcher</a>
 */
public class MultiplyShiftCharPredicate implements CharPredicate {
    private final char[] chars;
    private final int multiply;
    private final int shift;

    public MultiplyShiftCharPredicate(String table,
                                      int multiply,
                                      int shift) {
        this.chars = table.toCharArray();
        this.multiply = multiply;
        this.shift = shift;
    }

    @Override
    public boolean test(char value) {
        return chars[(value * multiply) >>> shift] == value;
    }
}