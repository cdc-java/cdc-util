package cdc.util.function;

import java.util.Map;
import java.util.function.Function;

public final class FunctionUtils {
    private FunctionUtils() {
    }

    public static <T, R> Function<T, R> fromMap(Map<T, Function<T, R>> map) {
        return t -> {
            final Function<T, R> function = map.get(t);
            return function == null ? null : function.apply(t);
        };
    }
}