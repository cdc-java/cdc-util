package cdc.util.stats;

import cdc.util.lang.Checks;

/**
 * Utility class used to gather double measures and build elementary statistics
 * (min, max, mean, ...) on those values.
 * <p>
 * Values are not kept, and errors may accumulate.<br>
 * This should only be used for toys code, 'small' values and a limited number
 * of values.
 *
 * @author Damien Carbonne
 *
 */
public final class RunningStats {
    /** Number of measures. */
    private int count = 0;
    /** Minimum value. */
    private double min;
    /** Maximum value. */
    private double max;
    /** Values mean. */
    private double mk;
    /** Sum of square of differences of values to values mean. */
    private double sk;

    public RunningStats() {
        clear();
    }

    private void checkNonEmpty() {
        Checks.assertTrue(count > 0, "Empty stats");
    }

    public void clear() {
        count = 0;
    }

    /**
     * Add a new value.
     *
     * @param value The value to add.
     */
    public void add(double value) {
        count++;
        if (count == 1) {
            min = value;
            max = value;
            mk = value;
            sk = 0.0;
        } else {
            min = Math.min(min, value);
            max = Math.max(max, value);

            final double delta = value - mk;
            mk += delta / count;
            sk += delta * (value - mk);
        }
    }

    /**
     * @return The number of measures.
     */
    public int size() {
        return count;
    }

    /**
     * @return The minimum value.
     */
    public double getMin() {
        checkNonEmpty();

        return min;
    }

    /**
     * @return The maximum value.
     */
    public double getMax() {
        checkNonEmpty();

        return max;
    }

    /**
     * @return The mean of values.
     */
    public double getMean() {
        checkNonEmpty();

        return mk;
    }

    /**
     * @return The unbiased variance.
     */
    public double getVariance() {
        checkNonEmpty();

        if (count == 1) {
            return 0.0;
        } else {
            return sk / (count - 1);
        }
    }

    public double getStandardDeviation() {
        return Math.sqrt(getVariance());
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(size() + " sample(s)");
        if (size() == 0) {
            builder.append(" min:?");
            builder.append(" max:?");
            builder.append(" mean:?");
            builder.append(" stddev:?");
        } else {
            builder.append(" min:" + getMin());
            builder.append(" max:" + getMax());
            builder.append(" mean:" + getMean());
            builder.append(" stddev:" + getStandardDeviation());
        }

        return builder.toString();
    }
}