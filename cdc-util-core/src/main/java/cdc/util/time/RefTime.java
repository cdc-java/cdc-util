package cdc.util.time;

import java.lang.management.ManagementFactory;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import cdc.util.lang.UnexpectedValueException;

/**
 * Reference time for an application. This can be used to measure elapsed time
 * since the beginning of the execution of an application.
 *
 * @author Damien Carbonne
 *
 */
public final class RefTime {
    /** Time since the VM started. */
    public static final long SHIFT_MILLIS = ManagementFactory.getRuntimeMXBean().getUptime();

    /** Current nanos. */
    private static final long NOW_NANOS = System.nanoTime();

    /**
     * Current Instant. Considered as approximately corresponding to NOW_NANOS. It
     * should in fact be a bit late, but by a small amount.
     */
    private static final Instant NOW_INSTANT = Instant.now();

    /**
     * Reference nano time. Can only be used by comparing it to
     * System.nanoTime().
     */
    public static final long REF_NANOS = NOW_NANOS - 1000000 * SHIFT_MILLIS;

    /**
     * Approximate Instant corresponding to REF_NANOS.
     */
    public static final Instant REF_INSTANT = NOW_INSTANT.plusMillis(SHIFT_MILLIS);

    /**
     * Approximate date corresponding to REF_NANOS.
     */
    public static final Date REF_DATE = Date.from(NOW_INSTANT);

    public static final LocalDateTime REF_LOCAL_DATE_TIME = LocalDateTime.ofInstant(NOW_INSTANT, ZoneId.systemDefault());

    private RefTime() {
    }

    /**
     * Should be invoked as early as possible in the main.
     */
    public static void elaborate() {
        // Ignore
    }

    /**
     * Return the approximated up time in nano seconds.
     *
     * @return the approximated up time in nano seconds.
     */
    public static long getUptimeNanos() {
        return System.nanoTime() - REF_NANOS;
    }

    public static LocalDateTime nanosToLocalDataTime(long nanos) {
        return REF_LOCAL_DATE_TIME.plus(nanos - REF_NANOS, ChronoUnit.NANOS);
    }

    public static LocalDateTime uptimeNanosToLocalDataTime(long nanos) {
        return REF_LOCAL_DATE_TIME.plus(nanos, ChronoUnit.NANOS);
    }

    public static String nanosToString(long nanos) {
        final long micros = (nanos / 1000) % 1000;
        final long millis = (nanos / 1000000) % 1000;
        final long secs = (nanos / 1000000000);

        if (secs != 0) {
            return String.format("%ds %03dms", secs, millis);
        } else if (millis == 0) {
            final long inanos = nanos % 1000;
            return String.format("%d\u00B5s %03dns", micros, inanos);
        } else {
            return String.format("%dms %03d\u00B5s", millis, micros);
        }
    }

    public static String nanosToString(long nanos,
                                       TimeUnit unit) {
        final long inanos = nanos % 1000;
        final long imicros = (nanos / 1000) % 1000;
        final long imillis = (nanos / 1000000) % 1000;
        final long secs = (nanos / 1000000000);
        final long isecs = secs % 60;
        final long mins = secs / 60;
        final long imins = mins % 60;
        final long hours = mins / 60;

        switch (unit) {
        case HOUR:
        case MINUTE:
            return String.format("%02d:%02d", hours, imins);
        case SECOND:
            return String.format("%02d:%02d:%02d", hours, imins, isecs);
        case MILLI:
            return String.format("%02d:%02d:%02d.%03d", hours, imins, isecs, imillis);
        case MICRO:
            return String.format("%02d:%02d:%02d.%03d %03d", hours, imins, isecs, imillis, imicros);
        case NANO:
        default:
            return String.format("%02d:%02d:%02d.%03d %03d %03d", hours, imins, isecs, imillis, imicros, inanos);
        }
    }

    public static double nanosToDouble(long nanos,
                                       TimeUnit unit) {
        switch (unit) {
        case NANO:
            return nanos;
        case MICRO:
            return nanos / 1.0e3;
        case MILLI:
            return nanos / 1.0e6;
        case SECOND:
            return nanos / 1.0e9;
        case MINUTE:
            return nanos / 60.0e9;
        case HOUR:
            return nanos / 3600.0e9;
        default:
            throw new UnexpectedValueException(unit);
        }
    }
}