package cdc.util.time;

public enum TimeMeasureMode {
    ABSOLUTE,
    RELATIVE
}