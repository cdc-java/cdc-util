package cdc.util.time;

public enum TimeUnit {
    NANO,
    MICRO,
    MILLI,
    SECOND,
    MINUTE,
    HOUR
}