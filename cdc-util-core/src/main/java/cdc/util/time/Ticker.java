package cdc.util.time;

/**
 * Utility used to measure time between ticks.
 *
 * @author Damien Carbonne
 *
 */
public class Ticker {
    private int count = 0;
    private long lastTime = 0;
    private long lastPeriod = 0;

    public void reset() {
        count = 0;
        lastTime = 0;
        lastPeriod = 0;
    }

    public void tick() {
        final long currentTime = System.nanoTime();
        count++;
        if (count > 1) {
            lastPeriod = currentTime - lastTime;
        }
        lastTime = currentTime;
    }

    /**
     * @return The number of times tick() was called since creation or last reset.
     */
    public int getTickCount() {
        return count;
    }

    /**
     * Returns the time that elapsed between previous ticks.
     * <p>
     * At least 2 call to ticks() must have been done to have a significant result.
     * If this is not the case, returns 0.
     *
     * @return The time elapsed between previous ticks.
     */
    public long getLastPeriod() {
        return lastPeriod;
    }
}