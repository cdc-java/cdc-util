package cdc.util.time;

import org.apache.logging.log4j.Logger;

/**
 * Class used to measure times.
 * <p>
 * Usually named Stopwatch.
 *
 * @author Damien Carbonne
 *
 */
public final class Chronometer {
    /**
     * Elapsed time during previous active sessions.
     */
    private long totalNanos = 0;

    /**
     * Start time of last active session.
     */
    private long previousStartNanos = 0;

    private boolean active = false;

    /**
     * Creates a suspended (non active) Chronometer.
     */
    public Chronometer() {
        super();
    }

    /**
     * Creates a Chronometer and set its initial activity.
     *
     * @param active If true, the created chronometer is started.
     */
    public Chronometer(boolean active) {
        if (active) {
            start();
        }
    }

    public boolean isActive() {
        return active;
    }

    /**
     * Starts this chronometer.
     * <p>
     * Elapsed time is reset.
     */
    public void start() {
        previousStartNanos = System.nanoTime();
        totalNanos = 0;
        active = true;
    }

    /**
     * Suspends this chronometer (if active).
     * <p>
     * If this chronometer is inactive, will do nothing.
     */
    public void suspend() {
        if (active) {
            totalNanos += System.nanoTime() - previousStartNanos;
            active = false;
        }
    }

    /**
     * Resumes this chronometer (if inactive).
     * <p>
     * If this chronometer is active, will do nothing.
     */
    public void resume() {
        if (!active) {
            previousStartNanos = System.nanoTime();
            active = true;
        }
    }

    /**
     * Resets this chronometer.
     * <p>
     * It becomes inactive and its elapsed time counter set to 0.
     */
    public void reset() {
        active = false;
        totalNanos = 0;
        previousStartNanos = 0;
    }

    /**
     * Returns the elapsed time counted by this Chronometer, in nano seconds.
     * <p>
     * This is the sum of elapsed time of past active sessions and elapsed time of current active session, if any.
     *
     * @return The elapsed time counted by this Chronometer.
     */
    public long getElapsedNanos() {
        if (active) {
            return (System.nanoTime() - previousStartNanos) + totalNanos;
        } else {
            return totalNanos;
        }
    }

    public long getElapsedMillis() {
        return getElapsedNanos() / 1000000;
    }

    public double getElapsedSeconds() {
        final long giga = 1000000000L;
        final long nanos = getElapsedNanos();
        final long sec = nanos / giga;
        final long frac = nanos % giga;
        return sec + ((double) frac / (double) giga);
    }

    @Override
    public String toString() {
        return RefTime.nanosToString(getElapsedNanos());
    }

    public static interface Procedure<E extends Exception> {
        public void invoke() throws E;
    }

    public static <E extends Exception> void time(Procedure<E> procedure,
                                                  String before,
                                                  String after,
                                                  Logger logger) throws E {
        logger.info(before);
        final Chronometer chrono = new Chronometer();
        try {
            chrono.start();
            procedure.invoke();
        } finally {
            chrono.suspend();
            logger.info("{} ({})", after, chrono);
        }
    }
}