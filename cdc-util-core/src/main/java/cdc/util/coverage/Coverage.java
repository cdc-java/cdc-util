package cdc.util.coverage;

import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.BlackHole;

public final class Coverage {
    private static final Logger LOGGER = LogManager.getLogger(Coverage.class);

    private Coverage() {
    }

    public static <E extends Enum<E>> boolean enumStandardCoverage(Class<E> enumClass) {
        try {
            for (final E value : enumClass.getEnumConstants()) {
                enumClass.cast(enumClass.getMethod("valueOf", String.class).invoke(null, value.name()));
            }
            return true;
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                | SecurityException e) {
            LOGGER.catching(e);
            return false;
        }
    }

    public static <T> boolean objectBasicCoverage(T object) {
        final T other = object;
        BlackHole.discard(object.hashCode());
        BlackHole.discard(object.toString());
        BlackHole.discard(object.equals("foo"));
        BlackHole.discard(object.equals(other));
        return true;
    }
}