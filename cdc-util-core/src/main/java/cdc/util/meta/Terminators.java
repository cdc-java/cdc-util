package cdc.util.meta;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;
import cdc.util.function.MultiplyShiftCharPredicate;

/**
 * Recognition of line terminator characters.
 *
 * @author Damien Carbonne
 */
final class Terminators {
    /**
     * String containing characters that are considered as line terminators.
     */
    static final String CHARS = ""
            + "\n"     // LINE FEED (U+000A)
            + "\r"     // CARRIAGE RETURN (U+000D)
            + "\u0085" // NEXT LINE
            + "\u2028"; // LINE SEPARATOR

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * '\n' is used as the filling character.<br>
     * <b>WARNING:</b> This is computed and must match {@link #CHARS}.
     */
    static final String STABLE =
            "\n\r\n\n\n\n\n\n\n\u2028\u0085\n\n\n\n\n";

    /**
     * Conversion of {@link #STABLE} as a char array.
     */
    static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    static final int MULTIPLY = 20_648_882;

    /**
     * The shift to use for {@link #TABLE}.
     */
    static final int SHIFT = 28;

    private Terminators() {
    }

    public static final CharPredicate MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharPredicate(STABLE,
                                           MULTIPLY,
                                           SHIFT);

    public static final CharPredicate MULTIPLY_SHIFT_INLINE_MATCHER =
            c -> TABLE[(c * MULTIPLY) >>> SHIFT] == c;

    public static final CharPredicate BIT_SET_MATCHER =
            new BitSetCharPredicate(CHARS);

    public static final CharPredicate BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharPredicate(CHARS);

    /**
     * Matcher that must be used to identify one character tokens.
     */
    public static final CharPredicate BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}