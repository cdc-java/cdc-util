package cdc.util.meta;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.util.lang.Checks;

/**
 * Simple implementation of MetaData.
 *
 * @author Damien Carbonne
 *
 */
public class BasicMetaData implements MetaData {
    private final Map<String, String> map = new HashMap<>();
    private boolean locked = false;

    private void checkNotLocked() {
        if (locked) {
            throw new IllegalStateException("Locked");
        }
    }

    public void lock() {
        this.locked = true;
    }

    public boolean isLocked() {
        return locked;
    }

    @Override
    public void clear() {
        checkNotLocked();
        map.clear();
    }

    @Override
    public void put(String key,
                    String value) {
        Checks.isNotNullOrEmpty(key, "key");
        checkNotLocked();
        map.put(key, value);
    }

    @Override
    public void putAll(MetaData other) {
        Checks.isNotNull(other, "other");
        checkNotLocked();
        for (final String key : other.getKeys()) {
            put(key, other.get(key));
        }
    }

    @Override
    public void remove(String key) {
        checkNotLocked();
        map.remove(key);
    }

    @Override
    public Set<String> getKeys() {
        return map.keySet();
    }

    @Override
    public Set<String> getKeysSet() {
        return getKeys();
    }

    @Override
    public boolean hasKey(String key) {
        return map.containsKey(key);
    }

    @Override
    public String get(String key) {
        return map.get(key);
    }

    @Override
    public String get(String key,
                      String def) {
        return map.getOrDefault(key, def);
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof BasicMetaData)) {
            return false;
        }
        final BasicMetaData other = (BasicMetaData) object;
        return map.equals(other.map);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        boolean first = true;
        for (final String key : getKeys()) {
            if (first) {
                first = false;
            } else {
                builder.append(", ");
            }
            builder.append(key);
            builder.append(':');
            builder.append(get(key));
        }
        builder.append(']');
        return builder.toString();
    }
}