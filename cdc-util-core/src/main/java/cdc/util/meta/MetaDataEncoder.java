package cdc.util.meta;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.function.IterableUtils;
import cdc.util.lang.UnexpectedValueException;

/**
 * Encodes or decodes MetaData to/from strings.
 * <p>
 * Entries are separated by on of {@link Terminators#CHARS} characters.<br>
 * Keys and values are separated by the {@link #KEY_VALUE_SEPARATOR} character.<br>
 * Keys and values can be escaped, delimited by the {@link #DELIMITER} character.
 * <p>
 * Escaping is necessary when the string contains a {@link Separators#CHARS} character.
 *
 * @author Damien Carbonne
 */
public final class MetaDataEncoder {
    public static final char KEY_VALUE_SEPARATOR = ':';
    public static final char DELIMITER = '"';

    private MetaDataEncoder() {
    }

    /**
     * @param s The string.
     * @return {@code true} if {@code s} needs to be escaped.
     */
    public static boolean needsEscape(String s) {
        // Never used with null
        for (int index = 0; index < s.length(); index++) {
            if (Separators.BEST_MATCHER.test(s.charAt(index))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Appends a String to a StringBuilder, protecting it if necessary.
     *
     * @param s The string to append.
     * @param builder The builder to fill.
     */
    private static void appendProtected(String s,
                                        StringBuilder builder) {
        if (s == null) {
            // Ignore
        } else if (needsEscape(s)) {
            builder.append(DELIMITER);
            for (int index = 0; index < s.length(); index++) {
                final char c = s.charAt(index);
                if (c == DELIMITER) {
                    builder.append(c);
                }
                builder.append(c);
            }
            builder.append(DELIMITER);
        } else {
            builder.append(s);
        }
    }

    /**
     * Encodes a MetaData to a string.
     *
     * @param meta The MetaData.
     * @return The encoded string.
     */
    public static String encode(MetaData meta) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final String key : IterableUtils.toSortedList(meta.getKeysSet())) {
            if (first) {
                first = false;
            } else {
                builder.append('\n');
            }
            appendProtected(key, builder);
            builder.append(KEY_VALUE_SEPARATOR);
            appendProtected(meta.get(key), builder);
        }
        return builder.toString();
    }

    /**
     * Converts an encoded string to a {@link MetaData}.
     * <p>
     * <b>WARNING:</b> the result may be immutable.
     *
     * @param code The encoded string.
     * @return The corresponding {@link MetaData}.
     * @throws IllegalArgumentException When {@code code } could not be decoded.
     */
    public static MetaData decode(String code) {
        try {
            if (code == null || code.isBlank()) {
                return MetaData.EMPTY;
            } else {
                final BasicMetaData meta = new BasicMetaData();
                final Tokenizer tokenizer = new Tokenizer(code);
                tokenizer.nextToken();
                while (tokenizer.getCurrent() != Tokenizer.Token.EPSILON) {
                    // Skip all entry separators (end of lines)
                    while (tokenizer.getCurrent() == Tokenizer.Token.ENTRY_SEPARATOR) {
                        tokenizer.nextToken();
                    }
                    if (tokenizer.getCurrent() != Tokenizer.Token.EPSILON) {
                        // We should have a key
                        final String key = tokenizer.consumeString();
                        // We should have a key value separator
                        tokenizer.expect(Tokenizer.Token.KEY_VALUE_SEPARATATOR);
                        // We should have a value
                        final String value = tokenizer.consumeString();
                        meta.put(key, value);
                    }
                }
                return meta;
            }
        } catch (final RuntimeException e) {
            throw new IllegalArgumentException("Failed to decode '" + code + "' as a MetaData.", e);
        }
    }

    private static class Tokenizer {
        private static final Logger LOGGER = LogManager.getLogger(MetaDataEncoder.Tokenizer.class);
        private final String code;
        private final char[] chars;
        private final int charsLength;
        private int pos;
        private int begin;
        private int end;
        private Token current = Token.EPSILON;

        private enum Token {
            STRING,
            ESCAPED_STRING,
            KEY_VALUE_SEPARATATOR,
            ENTRY_SEPARATOR,
            EPSILON
        }

        Tokenizer(String code) {
            LOGGER.trace("<init>({})", code);
            this.code = code;
            this.chars = code.toCharArray();
            this.charsLength = chars.length;
            this.pos = 0;
            skipSpaces();
        }

        private static boolean isTerminator(char c) {
            return Terminators.BEST_MATCHER.test(c);
        }

        private static boolean isSeparator(char c) {
            return Separators.BEST_MATCHER.test(c);
        }

        private static String unescape(String s) {
            final StringBuilder builder = new StringBuilder();
            int index = 1;
            while (index < s.length() - 1) {
                final char c = s.charAt(index);
                builder.append(c);
                if (c == DELIMITER) {
                    index++;
                }
                index++;
            }
            return builder.toString();
        }

        String getText() {
            return code.substring(begin, end);
        }

        Token getCurrent() {
            return current;
        }

        /**
         * Analyzes input string from current position and determine the current token.
         * After calling this, position is at the beginning of following token.
         */
        void nextToken() {
            LOGGER.trace("nextToken()");
            if (hasCharAt(pos)) {
                // Index of token first character (inclusive)
                begin = pos;
                // Current char
                final char c = chars[pos];
                // Advance reading position.
                pos++;

                if (c == DELIMITER) {
                    // Found an escaped text
                    final boolean closed = skipEscapedText();
                    if (!closed) {
                        throw new IllegalArgumentException("[nextToken] no closing '\"' found.");
                    }
                    current = Token.ESCAPED_STRING;
                } else if (c == KEY_VALUE_SEPARATOR) {
                    current = Token.KEY_VALUE_SEPARATATOR;
                } else if (isTerminator(c)) {
                    current = Token.ENTRY_SEPARATOR;
                } else {
                    advanceToNextSeparator();
                    current = Token.STRING;
                }
                end = pos;
                skipSpaces();
            } else {
                current = Token.EPSILON;
                begin = pos;
                end = begin;
            }

            LOGGER.trace("   current: {}", current);
        }

        boolean hasCharAt(int index) {
            return index < charsLength;
        }

        /**
         * Increments pos to the next separator.
         * <p>
         * If pos already designates a separator, does not move.
         */
        void advanceToNextSeparator() {
            while (hasCharAt(pos) && !isSeparator(chars[pos])) {
                pos++;
            }
        }

        void skipSpaces() {
            while (pos < charsLength && Spaces.BEST_MATCHER.test(chars[pos])) {
                pos++;
            }
        }

        /**
         * Search the closing '"' character.
         * <p>
         * It is a '"' not followed by another '"'.
         * Must be invoked with pos designating the first character after the opening '"'.
         * After that, pos designates the first character following the escaped text.
         *
         * @return {@code true} when closing '"' has been found.
         */
        boolean skipEscapedText() {
            while (pos < charsLength) {
                if (chars[pos] == DELIMITER) {
                    pos++;
                    if (pos < charsLength && chars[pos] == DELIMITER) {
                        // One char after '""'
                        pos++;
                        // continue exploration
                    } else {
                        // One char after closing '"'
                        return true;
                    }
                } else {
                    pos++;
                }
            }
            return false;
        }

        String consumeString() {
            LOGGER.trace("parseString()");
            final String s;
            switch (current) {
            case STRING:
                s = getText();
                break;
            case ESCAPED_STRING:
                s = unescape(getText());
                break;
            case ENTRY_SEPARATOR:
            case EPSILON:
                s = "";
                break;
            default:
                throw new UnexpectedValueException("Unexpected token " + current);
            }
            nextToken();
            return s;
        }

        void expect(Token token) {
            LOGGER.trace("expect({})", token);
            if (current != token) {
                throw new UnexpectedValueException("Unexpected token " + current);
            }
            nextToken();
        }
    }
}