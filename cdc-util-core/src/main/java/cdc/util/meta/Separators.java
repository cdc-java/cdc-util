package cdc.util.meta;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;
import cdc.util.function.MultiplyShiftCharPredicate;

/**
 * Recognition of separators that hold on one character.
 * <p>
 * It is either a space of ':'.
 *
 * @author Damien Carbonne
 */
final class Separators {
    /**
     * String containing characters that are considered as one character separators.
     */
    static final String CHARS = ":\"" + Spaces.CHARS + Terminators.CHARS;

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * ' ' is used as the filling character.<br>
     * <b>WARNING:</b> this is computed and must match {@link #CHARS}.
     */
    static final String STABLE =
            "::\u0085\u205f::::\t\n\u000b\f\r\u2028\u2029::::\u1680\u202f:\u303f:\u3000:\u180e:\u00a0::\u0020:\":::\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b:\ufeff:::::::::::::";

    /**
     * Conversion of {@link #STABLE} as a char array
     */
    static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    static final int MULTIPLY = 65_846_348;

    /**
     * The shift to use for {@link #TABLE}.
     */
    static final int SHIFT = 26;

    private Separators() {
    }

    public static final CharPredicate MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharPredicate(STABLE,
                                           MULTIPLY,
                                           SHIFT);

    public static final CharPredicate MULTIPLY_SHIFT_INLINE_MATCHER =
            c -> TABLE[(c * MULTIPLY) >>> SHIFT] == c;

    public static final CharPredicate BIT_SET_MATCHER =
            new BitSetCharPredicate(CHARS);

    public static final CharPredicate BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharPredicate(CHARS);

    /**
     * Matcher that must be used to identify one character separators.
     */
    public static final CharPredicate BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}