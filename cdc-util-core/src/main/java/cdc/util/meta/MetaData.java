package cdc.util.meta;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Simple (key, value) map of strings.
 *
 * @author Damien Carbonne
 *
 */
public interface MetaData {
    /**
     * Implementation of {@link MetaData} that is empty and immutable.
     */
    public static final MetaData EMPTY = new MetaData() {
        @Override
        public void remove(String key) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void putAll(MetaData other) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void put(String key,
                        String value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean hasKey(String key) {
            return false;
        }

        @Override
        public Iterable<String> getKeys() {
            return Collections.emptySet();
        }

        @Override
        public String get(String key) {
            return null;
        }

        @Override
        public String get(String key,
                          String def) {
            return def;
        }

        @Override
        public void clear() {
            // Ignore
        }

        @Override
        public String toString() {
            return "[EMPTY MetaData]";
        }
    };

    /**
     * @return An iterable of all keys. This may contain duplicates.
     */
    public Iterable<String> getKeys();

    /**
     * @return A set of keys. This may be expensive.
     */
    public default Set<String> getKeysSet() {
        return IterableUtils.toSet(getKeys());
    }

    /**
     * @return A sorted list of (unique) keys.
     */
    public default List<String> getSortedKeys() {
        return IterableUtils.toSortedList(getKeysSet());
    }

    /**
     * Returns {@code true} if a key is contained.
     *
     * @param key The key.
     * @return {@code true} if {@code key} is contained.
     */
    public boolean hasKey(String key);

    /**
     * Returns the value associated to a key or a default value.
     *
     * @param key The key.
     * @param def The default value.
     * @return The value associated to {@code key} or {@code def}.
     */
    public String get(String key,
                      String def);

    /**
     * Returns the value associated to a key or {@code null}.
     *
     * @param key The key.
     * @return The value associated to {@code key} or {@code null}.
     */
    public String get(String key);

    /**
     * Adds or replaces a (key, value) pair.
     *
     * @param key The key.
     * @param value The value.
     */
    public void put(String key,
                    String value);

    /**
     * Adds all (key, value) pairs of another MetaData to this one.
     *
     * @param other The other MetaData.
     */
    public void putAll(MetaData other);

    /**
     * Remove all (key, value) pairs.
     */
    public void clear();

    /**
     * Removes a (key, value) pair.
     *
     * @param key The key to remove.
     */
    public void remove(String key);

    /**
     * Compose this meta data with another one (that has a lower priority).
     * <p>
     * Search is done primarily in this MetaData and secondarily in {@code delegate}.
     *
     * @param delegate The other meta data.
     * @return A new MetaData that is composed of this one and {@code delegate}.
     */
    public default MetaData with(MetaData delegate) {
        Checks.isNotNull(delegate, "delegate");
        return new MetaData() {
            @Override
            public Iterable<String> getKeys() {
                return IterableUtils.join(MetaData.this.getKeys(),
                                          delegate.getKeys());
            }

            @Override
            public boolean hasKey(String key) {
                return MetaData.this.hasKey(key) || delegate.hasKey(key);
            }

            @Override
            public String get(String key) {
                if (MetaData.this.hasKey(key)) {
                    return MetaData.this.get(key);
                } else {
                    return delegate.get(key);
                }
            }

            @Override
            public String get(String key,
                              String def) {
                if (MetaData.this.hasKey(key)) {
                    return MetaData.this.get(key);
                } else {
                    return delegate.get(key, def);
                }
            }

            @Override
            public void put(String key,
                            String value) {
                MetaData.this.put(key, value);
            }

            @Override
            public void putAll(MetaData other) {
                for (final String key : other.getKeys()) {
                    MetaData.this.put(key, other.get(key));
                }
            }

            @Override
            public void clear() {
                MetaData.this.clear();
                delegate.clear();
            }

            @Override
            public void remove(String key) {
                MetaData.this.remove(key);
                delegate.remove(key);
            }
        };
    }
}