package cdc.util.meta;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;
import cdc.util.function.MultiplyShiftCharPredicate;

/**
 * Recognition of space characters.
 *
 * @author Damien Carbonne
 */
final class Spaces {
    /**
     * String containing characters that are considered as spaces.
     */
    static final String CHARS = ""
            + "\t"     // HORIZONTAL TABULATION (U+0009)
            // + "\n" // LINE FEED (U+000A)
            + "\u000B" // VERTICAL TABULATION
            + "\f"     // FORM FEED (U+000C)
            // + "\r" // CARRIAGE RETURN (U+000D)
            + "\u0020" // SPACE
            // + "\u0085" // NEXT LINE
            + "\u00A0" // NO-BREAK SPACE
            + "\u1680" // OGHAM SPACE MARK
            + "\u180E" // MONGOLIAN VOWEL SEPARATOR
            + "\u2000" // EN QUAD
            + "\u2001" // EM QUAD
            + "\u2002" // EN SPACE
            + "\u2003" // EM SPACE
            + "\u2004" // THREE-PER-EM SPACE
            + "\u2005" // FOUR-PER-EM SPACE
            + "\u2006" // SIX-PER-EM SPACE
            + "\u2007" // FIGURE SPACE
            + "\u2008" // PUNCTUATION SPACE
            + "\u2009" // THIN SPACE
            + "\u200A" // HAIR SPACE
            + "\u200B" // ZERO WIDTH SPACE
            // + "\u2028" // LINE SEPARATOR
            + "\u2029" // PARAGRAPH SEPARATOR
            + "\u202F" // NARROW NO-BREAK SPACE
            + "\u205F" // MEDIUM MATHEMATICAL SPACE
            + "\u3000" // IDEOGRAPHIC SPACE
            + "\u303F" // IDEOGRAPHIC HALF FILL SPACE
            + "\uFEFF";// ZERO WIDTH NO-BREAK SPACE

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * ' ' is used as the filling character.<br>
     * <b>WARNING:</b> This is computed and must match {@link #CHARS}.
     */
    static final String STABLE =
            "\u200a\u200b\u2029\u0020.\ufeff.\u180e\u1680\u202f\t.\u000b\f.\u3000...\u00a0\u2000\u2001\u303f\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u205f";

    /**
     * Conversion of {@link #STABLE} as a char array.
     */
    static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    static final int MULTIPLY = 150_811_565;

    /**
     * The shift to use for {@link #TABLE}.
     */
    static final int SHIFT = 27;

    private Spaces() {
    }

    public static final CharPredicate MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharPredicate(STABLE,
                                           MULTIPLY,
                                           SHIFT);

    public static final CharPredicate MULTIPLY_SHIFT_INLINE_MATCHER =
            c -> TABLE[(c * MULTIPLY) >>> SHIFT] == c;

    public static final CharPredicate BIT_SET_MATCHER =
            new BitSetCharPredicate(CHARS);

    public static final CharPredicate BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharPredicate(CHARS);

    /**
     * Matcher that must be used to identify one character tokens.
     */
    public static final CharPredicate BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}