<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- <xsl:import href="cdc-util.xsl"/> -->
  <xsl:import href="cdc-markdown.xsl"/>

  <xsl:template name="cdc-debug">
    <xsl:param name="message"/>
    <xsl:param name="text" select="''"/>
    <xsl:element name="p">
      <xsl:element name="pre">
        <xsl:element name="font">
          <xsl:attribute name="color">red</xsl:attribute>
          <xsl:value-of select="$message"/>
        </xsl:element>
      </xsl:element>
      <xsl:if test="$text != ''">
        <xsl:element name="div">
          <xsl:attribute name="style">background-color:#AAAAAA;</xsl:attribute>
          <xsl:call-template name="cdc-debug-blocks">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <!-- Print lines for debug. -->
  <xsl:template name="cdc-debug-lines">
    <xsl:param name="text"/>
    <table>
      <thead>
        <tr>
          <th>Number</th>
          <th>Kind</th>
          <th>Content</th>
        </tr>
      </thead>
      <tbody>
        <xsl:call-template name="cdc-debug-lines-body">
          <xsl:with-param name="text" select="$text"/>
          <xsl:with-param name="number" select="1"/>
        </xsl:call-template>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template name="cdc-debug-lines-body">
    <xsl:param name="text"/>
    <xsl:param name="number"/>
    <xsl:variable name="line">
      <xsl:call-template name="cdc-first-line">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$line"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="others">
      <xsl:call-template name="cdc-all-but-first-line">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <tr>
      <td style="vertical-align:top">
        <xsl:value-of select="$number"/>
      </td>
      <td style="vertical-align:top">
        <xsl:value-of select="$kind"/>
        <xsl:if test="$kind = $cdc-kind-quote">
          <xsl:text> level: </xsl:text>
          <xsl:call-template name="cdc-quote-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="$kind = $cdc-kind-bullet-item or $kind = $cdc-kind-number-item">
          <xsl:text> level: </xsl:text>
          <xsl:call-template name="cdc-list-item-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:if>
      </td>
      <td style="vertical-align:top">
        <xsl:call-template name="cdc-debug-highlight">
          <xsl:with-param name="text">
            <xsl:call-template name="cdc-trim-tail-cr">
              <xsl:with-param name="text" select="$line"/>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="light-color" select="'#CCFFCC'"/>
          <xsl:with-param name="dark-color" select="'#AAFFAA'"/>
          <!-- <xsl:with-param name="replace-special-chars" select="true()"/> -->
        </xsl:call-template>
      </td>
    </tr>
    <xsl:if test="$others != ''">
      <xsl:call-template name="cdc-debug-lines-body">
        <xsl:with-param name="text" select="$others"/>
        <xsl:with-param name="number" select="$number + 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Print blocks for debug. -->
  <xsl:template name="cdc-debug-blocks">
    <xsl:param name="text"/>
    <table>
      <thead>
        <tr>
          <th>Number</th>
          <th>Kind</th>
          <th>Content</th>
          <th>Scope</th>
          <th>After Scope</th>
          <th>Item Scope</th>
          <th>After Item Scope</th>
        </tr>
      </thead>
      <tbody>
        <xsl:call-template name="cdc-debug-blocks-body">
          <xsl:with-param name="text" select="$text"/>
          <xsl:with-param name="number" select="1"/>
        </xsl:call-template>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template name="cdc-debug-blocks-body">
    <xsl:param name="text"/>
    <xsl:param name="number"/>
    <xsl:variable name="block">
      <xsl:call-template name="cdc-block-scope">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$block"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="others">
      <xsl:call-template name="cdc-block-after-scope">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <tr>
      <td style="vertical-align:top">
        <xsl:value-of select="$number"/>
      </td>
      <td style="vertical-align:top">
        <xsl:value-of select="$kind"/>
        <xsl:if test="$kind = $cdc-kind-quote">
          <xsl:text> level: </xsl:text>
          <xsl:call-template name="cdc-quote-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="$kind = $cdc-kind-bullet-item or $kind = $cdc-kind-number-item">
          <xsl:text> level: </xsl:text>
          <xsl:call-template name="cdc-list-item-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:if>
      </td>
      <td style="vertical-align:top">
        <xsl:call-template name="cdc-debug-highlight">
          <xsl:with-param name="text">
            <xsl:call-template name="cdc-trim-tail-cr">
              <xsl:with-param name="text" select="$block"/>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="light-color" select="'#CCCCFF'"/>
          <xsl:with-param name="dark-color" select="'#AAAAFF'"/>
        </xsl:call-template>
      </td>
      <xsl:choose>
        <xsl:when test="$kind = $cdc-kind-quote">
          <td style="vertical-align:top">
            <xsl:call-template name="cdc-debug-highlight">
              <!-- <xsl:with-param name="title" select="'quote-scope'"/> -->
              <xsl:with-param name="text">
                <xsl:call-template name="cdc-quote-scope">
                  <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="light-color" select="'#FFFFCC'"/>
              <xsl:with-param name="dark-color" select="'#FFFFAA'"/>
            </xsl:call-template>
          </td>
          <td style="vertical-align:top">
            <xsl:call-template name="cdc-debug-highlight">
              <!-- <xsl:with-param name="title" select="'quote-after-scope'"/> -->
              <xsl:with-param name="text">
                <xsl:call-template name="cdc-quote-after-scope">
                  <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="light-color" select="'#FFCCFF'"/>
              <xsl:with-param name="dark-color" select="'#FFAAFF'"/>
            </xsl:call-template>
          </td>
          <!-- TODO -->
          <td></td>
          <td></td>
        </xsl:when>
        <xsl:when test="$kind = $cdc-kind-bullet-item or $kind = $cdc-kind-number-item">
          <td style="vertical-align:top">
            <xsl:call-template name="cdc-debug-highlight">
              <!-- <xsl:with-param name="title" select="'list-scope'"/> -->
              <xsl:with-param name="text">
                <xsl:call-template name="cdc-list-scope">
                  <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="light-color" select="'#FFFFCC'"/>
              <xsl:with-param name="dark-color" select="'#FFFFAA'"/>
            </xsl:call-template>
          </td>
          <td style="vertical-align:top">
            <xsl:call-template name="cdc-debug-highlight">
              <!-- <xsl:with-param name="title" select="'list-after-scope'"/> -->
              <xsl:with-param name="text">
                <xsl:call-template name="cdc-list-after-scope">
                  <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="light-color" select="'#FFCCFF'"/>
              <xsl:with-param name="dark-color" select="'#FFAAFF'"/>
            </xsl:call-template>
          </td>
          <td style="vertical-align:top">
            <xsl:call-template name="cdc-debug-highlight">
              <!-- <xsl:with-param name="title" select="'list-item-scope'"/> -->
              <xsl:with-param name="text">
                <xsl:call-template name="cdc-list-item-scope">
                  <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="light-color" select="'#FFFFCC'"/>
              <xsl:with-param name="dark-color" select="'#FFFFAA'"/>
            </xsl:call-template>
          </td>
          <td style="vertical-align:top">
            <xsl:call-template name="cdc-debug-highlight">
              <!-- <xsl:with-param name="title" select="'list-item-after-scope'"/> -->
              <xsl:with-param name="text">
                <xsl:call-template name="cdc-list-item-after-scope">
                  <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="light-color" select="'#FFCCFF'"/>
              <xsl:with-param name="dark-color" select="'#FFAAFF'"/>
            </xsl:call-template>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </xsl:otherwise>
      </xsl:choose>
    </tr>
    <xsl:if test="$others != ''">
      <xsl:call-template name="cdc-debug-blocks-body">
        <xsl:with-param name="text" select="$others"/>
        <xsl:with-param name="number" select="$number + 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>