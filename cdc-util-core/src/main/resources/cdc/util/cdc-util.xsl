<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:variable name="cdc-upper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ'"/>
  <xsl:variable name="cdc-lower" select="'abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ'"/>
  <xsl:variable name="cdc-tab" select="'&#9;'"/>
  <xsl:variable name="cdc-nl" select="'&#10;'"/>
  <xsl:variable name="cdc-cr" select="'&#13;'"/>

  <!-- Compute the maximum of 2 values. -->
  <xsl:template name="cdc-max">
    <xsl:param name="value1"/>
    <xsl:param name="value2"/>
    <xsl:choose>
      <xsl:when test="$value1 >= $value2">
        <xsl:value-of select="$value1"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$value2"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Compute the minimum of 2 values. -->
  <xsl:template name="cdc-min">
    <xsl:param name="value1"/>
    <xsl:param name="value2"/>
    <xsl:choose>
      <xsl:when test="$value1 >= $value2">
        <xsl:value-of select="$value2"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$value1"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Convert an input string to upper case. -->
  <xsl:template name="cdc-to-upper-case">
    <xsl:param name="text"/>
    <xsl:value-of select="translate($text,$cdc-lower,$cdc-upper)"/>
  </xsl:template>
  
  <!-- Convert an input string to lower case. -->
  <xsl:template name="cdc-to-lower-case">
    <xsl:param name="text"/>
    <xsl:value-of select="translate($text,$cdc-upper,$cdc-lower)"/>
  </xsl:template>

  <!-- Revert the content of a string ('abc' => 'cba') -->
  <xsl:template name="cdc-reverse">
    <xsl:param name="text"/>
    <xsl:variable name="len" select="string-length($text)"/>
    <xsl:choose>
          <!-- Strings of length less than 2 are trivial to reverse -->
      <xsl:when test="2 > $len">
        <xsl:value-of select="$text"/>
      </xsl:when>
          <!-- Strings of length 2 are also trivial to reverse -->
      <xsl:when test="$len = 2">
        <xsl:value-of select="substring($text,2,1)"/>
        <xsl:value-of select="substring($text,1,1)"/>
      </xsl:when>
      <xsl:otherwise>
        <!-- Swap the recursive application of this template to
             the first half and second half of input -->
        <xsl:variable name="mid" select="floor($len div 2)"/>
        <xsl:call-template name="cdc-reverse">
          <xsl:with-param name="text" select="substring($text,$mid+1,$mid+1)"/>
        </xsl:call-template>
        <xsl:call-template name="cdc-reverse">
          <xsl:with-param name="text" select="substring($text,1,$mid)"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-replace-first">
    <xsl:param name="text"/>
    <xsl:param name="word"/>
    <xsl:param name="by"/>
    <xsl:choose>
      <xsl:when test="contains($text, $word)">
        <xsl:value-of select="substring-before($text, $word)"/>
        <xsl:value-of select="$by"/>
        <xsl:value-of select="substring-after($text, $word)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-replace-all">
    <xsl:param name="text"/>
    <xsl:param name="word"/>
    <xsl:param name="by"/>
    <xsl:choose>
      <xsl:when test="contains($text, $word)">
        <xsl:variable name="tmp">
          <xsl:value-of select="substring-before($text, $word)"/>
          <xsl:value-of select="$by"/>
          <xsl:value-of select="substring-after($text, $word)"/>
        </xsl:variable>
        <xsl:call-template name="cdc-replace-all">
          <xsl:with-param name="text" select="$tmp"/>
          <xsl:with-param name="word" select="$word"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  <!--  Remove last character when it is a cr -->
  <xsl:template name="cdc-trim-tail-cr">
    <xsl:param name="text"/>
    <xsl:variable name="reversed">
      <xsl:call-template name="cdc-reverse">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$text = ''">
        <xsl:value-of select="''"/>
      </xsl:when>
      <xsl:when test="starts-with($reversed, $cdc-cr)">
        <xsl:call-template name="cdc-reverse">
          <xsl:with-param name="text" select="substring($reversed,2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  Remove leading spaces -->
  <xsl:template name="cdc-trim-head-spaces">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="$text = ''">
        <xsl:value-of select="''"/>
      </xsl:when>
      <xsl:when test="starts-with($text, $cdc-tab) or starts-with($text, ' ')">
        <xsl:call-template name="cdc-trim-head-spaces">
          <xsl:with-param name="text" select="substring($text, 2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  Extract leading spaces -->
  <xsl:template name="cdc-head-spaces">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="$text = ''">
        <xsl:value-of select="''"/>
      </xsl:when>
      <xsl:when test="starts-with($text, $cdc-tab) or starts-with($text, ' ')">
        <xsl:value-of select="substring($text, 1, 1)"/>
        <xsl:call-template name="cdc-head-spaces">
          <xsl:with-param name="text" select="substring($text, 2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  Number of head spaces -->
  <xsl:template name="cdc-head-spaces-count">
    <xsl:param name="text"/>
    <xsl:variable name="spaces">
      <xsl:call-template name="cdc-head-spaces">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="string-length($spaces)"/>
  </xsl:template>


  <!-- Return true when a text starts with one character among a string -->
  <!-- text: the text to test -->
  <!-- chars: the set of characters to test -->
  <xsl:template name="cdc-starts-with-one-of">
    <xsl:param name="text"/>
    <xsl:param name="chars"/>
    <xsl:choose>
      <xsl:when test="$text = '' or $chars = ''">
        <xsl:value-of select="false()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="first" select="substring($chars, 1, 1)"/>
        <xsl:choose>
          <!-- Test first candidate char -->
          <xsl:when test="starts-with($text, $first)">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <!-- Test other candidate chars -->
          <xsl:otherwise>
            <xsl:call-template name="cdc-starts-with-one-of">
              <xsl:with-param name="text" select="$text"/>
              <xsl:with-param name="chars" select="substring($chars, 2)"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Return the number of occurrences of some characters at the beginning of a text -->
  <xsl:template name="cdc-head-chars-count">
    <xsl:param name="text"/>
    <xsl:param name="chars"/>
    <xsl:variable name="found">
      <xsl:call-template name="cdc-starts-with-one-of">
        <xsl:with-param name="text" select="$text"/>
        <xsl:with-param name="chars" select="$chars"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$found = 'true'">
        <xsl:variable name="remaining">
          <xsl:call-template name="cdc-head-chars-count">
            <xsl:with-param name="text" select="substring($text, 2)"/>
            <xsl:with-param name="chars" select="$chars"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="1 + $remaining"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  Extract first line. -->
  <xsl:template name="cdc-first-line">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="contains($text, $cdc-nl)">
        <xsl:value-of select="concat(substring-before($text, $cdc-nl), $cdc-nl)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Extract all lines after first line.  -->
  <xsl:template name="cdc-all-but-first-line">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="contains($text, $cdc-nl)">
        <xsl:value-of select="substring-after($text, $cdc-nl)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Return the first word of a text -->
  <!-- The end of first word is marked by a separator -->
  <xsl:template name="cdc-first-word">
    <xsl:param name="text"/>
    <xsl:variable name="line">
      <xsl:call-template name="cdc-first-line">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="contains($line, ' ')">
        <xsl:value-of select="substring-before($text,' ')"/>
      </xsl:when>
      <!--  TODO: take into account other separator characters -->
      <xsl:otherwise>
        <xsl:call-template name="cdc-trim-tail-cr">
          <xsl:with-param name="text" select="$line"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Replace CR by <cr>, NL by <nl> and TAB by <tab> -->
  <xsl:template name="cdc-highlight-special-chars">
    <xsl:param name="text"/>
    <xsl:variable name="tmp1">
      <xsl:call-template name="cdc-replace-all">
        <xsl:with-param name="text" select="$text"/>
        <xsl:with-param name="word" select="$cdc-cr"/>
        <xsl:with-param name="by" select="'&lt;cr&gt;'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="tmp2">
      <xsl:call-template name="cdc-replace-all">
        <xsl:with-param name="text" select="$tmp1"/>
        <xsl:with-param name="word" select="$cdc-nl"/>
        <xsl:with-param name="by" select="'&lt;nl&gt;'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="tmp3">
      <xsl:call-template name="cdc-replace-all">
        <xsl:with-param name="text" select="$tmp2"/>
        <xsl:with-param name="word" select="$cdc-tab"/>
        <xsl:with-param name="by" select="'&lt;tab&gt;'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$tmp3"/>
  </xsl:template>

  <xsl:template name="cdc-debug-highlight">
    <xsl:param name="title" select="''"/>
    <xsl:param name="text"/>
    <xsl:param name="light-color"/>
    <xsl:param name="dark-color"/>
    <xsl:param name="replace-special-chars" select="false()"/>
    <xsl:if test="$title != ''">
      <xsl:element name="span">
        <xsl:attribute name="style">font-style:italic;text-decoration:underline;color:red;</xsl:attribute>
        <xsl:value-of select="$title"/>
      </xsl:element>
    </xsl:if>
    <xsl:element name="pre">
      <xsl:attribute name="style">background-color:<xsl:value-of select="$dark-color"/>;</xsl:attribute>
      <xsl:element name="span">
        <xsl:attribute name="style">background-color:<xsl:value-of select="$light-color"/>;</xsl:attribute>
        <xsl:choose>
          <xsl:when test="$replace-special-chars = 'true'">
            <xsl:call-template name="cdc-highlight-special-chars">
              <xsl:with-param name="text" select="$text"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$text"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>