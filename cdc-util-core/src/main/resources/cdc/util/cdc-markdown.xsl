<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="cdc-util.xsl"/>

  <xsl:variable name="cdc-kind-empty" select="'empty'"/>
  <xsl:variable name="cdc-kind-heading" select="'heading'"/>
  <xsl:variable name="cdc-kind-hr" select="'horizontal-rule'"/>
  <xsl:variable name="cdc-kind-quote" select="'quote'"/>
  <xsl:variable name="cdc-kind-normal" select="'normal'"/>
  <xsl:variable name="cdc-kind-bullet-item" select="'bullet-item'"/>
  <xsl:variable name="cdc-kind-number-item" select="'number-item'"/>

  <xsl:template name="cdc-markdown">
    <xsl:param name="text"/>
    <xsl:if test="$text != ''">
      <xsl:variable name="current-block">
        <xsl:call-template name="cdc-block-scope">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="current-kind">
        <xsl:call-template name="cdc-text-kind">
          <xsl:with-param name="text" select="$current-block"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:choose>
        <xsl:when test="$current-kind = 'heading'">
          <!-- heading processing -->
          <xsl:call-template name="cdc-heading">
            <xsl:with-param name="text" select="$current-block"/>
          </xsl:call-template>
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown-after-block">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$current-kind = $cdc-kind-empty">
          <!--  Skip empty block -->
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown-after-block">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$current-kind = 'horizontal-rule'">
          <xsl:element name="hr"/>
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown-after-block">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$current-kind = $cdc-kind-normal">
          <!-- Transform current block into paragraph -->
          <xsl:element name="p">
            <xsl:call-template name="cdc-inline">
              <xsl:with-param name="text" select="$current-block"/>
            </xsl:call-template>
          </xsl:element>
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown-after-block">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$current-kind = $cdc-kind-quote">
          <!--  process all contiguous quote blocks -->
          <xsl:call-template name="cdc-quote">
            <xsl:with-param name="text" select="$text"/>
            <xsl:with-param name="level" select="0"/>
          </xsl:call-template>
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown">
            <xsl:with-param name="text">
              <xsl:call-template name="cdc-quote-after-scope">
                <xsl:with-param name="text" select="$text"/>
                <!-- <xsl:with-param name="level" select="0"/> -->
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$current-kind = $cdc-kind-bullet-item or $current-kind = $cdc-kind-number-item">
          <xsl:call-template name="cdc-list">
            <xsl:with-param name="text" select="$text"/>
            <xsl:with-param name="context-level" select="0"/>
          </xsl:call-template>
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown">
            <xsl:with-param name="text">
              <xsl:call-template name="cdc-list-after-scope">
                <xsl:with-param name="text" select="$text"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:element name="b">
            <xsl:element name="font">
              <xsl:attribute name="color">red</xsl:attribute>
              <xsl:text>TODO: </xsl:text>
            </xsl:element>
          </xsl:element>
          <xsl:value-of select="$current-kind"/>
          <pre style="background-color:#FFAAAA;">
            <xsl:value-of select="$current-block"/>
          </pre>
          <!--  Continue markdown analysis -->
          <xsl:call-template name="cdc-markdown-after-block">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="cdc-markdown-after-block">
    <xsl:param name="text"/>
    <xsl:if test="$text != ''">
      <xsl:call-template name="cdc-markdown">
        <xsl:with-param name="text">
          <xsl:call-template name="cdc-block-after-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!--  Return text kind -->
  <!--  Analyze heading of first line -->
  <xsl:template name="cdc-text-kind">
    <xsl:param name="text"/>
    <!--  First line of text -->
    <xsl:variable name="head">
      <xsl:call-template name="cdc-first-line">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <!--  First line with heading spaces trimmed -->
    <xsl:variable name="non-spaces">
      <xsl:call-template name="cdc-trim-head-spaces">
        <xsl:with-param name="text" select="$head"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$head = '' or $head = '&#xA;'">
        <xsl:value-of select="$cdc-kind-empty"/>
      </xsl:when>
      <xsl:when test="starts-with($head, '#')">
        <xsl:value-of select="$cdc-kind-heading"/>
      </xsl:when>
      <xsl:when test="starts-with($head, '---')">
        <xsl:value-of select="$cdc-kind-hr"/>
      </xsl:when>
      <xsl:when test="starts-with($non-spaces, '- ')">
        <xsl:value-of select="$cdc-kind-bullet-item"/>
      </xsl:when>
      <xsl:when test="starts-with($non-spaces, '>')">
        <xsl:value-of select="$cdc-kind-quote"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="is-number-item">
          <xsl:call-template name="cdc-starts-with-number-item">
            <xsl:with-param name="non-spaces" select="$non-spaces"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$is-number-item = 'true'">
            <xsl:value-of select="$cdc-kind-number-item"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$cdc-kind-normal"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- template dedicated to recognition of numbered items -->
  <!-- [0-9.]+[)] Should add a space in the end ?-->
  <xsl:template name="cdc-starts-with-number-item">
    <xsl:param name="non-spaces"/>
    <xsl:variable name="word">
      <xsl:call-template name="cdc-first-word">
        <xsl:with-param name="text" select="$non-spaces"/>
      </xsl:call-template>
    </xsl:variable>
    <!-- Number of leading digits and dots -->
    <xsl:variable name="digits">
      <xsl:call-template name="cdc-head-chars-count">
        <xsl:with-param name="text" select="$word"/>
        <xsl:with-param name="chars" select="'0123456789.'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$digits > 0">
        <xsl:variable name="tail" select="substring($word, $digits + 1)"/>
        <xsl:choose>
          <xsl:when test="string-length($tail) = 1 and starts-with($tail, ')')">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Extract in text the section that corresponds to next block. -->
  <!-- A block starts on a non empty line and continues with all following normal lines. -->
  <!-- Except for one-line blocks -->
  <xsl:template name="cdc-block-scope">
    <xsl:param name="text"/>
    <!--  Extract first line of text -->
    <xsl:variable name="head">
      <xsl:call-template name="cdc-first-line">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$head"/>
    <xsl:variable name="head-kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$head"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$head-kind != $cdc-kind-empty">
      <xsl:variable name="tail">
        <xsl:call-template name="cdc-all-but-first-line">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="tail-kind">
        <xsl:call-template name="cdc-text-kind">
          <xsl:with-param name="text" select="$tail"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:if test="$tail-kind = $cdc-kind-normal and $head-kind != $cdc-kind-heading and $head-kind != $cdc-kind-hr">
        <xsl:call-template name="cdc-block-scope">
          <xsl:with-param name="text" select="$tail"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <!-- Extract in text the section that follows first block. -->
  <xsl:template name="cdc-block-after-scope">
    <xsl:param name="text"/>
    <xsl:variable name="block">
      <xsl:call-template name="cdc-block-scope">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="substring-after($text, $block)"/>
  </xsl:template>


  <xsl:template name="cdc-quote">
    <xsl:param name="text"/>
    <xsl:param name="level"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select='$text'/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$kind = $cdc-kind-quote">
      <xsl:variable name="current-level">
        <xsl:call-template name="cdc-quote-level">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$current-level > $level">
          <xsl:element name="blockquote">
            <xsl:call-template name="cdc-quote">
              <xsl:with-param name="text" select="$text"/>
              <xsl:with-param name="level" select="$level + 1"/>
            </xsl:call-template>
          </xsl:element>
          <xsl:variable name="after">
            <xsl:call-template name="cdc-quote-after-scope">
              <xsl:with-param name="text" select="$text"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:if test="$after != ''">
            <xsl:call-template name="cdc-quote">
              <xsl:with-param name="text" select="$after"/>
              <xsl:with-param name="level" select="$level"/>
            </xsl:call-template>
          </xsl:if>
        </xsl:when>
        <xsl:when test="$current-level = $level">
          <xsl:call-template name="cdc-inline">
            <xsl:with-param name="text">
              <xsl:call-template name="cdc-block-scope">
                <xsl:with-param name="text">
                  <xsl:call-template name="cdc-quote-trim">
                    <xsl:with-param name="text" select="$text"/>
                  </xsl:call-template>
                </xsl:with-param>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="cdc-quote">
            <xsl:with-param name="text">
              <xsl:call-template name="cdc-block-after-scope">
                <xsl:with-param name="text" select="$text"/>
              </xsl:call-template>
            </xsl:with-param>
            <xsl:with-param name="level" select="$level"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
        <!--  Ignore -->
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="cdc-quote-scope">
    <xsl:param name="text"/>

    <!--  kind of the first block. -->
    <xsl:variable name="block-kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <!-- First block is a quote. -->
      <xsl:when test="$block-kind = $cdc-kind-quote">
        <!-- First block -->
        <xsl:variable name="block">
          <xsl:call-template name="cdc-block-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- Level of first block -->
        <xsl:variable name="quote-level">
          <xsl:call-template name="cdc-quote-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Other blocks  -->
        <xsl:variable name="others">
          <xsl:call-template name="cdc-block-after-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Keep first block -->
        <xsl:value-of select="$block"/>
        
        <!-- Keep following blocks which are deeper. -->
        <xsl:call-template name="cdc-quote-deeper-scope">
          <xsl:with-param name="text" select="$others"/>
          <xsl:with-param name="level" select="$quote-level"/>
        </xsl:call-template>
      </xsl:when>
      
      <!-- First block is NOT a quote. -->
      <xsl:otherwise>
        <!-- Return an empty string -->
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  

  <!--  Skip all contiguous quote blocks with level >= level and return remaining text  -->
  <xsl:template name="cdc-quote-after-scope">
    <xsl:param name="text"/>
    <xsl:variable name="scope">
      <xsl:call-template name="cdc-quote-scope">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="substring-after($text, $scope)"/>
  </xsl:template>

  <xsl:template name="cdc-quote-deeper-scope">
    <xsl:param name="text"/>
    <xsl:param name="level"/>

    <!-- Kind of the first block. -->
    <xsl:variable name="block-kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    
    <!-- Level of first block (if it is a quote) -->
    <xsl:variable name="quote-level">
      <xsl:call-template name="cdc-quote-level">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <!-- First block is a quote with an adequate level -->
      <!-- Keep it and recurse with remaining blocks -->
      <xsl:when test="$block-kind = $cdc-kind-quote and $quote-level >= $level">
        <!-- First block -->
        <xsl:variable name="block">
          <xsl:call-template name="cdc-block-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Other blocks  -->
        <xsl:variable name="others">
          <xsl:call-template name="cdc-block-after-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- Keep first block -->
        <xsl:value-of select="$block"/>

        <!-- Keep following blocks which are deeper. -->
        <xsl:call-template name="cdc-quote-deeper-scope">
          <xsl:with-param name="text" select="$others"/>
          <xsl:with-param name="level" select="$level"/>
        </xsl:call-template>
      </xsl:when>
      
      <!-- First block is not a quote or has an incorrect level. -->
      <xsl:otherwise>
        <!-- Return an empty string -->
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  

  <!--  Return the quote level of a block quote -->
  <!--  If block is not a block quote, return 0 -->
  <xsl:template name="cdc-quote-level">
    <xsl:param name="text"/>
    <xsl:param name="count" select="0"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$kind = $cdc-kind-quote">
        <xsl:variable name="line">
          <xsl:call-template name="cdc-trim-head-spaces">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:call-template name="cdc-quote-level">
          <xsl:with-param name="text" select="substring($line, 2)"/>
          <xsl:with-param name="count" select="$count + 1"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$count"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-quote-trim">
    <xsl:param name="text"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="line">
      <xsl:call-template name="cdc-trim-head-spaces">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$kind = $cdc-kind-quote">
        <xsl:call-template name="cdc-quote-trim">
          <xsl:with-param name="text" select="substring($line, 2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$line"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-list">
    <xsl:param name="text"/>
    <xsl:param name="context-level"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select='$text'/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$kind = $cdc-kind-bullet-item">
      <xsl:variable name="current-level">
        <xsl:call-template name="cdc-list-item-level">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:element name="ul">
        <xsl:choose>
          <xsl:when test="$current-level > ($context-level + 1)">
            <xsl:call-template name="cdc-list">
              <xsl:with-param name="text" select="$text"/>
              <xsl:with-param name="context-level" select="$context-level + 1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$current-level = ($context-level + 1)">
            <xsl:call-template name="cdc-list-item">
              <xsl:with-param name="text" select="$text"/>
              <xsl:with-param name="context-level" select="$context-level"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:element>
    </xsl:if>
    <xsl:if test="$kind = $cdc-kind-number-item">
      <xsl:variable name="current-level">
        <xsl:call-template name="cdc-list-item-level">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:element name="ol">
        <xsl:choose>
          <xsl:when test="$current-level > ($context-level + 1)">
            <xsl:call-template name="cdc-list">
              <xsl:with-param name="text" select="$text"/>
              <xsl:with-param name="context-level" select="$context-level + 1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$current-level = ($context-level + 1)">
            <xsl:call-template name="cdc-list-item">
              <xsl:with-param name="text" select="$text"/>
              <xsl:with-param name="context-level" select="$context-level"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:element>
    </xsl:if>
  </xsl:template>

  <xsl:template name="cdc-list-item">
    <xsl:param name="text"/>
    <xsl:param name="context-level"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$kind = $cdc-kind-bullet-item or $kind = $cdc-kind-number-item">
      <xsl:variable name="current-level">
        <xsl:call-template name="cdc-list-item-level">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="item">
        <xsl:call-template name="cdc-list-item-scope">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="after-item">
        <xsl:call-template name="cdc-list-item-after-scope">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="first-block">
        <xsl:call-template name="cdc-block-scope">
          <xsl:with-param name="text" select="$item"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="other-blocks">
        <xsl:call-template name="cdc-block-after-scope">
          <xsl:with-param name="text" select="$item"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:element name="li">
        <xsl:call-template name="cdc-inline">
          <xsl:with-param name="text">
            <xsl:call-template name="cdc-list-item-trim">
              <xsl:with-param name="text" select="$first-block"/>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
        <xsl:if test="$other-blocks != ''">
          <xsl:call-template name="cdc-list">
            <xsl:with-param name="text" select="$other-blocks"/>
            <xsl:with-param name="context-level" select="$context-level + 1"/>
          </xsl:call-template>
        </xsl:if>
      </xsl:element>
      <xsl:if test="$after-item != ''">
        <xsl:call-template name="cdc-list-item">
          <xsl:with-param name="text" select="$after-item"/>
          <xsl:with-param name="context-level" select="$context-level"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:if>
  </xsl:template>
 
  <!-- Return the text corresponding to a list (bullet or number) scope, when first block is a list item. -->
  <xsl:template name="cdc-list-scope">
    <xsl:param name="text"/>

    <!--  kind of the first block. -->
    <xsl:variable name="block-kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <!-- First block is a list item. -->
      <xsl:when test="$block-kind = $cdc-kind-bullet-item or $block-kind = $cdc-kind-number-item">
        <!-- First block -->
        <xsl:variable name="block">
          <xsl:call-template name="cdc-block-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- Level of first block -->
        <xsl:variable name="item-level">
          <xsl:call-template name="cdc-list-item-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Other blocks  -->
        <xsl:variable name="others">
          <xsl:call-template name="cdc-block-after-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Keep first block -->
        <xsl:value-of select="$block"/>
        
        <!-- Keep following blocks which are deeper. -->
        <xsl:call-template name="cdc-list-item-deeper-scope">
          <xsl:with-param name="text" select="$others"/>
          <xsl:with-param name="level" select="$item-level"/>
        </xsl:call-template>
      </xsl:when>
      
      <!-- First block is NOT a list item. -->
      <xsl:otherwise>
        <!-- Return an empty string -->
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-list-after-scope">
    <xsl:param name="text"/>
    <xsl:variable name="scope">
      <xsl:call-template name="cdc-list-scope">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="substring-after($text, $scope)"/>
  </xsl:template>
  
  <!-- Return the text corresponding to a list item scope, when first block is a list item. -->
  <xsl:template name="cdc-list-item-scope">
    <xsl:param name="text"/>

    <!--  kind of the first block. -->
    <xsl:variable name="block-kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <!-- First block is a list item. -->
      <xsl:when test="$block-kind = $cdc-kind-bullet-item or $block-kind = $cdc-kind-number-item">
        <!-- First block -->
        <xsl:variable name="block">
          <xsl:call-template name="cdc-block-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- Level of first block -->
        <xsl:variable name="item-level">
          <xsl:call-template name="cdc-list-item-level">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Other blocks  -->
        <xsl:variable name="others">
          <xsl:call-template name="cdc-block-after-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Keep first block -->
        <xsl:value-of select="$block"/>
        
        <!-- Keep following blocks which are strictly deeper. -->
        <xsl:call-template name="cdc-list-item-deeper-scope">
          <xsl:with-param name="text" select="$others"/>
          <xsl:with-param name="level" select="$item-level + 1"/>
        </xsl:call-template>
      </xsl:when>
      
      <!-- First block is NOT a list item. -->
      <xsl:otherwise>
        <!-- Return an empty string -->
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-list-item-after-scope">
    <xsl:param name="text"/>
    <xsl:variable name="scope">
      <xsl:call-template name="cdc-list-item-scope">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="substring-after($text, $scope)"/>
  </xsl:template>


  <xsl:template name="cdc-list-item-deeper-scope">
    <xsl:param name="text"/>
    <xsl:param name="level"/>

    <!--  kind of the first block. -->
    <xsl:variable name="block-kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    
    <!-- Level of first block (if it is a list item) -->
    <xsl:variable name="item-level">
      <xsl:call-template name="cdc-list-item-level">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <!-- First block is a list item with an adequate level -->
      <!-- Keep it and recurse with remaining blocks -->
      <xsl:when test="($block-kind = $cdc-kind-bullet-item or $block-kind = $cdc-kind-number-item) and $item-level >= $level">
        <!-- First block -->
        <xsl:variable name="block">
          <xsl:call-template name="cdc-block-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        
        <!-- Other blocks  -->
        <xsl:variable name="others">
          <xsl:call-template name="cdc-block-after-scope">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- Keep first block -->
        <xsl:value-of select="$block"/>

        <!-- Keep following blocks which are deeper. -->
        <xsl:call-template name="cdc-list-item-deeper-scope">
          <xsl:with-param name="text" select="$others"/>
          <xsl:with-param name="level" select="$level"/>
        </xsl:call-template>
      </xsl:when>
      
      <!-- First block is not a list item or has an incorrect level. -->
      <xsl:otherwise>
        <!-- Return an empty string -->
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-list-item-trim">
    <xsl:param name="text"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="line">
      <xsl:call-template name="cdc-trim-head-spaces">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$kind = $cdc-kind-bullet-item">
        <xsl:call-template name="cdc-trim-head-spaces">
          <!--  Remove '- ' -->
          <xsl:with-param name="text" select="substring($line, 2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$kind = $cdc-kind-number-item">
        <xsl:call-template name="cdc-trim-head-spaces">
          <!-- Remove [0-9.]+[)] -->
          <xsl:with-param name="text" select="substring($line, string-length(substring-before($line, ')')) + 2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$line"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-list-item-level">
    <xsl:param name="text"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$kind = $cdc-kind-bullet-item or $kind = $cdc-kind-number-item">
        <xsl:variable name="count">
          <xsl:call-template name="cdc-head-spaces-count">
            <xsl:with-param name="text" select="$text"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="1 + floor($count div 4)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
    
  <!-- Analyze a text and recognize the following patterns:  -->
  <!-- # header 1 -->
  <!-- ## header 2 -->
  <!-- ### header 3 -->
  <!-- #### header 4 -->
  <xsl:template name="cdc-heading">
    <xsl:param name="text"/>
    <xsl:variable name="head">
      <xsl:call-template name="cdc-first-line">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="level">
      <xsl:call-template name="cdc-heading-level">
        <xsl:with-param name="text" select="$head"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$level >= 0">
        <xsl:variable name="elevel">
          <xsl:call-template name="cdc-min">
            <xsl:with-param name="value1" select="$level"/>
            <xsl:with-param name="value2" select="'6'"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:element name="h{$elevel}">
          <xsl:call-template name="cdc-inline">
            <xsl:with-param name="text" select="substring($head,$elevel+1)"/>
          </xsl:call-template>
        </xsl:element>
      </xsl:when>
      <xsl:otherwise>
        <!--  TODO -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="cdc-heading-level">
    <xsl:param name="text"/>
    <xsl:param name="count" select="0"/>
    <xsl:variable name="kind">
      <xsl:call-template name="cdc-text-kind">
        <xsl:with-param name="text" select="$text"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$kind = $cdc-kind-heading">
        <xsl:call-template name="cdc-heading-level">
          <xsl:with-param name="text" select="substring($text, 2)"/>
          <xsl:with-param name="count" select="$count + 1"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$count"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  

  <!-- Analyze a text and recognize the following patterns:  -->
  <!--  - __bold__... -->
  <!--  - **bold**... -->
  <!--  - _italic_... -->
  <!--  - *italic*... -->
  <!--  - `code`... -->
  <!--  - [link](url)... -->
  <xsl:template name="cdc-inline">
    <xsl:param name="text"/>
    <xsl:if test="$text != ''">
      <xsl:choose>
        <xsl:when test="starts-with($text,'  &#10;')">
          <!--  Line ending with 2 spaces: insert break. -->
          <xsl:element name="br"/>
          <xsl:call-template name="cdc-inline">
            <xsl:with-param name="text" select="substring($text,4)"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="starts-with($text,'__')">
          <!-- Try: __bold__... -->
          <xsl:call-template name="cdc-wrap">
            <xsl:with-param name="text" select="substring($text,3)"/>
            <xsl:with-param name="mark" select="'__'"/>
            <xsl:with-param name="tag" select="'strong'"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="starts-with($text,'**')">
          <!-- Try: __bold__... -->
          <xsl:call-template name="cdc-wrap">
            <xsl:with-param name="text" select="substring($text,3)"/>
            <xsl:with-param name="mark" select="'**'"/>
            <xsl:with-param name="tag" select="'strong'"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="starts-with($text,'_')">
          <!-- Try: _italic_... -->
          <xsl:call-template name="cdc-wrap">
            <xsl:with-param name="text" select="substring($text,2)"/>
            <xsl:with-param name="mark" select="'_'"/>
            <xsl:with-param name="tag" select="'em'"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="starts-with($text,'*')">
          <!-- Try: _italic_... -->
          <xsl:call-template name="cdc-wrap">
            <xsl:with-param name="text" select="substring($text,2)"/>
            <xsl:with-param name="mark" select="'*'"/>
            <xsl:with-param name="tag" select="'em'"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="starts-with($text,'`')">
          <!-- Try: `code`... -->
          <xsl:call-template name="cdc-wrap">
            <xsl:with-param name="text" select="substring($text,2)"/>
            <xsl:with-param name="mark" select="'`'"/>
            <xsl:with-param name="tag" select="'code'"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="starts-with($text,'[')">
          <!-- Try: [link](url)... -->
          <xsl:call-template name="cdc-link">
            <xsl:with-param name="text" select="substring($text,2)"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <!--  Print first character and continue analysis with the remaining text -->
          <xsl:value-of select="substring($text,1,1)"/>
          <xsl:call-template name="cdc-inline">
            <xsl:with-param name="text" select="substring($text,2)"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <!--  Processing of: 'link](url)...' -->
  <!--  Called after '[' has been found -->
  <xsl:template name="cdc-link">
    <xsl:param name="text"/>
    <xsl:variable name="link" select="substring-before($text,']')"/>
    <xsl:choose>
      <xsl:when test="$link != ''">
        <xsl:call-template name="cdc-href">
          <xsl:with-param name="text" select="substring-after($text,']')"/>
          <xsl:with-param name="link" select="$link"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'['"/>
        <xsl:call-template name="cdc-inline">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  Processing of: '(url)...' -->
  <!--  Called after '[link]...' has been found. -->
  <xsl:template name="cdc-href">
    <xsl:param name="link"/>
    <xsl:param name="text"/>
    <xsl:variable name="href" select="substring-before(substring($text,2),')')"/>
    <xsl:choose>
      <xsl:when test="starts-with($text,'(') and $href != ''">
        <xsl:element name="a">
          <xsl:attribute name="href"><xsl:value-of select="$href"/></xsl:attribute>
          <xsl:value-of select="$link"/>
        </xsl:element>
        <xsl:call-template name="cdc-inline">
          <xsl:with-param name="text" select="substring-after($text,')')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('[',$link,']')"/>
        <xsl:call-template name="cdc-inline">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Search an end pattern.  -->
  <!-- If found, wrap the text before it in <tag> </tag> and call inline with the remaining text -->
  <!-- otherwise, print the pattern and call inline with the text. -->
  <!--  Processing of ...'mark' -->
  <!--  Called after 'mark'... has been found -->
  <xsl:template name="cdc-wrap">
    <xsl:param name="text"/>
    <xsl:param name="mark"/>
    <xsl:param name="tag"/>
    <xsl:variable name="inside" select="substring-before($text,$mark)"/>
    <xsl:choose>
      <xsl:when test="$inside != ''">
        <!--  mark has been found: wrap text before it in tags -->
        <xsl:element name="{$tag}">
          <xsl:value-of select="$inside"/>
        </xsl:element>
        <!-- Continue processing with the rest of text. -->
        <xsl:call-template name="cdc-inline">
          <xsl:with-param name="text" select="substring-after($text,$mark)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <!--  mark has not been found, print mark and continue analysis. Recognition abandoned. -->
        <xsl:value-of select="$mark"/>
        <xsl:call-template name="cdc-inline">
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>